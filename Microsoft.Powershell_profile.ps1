﻿# Bry's Powershell environment
$global:HomeDir         = "$env:HOMEDRIVE" + "$env:HOMEPATH"
if(-not $global:HomeDir.EndsWith("\")){
	$global:HomeDir += "\"
}
# Am I at home, or work?
if($env:computername -in @('ATLAS','HERA')){
	$global:AtHome = $true
	$global:PSEnv           = $HomeDir + "Documents\WindowsPowershell"
}else{
	$global:AtHome = $false
	$global:AdminCreds           = $null
	$global:PSEnv                = $HomeDir + "MyGEOSProfile\FDR\MyDocuments\WindowsPowershell"
	[System.Net.WebRequest]::DefaultWebProxy = [System.Net.WebRequest]::GetSystemWebProxy()
	[System.Net.WebRequest]::DefaultWebProxy.Credentials = [System.Net.CredentialCache]::DefaultNetworkCredentials
	# Check to see if we're running as local admin
	if(([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")){
		$global:PSEnv = "\\eurfiler14home.fm.rbsgrp.net\childba\MyGEOSProfile\FDR\MyDocuments\WindowsPowershell"
	}
	# DWS Command line completion
	. $psenv\dws_completions.ps1
}
$global:WindowsIdentity = [System.Security.Principal.WindowsIdentity]::GetCurrent()
$global:CurrentDomain   = $global:WindowsIdentity.Name.Split("\")[0]
$global:CurrentUser     = $global:WindowsIdentity.Name.Split("\")[1]
$global:QuestPath       = "C:\Program Files\Quest Software\Management Shell for AD\"
$env:PSModulePath       = "H:\MyGEOSProfile\FDR\MyDocuments\WindowsPowershell\Modules;"
$env:PSModulePath      += "C:\Users\" + $env:USERNAME + "\AppData\Roaming\Powershell\Modules;"
$env:PSModulePath      += "C:\Program Files\WindowsPowerShell\Modules;"
$env:PSModulePath      += "C:\Windows\system32\WindowsPowerShell\v1.0\Modules\;"
$env:PSModulePath      += $global:PSEnv + "\Modules;"
$env:HOME               = "C:\Users\" + $env:Username + "\"

$OutputEncoding = [System.Text.Encoding]::UTF8

$GitPath = "C:\Program Files\Git"

Write-Host "Getting available modules..."
$Script:AllModules = Get-Module -ListAvailable

# Import Quest AD Addins (if present)
if(Test-Path $QuestPath -PathType Container){
	Write-Host "Importing Quest Active Directory module..."
	$QuestModule = $QuestPath + "Quest.ActiveRoles.ArsPowershellSnapIn.dll"
	$QuestFormat = $QuestPath + "Quest.ActiveRoles.ADManagement.Format.ps1xml"
	Import-Module -DisableNameChecking -Name $QuestModule
	Update-FormatData $QuestFormat
}

# Importing PSReadLine must happen after loading previous command history
if($PSVersionTable.PSVersion.Major -ge 5){
	# PSReadline is available natively in verion 5+ and doesn't need explicit loading
	$Env:ConEmuANSI="ON"
	$Script:PSReadline=$true
}
if($PSVersionTable.PSVersion.Major -eq 5){
	Write-Host "Removing curl/wget aliases"
	Remove-Item -Force Alias:curl
	Remove-Item -Force Alias:wget

}
if($Host.Name -eq "ConsoleHost" -and
	(($AllModules.Name -contains "PSReadLine") -and
	$PSVersionTable.PSVersion.Major -ge 3 -and
	$PSVersionTable.CLRVersion.Major -ge 4 -and
	$PSVersionTable.PSVersion.Major -lt 5)) {
		# oh-my-posh depends on PSReadline, so loads here.
		# Load it first, as we want to override some of the defaults
		# it sets for PSReadLine KeyHandlers in it's initialisation
		# later on when we load PSReadline ourselves.
		if($AllModules.Name -contains "oh-my-posh"){
			Write-Host "Importing oh-my-posh module..."
			Import-Module oh-my-posh
		}
		$Env:ConEmuANSI="ON"
		Write-Host "Importing PSReadline module..."
		Import-Module PSReadLine
		$Script:PSReadline=$true
}
if($Script:PSReadline){
		Set-PSReadLineOption -EditMode Emacs
		# Change the console history location as leaving it on the network causes exceptions in
		# PSReadline when you use New-ImpersonateUser
		If(-not $global:AtHome){
			Set-PSReadLineOption -HistorySavePath "C:\Users\Childba\PSReadLineHistory\ConsoleHost_history.txt"
		} else {
			Set-PSReadLineOption -HistorySavePath "C:\Users\bryan\PSReadLineHistory\ConsoleHost_history.txt"
		}
		Set-PSReadlineKeyHandler -Key Tab             -Function Complete
		# Set-PSReadLineKeyHandler -Key UpArrow         -Function HistorySearchBackward
		Set-PSReadLineKeyHandler -Key DownArrow       -Function HistorySearchForward
		Set-PSReadLineKeyHandler -Key Ctrl+V          -Function Paste
		Set-PSReadLineKeyHandler -Key Ctrl+RightArrow -Function ForwardWord
		Set-PSReadLineKeyHandler -Key Ctrl+LeftArrow  -Function BackwardWord
		Set-PSReadLineKeyHandler -Key Ctrl+T          -Function SwapCharacters
		Set-PSReadLineKeyHandler -Key UpArrow         -Scriptblock {
			[Microsoft.PowerShell.PSConsoleReadLine]::HistorySearchBackward()
			[Microsoft.PowerShell.PSConsoleReadLine]::EndOfLine()
		}
}

if($AllModules.Name -contains "PSColor"){
	Write-Host "Importing PSColor module..."
	Import-Module PSColor

	$global:PSColor = @{
		 File = @{
				Default    = @{ Color = 'White' }
				Directory  = @{ Color = 'Cyan' }
				Hidden     = @{ Color = 'DarkGray'; Pattern = '^\.' }
				Code       = @{ Color = 'Magenta'; Pattern = '\.(java|c|cpp|cs|js|css|html)$' }
				Executable = @{ Color = 'Red'; Pattern = '\.(exe|bat|cmd|py|pl|ps1|psm1|vbs|rb|reg)$' }
				Text       = @{ Color = 'Yellow'; Pattern = '\.(txt|cfg|conf|ini|csv|log|config|xml|yml|md|markdown)$' }
				Compressed = @{ Color = 'Green'; Pattern = '\.(zip|tar|gz|rar|jar|war)$' }
		 }
		 Service = @{
				Default = @{ Color = 'White' }
				Running = @{ Color = 'DarkGreen' }
				Stopped = @{ Color = 'DarkRed' }
		 }
		 Match = @{
				Default    = @{ Color = 'White' }
				Path       = @{ Color = 'Cyan'}
				LineNumber = @{ Color = 'Yellow' }
				Line       = @{ Color = 'White' }
		 }
		 NoMatch = @{
				Default    = @{ Color = 'White' }
				Path       = @{ Color = 'Cyan'}
				LineNumber = @{ Color = 'Yellow' }
				Line       = @{ Color = 'White' }
		 }
	}
}

# Set up path for git if available
if(Test-Path $GitPath -PathType Container){
	Write-Host "Importing Git module..."
	$env:Path = $GitPath + "\bin;" + $env:Path
}

#if($AllModules.Name -contains "ActiveDirectory"){
	# Check domain functional level before bothering to load the module:
#	Write-Host "Importing Active Directory Module..."
#	Import-Module ActiveDirectory
#}
if($AllModules.Name -contains "Pscx"){
	Write-Host "Importing Powershell Community Extensions module..."
	Import-Module Pscx
}
if($AllModules.Name -contains "ImportExcel"){
	Write-Host "Importing ImportExcel module..."
	Import-Module ImportExcel
}

Write-Host "Importing User module..."
$Script:UserModule = $Global:PSEnv + "\Scripts\Bryan\Bryan.psm1"
Import-Module -DisableNameChecking $Script:UserModule
Write-Host "Importing Syntax Highlighting module..."
$Script:SyntaxHiliteModule = $Global:PSEnv + "\Scripts\Bryan\Highlight-Syntax.psm1"
Import-Module -DisableNameChecking $Script:SyntaxHiliteModule

Set-Location $PSEnv

# Chocolatey profile
$ChocolateyProfile = "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
if (Test-Path($ChocolateyProfile)) {
	Import-Module "$ChocolateyProfile"
}

Set-Location $PSEnv

# Chocolatey profile
$ChocolateyProfile = "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
if (Test-Path($ChocolateyProfile)) {
  Import-Module "$ChocolateyProfile"
}


if ($AtHome) {
    Set-Location -Path "C:\Users\bryan"
} else {
    Set-Location -Path "C:\Users\Childba"
}
