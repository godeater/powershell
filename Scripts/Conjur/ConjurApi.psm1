<#
.SYNOPSIS
.DESCRIPTION
#>
#Requires -Version 3.0

[Uri]$Script:Uri
[Net.NetworkCredential]$Script:NetworkCredential
[string]$Global:token
[bool]$Script:tokenExpired = $true
[Timers.Timer]$Script:Timer

[ScriptBlock]$ExpireToken = { $Script:tokenExpired = $true }

Function Get-ApiKeyAuthenticator{
    param(
        [Parameter(Mandatory=$true)]
        [Uri]$authnUri,
        [Parameter(Mandatory=$true)]
        [Net.NetworkCredential]$credential
    )


    $Script:NetworkCredential = $credential
    $Script:Uri               = New-Object Uri($authnUri.OriginalString + "/users/" + [Net.WebUtility]::UrlEncode($Script:NetworkCredential.UserName) + "/authenticate")
    $Script:Timer             = New-Object Timers.Timer
    Register-ObjectEvent -InputObject $Script:Timer -EventName Elapsed -SourceIdentifier TokenExpiry -Action $Script:ExpireToken
}

Function Start-TokenTimer{
    param(
        [Parameter(Mandatory=$true)]
        [double]$Timeout
    )
    $Script:tokenExpired = $false
    $Script:Timer.Interval = $Timeout
    $Script:Timer.Start()
}

Function Format-Token{
    param(
        [Parameter(Mandatory=$true)]
        [string]$JsonToken
    )
    [byte[]]$InputBytes = [Text.Encoding]::UTF8.GetBytes($JsonToken)
    [string]$FormattedToken = [String]::Join('Token token "', [Convert]::ToBase64String($InputBytes),'"')
	return $FormattedToken
}

Function Get-Token {
    if($Script:tokenExpired){
        [Net.WebRequest]$request = [Net.WebRequest]::Create($Script:Uri)
        $request.Method = "POST"

        [IO.StreamWriter]$Writer = New-Object IO.StreamWriter($request.GetRequestStream())
        $Writer.Write($Script.NetworkCredential.Password)

        $Script:token = $request.GetResponse()
        [TimeSpan]$tokenTimeSpan = New-Object TimeSpan(0, 7, 30)
        Start-TokenTimer($tokenTimeSpan.TotalMilliseconds)
    }
    return Format-Token $Script:token
}
