# Establish distance - 4D6 yards
# Establish direction - front/behind underground, clock face outdoors
# Surprise?
# Initiative (may delay, otherwise taken from stat I)

# Action options
## Move
## Charge
## Missile
## Combat
## Magic
## Take-up (maybe)
## Drop

$CSharpSource =
@"
    using System;
    using System.Collections;
    using System.Collections.Generic;

    public enum eArmourType{
        None,
        Leather,
        Metal
    }

    public enum eHitLocation{
        Head,
        Body,
        LeftArm,
        RightArm,
        LeftLeg,
        RightLeg
    }

    public class cArmourType{
        public int Value { get; set; }
        public eArmourType ArmourType { get; set; }

        public cArmourType(){
            Value = 0;
            ArmourType = eArmourType.None;
        }

        public cArmourType(int newValue, eArmourType newArmourType){
            Value = newValue;
            ArmourType = newArmourType;
        }

        public override string ToString(){
            string sValue = Value.ToString();
            string sArmourType = ArmourType.ToString();
            return "Type: " + sArmourType + ", Rating: " + sValue;
        }
    }

    public class WFRPCharacter{
        public int Movement       { get; set; }                         // Usual range 1-10, but no upper limit really
        public int WeaponSkill    { get; set; }
        public int BallisticSkill { get; set; }
        public int Strength       { get; set; }
        public int Toughness      { get; set; }
        public int Wounds         { get; set; }                         // Usual range 1-10, but no upper limit really
        public int Initiative     { get; set; }
        public int Attacks        { get; set; }                         // Usual range 1-10, but no upper limit really
        public int Dexterity      { get; set; }
        public int Leadership     { get; set; }
        public int Intelligence   { get; set; }
        public int Cool           { get; set; }
        public int WillPower      { get; set; }
        public int Fellowship     { get; set; }
        public Dictionary<eHitLocation, cArmourType> Armour;

        public WFRPCharacter(){
            Movement = 1;
            WeaponSkill = 1;
            BallisticSkill = 1;
            Strength = 1;
            Toughness = 1;
            Wounds = 1;
            Initiative = 1;
            Attacks = 1;
            Dexterity = 1;
            Leadership = 1;
            Intelligence = 1;
            Cool = 1;
            WillPower = 1;
            Fellowship = 1;

            Armour = new Dictionary<eHitLocation, cArmourType>();
            Armour[eHitLocation.Head]     = new cArmourType(0,eArmourType.None);
            Armour[eHitLocation.Body]     = new cArmourType(0,eArmourType.None);
            Armour[eHitLocation.LeftArm]  = new cArmourType(0,eArmourType.None);
            Armour[eHitLocation.RightArm] = new cArmourType(0,eArmourType.None);
            Armour[eHitLocation.LeftLeg]  = new cArmourType(0,eArmourType.None);
            Armour[eHitLocation.RightLeg] = new cArmourType(0,eArmourType.None);
        }

        public void SetArmour(eHitLocation Location, eArmourType ArmourType, int Value){
            Armour[Location].Value = Value;
            Armour[Location].ArmourType = ArmourType;
        }
    }
"@

Add-Type -TypeDefinition $CSharpSource

$Armour = @{
    "Shield"                =@('Head','Body','LeftArm','RightArm','LeftLeg','RightLeg')
    "Mail Shirt"            =@('Body')
    "Sleeved Mail Shirt"    =@('Body','LeftArm','RightArm')
    "Mail Coat"             =@('Body','LeftLeg','RightLeg')
    "Sleeved Mail Coat"     =@('Body','LeftArm','RightArm','LeftLeg','RightLeg')
    "Mail Coif"             =@('Head')
    "Breastplate"           =@('Body')
    "Bracers"               =@('LeftArm','RightArm')
    "Leggings"              =@('LeftLeg','RightLeg')
    "Helmet"                =@('Head')

}

$NightVision = @{
    "Basilisk"=20;        "LizardMan"=30;
    "Bat"=15;             "Orc"=10;
    "Bat-Giant"=20;       "Orc-Black"=10;
    "Beetle-Giant"=20;    "Owl"=50;
    "Cat-Wild"=20;        "Owl-Giant"=50;
    "Dragon"=20;          "Pack-Wolf"=15;
    "Dwarf"=30;           "Rat"=10;
    "Eagle"=20;           "Rat-Giant"=20;
    "Elemental"=100;      "Rat-Rock"=15;
    "Elf-Wood"=30;        "Scorpion-Giant"=10;
    "Elf-Sea"=20;         "Skaven"=30;
    "Elf-High"=20;        "Snake"=20;
    "Fimir"=15;           "Snotling"=10;
    "Fox"=10;             "Spider-Giant"=10;
    "Goblin"=10;          "Stoat"=10;
    "Gnome"=30;           "Troglodyte"=30;
    "Halfling"=30;        "Undead"=100;
    "Hobhound"=10;        "Wolf"=15;
    "Hydra"=20;
    "Jabberwock"=20;
}

$HitLocation = @{
    [eHitLocation]::Head="01-15";
    [eHitLocation]::RightArm="16-35";
    [eHitLocation]::LeftArm="36-55";
    [eHitLocation]::Body="56-80";
    [eHitLocation]::RightLeg="81-90";
    [eHitLocation]::LeftLeg="91-00";
}

Function Set-Stats{
    <#
    .synopsis
    Naïvely set all stats to random number 2D10 + 20

    .description
    Needs some work to take into account the different ranges allowed for some of the stats,
    but it's a great example of using reflection to iterate through properties on an object,
    rather than doing them all explicitly.

    .parameter FQDN
    The WFRPCharacter object you want to set the stats for

    .example
    Set-Stats $MyCharacter

    #>
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        [WFRPCharacter]$Character
    )
    $Type = $Character.GetType()
    foreach($prop in $Type.GetProperties()){
        $diceRoll = Get-DiceRoll -DiceType 10 -DiceQuantity 2 -DiceModifier 20
        $stat = $Type.GetProperty($prop.Name)
        $stat.SetValue($Character,$diceRoll,$null)
    }

}

Function Get-DiceRoll{
    param(
        [Parameter(Mandatory=$true)]
        [ValidateSet(2,3,4,6,8,10,12,20,100)]
        [int]$DiceType,
        [Parameter(Mandatory=$false)]
        [int]$DiceQuantity=1,
        [Parameter(Mandatory=$false)]
        [int]$DiceModifier=0
    )
    [int]$total = 0
    for($i=1;$i -le $DiceQuantity;$i++){
        $total += Get-Random -Minimum 1 -Maximum $DiceType
    }
    $total += $DiceModifier
    return $total
}

Function Get-EncounterDistance{
    return Get-DiceRoll -DiceType 6 -DiceQuantity 4
}

Function Switch-Digits{
    param(
        [Parameter(Mandatory=$true)]
        [int]$number
    )
    [string]$strNumber = $number
    if($strNumber.Length -eq 2){ # if the roll was 0n, then the number will be n, i.e. not two digits
        [int]$flippedNumber = $strNumber.SubString(1,1) + $strNumber.SubString(0,1)
        return $flippedNumber
    } else {
        [int]$flippedNumber = $strNumber.SubString(0,1) + "0"
        return $flippedNumber
    }

}

Function Get-HitLocation{
    param(
        [Parameter(Mandatory=$true)]
        [int]$location
    )
    $location = Switch-Digits $location
    if($location -eq 0){$location = 100}

    if($location -ge 91){return [eHitLocation]::LeftLeg}
    if($location -ge 80){return [eHitLocation]::RightLeg}
    if($location -ge 56){return [eHitLocation]::Body}
    if($location -ge 36){return [eHitLocation]::LeftArm}
    if($location -ge 16){return [eHitLocation]::RightArm}
    if($location -ge 1) {return [eHitLocation]::Head}
}

Function Get-AdditionalDamage{
    [int]$damageTotal = 0
    [int]$newDamageRoll = 0
    if(Get-DiceRoll -DiceType 100 -lt $WS){
        # We hit again!
        $newDamageRoll = Get-DiceRoll -DiceType 6
        $damageTotal += $newDamageRoll
        while($newDamageRoll -eq 6){
            # If we rolled yet another 6, now just keep rolling
            # and adding the to the total until we don't roll
            # another 6
            $newDamageRoll = Get-DiceRoll -DiceType 6
            $damageTotal += $newDamageRoll
        }
    }
    return $damageTotal
}

Function Get-PreArmourDamage{
    param(
        [Parameter(Mandatory=$true,Position=0)]
        [int]$Strength,
        [Parameter(Mandatory=$true,Position=1)]
        [int]$Toughness,
        [Parameter(Mandatory=$true,Position=2)]
        [int]$WS
    )
    [int]$damageTotal  = 0
    [int]$damageResult = 0
    [int]$damageRoll   = 0

    $damageRoll = Get-DiceRoll -DiceType 6
    $damageTotal += $damageRoll
    if($damageRoll -eq 6){
        $damageTotal += Get-AdditionalDamage
    }
    $damageRoll += $Strength
    $damageResult = $damageRoll - $Toughness
    if($damageResult -ge 0){
        return $damageResult
    } else {
        return 0
    }
}

Function Get-PostArmourDamage{
    param(
        [Parameter(Mandatory=$true,Position=0)]
        [int]$Damage,
        [Parameter(Mandatory=$true,Position=1)]
        [eHitLocation]$Location,
        [Parameter(Mandatory=$true,Position=2)]
        [WFRPCharacter]$Defender
    )
    if($Defender.Armour[$Location].Value -eq 0){
        # No armour there at all
        return $Damage
    } else {
        if($Damage -gt 3){
            # Damage was more than leather can absorb, so test to see if armour is leather
            if($Defender.Armour[$Location].ArmourType -eq [eArmourType]::Metal){
                # Nope, Metal
                return @{$true=0;$false=$Damage - $Defender.Armour[$Location].Value}[$Damage - $Defender.Armour[$Location].Value -lt 0]
            } else {
                # Yep, Leather
                return $Damage
            }
        } else {
            # Damage was less than leather can absorb, so apply armour value no matter its type
            return @{$true=0;$false=$Damage - $Defender.Armour[$Location].Value}[$Damage - $Defender.Armour[$Location].Value -lt 0]
        }

    }
}

Function Start-MeleeAttack{
    param(
        [Parameter(Mandatory=$true)]
        [WFRPCharacter]$Attacker,
        [Parameter(Mandatory=$true)]
        [WFRPCharacter]$Defender
    )

    [int]$diceRoll = Get-DiceRoll -DiceType 100

    if($diceRoll -le $Attacker.WeaponSkill){                                                                         # First of all, does the attack hit?
        $Location                = Get-HitLocation $diceRoll                                                         # Secondly, where does it hit?
        [int]$initialDamage      = Get-PreArmourDamage $Attacker.Strength $Defender.Toughness $Attacker.WeaponSkill  # Thirdly, does it do any damage?
        [int]$damageAfterArmour  = Get-PostArmourDamage $initialDamage $Location $Defender                           # Fourthly, does the location it hit have any armour?
        Write-Host "Attack hit and did " $damageAfterArmour " points of damage"
        return $damageAfterArmour
    } else {
        Write-Host "Dice roll was " $diceRoll ", Attack missed because attackers WS is only " $Attacker.WeaponSkill
        return 0
    }
}

Function Start-Combat{
    
}