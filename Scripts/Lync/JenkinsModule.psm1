﻿#If(-not (Get-Module ActiveDirectory)){Import-Module ActiveDirectory}
#If(-not (Get-Module Lync)){Import-Module Lync}

# Every 'build' Jenkins executes is run as the Local System account which has basically no
# rights anywhere on the UBS network. The New-ImpersonateUser function allows individual
# steps within a build to be executed as a specified user, e.g. SQL look ups without
# having to give the entire Jenkins process access to run them.
Function New-ImpersonateUser {
	<# 
    .SYNOPSIS 
    Impersonates another user on a local machine or Active Directory. 
     
    .DESCRIPTION 
    New-ImpersonateUser uses the LogonUser method from the advapi32.dll to get a token that can then be used to call the WindowsIdentity.Impersonate method in order to impersonate another user without logging off from the current session.  You can pass it either a PSCredential or each field separately. Once impersonation is done, it is highly recommended that Remove-ImpersonateUser (a function added to the global scope at runtime) be called to revert back to the original user.  
     
    .PARAMETER Credential 
    The PS Credential to be used, eg. from Get-Credential 
     
    .PARAMETER Username 
    The username of the user to impersonate. 
     
    .PARAMETER Domain 
    The domain of the user to impersonate.  If the user is local, use the name of the local computer stored in $env:COMPUTERNAME 
     
    .PARAMETER Password 
    The password of the user to impersonate.  This is in cleartext which is why sending a PSCredential is recommended. 
     
    .PARAMETER Quiet 
    Using the Quiet parameter will force New-ImpersonateUser to have no outputs. 
     
    .INPUTS 
    None.  You cannot pipe objects to New-ImpersonateUser 
     
    .OUTPUTS 
    System.String 
    By default New-ImpersonateUser will output strings confirming Impersonation and a reminder to revert back. 
     
    None 
    The Quiet parameter will force New-ImpersonateUser to have no outputs. 
     
    .EXAMPLE 
    PS C:\> New-ImpersonateUser -Credential (Get-Credential) 
     
    This command will impersonate the user supplied to the Get-Credential cmdlet. 
    .EXAMPLE 
    PS C:\> New-ImpersonateUser -Username "user" -Domain "domain" -Password "password" 
     
    This command will impersonate the user "domain\user" with the password "password." 
    .EXAMPLE 
    PS C:\> New-ImpersonateUser -Credential (Get-Credential) -Quiet 
     
    This command will impersonate the user supplied to the Get-Credential cmdlet, but it will not produce any outputs. 
    .NOTES 
    It is recommended that you read some of the documentation on MSDN or Technet regarding impersonation and its potential complications, limitations, and implications. 
    Author:  Chris Carter 
    Version: 1.0 
     
    .LINK 
    http://msdn.microsoft.com/en-us/library/chf6fbt4(v=vs.110).aspx (Impersonate Method) 
    http://msdn.microsoft.com/en-us/library/windows/desktop/aa378184(v=vs.85).aspx (LogonUser function) 
    Add-Type 
     
    #> 
	
	[CmdletBinding(DefaultParameterSetName="Credential")] 
	Param( 
		[Parameter(ParameterSetName="ClearText", Mandatory=$true)][string]$Username, 
		[Parameter(ParameterSetName="ClearText", Mandatory=$true)][string]$Domain, 
		[Parameter(ParameterSetName="ClearText", Mandatory=$true)][string]$Password, 
		[Parameter(ParameterSetName="Credential", Mandatory=$true, Position=0)][PSCredential]$Credential, 
		[Parameter()][Switch]$Quiet 
	) 
	
	#Import the LogonUser Function from advapi32.dll and the CloseHandle Function from kernel32.dll 
	Add-Type -Namespace Import -Name Win32 -MemberDefinition @"
[DllImport("advapi32.dll", SetLastError = true)] 
public static extern bool LogonUser(string user, string domain, string password, int logonType, int logonProvider, out IntPtr token); 

[DllImport("kernel32.dll", SetLastError = true)] 
public static extern bool CloseHandle(IntPtr handle); 
"@ 
	
	#Set Global variable to hold the Impersonation after it is created so it may be ended after script run 
	$Global:ImpersonatedUser = @{} 
	#Initialize handle variable so that it exists to be referenced in the LogonUser method 
	$tokenHandle = 0 
	
	#Pass the PSCredentials to the variables to be sent to the LogonUser method 
	if ($Credential) { 
		Get-Variable Username, Domain, Password | ForEach-Object { 
			Set-Variable $_.Name -Value $Credential.GetNetworkCredential().$($_.Name)} 
	} 
	
	#Call LogonUser and store its success.  [ref]$tokenHandle is used to store the token "out IntPtr token" from LogonUser. 
	$returnValue = [Import.Win32]::LogonUser($Username, $Domain, $Password, 2, 0, [ref]$tokenHandle) 
	
	#If it fails, throw the verbose with the error code 
	if (!$returnValue) { 
		$errCode = [System.Runtime.InteropServices.Marshal]::GetLastWin32Error(); 
		Write-Host "Impersonate-User failed a call to LogonUser with error code: $errCode" 
		throw [System.ComponentModel.Win32Exception]$errCode 
	} 
	#Successful token stored in $tokenHandle 
	else { 
		#Call the Impersonate method with the returned token. An ImpersonationContext is returned and stored in the 
		#Global variable so that it may be used after script run. 
		$Global:ImpersonatedUser.ImpersonationContext = [System.Security.Principal.WindowsIdentity]::Impersonate($tokenHandle) 
		
		#Close the handle to the token. Voided to mask the Boolean return value. 
		[void][Import.Win32]::CloseHandle($tokenHandle) 
		
		#Write the current user to ensure Impersonation worked and to remind user to revert back when finished. 
		if (!$Quiet) { 
			Write-Host "You are now impersonating user $([System.Security.Principal.WindowsIdentity]::GetCurrent().Name)" 
			Write-Host "It is very important that you call Remove-ImpersonateUser when finished to revert back to your user." `
              -ForegroundColor DarkYellow -BackgroundColor Black 
		} 
	} 
	
	#Clean up sensitive variables 
	$Username = $Domain = $Password = $Credential = $null
}
	
#Function put in the Global scope to be used when Impersonation is finished. 
Function Remove-ImpersonateUser { 
	<# 
    .SYNOPSIS 
    Used to revert back to the orginal user after New-ImpersonateUser is called. You can only call this function once; it is deleted after it runs. 

    .INPUTS 
    None.  You cannot pipe objects to Remove-ImpersonateUser 

    .OUTPUTS 
    None.  Remove-ImpersonateUser does not generate any output. 
    #> 
		
	#Calling the Undo method reverts back to the original user. 
	$ImpersonatedUser.ImpersonationContext.Undo() 
		
	#Clean up the Global variable.
	Remove-Variable ImpersonatedUser -Scope Global 
}

Function Run-PrivilegedSql {
	param(
		[Parameter(Mandatory=$true,Position=0)]
		[string]$ConnectionString,
		[Parameter(Mandatory=$true,Position=1)]
		[string]$QueryString,
		[Parameter(Mandatory=$true,Position=2)]
		[PSCredential]$DatabaseCredentials
	)

	try{
		New-ImpersonateUser -Credential $DatabaseCredentials -Quiet
		# Connect to DB
		$Connection = New-Object Data.SqlClient.SQLConnection
		$Connection.ConnectionString = $ConnectionString
		$Connection.Open()

		$Command = New-Object Data.SqlClient.SqlCommand($QueryString,$Connection)
		$Adapter = New-Object Data.SqlClient.SqlDataAdapter
		$DataSet = New-Object Data.DataSet

		$Adapter.SelectCommand = $Command
		[void]$Adapter.Fill($DataSet)

		$Connection.Close()
		Remove-ImpersonateUser

		return $DataSet
	}catch{
		Write-Error $_
	}
}

Function UnzipBytes {
	param(
		[ValidateNotNullOrEmpty()]
		[byte[]]$bytes
	)
	$stream       = New-Object IO.MemoryStream (,$bytes)
	$gzipStream   = New-Object IO.Compression.GZipStream $stream, ([IO.Compression.CompressionMode]::Decompress)
	$memoryStream = New-Object IO.MemoryStream
	[int]$num = 0
	while(($num = $gzipStream.ReadByte()) -ne -1){
		$memoryStream.WriteByte([byte]$num)
	}
	$gzipStream.Close()
	[byte[]]$result = $memoryStream.ToArray()
	$memoryStream.Close()
	return $result
}

Function UnzipBytesFromBase64 {
	param(
		[ValidateNotNullOrEmpty()]
		[string]$base64String
	)
	[byte[]]$fromBase64 = UnzipBytes([System.Convert]::FromBase64String($base64String))
	return [System.Text.Encoding]::UTF8.GetString($fromBase64)
}

Function Get-GCPrefToXml {
	param(
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[ValidateNotNullOrEmpty()]
		[ValidatePattern('^(sip:){0,1}[\w-]+\.[\w-]+@ubs.com$')]
		[string]$SipAddress,
		[Parameter(Mandatory=$false)]
		[ValidateSet('DisplayThemes','FileDownloadPrefs','Folders','GeneratedFilters','GroupChannels','IMChannels',
					 'Invitations','KnownDomains','ShowFeature','UserFilters','UserOptions')]
		[string]$PreferenceSet="GroupChannels",
		[Parameter(Mandatory=$true)]
		[PSCredential]$DBCreds
	)
	$connectionString = "Server=nzurc036psq.ubsprod.msad.ubs.net;Initial Catalog=LyncGCF;Trusted_Connection=True;"

	if(!$SipAddress.StartsWith('sip:')){
		$SipAddress = "sip:" + $SipAddress
	}
	
	[string]$sqlQuery = "select prefContent from tblPreference where prefLabel like '" + $SipAddress + "%" + $PreferenceSet + "'"
	$sqlResult = Run-PrivilegedSql -ConnectionString $connectionString -QueryString $sqlQuery -DatabaseCredentials $DBCreds
	[string]$base64String = $sqlResult.Tables[0].Rows[0].prefContent

	return UnzipBytesFromBase64 $base64String
}

Function Get-LyncPoolUserCounts {
	param(
		[Parameter(ValueFromPipeline=$True,ValueFromPipelineByPropertyName=$True)]
		[string[]]$PoolName,
		[Parameter(Mandatory=$False)]
		[int]$Throttle = 15
	)

	Begin {
		Function Get-RunspaceData{
			[CmdletBinding()]
			param(
				[switch]$Wait
			)
			Do {
				$more = $false
				ForEach($runspace in $runspaces){
					If($runspace.Runspace.isCompleted){
						$runspace.powershell.EndInvoke($runspace.Runspace)
						$runspace.powershell.dispose()
						$runspace.Runspace = $null
						$runspace.powershell = $null
						$Script:i++
					}Elseif ($runspace.Runspace -ne $null) {
						$more = $true
					}
				}
				If($more -and $PSBoundParameters['Wait']){
					Start-Sleep -Milliseconds 100
				}
				# Clean out unused runspace jobs
				$temphash = $runspaces.clone()
				$temphash | Where {
					$_.runspace -eq $null
				} | ForEach {
					Write-Verbose ("Removing {0}" -f $_.computer)
					$Runspaces.remove($_)
				}
			} While ($more -and $PSBoundParameters['Wait'])
		}

		$Script:report = @()

		Write-Verbose ("Building hash table for Get-CsUser parameters")
		$SessionHash = [Hashtable]::Synchronized(@{})
		$SessionHash.host = $host
		$GetCsUserHash = @{
			Verbose     = $true
			ErrorAction = "Stop"
		}

		$runspacehash = @{}

		$scriptblock = {
			Param (
				$RegistrarPool,
				$GetCsUserHash,
				$SessionHash
			)
			Import-Module Lync
			$Users = $null
			$GetCsUserHash.Filter = "RegistrarPool -eq '$RegistrarPool'"
			
			Try {
				$Users = Get-CsUser @GetCsUserHash
				$ResultHash = @{
					PoolName = $RegistrarPool
					UserCount = $Users.Count
				}
				$PoolCount = New-Object PSObject -Property $ResultHash
				$PoolCount.PSTypeNames.Insert(0,"LyncPool.Information")
				$PoolCount
			} Catch {
				Write-Warning ("{0}: {1}" -f $RegistrarPool, $_.Exception.Message)
				Break
			}
		}

		Write-Verbose ("Creating runspace pool and session states")
		$sessionstate = [System.Management.Automation.Runspaces.initialsessionstate]::CreateDefault()
		$runspacepool = [RunspaceFactory]::CreateRunspacePool(1, $Throttle, $sessionstate, $Host)
		$runspacepool.Open()

		Write-Verbose ("Creating empty collection to hold runspace jobs")
		$Script:runspaces = New-Object System.Collections.ArrayList
	}
	Process {
		$totalCount = $PoolName.Count
		Write-Verbose ("Running count for {0} pools" -f $totalCount)
		ForEach ($Pool in $PoolName) {
			Write-Verbose("{0}: Running for pool" -f $Pool)
			$powershell = [PowerShell]::Create().AddScript($ScriptBlock).AddArgument($Pool).AddArgument($GetCsUserHash).AddArgument($SessionHash)
			$powershell.RunspacePool = $runspacepool

			$temp = "" | Select-Object Powershell,Runspace,Pool
			$temp.Pool = $Pool
			$temp.Powershell = $powershell
			$temp.runspace = $powershell.BeginInvoke()
			Write-Verbose ("Adding {0} to collection" -f $temp.Pool)
			$runspaces.Add($temp) | Out-Null

			Write-Verbose("Checking status of runspace jobs")
			Get-RunspaceData @runspacehash
		}
	}
	End {
		Write-Verbose("Finish processing the remaining runspace jobs: {0}" -f (@(($runspaces | Where {$_.Runspace -ne $Null}).Count)))
		$runspacehash.Wait = $true
		Get-RunspaceData @runspacehash
		Write-Verbose("Closing the runspace pool")
		$runspacepool.close()
	}
}

# Useful stuff for Jenkins jobs

$pwd            = $env:SvcVantageAppGlobalPwd
$svcAccntPass   = ConvertTo-SecureString -AsPlainText -Force $pwd
$svcAccntCred   = New-Object Management.Automation.PSCredential -ArgumentList "UBSPROD\svc_vantage_app_gbl", $svcAccntPass
$employeeNumber = $env:BUILD_USER_ID.SubString(0,$env:BUILD_USER_ID.Length - 2)
$emailAddress   = $(Get-AdUser -ldapFilter "(&(objectclass=user)(objectCategory=user)(employeeType=primary)(employeeNumber=$employeeNumber))" -properties mail).mail

Export-ModuleMember -Variable * -Function * -Alias *