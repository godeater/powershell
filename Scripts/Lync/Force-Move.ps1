﻿Function Move-CsUserWithProgress {
    param(
        [Parameter(Mandatory=$true,Position=0)]
        [string]$PoolToMoveFrom,
        [Parameter(Mandatory=$true,Position=1)]
        [string]$PoolToMoveTo
    )
    $i = 0
    $HayesUsers = @()
    $HayesUsers += Get-CsUser -Filter {RegistrarPool -eq $PoolToMoveFrom}
	
    $HayesUsers | Foreach-Object {
        $i++
        Move-CsUser -Identity $_.SipAddress -Target $PoolToMoveTo -Force
		Start-Sleep -Milliseconds 200
        Write-Progress -Activity "Moving Users" -Status "from hayes to ironwood" -PercentComplete (($i/$HayesUsers.count)*100)
    }
}