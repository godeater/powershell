# Powershell Script To Enable Firewall rules to allow SmartTAP components communication


# Configuration section start #########################################################

$machine_type = "fe" # The type of machine the script is running on, one of the following: "smarttap", "edge", "fe","database"
# The following parameters need to be configured only once per install then the script can be run on different machines: "smarttap", "edge", "fe","database"
$edge_solution = $true # In case of installing SmartTAP Edge solution set the flag to $true, otherwise to $false
$cluster_solution = $true # In case of installing SmartTAP CLuster Applicaiton solution set the flag to $true, otherwise to $false
$smarttap_ip = "139.149.40.169,139.149.40.170,14.64.156.179,14.64.156.182,139.149.40.14,139.149.40.174,14.64.156.183,14.64.156.184,139.149.40.177,14.64.156.188,139.149.40.18,14.64.156.191" # IP address of machine SmartTAP server installed on.  If Clusetred, VIP of SmartTAP application, Host IP address of each clustered node separated by comma
$calldelivery_ips = "139.149.26.6,139.149.132.203" # In case there are more than one CallDelivery installed, list all CallDeliverys IP addreses separated by comma
$plugin_ips = "14.64.155.193,14.64.155.146,14.64.49.147,14.64.49.181,14.64.188.54,14.67.248.177" # In case there are more than one plug-in installed, list all FEs IP addreses separated by comma

# Configuration section end ###########################################################


function Enable-Firewall-Rules-On-FE
{
	$remote_ip = ""
	if ($edge_solution)
	{
		$remote_ip = $calldelivery_ips
	}
	else 
	{
		$remote_ip = $smarttap_ip
	}
	
	# Add inbound rule
	netsh advfirewall firewall add rule name="SmartTAP Plug-in from CD" dir=in protocol=TCP localport=9901 remoteip=$remote_ip remoteport=any action=allow
	
	# Add outbound rule
	netsh advfirewall firewall add rule name="SmartTAP Plug-in to CD" dir=out protocol=TCP localport=any remoteip=$remote_ip remoteport=9090 action=allow	
}

function Enable-Firewall-Rules-On-Edge
{	
	if($edge_solution)
	{
             
		if($cluster_solution)
		{
			# Add inbound rules
			netsh advfirewall firewall add rule name="SmartTAP CD from ST" dir=in protocol=UDP localport=5066 remoteip=$smarttap_ip remoteport=any action=allow

			# Add inbound rules
			netsh advfirewall firewall add rule name="SmartTAP CD from FEs plugin" dir=in protocol=TCP localport=9090 remoteip=$plugin_ips remoteport=any action=allow
			netsh advfirewall firewall add rule name="SmartTAP CD from ST" dir=in protocol=UDP localport=12161 remoteip=$smarttap_ip remoteport=any action=allow
		
			# Add outbound rules
			netsh advfirewall firewall add rule name="SmartTAP CD to FEs plugin" dir=out protocol=TCP localport=any remoteip=$plugin_ips remoteport=9901 action=allow
			netsh advfirewall firewall add rule name="SmartTAP CD to ST (UDP)" dir=out protocol=UDP localport=5066 remoteip=$smarttap_ip remoteport=5060 action=allow	
			netsh advfirewall firewall add rule name="SmartTAP CD to ST (TCP)" dir=out protocol=TCP localport=any remoteip=$smarttap_ip remoteport=9000 action=allow	
			netsh advfirewall firewall add rule name="SmartTAP CD to MS" dir=out protocol=UDP localport=any remoteip=$smarttap_ip remoteport=40000-45000 action=allow
			
		}
		else
		{
			# Add inbound rules
			netsh advfirewall firewall add rule name="SmartTAP CD from FEs plugin" dir=in protocol=TCP localport=9090 remoteip=$plugin_ips remoteport=any action=allow
			netsh advfirewall firewall add rule name="SmartTAP CD from ST" dir=in protocol=UDP localport=12161 remoteip=$smarttap_ip remoteport=any action=allow
		
			# Add outbound rules
			netsh advfirewall firewall add rule name="SmartTAP CD to FEs plugin" dir=out protocol=TCP localport=any remoteip=$plugin_ips remoteport=9901 action=allow
			netsh advfirewall firewall add rule name="SmartTAP CD to ST (UDP)" dir=out protocol=UDP localport=5066 remoteip=$smarttap_ip remoteport=5060 action=allow	
			netsh advfirewall firewall add rule name="SmartTAP CD to ST (TCP)" dir=out protocol=TCP localport=any remoteip=$smarttap_ip remoteport=9000 action=allow	
			netsh advfirewall firewall add rule name="SmartTAP CD to MS" dir=out protocol=UDP localport=any remoteip=$smarttap_ip remoteport=40000-45000 action=allow
		}	
	}
	else
	{
		Write-host "Can't add rules on Edge Server FW when edge_solution parameter is equal to false"
	}
}

function Enable-Firewall-Rules-On-SmartTAP
{
	netsh advfirewall firewall add rule name="SmartTAP Web Browser" dir=in protocol=TCP localport=80 remoteip=any remoteport=any action=allow	
	if ($edge_solution)
	{
		# Add inbound rules
		netsh advfirewall firewall add rule name="SmartTAP from CD (UDP)" dir=in protocol=UDP localport=5060 remoteip=$calldelivery_ips remoteport=5066 action=allow	
		netsh advfirewall firewall add rule name="SmartTAP from CD (TCP)" dir=in protocol=TCP localport=9000 remoteip=$calldelivery_ips remoteport=any action=allow
		netsh advfirewall firewall add rule name="SmartTAP from CD (UDP)" dir=in protocol=UDP localport=40000-45000 remoteip=$calldelivery_ips remoteport=any action=allow
	
		# Add outbound rule
		netsh advfirewall firewall add rule name="SmartTAP to CD" dir=out protocol=UDP localport=any remoteip=$calldelivery_ips remoteport=12161 action=allow
	}
	else
	{
		# Add inbound rule
		netsh advfirewall firewall add rule name="SmartTAP CD from FEs plugin" dir=in protocol=TCP localport=9090 remoteip=$plugin_ips remoteport=any action=allow
		
		# Add outbound rule
		netsh advfirewall firewall add rule name="SmartTAP CD to FEs plugin" dir=out protocol=TCP localport=any remoteip=$plugin_ips remoteport=9901 action=allow
	}
}

function Enable-Firewall-Rules-On-Database
{	
	netsh advfirewall firewall add rule name="MySQL" dir=in protocol=TCP localport=3306 remoteip=$smarttap_ip remoteport=any action=allow		
	
}

function Enable-SmartTAP-FW-Rules
{
	if ($machine_type -eq "edge")
	{
		Enable-Firewall-Rules-On-Edge
	}
	elseif ($machine_type -eq "fe")
	{
		Enable-Firewall-Rules-On-FE
	}
	elseif ($machine_type -eq "database")
	{
		Enable-Firewall-Rules-On-Database
        }
	else
	{
		Enable-Firewall-Rules-On-SmartTAP
	}
}

Enable-SmartTAP-FW-Rules