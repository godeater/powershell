﻿<#
#>


param(
	[Parameter(Mandatory=$false,ValueFromPipeline=$true)]
	$queueName = ".\private$\groupchatmonitor",
	[Parameter(Mandatory=$false)]
	$EmailTo = 'dl-liam-escalation@ubs.com',
	[Parameter(Mandatory=$false)]
	$EmailFrom = 'groupchatmonitor@ubs.com',
	[Parameter(Mandatory=$false)],
	$EmailPriority = "High",
	[Parameter(Mandatory=$false)],
	$SmtpHost = "mailhost.ldn.swissbank.com"
)

Import-Module -DisableNameChecking GroupChat-DomainController.psm1

# Main Script Body
Initialize-MSMQ

$EmailHeader = Get-Content GroupChatDC_EmailHeader.txt

$EmailBody = ""

$EmailFooter = Get-Content GroupChatDC_EmailFooter.txt

$GroupChatServers = @()

$Messages = @(Read-MSMQMessages $queueName)

$Messages | Foreach-Object {
	$ServerInfo = $_.Body | ConvertFrom-CliXml
	$GroupChatServers += $ServerInfo
}

$GroupChatServers | Foreach-Object {
	$EmailBody += $_.GroupChatHost + "`t`t`t" + $_.DomainController + "`n"
}

$EntireEmail = $EmailHeader + $EmailBody + $EmailFooter

Send-MailMessage -From 'bryan.childs@ubs.com' -To 'bryan.childs@ubs.com' -Subject 'GroupChat Domain Controllers' -Priority High -BodyAsHtml $EntireEmail -SmtpServer mailhost.ldn.swissbank.com

Purge-MSMQMessage -queueName $queueName
