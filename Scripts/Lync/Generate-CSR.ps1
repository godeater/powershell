﻿<#
.SYNOPSIS
    Dynamically generates a .inf file suitable for input to certreq.exe, and then uses it
    as input to certreq in order to get a valid Certificate Request on the server the script
    is run on.
.DESCRIPTION
    Uses a template .inf file embedded within the body of the script.
#>
param(
	[parameter(ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true,Mandatory=$true)]
	[string]$HostName
)



If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
    Write-Warning "You do not have Administrator rights to run this script"
	Write-Warning "Please re-run this script as an Administrator"
	Break
}


$Today = (Get-Date).ToString('yyyyMMdd')
$ReqFileName = "Cert_Req-$HostName-" + $Today + ".req"

$CertReqInfFileContent =
@"
[Version]
Signature= "`$Windows NT`$"
[NewRequest]
Subject = "CN=$HostName"
KeySpec = 1
KeyLength = 2048
Exportable = TRUE 
MachineKeySet = TRUE 
SMIME = FALSE
PrivateKeyArchive = FALSE
UserProtected = FALSE
UseExistingKeySet = FALSE
ProviderName = "Microsoft RSA Schannel Cryptographic Provider" 
ProviderType = 12 
RequestType = PKCS10
KeyUsage = 0xa0
[EnhancedKeyUsageExtension]
OID=1.3.6.1.5.5.7.3.1
"@

Write-Host "Generating Certificate Request file..." -ForegroundColor Yellow
$FinalInfFileName = "Cert_Req-$HostName-" + $Today + ".inf"

New-Item $FinalInfFileName -Type File -Value $CertReqInfFileContent

c:\windows\system32\cmd.exe /c c:\windows\system32\certreq.exe -new $FinalInfFileName $ReqFileName

Write-Host " "
Write-Host "Certificate request file for $HostName successfully generated" -ForegroundColor DarkGreen
