
$vps = get-csvoicepolicy

$array=@()
$vps | % {
	
	[string]$usages=""
	foreach ($eml in $_.PstnUsages) {
		$usages = $usages + ";" + $eml
	}

	$obj = New-Object -Type PSObject -Property @{Identity=$_.Identity;
						     PstnUsage=$usages;
						     Description=$_.Description;
						     AllowSimRing=$_.AllowSimulRing;
						     AllowCallForwarding=$_.AllowCallForwarding;
						     AllowPSTNReRouting=$_.AllowPSTNReRouting;
						     EnableDelegation=$_.EnableDelegation;
						     EnableTeamcall=$_.EnableTeamcall;
						     EnableCallTransfer=$_.EnableCallTransfer;
						     EnableCallPark=$_.EnablecallPark;
						     EnableMaliciousCallTracing=$_.EnableMaliciousCallTracing;
						     EnableBWPolicyOverride=$_.EnableBWPolicyOverride;
						     PreventPSTNTollBypass=$_.PreventPSTNTollBypass}
	$array += $obj

}

$array
	
