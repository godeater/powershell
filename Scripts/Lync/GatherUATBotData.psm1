﻿param(
	[Parameter(Mandatory=$true,Position=0)]
	[string]$Spreadsheet
)
$Global:ErrorActionPreference = "Stop"

Function Initialize-Module{
	param(
		[Parameter(Mandatory=$true,ValueFromPipeline=$true,Position=0)]
		[string]$Spreadsheet
	)
	if(Test-Path -Path $Spreadsheet -PathType Leaf){
		$Global:WorkBookName = $Spreadsheet
		#$Global:WorkBookName = "C:\UBS\Dev\Tmp\Bot Owners Helpers and SVC Accounts Checks 6-07-16.xlsx"
	}else{
		Write-Error "Couldn't load " $Spreadsheet
		return
	}
	$Global:ChrisUserData = Import-Excel -Path $WorkBookName -WorkSheetname "users"
	$Global:ChrisSvcData  = Import-Excel -Path $WorkBookName -WorkSheetname "svc accounts"
}

Function Get-UserData {
	$ChrisUserData | Foreach-Object {
		$IsCsUser = $True
		$Index = $ChrisUserData.IndexOf($_)
		$UserGPN = $_.GPN.ToString("00000000")
		$UserSAN = $_.'NT ID'
		Write-Host -NoNewLine "Checking for $UserGPN ... "
		$LdapFilter = "(&(employeenumber=" + $UserGPN + ")(|(employeeType=primary)(employeeType=DTPAccount))(objectClass=user)(objectCategory=user)(!(extensionAttribute5=FR*)))"
		$QADUser = Get-QADUser -LdapFilter $LdapFilter -IncludedProperties @('extensionAttribute15')
		if($QADUser){
			Write-Host -NoNewLine -ForegroundColor Green "Found AD User" $QADUser.SamAccountName " "
			# Update Chris's data for the user, as he has incorrect samAccountNames/T-Numbers for some people
			$ChrisUserData[$Index].'NT ID' = $QADUser.SamAccountName
			$ChrisUserData[$Index].TNumber = $QADUser.extensionAttribute15
			$ChrisUserData[$Index].'DTP UAT Account AD Enabled?' = -not $QADUser.AccountIsDisabled
			$AD_and_Sam = "TEST\" + $QADUser.samaccountname
			try{
				$CSUser  = Get-CsUser $AD_and_Sam
			} catch {
				$IsCsUser = $False
				Write-Host -ForegroundColor Red "Couldn't find Lync User"
				$ChrisUserData[$Index].'Lync UAT Profile Enabled?' = $False
			}
			if($IsCSUser -and $CSUser.ConferencingPolicy -ne "P00 Conferencing Policy"){
				Write-Host -ForegroundColor Green " and found enabled Lync User"
				$ChrisUserData[$Index].'Lync UAT Profile Enabled?' = $True
			} elseif($IsCsUser) {
				Write-Host -ForegroundColor Yellow " but Lync user is disabled"
				$ChrisUserData[$Index].'Lync UAT Profile Enabled?' = $False
			}

		} else {
			Write-Host -ForegroundColor Red "Couldn't find AD User"
			$ChrisUserData[$Index].'DTP UAT Account AD Enabled?' = $False
			$ChrisUserData[$Index].'Lync UAT Profile Enabled?' = $False
		}
	}
	$ChrisUserData | Export-Excel -Path $Global:WorkBookName -WorkSheetname "users"
}

Function Get-SvcData {
	$ChrisSvcData | Foreach-Object {
		$IsCsUser = $True
		$Index = $ChrisSvcData.IndexOf($_)
		$BotLogin = $_.'UAT Bot Lync Login'
		Write-Host -NoNewLine "Checking for $BotLogin ... "
		$LdapFilter = "(&(samaccountname=" + $BotLogin + ")(employeeType=service)(objectClass=user)(objectCategory=user))"
		$QADUser = Get-QADUser -LdapFilter $LdapFilter
		if($QADUser){
			Write-Host -NoNewLine -ForegroundColor Green "Found AD User "
			$ChrisSvcData[$Index].'Is SVC UAT Account AD Enabled?' = -not $QADUser.AccountIsDisabled
			$AD_and_Sam = "TEST\" + $QADUser.samaccountname
			try{
				$CSUser  = Get-CsUser $AD_and_Sam
			} catch {
				$IsCsUser = $False
				Write-Host -ForegroundColor Red "Couldn't find Lync User"
				$ChrisSvcData[$Index].'Is SVC UAT Account Lync Enabled?' = $False
			}
			if($IsCSUser -and $CSUser.ConferencingPolicy -ne "P00 Conferencing Policy"){
				Write-Host -ForegroundColor Green "and found enabled Lync User"
				$ChrisSvcData[$Index].'Is SVC UAT Account Lync Enabled?' = $True
			} elseif($IsCsUser) {
				Write-Host -ForegroundColor Yellow " but Lync user is disabled"
				$ChrisSvcData[$Index].'Is SVC UAT Account Lync Enabled?' = $False
			}

		} else {
			Write-Host -ForegroundColor Red "Couldn't find AD User"
			$ChrisSvcData[$Index].'Is SVC UAT Account AD Enabled?' = $False
			$ChrisSvcData[$Index].'Is SVC UAT Account Lync Enabled?' = $False
		}
	}
	$ChrisSvcData | Export-Excel -Path $Global:WorkBookName -WorkSheetname "svc accounts"
}
Initialize-Module -Spreadsheet $Spreadsheet
Connect-QADToTest
Export-ModuleMember -Function * -Alias *