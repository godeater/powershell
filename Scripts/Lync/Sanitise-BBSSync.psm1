﻿Import-Module $PSEnv\Scripts\Bryan\Oracle.psm1 -DisableNameChecking

function Get-MindAlignNames {
	param(
		[Parameter(Mandatory=$true,Position=0)]
		[string]$ChannelFile
	)

	try{
		$MA_Channel_IDs = Get-Content $ChannelFile
	} catch {
		Write-Error $_
	}

	try {
		$MA_Names = @()
		
		# Connect to MALIGNP1
		$MADB_Connection = Connect-ToMalignP

		# Switch to MALIGN Schema
		$SchemaChange    = "alter session set current_schema = malign"
		Query-MalignP -Query $SchemaChange -Connection $MADB_Connection

		# Get list of channel names
		if($MA_Channel_IDs.count -gt 1000){
			# Oracle's .NET Adaptor doesn't allow lists in queries
			# with more than 1000 entries, so we need to split
			# the list into blocks if the list is larger than that.

			# Declare result as int to get whole number in division
			[int]$Blocks          = $MA_Channel_IDs.Count / 1000
			# Get left over modulus
			[int]$FinalBlockSize  = $MA_Channel_IDs.Count % 1000

			for([int]$i = 0; $i -le $Blocks; $i++){
				$j = $i * 1000
				if($i -ne $Blocks){
					$k = $j + 999
					$MAList = "(" + $($MA_Channel_IDs[$j..$k] -join ',') + ")"
				} else {
					$k = $j + $FinalBlockSize - 1
					$MAList = "(" + $($MA_Channel_IDs[$j..$k] -join ',') + ")"
				}
				$query = "select plgrpname from plgroup where plgrpid in " + $MAList
				$MA_Names += Query-MalignP -Query $query -Connection $MADB_Connection
			}
		} else {
			$k = $MA_Channel_IDs.count - 1
			$MAList = "(" + $($MA_Channel_IDs[0..$k] -join ',') + ")"
			$query = "select plgrpname from plgroup where plgrpid in " + $MAList
			$MA_Names += Query-MalignP -Query $query -Connection $MADB_Connection
		}
		[string[]]$MA_Names_As_String = $MA_Names | Select-Object -ExpandProperty PLGRPNAME
		return $MA_Names_As_String
	} catch {
		Write-Error $_
	}
}

function Get-BbsIds {
	param(
		[Parameter(Mandatory=$true,Position=0)]
		[string[]]$MA_Names,
		[Parameter(Mandatory=$true,Position=1)]
		[PSCredential]$DatabaseCredentials
	)
	$IdsTable = @()
	$k = $MA_Names.count -1
	$CSVMindAlignNames = "'" + $($MA_Names[0..$k] -join "','") + "'"
	
	[scriptblock] $sqlJobBlock = {
		param($MA_Names)
		try{
			$Query = "select gcId,bbsID from bbs_groupchat_mapping where gcId in (" + $MA_Names + ")"
			# Connect to ChatWeb DB
			$CW_DBConnection = New-Object Data.SqlClient.SQLConnection
			$CW_DBConnection.ConnectionString = "Data Source=nzurc036psq.ubsprod.msad.ubs.net;Initial Catalog=CWebDB1;Integrated Security=SSPI;"
			$CW_DBConnection.Open()

			$Command = New-Object Data.SqlClient.SqlCommand($Query,$CW_DBConnection)
			$Adapter = New-Object Data.SqlClient.SqlDataAdapter
			$DataSet = New-Object Data.DataSet
		
			$Adapter.SelectCommand = $Command
			[void]$Adapter.Fill($DataSet)

			$CW_DBConnection.Close()
			
			$Serialized_DataSet = [Management.Automation.PSSerializer]::Serialize($DataSet, 2)

			return $Serialized_DataSet
		} catch{
			Write-Error $_
		}
	}

	$sqlJob = Start-Job -ScriptBlock $sqlJobBlock -ArgumentList $CSVMindAlignNames -Credential $DatabaseCredentials
	$serializedResults = Receive-Job $sqlJob -Wait:$true -WriteJobInResults:$false
	$sqlResults = [Management.Automation.PSSerializer]::Deserialize($serializedResults)
	Remove-Job $sqlJob

	$sqlResults.Tables[0] | Foreach-Object {
	 	$ThisChannel = New-Object PSCustomObject -Property @{gcId=$_.gcId;bbsId=$_.bbsId}
	 	$IdsTable += $ThisChannel
	}
	return $IdsTable
}

function Remove-BbsIdsFromFile {
	param(
		[Parameter(Mandatory=$true,Position=0)]
		[string]$BBSFile,
		[Parameter(Mandatory=$true,Position=1)]
		[Object[]]$IgnorePatterns
	)
	$ExcludeOutput = @()
	$IncludeOutput = @()
	$Regex = ($IgnorePatterns | ForEach-Object{[regex]::Escape($_.bbsId)}) -join "|"
	
	if(Test-Path $BBSFile){
		$BBSFileWithPath = Resolve-Path $BBSFile
		$Reader = New-Object IO.StreamReader($BBSFileWithPath)
		$Line = $Reader.ReadLine()

		While($Line -ne $null){
			If($Line -notmatch $Regex){
				$ExcludeOutput += $Line
			}Else{
				$IncludeOutput += $Line
			}
			$Line=$Reader.ReadLine()
		}
		$Reader.Close()
		$ExcludeOutput | Set-Content "Cleaned_ABEME901"
		$IncludeOutput | Set-Content "ME9ABE01"
	
	} Else {
		Write-Error "Cannot locate:" $BBSFile
	}
}