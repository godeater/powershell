﻿#Requires -Version 2.0

#Import-Module Lync
Function Get-LyncPoolUserCounts {
	param(
		[Parameter(ValueFromPipeline=$True,ValueFromPipelineByPropertyName=$True)]
		[string[]]$PoolName,
		[Parameter(Mandatory=$False)]
		[int]$Throttle = 15
	)

	Begin {
		Function Get-RunspaceData{
			[CmdletBinding()]
			param(
				[switch]$Wait
			)
			Do {
				$more = $false
				ForEach($runspace in $runspaces){
					If($runspace.Runspace.isCompleted){
						$runspace.powershell.EndInvoke($runspace.Runspace)
						$runspace.powershell.dispose()
						$runspace.Runspace = $null
						$runspace.powershell = $null
						$Script:i++
					}Elseif ($runspace.Runspace -ne $null) {
						$more = $true
					}
				}
				If($more -and $PSBoundParameters['Wait']){
					Start-Sleep -Milliseconds 100
				}
				# Clean out unused runspace jobs
				$temphash = $runspaces.clone()
				$temphash | Where {
					$_.runspace -eq $null
				} | ForEach {
					Write-Verbose ("Removing {0}" -f $_.computer)
					$Runspaces.remove($_)
				}
			} While ($more -and $PSBoundParameters['Wait'])
		}

		$Script:report = @()

		Write-Verbose ("Building hash table for Get-CsUser parameters")
		$SessionHash = [Hashtable]::Synchronized(@{})
		$SessionHash.host = $host
		$GetCsUserHash = @{
			Verbose     = $true
			ErrorAction = "Stop"
		}

		$runspacehash = @{}

		$scriptblock = {
			Param (
				$RegistrarPool,
				$GetCsUserHash,
				$SessionHash
			)
			Import-Module Lync
			$Users = $null
			$GetCsUserHash.Filter = "RegistrarPool -eq '$RegistrarPool'"
			
			Try {
				$Users = Get-CsUser @GetCsUserHash
				$ResultHash = @{
					PoolName = $RegistrarPool
					UserCount = $Users.Count
				}
				$PoolCount = New-Object PSObject -Property $ResultHash
				$PoolCount.PSTypeNames.Insert(0,"LyncPool.Information")
				$PoolCount
			} Catch {
				Write-Warning ("{0}: {1}" -f $RegistrarPool, $_.Exception.Message)
				Break
			}
		}

		Write-Verbose ("Creating runspace pool and session states")
		$sessionstate = [System.Management.Automation.Runspaces.initialsessionstate]::CreateDefault()
		$runspacepool = [RunspaceFactory]::CreateRunspacePool(1, $Throttle, $sessionstate, $Host)
		$runspacepool.Open()

		Write-Verbose ("Creating empty collection to hold runspace jobs")
		$Script:runspaces = New-Object System.Collections.ArrayList
	}
	Process {
		$totalCount = $PoolName.Count
		Write-Verbose ("Running count for {0} pools" -f $totalCount)
		ForEach ($Pool in $PoolName) {
			Write-Verbose("{0}: Running for pool" -f $Pool)
			$powershell = [PowerShell]::Create().AddScript($ScriptBlock).AddArgument($Pool).AddArgument($GetCsUserHash).AddArgument($SessionHash)
			$powershell.RunspacePool = $runspacepool

			$temp = "" | Select-Object Powershell,Runspace,Pool
			$temp.Pool = $Pool
			$temp.Powershell = $powershell
			$temp.runspace = $powershell.BeginInvoke()
			Write-Verbose ("Adding {0} to collection" -f $temp.Pool)
			$runspaces.Add($temp) | Out-Null

			Write-Verbose("Checking status of runspace jobs")
			Get-RunspaceData @runspacehash
		}
	}
	End {
		Write-Verbose("Finish processing the remaining runspace jobs: {0}" -f (@(($runspaces | Where {$_.Runspace -ne $Null}).Count)))
		$runspacehash.Wait = $true
		Get-RunspaceData @runspacehash
		Write-Verbose("Closing the runspace pool")
		$runspacepool.close()
	}
}
