[CmdletBinding(DefaultParameterSetName="UserID")]
param(
    [Parameter(Mandatory=$true,ValueFromPipeline=$true,ParameterSetName="UserID",Position=0)]
    [string]$UserID,
    [Parameter(Mandatory=$true,ValueFromPipeline=$false,ParameterSetName="File",Position=0)]
    [string]$FileName
    )

$ErrorActionPreference = "Stop"

Function New-LuwareUsers {
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        [string]$UserID
    )

    Begin{
        try{
            If(-not (Get-Module "ActiveDirectory")){
                Import-Module "ActiveDirectory"
            }
        } catch {
            Throw "This script needs the ActiveDirectory module to be loaded"
        }

        try{
            If(-not (Get-Module "SkypeForBusiness")){
                Import-Module "SkypeForBusiness"
            }
        }catch{
            Throw "This script needs the SkypeForBusiness module to be loaded"
        }
    }

    Process{
        $AdInfo = Get-AdUser $UserID -Properties @("GivenName","Surname","SamAccountName","SID","msRTCSIP-PrimaryUserAddress")

        $CsInfo = Get-CsUser $AdInfo.'msRTCSIP-PrimaryUserAddress'

        $LuWarePerson = @{FirstName=$AdInfo.GivenName;
                          LastName=$AdInfo.Surname;
                          Sam=$AdInfo.SamAccountName;
                          Sid=$AdInfo.SID;
                          OrganizationalUnitId="UBS";
                          SipAddress=$CsInfo.SipAddress;

        }

        # New-PaPerson -FirstName $AdInfo.GivenName `
        #              -LastName $AdInfo.Surname `
        #              -Sam $AdInfo.SamAccountName `
        #              -Sid $AdInfo.SID `
        #              -OrganizationalUnitId "UBS" `
        #              -SipAddress $CsInfo.SipAddress `
        #              -IsMaEnabled $true `
        #              -IsPaEnabled $true

        $LuWarePerson
        "`n"
    }
}

switch ($PSCmdlet.ParameterSetName) {
    "UserID" {
                Write-Host -ForegroundColor Green "Script called by piping in user ids"
                $UserID | New-LuwareUsers}
    "File"   {
                Write-Host -ForegroundColor Green "Script called by specifying a filename"
                try{
                $UserIDs = Get-Content $FileName
                $UserIDs | New-LuwareUsers
                } catch {
                    throw $_
                }
    }
}
