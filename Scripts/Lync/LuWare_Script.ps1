<#
.SYNOPSIS
    This script currently demonstrates how to retrieve information from Active
    Directory and Lync / Skype For Business about users.
.DESCRIPTION
    Until the latest version of LuWare is deployed, this script remains a way
    to retrieve specific information from both Active Directory and Lync or 
    Skype For Business which will ultimately be required for using LuWare's
    New-PaPerson powershell cmdlet to add new users to the LuWare application

    Eventually however, once the LuWare Powershell module is availble, this
    script will be used to allow mass creation of new users of the LuWare 
    application.

.PARAMETER UserID
    This is the SamAccountName of the user for which an object in the LuWare
    application is to be created. This can be specified via -UserID when 
    calling the script, or it can be simply piped in.

.PARAMETER FileName
    This is the name of a text file which lists SamAccountNames (one per line)
    which the script will load, instead of specifying userid on the command
    line, or piping in objects.

.EXAMPLE
    Using the pipeline calling convention:
    $Users = Get-Content "List of Users.txt"
    $Users | .\LuWare_Script.ps1

.EXAMPLE
    Using the filename calling convention:
    .\LuWare_Script.ps1 -FileName "List of Users.txt"

#>

# The comment block above this is using the special syntax for Powershell files
# which allows them to generate help with the Get-Help cmdlet.
# If you run Get-Help .\LuWare_Script.ps1 - you will see help generated
# in the same format as Microsoft's own Powershell commands.

# Steps to run script:
# 1) cd documents (where the userid.txt and the LuWarePA2.ps1 files are located)
# 2) $users = Get-Content "userid.txt"
# 3) $users | .\LuWarePA2.ps1

# This block now says there are two ways you can use this script.
# You can either run it the original way, as specified above in steps 1 to 3
# Or you can just tell it the Filename to load the users from, and it will run
# the commands to create new LuWare users.
# The script will default to accepting users piped to it on the command line.
[CmdletBinding(DefaultParameterSetName="UserID")]
param(
    [Parameter(Mandatory=$true,ValueFromPipeline=$true,ParameterSetName="UserID",Position=0)]
    [string]$UserID,
    [Parameter(Mandatory=$true,ValueFromPipeline=$false,ParameterSetName="File",Position=0)]
    [string]$FileName
    )

# Setting $ErrorAction to "Stop" means that every cmdlet that causes any sort of
# error at all will stop execution of the script right there, unless it is wrapped
# in at try/catch block - where whatever the error was can be handled.
$ErrorActionPreference = "Stop"

Function New-LuwareUsers {
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        [string]$UserID
    )

    # The begin block is run only once when the script is run, even when you pipe in
    # mulitple objects like with step 4( above.
    Begin{
        # Try to import the ActiveDirectory module, and exit if the import fails
        try{
            # The if statement checks to see if the ActiveDirectory module is already loaded
            # or not, if it is then the script will just skip to the SkypeForBusiness
            # section below
            If(-not (Get-Module "ActiveDirectory")){
                Import-Module "ActiveDirectory"
            }
        } catch {
            # If the script gets in here, it means the command to import the ActiveDirectory
            # module failed, and the script will immediately stop after posting this error
            # message to the user.
            Throw "This script needs the ActiveDirectory module to be loaded"
        }

        # Try to import the SkypeForBusiness module, and exit if the import fails
        try{
            # The if statement checks to see if the SkypeForBusiness module is already loaded
            # or not, if it is then the script will just skip to the Process
            # section below
            If(-not (Get-Module "SkypeForBusiness")){
                Import-Module "SkypeForBusiness"
            }
        }catch{
            # If the script gets in here, it means the command to import the SkypeForBusiness
            # module failed, and the script will immediately stop after posting this error
            # message to the user.
            Throw "This script needs the SkypeForBusiness module to be loaded"
        }
    }

    # The process block is run for every UserID that is piped in with the step 5) above
    # So if the userid.txt file contains two user ids, this section will run twice.
    Process{
        # First command retrieves data about this User from Active Directory, and stores that information
        # in the variable $AdInfo
        $AdInfo = Get-AdUser $UserID -Properties @("GivenName","Surname","SamAccountName","SID","msRTCSIP-PrimaryUserAddress")

        # Second command retrieves data about this User from Lync / Skype For Business, and stores that 
        # information in the variable $CsInfo
        $CsInfo = Get-CsUser $AdInfo.'msRTCSIP-PrimaryUserAddress'

        # Third command takes the items we're interested in from both $AdInfo and $CsInfo
        # and stores them in a HashTable called $LuWarePerson. This is only used to demonstrate
        # the script, and won't be needed at all once the LuWare powershell module is available.
        $LuWarePerson = @{FirstName=$AdInfo.GivenName;
                          LastName=$AdInfo.Surname;
                          Sam=$AdInfo.SamAccountName;
                          Sid=$AdInfo.SID;
                          OrganizationalUnitId="UBS";
                          SipAddress=$CsInfo.SipAddress;

        }

        # Eventually, when the LuWare powershell module is installed, this section can be uncommented
        # to actually create objects in the LuWare application.

        # New-PaPerson -FirstName $AdInfo.GivenName `
        #              -LastName $AdInfo.Surname `
        #              -Sam $AdInfo.SamAccountName `
        #              -Sid $AdInfo.SID `
        #              -OrganizationalUnitId "UBS" `
        #              -SipAddress $CsInfo.SipAddress `
        #              -IsMaEnabled $true `
        #              -IsPaEnabled $true

        # Until the LuWare powershell object is available, these two lines just output the
        # the LuWarePerson object so the user of the script can see it's working.
        $LuWarePerson
        "`n"
    }
}

# This block works out which calling convention was used - i.e.
# was the script called by piping content to it, or was it called
# by giving it a filename to load the content from.
switch ($PSCmdlet.ParameterSetName) {
    "UserID" {
                Write-Host -ForegroundColor Green "Script called by piping in user ids"
                $UserID | New-LuwareUsers}
    "File"   {
                Write-Host -ForegroundColor Green "Script called by specifying a filename"
                try{
                    $UserIDs = Get-Content $FileName
                    $UserIDs | New-LuwareUsers
                } catch {
                    throw $_
                }
    }
}
