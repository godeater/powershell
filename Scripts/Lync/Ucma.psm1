﻿[Reflection.Assembly]::LoadWithPartialName("Microsoft.Rtc.Collaboration")

[string]$sipaddress = "nope";
[string]$username = "nope";
[string]$password = "nope";
[string]$domain = "nope";
[string]$destinationSip = "nope";
[string]$IMMessage = "nope";

[Microsoft.Rtc.Collaboration.CollaborationPlatform]$Script:_collabPlatform
[Microsoft.Rtc.Collaboration.UserEndpoint]$Script:_endpoint
[bool]$_OKToQuit = $false

$Callbacks = [Hashtable]::Synchronized(@{})

#requires -version 2.0

Function New-ScriptBlockCallback {
    param(
		[parameter(Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[string]$callbackName,
        [parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [scriptblock]$Callback
    )
    <#
    .SYNOPSIS
        Allows running ScriptBlocks via .NET async callbacks.

    .DESCRIPTION
        Allows running ScriptBlocks via .NET async callbacks. Internally this is
        managed by converting .NET async callbacks into .NET events. This enables
        PowerShell 2.0 to run ScriptBlocks indirectly through Register-ObjectEvent.         

    .PARAMETER Callback
        Specify a ScriptBlock to be executed in response to the callback.
        Because the ScriptBlock is executed by the eventing subsystem, it only has
        access to global scope. Any additional arguments to this function will be
        passed as event MessageData.
        
    .EXAMPLE
        You wish to run a scriptblock in reponse to a callback. Here is the .NET
        method signature:
        
        void Bar(AsyncCallback handler, int blah)
        
        ps> [foo]::bar((New-ScriptBlockCallback { ... }), 42)                        

    .OUTPUTS
        A System.AsyncCallback delegate.
    #>
    # is this type already defined?
	$eventBridge   = $callbackName + "_EventBridge"
	$eventComplete = $callbackName + "_CallbackComplete"
    if (-not ($eventBridge -as [type])) {
$CSharpCode = @"
            using System;
            
            public sealed class CallbackEventBridge
            {
                public event AsyncCallback CallbackComplete = delegate { };

                private CallbackEventBridge() {}

                private void CallbackInternal(IAsyncResult result)
                {
                    CallbackComplete(result);
                }

                public AsyncCallback Callback
                {
                    get { return new AsyncCallback(CallbackInternal); }
                }

                public static CallbackEventBridge Create()
                {
                    return new CallbackEventBridge();
                }
            }
"@
		$CSharpCode = $CSharpCode.Replace("CallbackComplete",$eventComplete)
		$CSharpCode = $CSharpCode.Replace("CallbackEventBridge",$eventBridge)
	    Add-Type $CSharpCode
    }

	if(-not $Callbacks[$callbackName]){
		$dynamic_call = '$bridge = ' + "[$eventBridge]::Create()"
		Invoke-Expression $dynamic_call
		Register-ObjectEvent -input $bridge -EventName $eventComplete -action $callback -messagedata $args > $null
		$Callbacks[$callbackName] = $bridge.callback
		return $bridge.callback
	} else {
		return $null
	}
}

Function Main-UCMA{
    param(
		[string[]]$inputstrings
    )
    [string]$userAgent = "PowershellAgent"

    $platformSettings = New-Object Microsoft.Rtc.Collaboration.ClientPlatformSettings($userAgent, [Microsoft.Rtc.Signaling.SipTransportType]::Tls)
	Write-Host "Platform settings created"
    $Script:_collabPlatform = New-Object Microsoft.Rtc.Collaboration.CollaborationPlatform($platformSettings)
	Write-Host "Collaboration platform created"

    #Start up the platform, calling back asynchronously once it's done.
    $_collabPlatform.BeginStartup($EndCollabPlatformStartup, $null)
	Write-Host "Collaboration platform BeginStartup called"

    #In this example, wait for everything to finish before exiting
    while (-not $_OKToQuit){
        [System.Threading.Thread]::Sleep(2000)
		Write-Host "Sleeping..."
    }
}

$EndCollabPlatformStartup = New-ScriptBlockCallback "EndCollabPlatformStartup" {
    param(
		[System.IAsyncResult]$ar
    )
	Write-Host "EndCollabPlatformStartup entered"
	$global:ECPSar = $ar
    $Script:_collabPlatform.EndStartup($ar)

    #A collaboration plaform can have one or more Endpoints. An Endpoint is tied to a SIP Address.            
    [Microsoft.Rtc.Collaboration.UserEndpointSettings]$settings = New-Object Microsoft.Rtc.Collaboration.UserEndpointSettings($sipaddress)
	Write-Host "UserEndPointSettings created"
    $settings.Credential = New-Object System.Net.NetworkCredential($username, $password, $domain)
    $settings.AutomaticPresencePublicationEnabled = $true

    $Script:_endpoint = New-Object Microsoft.Rtc.Collaboration.UserEndpoint($Script:_collabPlatform, $settings)
	Write-Host "UserEndPoint created"
    $Script:_endpoint.BeginEstablish($UserEndpointEstablishCompleted, $null)
	Write-Host "Endpoint BeingEstablish called"
}


$UserEndpointEstablishCompleted = New-ScriptBlockCallback "UserEndpointEstablishCompleted" {
    param(
		[System.IAsyncResult]$ar
    )
	Write-Host "UserEndpointEstablishCompleted called"
    $Script:_endpoint.EndEstablish($ar)

    #Once the endpoint is in place, create a Conversation and an IM Call. 
    $Conversation = New-Object Microsoft.Rtc.Collaboration.Conversation($Script:_endpoint)
	Write-Host "Conversation created"
    $Call = New-Object Microsoft.Rtc.Collaboration.InstantMessagingCall($Conversation)
	Write-Host "Call created"
    $CallEstablishOptions = New-Object Microsoft.Rtc.Collaboration.CallEstablishOptions
	Write-Host "CallEstablishOptions created"

    #When the call is established, Flow will be created. Flow is how you sent IMs around. Therefore, just before
    #establishing, we attach an event handler to catch the flow being setup (it's state will change to Active)
    $Call.InstantMessagingFlowConfigurationRequested += $Call_InstantMessagingFlowConfigurationRequested
	Write-Host "MessagingFlowConfig called"
    $Call.BeginEstablish($destinationSip, $CallEstablishOptions, $EndBeginEstablish, $Call)
	Write-Host "Call BeginEstablish called"
}

$EndBeginEstablish = New-ScriptBlockCallback "EndBeginEstablish" {
    param(
		[System.IAsyncResult]$ar
    )
	Write-Host "EndBeingEstablish called"
    [Microsoft.Rtc.Collaboration.Call]$call = [Microsoft.Rtc.Collaboration.Call]$ar.AsyncState
	Write-Host "Call cast from IASyncResult"
    $call.EndEstablish($ar)
	Write-Host "call EndEstablish called"
}

Function Call_InstantMessagingFlowConfigurationRequested {
    param(
		$imsender,
		[Microsoft.Rtc.Collaboration.InstantMessagingFlowConfigurationRequestedEventArgs]$e
    )
	Write-Host "InstantMessageFlowConfigRequested entered"
    #Once we're notified about this, we get a handle to the newly created Flow. Let's use this to register for state changes.
    $e.Flow.StateChanged += $Flow_StateChanged
	Write-Host "Flow state changed"
}

Function Flow_StateChanged{
    param(
		$imsender,
		[Microsoft.Rtc.Collaboration.MediaFlowStateChangedEventArgs]$e
    )
	Write-Host "Flow_StateChanged entered"
    if ($e.State -eq [Microsoft.Rtc.Collaboration.MediaFlowState]::Active){
        #The flow is now active! We can use it to send messages.
        [Microsoft.Rtc.Collaboration.InstantMessagingFlow]$flow = [Microsoft.Rtc.Collaboration.InstantMessagingFlow]$imsender
		Write-Host "Flow directed at destination"
        $flow.BeginSendInstantMessage($IMMessage, $EndBeginSendInstanceMessage, $flow)
		Write-Host "Message sent"
    }else{
		Write-Host "Flow not active"
	}
}

$EndBeginSendInstanceMessage = New-ScriptBlockCallback "EndBeginSendInstanceMessage" {
    param(
		[System.IAsyncResult]$ar
    )
	Writer-Verbose "EndBeginSendInstanceMessage entered"
    [Microsoft.Rtc.Collaboration.InstantMessagingFlow]$flow = [Microsoft.Rtc.Collaboration.InstantMessagingFlow]$ar.AsyncState
	Write-Host "Flow cast from IASyncResult"
    $flow.EndSendInstantMessage($ar)
	Write-Host "EndSendInstantMessage called"

    #Having sent the message, terminate the conversation
    $flow.Call.Conversation.BeginTerminate($EndBeginTerminate, $flow.Call.Conversation)
	Write-Host "Call terminated"
}

$EndBeginTerminate = New-ScriptBlockCallback "EndBeginTerminate" {
    param(
		[System.IAsyncResult]$ar
    )
	Write-Host "EndBeginTerminate entered"
    [Microsoft.Rtc.Collaboration.Conversation]$conversation = [Microsoft.Rtc.Collaboration.Conversation]$ar.AsyncState
	Write-Host "Conversation cast from IAsyncResult"
    $conversation.EndTerminate($ar)
	Write-Host "Conversation terminated"
    
    $_OKToQuit = true
}

Export-ModuleMember -Variable * -Function * -Alias *
