﻿################################################################################
###
###	UserReport_1.11.ps1 	(Prod Version)
###
###	Creates a Lync UserReport
###	Deletes "@ubsprod.msad.ubs.net" from all entries in the UserPrincipalName column and renames the column to GPN
###	Removes entries starting with "svc_"
###	Sends the csv file to sh-messaging-zurich@ubs.com for further processing
###	added in 1.05: additional copy to \\14.1.145.66\ReportDropoff for louis.capece@ubs.com
###	changed in 1.06: Body text of email (P04-ev, formatting)
###	changed in 1.07: updated $dest = "\\161.239.54.110\ReportDropoff\Lync" (nstm1064psq.stm.swissbank.com) (old:"\\14.1.145.66\ReportDropoff\Lync")
###	changed in 1.08: updated $dest = "\\161.239.33.161\ReportDropoff\Lync" (nstm2889psq.stm.swissbank.com) (old:"\\161.239.54.110\ReportDropoff\Lync")
###	changed in 1.09: Sharepoint link updated to https://teams.cc.cnet.ubs.net/sites/LyncProg/Deployment/Weekly%20Profile%20Reports/ 
###	changed in 1.10: moved script from nzur1123pap to nzur6750pap
###	changed in 1.11: removed $dest = "\\161.239.54.110\ReportDropoff\Lync" (nstm1064psq.stm.swissbank.com)
###	changed in 1.12: new attribute 'Registrarpool' added for Christopher Brennan and Duncan Bradey
### changed in 1.13: Added some processing to remove the requirements for manual processing which used to be required before
###
################################################################################
###
###	Version:	       1.13
###	Date:		       21.05.2015
###	Author:		       martin-za.frei@ubs.com
### Additional author: bryan.childs@ubs.com
###
################################################################################

$datapath = "C:\_LyncScripts\UserReports\Data"
$filename = "UserReport_" + (Get-Date -uformat "%d%m%Y") + "_production.csv"
$filepath = $datapath + "\" + $filename

import-module lync
# Get all Lync users except those assigned the P00 (disabled user) conferencing policy
$NonP00Users  = get-csuser -filter {ConferencingPolicy -ne "P00 Conferencing Policy"}
# From those users, filter out those with sip addresses which match 'svc_' which indicate they're a service account
# and those which have EnterpriseVoiceEnabled AND AudioVideoDisabled set to FALSE, but are NOT assigned 'P04-ev Conferencing Policy' 
$EVAVDisabled = $NonP00Users | Where-Object {
	($_.SipAddress -inotmatch "svc_") `
	  -and (-not `
	  (($_.EnterpriseVoiceEnabled -eq $False -and $_.AudioVideoDisabled -eq $False) `
	   -and $_.ConferencingPolicy -ne 'P04-ev ConferencingPolicy'))
}
# Select the properties from those users which are required in the report, and during this rename
# the UserPrincipalName field to GPN, strip the @ubsprod.msad.ubs.net part of the value from it
# and truncate it to 8 characters
$UsersToWrite = $EVAVDisabled | select-object @{Name='GPN'; Expression={($_.UserPrincipalName -replace "@ubsprod.msad.ubs.net", "").SubString(0,8)}}, `
                                  sipaddress, dialplan, voicepolicy, clientpolicy, conferencingpolicy, enterprisevoiceenabled, audiovideodisabled, Registrarpool
# Export the objects to a csv file
$UsersToWrite | export-csv $datapath\$filename -Delimiter ";" -NoTypeInformation

# core logic is already over, a little housekeeping

$localfiles = @(Get-ChildItem $datapath\UserReport_*.csv | sort-object LastWriteTime -Descending)
$i = 60
while ($i -lt $localfiles.length) {
	Remove-Item $localfiles[$i].fullname
	$i++
}

$EmailBody =  "File $filepath created on nzur6750pap.ubsprod.msad.ubs.net. `n`nA few steps need to be done manually:`n`n"
# Shouldn't be need anymore due to processing carried out above
#$EmailBody += "1) Delete entries which have EnterpriseVoiceEnabled AND AudioVideoDisabled set to FALSE, but NOT 'P04-ev Conferencing Policy' `n"
# Shouldn't be needed anymore due to processing carried out above
#$EmailBody += "2) Format GPN column to show 8 digits (Format Cells: Number Tab --> Custom --> '00000000') For userfriendly formating, please compare with an existing report`n"
# Renumbered to 1), because of previous lines no longer being required
#$EmailBody += "3) Create a Pivot in a new worksheet named pivot (Row Field 'ConferencingPolicy', Data Items 'ConferencingPolicy'), save it as .xls and upload it to:`n"
$EmailBody += "1) Create a Pivot in a new worksheet named pivot (Row Field 'ConferencingPolicy', Data Items 'ConferencingPolicy'), save it as .xls and upload it to:`n"
$EmailBody += "https://teams.cc.cnet.ubs.net/sites/LyncProg/Deployment/Weekly%20Profile%20Reports/. `n`n`n"
$EmailBody += "Regards`nMartin"

Send-MailMessage -From "<sh-messaging-zurich@ubs.com>" -To "<sh-messaging-zurich@ubs.com>" -Cc "<martin-za.frei@ubs.com>", "<ivo.habermacher@ubs.com>" `
  -Subject "Weekly Lync User Report completed in PRODUCTION (nzur6750pap). $filename" `
  -Body $EmailBody -Attachments $filepath -SmtpServer smtpgate.mail.appl.ubs.ch

# $dest = "\\161.239.33.161\ReportDropoff\Lync"
# If (test-path $dest) {
#	copy-item $filepath $dest
# } else {
#	Send-MailMessage -From "<sh-messaging-zurich@ubs.com>" -to "<martin-za.frei@ubs.com>" -Subject "\\161.239.33.161\ReportDropoff\Lync not accessible" -SmtpServer smtpgate.mail.appl.ubs.ch
# }
