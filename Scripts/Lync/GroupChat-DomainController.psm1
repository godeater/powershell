﻿<#

    RPC endpoint mapper: port 135 TCP, UDP
    NetBIOS name service: port 137 TCP, UDP
    NetBIOS datagram service: port 138 UDP
    NetBIOS session service: port 139 TCP
    SMB over IP (Microsoft-DS): port 445 TCP, UDP
    LDAP: port 389 TCP, UDP
    LDAP over SSL: port 636 TCP
    Global catalog LDAP: port 3268 TCP
    Global catalog LDAP over SSL: port 3269 TCP
    Kerberos: port 88 TCP, UDP
    DNS: port 53 TCP, UDP
    WINS resolution: port 1512 TCP, UDP
    WINS replication: 42 TCP, UDP
    RPC: Dynamically-assigned ports TCP, unless restricted


#>

# Port number constants
Set-Variable LdapPort             -Option ReadOnly -Value ([int]389)
Set-Variable SSLLdapPort          -Option ReadOnly -Value ([int]636)
Set-Variable GlobalCatalogPort    -Option ReadOnly -Value ([int]3268)
Set-Variable SSLGlobalCatalogPort -Option ReadOnly -Value ([int]3269)

Function Get-ActiveTCPConnections {
	[cmdletbinding()]
	param(
	)

	try {
		$TCPProperties = [System.Net.NetworkInformation.IPGlobalProperties]::GetIPGlobalProperties()
		$Connections = $TCPProperties.GetActiveTcpConnections()
		foreach($Connection in $Connections) {
			if($Connection.LocalEndPoint.AddressFamily -eq "InterNetwork" ) { $IPType = "IPv4" } else { $IPType = "IPv6" }
			$OutputObj = New-Object -TypeName PSobject
			$OutputObj | Add-Member -MemberType NoteProperty -Name "LocalAddress" -Value $Connection.LocalEndPoint.Address
			$OutputObj | Add-Member -MemberType NoteProperty -Name "LocalPort" -Value $Connection.LocalEndPoint.Port
			
			$OutputObj | Add-Member -MemberType NoteProperty -Name "RemoteAddress" -Value $Connection.RemoteEndPoint.Address
			$OutputObj | Add-Member -MemberType NoteProperty -Name "RemotePort" -Value $Connection.RemoteEndPoint.Port
			
			$OutputObj | Add-Member -MemberType NoteProperty -Name "State" -Value $Connection.State
			$OutputObj | Add-Member -MemberType NoteProperty -Name "IPV4Or6" -Value $IPType
			$OutputObj
		}
	} catch {
		Write-Error "Failed to get active connections. $_"
	}
}

Function Find-DomainControllers {
	param(
		[Parameter(Mandatory=$true,Position=0,ValueFromPipeline=$true)]
		[PSObject[]]$Connections
	)
	try{
		$DCHosts = @()
		$DomainControllers = $Connections | Where-Object { $_.RemotePort -eq $LdapPort }
		ForEach($DC in $DomainControllers){
			[string]$DCHost = [System.Net.Dns]::GetHostEntry($DC.RemoteAddress).Hostname
			$DCHosts += $DCHost.Split('.')[0]
		}
		return $DCHosts
	} catch {
		Write-Error "Failed to find domain controllers. $_"
	}
}

Function Initialize-MSMQ {
	$SystemDotMessagingLoaded = $false
	$LoadedAssemblies = [AppDomain]::CurrentDomain.GetAssemblies()
	$LoadedAssemblies.GetEnumerator() | ForEach-Object {
		If($_.Location -like 'System.Messaging'){
			$SystemDotMessaingLoaded = $true
		}
	}
	If(!($SystemDotMessagingLoaded)){
		[Reflection.Assembly]::LoadWithPartialName('System.Messaging') | Out-Null
	}
}

Function Send-MSMQMessage {
	param(
		[Parameter(Mandatory=$true,Position=0)]
		[string]$queueName,
		[Parameter(Mandatory=$true,Position=1)]
		[string]$message
	)

	$queue = new-object System.Messaging.MessageQueue $queueName
	$utf8 = new-object System.Text.UTF8Encoding

	$tran = new-object System.Messaging.MessageQueueTransaction
	$tran.Begin()

	$msgContent = $message
	$msgBytes = $utf8.GetBytes($msgContent)

	$msgStream = new-object System.IO.MemoryStream
	$msgStream.Write($msgBytes, 0, $msgBytes.Length)
	
	$msg = new-object System.Messaging.Message
	$msg.BodyStream = $msgStream
	$queue.Send($msg, $tran)

	$tran.Commit()
}

Function Read-MSMQMessages {
	param(
		[Parameter(Mandatory=$true,Position=0)]
		[string]$queueName
	)
	$Messages = @()
	$queue = new-object System.Messaging.MessageQueue $queueName
	$utf8 = new-object System.Text.UTF8Encoding

	$msgs = $queue.GetAllMessages()
	$msgs | Foreach-Object {
		$Message = New-Object PSObject -Property @{Id=$_.Id;Body=$utf8.GetString($_.BodyStream.ToArray())}
		$Messages += $Message
		#write-host $_.Id
		#write-host $utf8.GetString($_.BodyStream.ToArray())
	}
	return $Messages
}

Function Purge-MSMQMessage {
	param(
		[Parameter(Mandatory=$true,Position=0)]
		[string]$queueName
	)
	$queue = New-Object -TypeName "System.Messaging.MessageQueue"
	$queue.Path = $queuename
	$messagecount = $queue.GetAllMessages().Length 
	$queue.Purge() 
	Write-Host "$queuename has been purged of $messagecount messages." -ForegroundColor Yellow
}

Function ConvertTo-CliXml {
    param(
        [Parameter(Position=0, Mandatory=$true, ValueFromPipeline=$true)]
        [ValidateNotNullOrEmpty()]
        [PSObject[]]$InputObject
    )
    begin {
        $type = [PSObject].Assembly.GetType('System.Management.Automation.Serializer')
        $ctor = $type.GetConstructor('instance,nonpublic', $null, @([System.Xml.XmlWriter]), $null)
        $sw = New-Object System.IO.StringWriter
        $xw = New-Object System.Xml.XmlTextWriter $sw
        $serializer = $ctor.Invoke($xw)
        #$method = $type.GetMethod('Serialize', 'nonpublic,instance', $null, [type[]]@([object]), $null)
    }
    process {
        try {
            [void]$type.InvokeMember("Serialize", "InvokeMethod,NonPublic,Instance", $null, $serializer, [object[]]@($InputObject))
        } catch {
            Write-Warning "Could not serialize $($InputObject.GetType()): $_"
        }
    }
    end {    
        [void]$type.InvokeMember("Done", "InvokeMethod,NonPublic,Instance", $null, $serializer, @())
        $sw.ToString()
        $xw.Close()
        $sw.Dispose()
    }
}

Function ConvertFrom-CliXml {
    param(
        [Parameter(Position=0, Mandatory=$true, ValueFromPipeline=$true)]
        [ValidateNotNullOrEmpty()]
        [String[]]$InputObject
    )
    begin
    {
        $OFS = "`n"
        [String]$xmlString = ""
    }
    process
    {
        $xmlString += $InputObject
    }
    end
    {
        $type = [PSObject].Assembly.GetType('System.Management.Automation.Deserializer')
        $ctor = $type.GetConstructor('instance,nonpublic', $null, @([xml.xmlreader]), $null)
        $sr = New-Object System.IO.StringReader $xmlString
        $xr = New-Object System.Xml.XmlTextReader $sr
        $deserializer = $ctor.Invoke($xr)
        $done = $type.GetMethod('Done', [System.Reflection.BindingFlags]'nonpublic,instance')
        while (!$type.InvokeMember("Done", "InvokeMethod,NonPublic,Instance", $null, $deserializer, @()))
        {
            try {
                $type.InvokeMember("Deserialize", "InvokeMethod,NonPublic,Instance", $null, $deserializer, @())
            } catch {
                Write-Warning "Could not deserialize ${string}: $_"
            }
        }
        $xr.Close()
        $sr.Dispose()
    }
}