﻿<#
.SYNOPSIS
Script written to automate shut down and start up of Lync services
.DESCRIPTION
Script written to automate shut down and start up of Lync services 
on selected servers in the Hayes data centre. Intended for use 
during the 2015-07-11 BCM test.
The services covered include:
   All native Lync servces
   Vantage Connector Service
   SmartTAP Connector Service
.PARAMETER Mode 
Either "Stop" or "Start"
Indicates whether you wish the script to stop services or start them
.PARAMETER ServerList 
CSV File (One header named "ServerName") containing list
of servers to operate on
.EXAMPLE
.\HayesBCMTest.ps1 -Mode "Stop"
.EXAMPLE
.\HayesBCMTest.ps1 -Mode "Start"

#>



param(
	[Parameter(Mandatory=$true)]
	[ValidateSet("Stop","Start")]
	[string]$Mode,
	[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
	[string]$ServerList
)


# Define list of servers we're going to work on :
$global:ImportedServers = @()

Function Test-LyncModuleLoaded {
	If(!(Get-Module "Lync")){
		If(!(Get-Module -ListAvailable "Lync")){
			Write-Error "Lync Module NOT Available - Cannot continue"
			Exit
		}Else{
			Write-Host -ForegroundColor Yellow "Loading Lync module"
			Import-Module Lync
		}
	}Else{
		Write-Host -ForegroundColor Green "Lync module is loaded"
	}
	return $true
}

Function Import-Servers {
	param(
		[Parameter(Mandatory=$true)]
		[string]$FileName
	)
	if(!(Test-Path -Path $FileName)){
		Write-Error "Can't find" $FileName
		Exit
	}
	$CsvServers = Import-Csv -Path $FileName
	$global:ImportedServers = $CsvServers | Select-Object -ExpandProperty ServerName
}

Function Stop-BCMService{
	param(
		[Parameter(Mandatory=$true)]
		[string]$ServerName,
		[Parameter(Mandatory=$true)]
		[string]$ServiceName
	)
	$ServiceToStop = Get-Service -ComputerName $ServerName -Name $ServiceName -ErrorAction SilentlyContinue
	if(!($ServiceToStop)){
		Write-Host -ForegroundColor Magenta "Server" $LyncServer "doesn't host a" $ServiceName  "service"
	} Else {
		Stop-Service -InputObject $ServiceToStop
	}

}
Function Start-BCMService{
	param(
		[Parameter(Mandatory=$true)]
		[string]$ServerName,
		[Parameter(Mandatory=$true)]
		[string]$ServiceName
	)
	$ServiceToStart = Get-Service -ComputerName $ServerName -Name $ServiceName -ErrorAction SilentlyContinue
	If(!($ServiceToStart)){
		Write-Host -ForegroundColor Magenta "Server" $LyncServer "doesn't host a" $ServiceName "service"
	} Else {
		Start-Service -InputObject $ServiceToStart
	}
}


Function Set-VantageServices {
	param(
		[Parameter(Mandatory=$true)]
		[ValidateSet("Stop","Start")]
		[string]$Mode
	)

	if($Mode -eq "Stop"){
		ForEach($LyncServer in $ImportedServers){
			Write-Host -ForegroundColor Yellow "Attempting to stop FtProcessController service on" $LyncServer
			Stop-BCMService -ServerName $LyncServer -ServiceName "FtProcessController"
		}
	}

	if($Mode -eq "Start"){
		ForEach($LyncServer in $ImportedServers){
			Write-Host -ForegroundColor Yellow "Attempting to start FtProcessController service on" $LyncServer
			Start-BCMService -ServerName $LyncServer -ServiceName "FtProcessController"
		}
	}
}

Function Set-VerbaServices {
	param(
		[Parameter(Mandatory=$true)]
		[ValidateSet("Stop","Start")]
		[string]$Mode
	)

	if($Mode -eq "Stop"){
		ForEach($LyncServer in $ImportedServers){
			Write-Host -ForegroundColor Yellow "Attempting to stop Verba services on" $LyncServer
			Stop-BCMService -ServerName $LyncServer -ServiceName "VerbaSysMon"
			Stop-BCMService -ServerName $LyncServer -ServiceName "VerbaLyncFilter"
			Stop-BCMService -ServerName $LyncServer -ServiceName "VerbaNMAgent"
		}
	}

	if($Mode -eq "Start"){
		ForEach($LyncServer in $ImportedServers){
			Write-Host -ForegroundColor Yellow "Attempting to start Verba services on" $LyncServer
			Start-BCMService -ServerName $LyncServer -ServiceName "VerbaSysMon"
			Start-BCMService -ServerName $LyncServer -ServiceName "VerbaLyncFilter"
			Start-BCMService -ServerName $LyncServer -ServiceName "VerbaNMAgent"
		}
	}
}


Function Set-LyncServices {
	param(
		[Parameter(Mandatory=$true)]
		[ValidateSet("Stop","Start")]
		[string]$Mode
	)

	if($Mode -eq "Stop"){
		ForEach($LyncServer in $ImportedServers){
			Write-Host -ForegroundColor Yellow "Attempting to stop all Lync services on" $LyncServer
			Stop-CsWindowsService -ComputerName $LyncServer
		}
	}

	if($Mode -eq "Start"){
		ForEach($LyncServer in $ImportedServers){
			Write-Host -ForegroundColor Yellow "Attempting to start all Lync services on" $LyncServer
			Start-CsWindowsService -ComputerName $LyncServer
		}
	}
}

if(Test-LyncModuleLoaded){
	Import-Servers -FileName $ServerList
	if($Mode -eq "Stop"){
		# Order is important, because of Lync component dependencies
		Write-Host -ForegroundColor Yellow "Running script in STOP mode"
		Set-LyncServices -Mode "Stop"
		Set-VerbaServices -Mode "Stop"
		Set-VantageServices -Mode "Stop"
	}
	if($Mode -eq "Start"){
		# Order is important, because of Lync component dependencies
		Write-Host -ForegroundColor Green "Running script in START mode"
		Set-VantageServices -Mode "Start"
		Set-VerbaServices -Mode "Start"
		Set-LyncServices -Mode "Start"
	}
	Write-Host -ForegroundColor Green "Script finished"
}

Function Move-CsUserWithProgress {
	param(
		[Parameter(Mandatory=$true,Position=0)]
		[string]$PoolToMoveFrom,
		[Parameter(Mandatory=$true,Position=1)]
		[string]$PoolToMoveTo
	)
	$i = 0
	$HayesUsers = @()
	$HayesUsers += Get-CsUser -Filter {RegistrarPool -eq $PoolToMoveFrom}

	$HayesUsers | Foreach-Object {
		$i++
		Move-CsUser -Identity $_.SipAddress -Target $PoolToMoveTo -Confirm:$false -Force
		Write-Progress -Activity "Moving Users" -Status "from hayes to ironwood" -PercentComplete (($i/$HayesUsers.count)*100)
	}
}
