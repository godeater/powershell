$ErrorActionPreference = "Stop"

Function Fix-GroupChannelsPrefs {
    param(
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()]
        [string]$PrefsPath
    )


    # Load the content of the "GroupChannels" file in the users Group Chat preferences folder into an XML object
    [xml]$GroupChannels = Get-Content "$PrefsPath\GroupChannels"
    # Find all the <member> nodes which represent either the value for the participantListPanelWidth or
    # the value for the inputBoxHeight
    $membersToFix = $GroupChannels.SelectNodes("(//member[@name='participantListPanelWidth'] | //member[@name='inputBoxHeight'])")

    # Iterate over the nodes, and just set all the values to "90"
    $membersToFix | Foreach-Object {
        $_.Value = "90"
    }

    # Save the preferences file back to disk
    $GroupChannels.Save("$PrefsPath\GroupChannels")

    # Load the content of the "GroupChannels.serverstatus.xml" file into an XML object
    [xml]$GroupChannelsServerStatus = Get-Content "$PrefsPath\GroupChannels.serverstatus.xml"
    # Cast the current sequence value to a 64 bit integer
    [int64]$currentSeq = $GroupChannelsServerStatus.ServerStatusRecord.SeqId
    # Add one to sequence
    $currentSeq++
    # Change current sequence value to the new one
    $GroupChannelsServerStatus.ServerStatusRecord.SeqId = $currentSeq.ToString()
    # Save the serverstatus.xml file back to disk
    $GroupChannelsServerStatus.Save("$PrefsPath\GroupChannels.serverstatus.xml")


}

Function Search-AD{
    param(
        [Parameter(Mandatory=$true)]
        [string]$LdapFilter,
        [Parameter(Mandatory=$false)]
        [string[]]$PropsToLoad=@()
    )

    $directoryEntry = "LDAP://UBSPROD.MSAD.UBS.NET/dc=UBSPROD,DC=MSAD,DC=UBS,DC=NET"
    $domain = New-Object DirectoryServices.DirectoryEntry($directoryEntry)

    $searcher = New-Object DirectoryServices.DirectorySearcher

    $searcher.SearchRoot = $domain
    $searcher.PageSize = $pageSize
    $searcher.Filter = $ldapFilter
    $searcher.SearchScope = "Subtree"

    if(!($propsToLoad.Count -eq 0)){
        foreach($i in $propsToLoad){
            $searcher.PropertiesToLoad.Add($i)
        }
    }

    $results = $searcher.FindAll()
    $output = @()

    foreach($resultset in $results){
        $properties = New-Object 'System.Collections.Generic.SortedDictionary[string,string]'
        $one_ad_object = $resultset.Properties
        $one_ad_object.GetEnumerator() | ForEach-Object {
            $properties.Add($_.Key,$_.Value)
        }
        $output += $properties
    }

    return $output
}

try{
	$working_folder = $env:TEMP + "\GroupChat_SizeFix\"
	$DateTime       = Get-Date -Format "yyyy-MM-dd HH.mm.ss"
	$LogName        = $working_folder + "GroupChat_SizeFix_Deploy_@_" + $DateTime + ".txt"
	$A3Prefix       = $env:APPDATA + "\Microsoft\AppV\Client\VFS\53685DD6-B6A8-453D-85E6-A582B22E530E\AppData\Microsoft\Group Chat\Group Chat Client\Preferences\"
	$DTPPrefix      = $env:APPDATA + "\Microsoft\Group Chat\Group Chat Client\Preferences\"

	if(-not $(Test-Path $working_folder -PathType Container)){
		New-Item -Path $working_folder -Force -ItemType Directory
	}
	Start-Transcript -Path $LogName -NoClobber -Append

	Write-Host "Trying to repair Group Chat preferences" -ForegroundColor green

	$samAccountName = [Security.Principal.WindowsIdentity]::GetCurrent().Name.Split("\")[1]
	$SipAddress     = $($(Search-AD -LdapFilter "(&(samaccountname=$samAccountName))").'msrtcsip-primaryuseraddress').Replace("sip:","").Replace("@","_")

	# Establish if the directory we need to use is in the DTP location
	# or the A3 location
	if(Test-Path -Path $A3Prefix -PathType Container){
		# User is on A3
		$PrefsDirectory = $A3Prefix + $SipAddress + "\"
	}else{
		# User is on DTP
		$PrefsDirectory = $DTPPrefix + $SipAddress + "\"
	}

	if(Test-Path -Path $PrefsDirectory -PathType Container){
		Write-Host "Group Chat Preferences Path Exists at:"
		Write-Host $PrefsDirectory

		$backup_path = $working_folder + "\backup_" + $DateTime
		Write-Host -ForegroundColor Green "Backing up the Preferences Folder to $backup_path" 

		# Attempt to create a backup folder and copy current files to it

		$backup_directory = New-Item -Path $backup_path -Force -ItemType Directory

		$PrefFileCount   = 0
		$PreferenceFiles = Get-ChildItem -Path $PrefsDirectory
		$PreferenceFiles | ForEach-Object {
			  Copy-Item -Path $_.FullName -Destination $backup_Directory.FullName -Force
			  $PrefFileCount++
			  Write-Progress -Activity "Backing up Group Chat preferences" -Status "Progress:" -PercentComplete ($PrefFileCount/$PreferenceFiles.count*100)
		}
	}else{
		Throw [IO.FileNotFoundException] "Unable to find Group Chat Preferences files at $PrefsDirectory"
	}

	Fix-GroupChannelsPrefs -PrefsPath $PrefsDirectory
	Write-Host -ForegroundColor Green "Script Complete"

} catch {
	$ErrorMessage = $_.Exception.Message
	$FailedItem   = $_.Exception.ItemName
	Write-Error $ErrorMessage
	Write-Error $FailedItem
} finally {
	# Stop running
	Stop-Transcript		
}
