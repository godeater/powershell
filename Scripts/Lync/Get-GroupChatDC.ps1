﻿<#
#>


param(
	[Parameter(Mandatory=$false,ValueFromPipeline=$true)]
	$queueName = "FormatName:Direct=TCP:165.222.137.178\private$\groupchatmonitor"
)

Import-Module -DisableNameChecking ".\GroupChat-DomainController.psm1"

# Main Script Body
Initialize-MSMQ

$CurrentGroupChatHost = [System.Net.Dns]::GetHostname()
$TCPConnections = Get-ActiveTCPConnections
$DomainControllers = @(Find-DomainControllers $TCPConnections | Sort-Object -Unique)

$GroupChatMonitorMessage = New-Object PSObject -Property @{GroupChatHost = $CurrentGroupChatHost;DomainController = $DomainControllers[0]}
$XmlRep = $GroupChatMonitorMessage | ConvertTo-CliXml
Send-MSMQMessage -queueName $queueName -message $XmlRep






