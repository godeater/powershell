﻿#Requires -Version 2.0
<#
.SYNOPSIS
  Installs windows roles and features required for a successful Lync 2010 server install
.DESCRIPTION
  Takes an input parameter describing the intended role of the server to select the
  correct subset of windows roles and features to install.
#>
param(
	[Parameter(ValueFromPipeline=$true,Mandatory=$true)]
	[ValidateSet("FE","DIR","SE","MS","MON")]
	[string]$role
)

If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
	Write-Warning "Please re-run this script from an Administrator powershell prompt"
	Break
}

Import-Module ServerManager

# Front End, Director
If ($role -eq "fe" -or $role -eq "dir" -or $role -eq "se") {
    echo "FE,SE or DIR selected"
    Add-WindowsFeature Web-Static-Content, Web-Default-Doc, Web-HTTP-Errors, Web-Asp-Net, Web-Net-Ext, Web-ISAPI-Ext, Web-ISAPI-Filter, Web-HTTP-Logging, Web-Log-Libraries, Web-Filtering, Web-HTTP-Tracing, Web-Basic-Auth, Web-Windows-Auth, Web-Client-Auth, Web-Stat-Compression, Web-Mgmt-Console, Web-Scripting-Tools
    Add-WindowsFeature MSMQ-Server, MSMQ-Directory
    dism.exe /online /add-package /packagepath:%windir%\servicing\Packages\Microsoft-Windows-Media-Format-Package~31bf3856ad364e35~amd64~~6.1.7601.17514.mum /ignorecheck
}

# Mediation Server
If ($role -eq "ms") {
    echo "MS selected"
    dism.exe /online /add-package /packagepath:%windir%\servicing\Packages\Microsoft-Windows-Media-Format-Package~31bf3856ad364e35~amd64~~6.1.7601.17514.mum /ignorecheck
}

# Monitoring Server
If ($role -eq "mon") {
    echo "MON selected"
    Add-WindowsFeature MSMQ-Server, MSMQ-Directory
}