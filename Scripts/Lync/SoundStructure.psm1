$ErrorActionPreference = "SilentlyContinue"

$soundstructure = import-excel "c:\ubs\dev\tmp\Sfb_Accounts.xlsx"

Function Set-ServiceAccounts{
    $soundstructure | Foreach-Object {
        $DomainUser = "UBSPROD\" + $_.Service_Account
        try {
            $CsUser = Get-CsUser $DomainUser
            if($CsUser -ne $null){
                Set-CsUser $DomainUser -LineUri $_.Full_Line_URI
            }
        } catch {
            Write-Error $_
        }
    }
}
