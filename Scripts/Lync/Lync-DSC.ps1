﻿#Requires -Version 4.0
Configuration LyncConfig {
	param(
		[Parameter(Mandatory=$true)]
		[string]$Role,
		[Parameter(Mandatory=$true)]
		[string]$Fqdn
	)
	Node $Fqdn {
		switch($Role) {
			"Front End" {
				WindowsFeature NetFrameworkCore {
					Ensure = "Present"
					Name   = "NET-Framework-Core"
				}
				WindowsFeature RsatAdds {
					Ensure = "Present"
					Name   = "RSAT-ADDS"
				}
				WindowsFeature WindowsIdentityFoundation {
					Ensure = "Present"
					Name   = "Windows-Identity-Foundation"
				}
				WindowsFeature WebServer {
					Ensure = "Present"
					Name   = "Web-Server"
					# Installs the following features :
					#
					# Web-Server
					# Web-WebServer
					# Web-Common-Http
					# Web-Default-Doc
					# Web-Dir-Browsing
					# Web-Http-Errors
					# Web-Static-Content
					# Web-Health
					# Web-Http-Logging
					# Web-Performance
					# Web-Stat-Compression
					# Web-Security
					# Web-Filtering
				}
				WindowsFeature WebAspNet {
					Ensure    = "Present"
					Name      = "Web-Asp-Net"
					DependsOn = "[WindowsFeature]WebServer"
				}
				WindowsFeature WebNetExt {
					Ensure    = "Present"
					Name      = "Web-Net-Ext"
					DependsOn = "[WindowsFeature]WebServer"					
				}
				WindowsFeature WebISAPIExt {
					Ensure    = "Present"
					Name      = "Web-ISAPI-Ext"
					DependsOn = "[WindowsFeature]WebServer"
				}
				WindowsFeature WebISAPIFilter {
					Ensure    = "Present"
					Name      = "Web-ISAPI-Filter"
					DependsOn = "[WindowsFeature]WebServer"
				}
				WindowsFeature WebLogLibraries {
					Ensure    = "Present"
					Name      = "Web-Log-Libraries"
					DependsOn = "[WindowsFeature]WebServer"
				}
				WindowsFeature WebRequestMonitor {
					Ensure    = "Present"
					Name      = "Web-Request-Monitor"
					DependsOn = "[WindowsFeature]WebServer"
				}
				WindowsFeature WebHttpTracing {
					Ensure    = "Present"
					Name      = "Web-Http-Tracing"
					DependsOn = "[WindowsFeature]WebServer"
				}
				WindowsFeature WebBasicAuth {
					Ensure    = "Present"
					Name      = "Web-Basic-Auth"
					DependsOn = "[WindowsFeature]WebServer"
				}
				WindowsFeature WebWindowsAuth {
					Ensure    = "Present"
					Name      = "Web-Windows-Auth"
					DependsOn = "[WindowsFeature]WebServer"
				}
				WindowsFeature WebClientAuth {
					Ensure    = "Present"
					Name      = "Web-Client-Auth"
					DependsOn = "[WindowsFeature]WebServer"
				}
				WindowsFeature WebDynCompression {
					Ensure    = "Present"
					Name      = "Web-Dyn-Compression"
					DependsOn = "[WindowsFeature]WebServer"
				}
				WindowsFeature WebAspNet45 {
					Ensure    = "Present"
					Name      = "Web-Asp-Net45"
					DependsOn = "[WindowsFeature]WebServer"
				}
				WindowsFeature WebMgmtTools {
					Ensure    = "Present"
					Name      = "Web-Mgmt-Tools"
					DependsOn = "[WindowsFeature]WebServer"
				}
				WindowsFeature WebScriptingTools {
					Ensure    = "Present"
					Name      = "Web-Scripting-Tools"
					DependsOn = "[WindowsFeature]WebServer"
				}
				WindowsFeature WebMgmtCompat {
					Ensure    = "Present"
					Name      = "Web-Mgmt-Compat"
					DependsOn = "[WindowsFeature]WebServer"
				}
				WindowsFeature NetWCFHTTPActivation45 {
					Ensure    = "Present"
					Name      = "NET-WCF-HTTP-Activation45"
					DependsOn = "[WindowsFeature]WebServer"
				}
				WindowsFeature DesktopExperience {
					Ensure    = "Present"
					Name      = "Desktop-Experience"
				}
				WindowsFeature Bits {
					Ensure    = "Present"
					Name      = "BITS"
				}
			}
		}
	}
}