﻿<#
.SYNOPSIS
Split users between two regional pools, or evacuate one regional pool to another
.DESCRIPTION
This script is designed to perform user moves between pools in a region. It operates in
two major modes, either taking half of the users on one pool and moving them to the
other (rebalance mode), or taking ALL users from one pool, and moving them to the other.

Note - in Rebalance mode, the script assumes ALL users are already on one pool, and so
moves half that pool's population to the other. If the starting situation is different
from that assumption, you won't end up with a 50/50 split of users across the pools.

.PARAMETER SourcePool
Specifies the source pool users will be moved from. 
Supplied as the first part of the pool's FQDN

.PARAMETER DestinationPool
Specifies the pool users will be moved to.
Supplied as the first part of the pool's FQDN

.PARAMETER Evacuate
Optional boolean parameter, defaults to $False
If set to true moves ALL users from the source to the destination pool.

.PARAMETER WhatIf
Optional boolean paramter, defaults to $True
If the user wishes the action to actually take place, they have to override
this with the value $False

.EXAMPLE
.\ReBalance_Pool.ps1 -SourcePool irolyncee03 -DestinationPool haylyncee04 

.EXAMPLE
.\ReBalance_Pool.ps1 -SourcePool stmlyncee05 -DestinationPool bluelyncee06 -Evacuate:$True -WhatIf:$False
#>
#requires -Version 2


param(
	[Parameter(Mandatory=$true)]
	[ValidateSet("zurlyncee01","urdlyncee02","irolyncee03","haylyncee04","stmlyncee05","blulyncee06","orqlyncee07","snglyncee08","hkglyncee09")]
	[string]$SourcePool,
	[Parameter(Mandatory=$true)]
	[ValidateSet("zurlyncee01","urdlyncee02","irolyncee03","haylyncee04","stmlyncee05","blulyncee06","orqlyncee07","snglyncee08","hkglyncee09")]
	[string]$DestinationPool,
	[Parameter(Mandatory=$False)]
	[bool]$Evacuate=$False,
	[Parameter(Mandatory=$False)]
	[bool]$WhatIf=$True
)

Function Format-WarningMessage {
	param(
		[Parameter(Position=0)]
		[string]$Message,
		[Parameter(Position=1)]
		[string]$ScriptMode,
		[Parameter(Position=2)]
		[string]$PopulationToMove,
		[Parameter(Position=3)]
		[string]$SourcePool,
		[Parameter(Position=4)]
		[string]$DestinationPool
	)
	$Message = $Message.Replace("<#ScriptMode#>",$ScriptMode)
	$Message = $Message.Replace("<#PopulationToMove#>",$PopulationToMove)
	$Message = $Message.Replace("<#SourcePool#>",$SourcePool)
	$Message = $Message.Replace("<#DestPool#>",$DestinationPool)
	return $Message
}

[string]$WarningMessage = "Script is running in <#ScriptMode#> mode. `r`n"
$WarningMessage        += "Moving <#PopulationToMove#> users from <#SourcePool#> to <#DestPool#> `r`n"
$WarningMessage        += "Continue? <y/n>"

# Append .ubs.com to pool names - no need to make people type them everytime
# when they use the script
$SrcPool = $SourcePool + ".ubs.com"
$DstPool = $DestinationPool + ".ubs.com"

# You can't use variables directly in a -filter, you have to 
# construct the filter block in advance like this :
[scriptblock]$UserFilter = [ScriptBlock]::Create("RegistrarPool -eq '$SrcPool'")

Write-Host -ForegroundColor Green "Getting all users on source pool. Please wait."

# Get all the users in the source pool
$SourcePoolUsers = Get-CsUser -Filter $UserFilter

if(-not $Evacuate){
	# If we're rebalancing pools, then we need to 
	# work out how many of the users to move - note use of [int]
	# to make sure we only ever use whole numbers.
	# Note - this script assumes you want to move half of the users
	# on the source pool to the destination. If the pools already have
	# a spread of users on them, this may not be what you want.
	[int]$SourcePoolPopulation = $SourcePoolUsers.count
	[int]$PopulationToMove     = $SourcePoolPopulation / 2
	$WarningMessage = Format-WarningMessage $WarningMessage "REBALANCE" $PopulationToMove $SrcPool $DstPool
} else {
	# If we're simply evactuation the source pool - just move everyone 
	# on it to the new pool
	[int]$PopulationToMove = $SourcePoolUsers.count
	$WarningMessage = Format-WarningMessage $WarningMessage "EVACUATE" $PopulationToMove $SrcPool $DstPool
}

# Get ready to keep track of how many we've done
[int]$Count = 0

# Make sure the user realises he's not change the "WhatIf" mode to true,
# so the script won't actually change anything
if($WhatIf){
	Write-Warning 'Script is executing with "WhatIf" set to $True - this means no users will actually move'
}

# All inputs configured, we're ready to go - warn the user and get confirmation:
Write-Host -ForegroundColor Red $WarningMessage
$ShouldContinue = Read-Host

if($ShouldContinue.ToLower() -eq 'y'){
	Write-Host "Move commencing. Remember to run a count from a seperate powershell window to monitor actual progress"

	$SourcePoolUsers[0..$PopulationToMove] | ForEach-Object {
		$Count++
		Write-Host "Moving User" $Count "of" $PopulationToMove $_.SipAddress
		Move-CsUser $_.SipAddress -Target $DstPool -Confirm:$False -WhatIf:$WhatIf
	}

	Write-Host -ForegroundColor Green "Finished, but run a count on pool users to monitor when the moves actually complete."
}else{
	Write-Host -ForegroundColor Green "Script run cancelled"
}
