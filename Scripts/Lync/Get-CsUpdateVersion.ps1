﻿<#
.SYNOPSIS
  Queries Lync Server 2010/2013 pools and Skype for Business 2015 pools and retrieves what Cumulative Update is installed on each server in the pool.

.DESCRIPTION
  Queries Lync Server 2010/2013 pools and Skype for Business 2015 pools and retrieves what Cumulative Update is installed on each server in the pool.

.NOTES
  Version               : 2.8 - See changelog at http://www.ehloworld.com/2642
  Wish list             : Better error trapping
  Rights Required       : CSAdministrator and local admin rights on the target computers
  Sched Task Required   : No
  Lync Server Version   : 2010 and 2013, and Skype for Business Server 2015
  Author(s)             : Dave Howe
                        : Pat Richard
  Email/Blog/Twitter    : daveh@microsoft.com
                          pat@innervation.com   http://www.ehloworld.com @patrichard
  Dedicated Post        : http://www.ehloworld.com/2641
  Disclaimer            : You running this script means you won't blame me if this breaks your stuff. This script is provided AS IS
                          without warranty of any kind. I disclaim all implied warranties including, without limitation, any implied
                          warranties of merchantability or of fitness for a particular purpose. The entire risk arising out of the use
                          or performance of the sample scripts and documentation remains with you. In no event shall I be liable for
                          any damages whatsoever (including, without limitation, damages for loss of business profits, business
                          interruption, loss of business information, or other pecuniary loss) arising out of the use of or inability
                          to use the script or documentation.
  Acknowledgements      :
  Assumptions           : ExecutionPolicy of AllSigned (recommended), RemoteSigned or Unrestricted (not recommended)
  Limitations           : PSRemoting must be enabled on the servers that are queried. It's enabled by default on Windows Server 2012
                          and later. For information on enabling PSRemoting, see http://technet.microsoft.com/en-us/library/hh849694.aspx
  Known issues          : Edge servers that aren't domain joined will show as "PSRemoting Failure".

.EXAMPLE
  .\Get-CsUpdateVersion.ps1

  Description
  -----------
  Runs the script and queries all pools in the environment.

.EXAMPLE
  .\Get-CsUpdateVersion.ps1 -PoolFqdn pool.contoso.com

  Description
  -----------
  Runs the script and queries just the pool specified.
#>
#Requires -Version 2.0

[CmdletBinding(SupportsShouldProcess=$true)]
param (
  # Specifies a single pool to query
  [parameter(ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)]
  [string] $PoolFqdn,

  # Skips the check for script age
  [parameter(ValueFromPipeline = $false, ValueFromPipelineByPropertyName = $true)]
  [switch] $SkipUpdateCheck
)

#region functions

function Set-ModuleStatus {
  [CmdletBinding(SupportsShouldProcess=$true)]
  param  (
    [parameter(ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true, Mandatory=$true, HelpMessage = "No module name specified!")]
    [ValidateNotNullOrEmpty()]
    [string] $name
  )
  PROCESS{
    # Executes once for each pipeline object
    # the $_ variable represents the current input object
    if (!(Get-Module -name "$name")) {
      if (Get-Module -ListAvailable | Where-Object Name -eq "$name") {
        Import-Module -Name "$name"
        # module was imported
        # return $true
      } else {
        # module was not available
        # return $false
      }
    } else {
      # Write-Output "$_ module already imported"
      # return $true
    }
  } # end PROCESS
} # end function Set-ModuleStatus

function Test-ScriptUpdates {
<#
.SYNOPSIS
  Checks the CreationTime parameter on the script itself. If it's over 30 days, it will prompt the user & optionally take them to the changelog for that script.

.DESCRIPTION
  Checks the CreationTime parameter on the script itself. If it's over 30 days, it will prompt the user & optionally take them to the changelog for that script.

.NOTES
  Version               : 1.0 - See changelog at
  Wish list             : Better error trapping
  Rights Required       : Local administrator on server
                        : If script is not signed, ExecutionPolicy of RemoteSigned (recommended)
                          or Unrestricted (not recommended)
                        : If script is signed, ExecutionPolicy of AllSigned (recommended, RemoteSigned,
                          or Unrestricted (not recommended)
  Sched Task Required   : No
  Lync Server Version   : N/A
  Exchange Version      : N/A
  Author/Copyright      : © Pat Richard, Skype for Business MVP - All Rights Reserved
  Email/Blog/Twitter    : pat@innervation.com   http://www.ehloworld.com @patrichard
  Dedicated Blog Post   :
  Disclaimer            : You running this script means you won't blame me if this breaks your stuff. This script is
                          provided AS IS without warranty of any kind. I disclaim all implied warranties including,
                          without limitation, any implied warranties of merchantability or of fitness for a particular
                          purpose. The entire risk arising out of the use or performance of the sample scripts and
                          documentation remains with you. In no event shall I be liable for any damages whatsoever
                          (including, without limitation, damages for loss of business profits, business interruption,
                          loss of business information, or other pecuniary loss) arising out of the use of or inability
                          to use the script or documentation.
  Assumptions           : ExecutionPolicy of AllSigned (recommended), RemoteSigned or Unrestricted (not recommended)
  Limitations           :
  Known issues          : None yet, but I'm sure you'll find some!
  Acknowledgements      :

.LINK


.EXAMPLE
  PS C:\> Get-ScriptUpdates -ChangeLogURL 1234 -Age 45

  Description
  -----------
  Executes the function, specifying the article number the user will be taken to if they choose yes to go online, as well as number of days that must have elapsed.

.INPUTS
  None. You cannot pipe objects to this function.

.OUTPUTS
  Text output
#>

  [CmdletBinding(SupportsShouldProcess=$true)]
  param (
    # Specifies the article number the user will be taken to if they choose yes to go online.
    [Parameter(Position = 0, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true)]
    [ValidateNotNullOrEmpty()]
    [int] $ChangelogUrl,

    # Specifies the number of days that must have elapsed before the prompt is displayed.
    [Parameter(Position = 1, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true)]
    [ValidateNotNullOrEmpty()]
    [int] $Age = 90,

    [Parameter(Position = 2, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true)]
    [ValidateNotNullOrEmpty()]
    [string] $FilePath = $MyInvocation.ScriptName
  )

  BEGIN{
    # Executes once before first item in pipeline is processed
    # the $_ variable represents the current input object
    [string]$Check4UpdatesPrompt = @"
┌──────────────────────────────────────────────────────────┐
│                Check For Updates?                        │
│            ==========================                    │
│    This script is more than $Age days old. Would you       │
│    like to check the script's website for a              │
│    newer version? It's imperative that you have the      │
│    latest build so all version number are accounted for  │
└──────────────────────────────────────────────────────────┘
"@
  } # end BEGIN

  PROCESS{
    # Executes once for each pipeline object
    # the $_ variable represents the current input object
    # echo $_
    if (((Get-Date) - (Get-Item $filepath).CreationTime).TotalDays -gt $age){
      Write-Host $Check4UpdatesPrompt -ForegroundColor Green
      if ((Read-host "Go online? [y/n]") -imatch "y"){
        Start-Process "http://www.ehloworld.com/$ChangelogUrl"
      }
    }
  } # end PROCESS

  END{
    # Executes once after last pipeline object is processed
    # the $_ variable represents the current input object
  } # end END
} # end function Test-ScriptUpdates

function Test-IsSigned {
<#
.SYNOPSIS

.DESCRIPTION

.NOTES
  Version               : 1.0 - See changelog at
  Wish list             : Better error trapping
  Rights Required       : Local administrator on server
  Sched Task Required   : No
  Lync Server Version   : N/A
  Author/Copyright      : © Pat Richard, Skype for Business MVP - All Rights Reserved
  Email/Blog/Twitter    : pat@innervation.com   http://www.ehloworld.com @patrichard
  Dedicated Blog Post   : http://www.ehloworld.com/1697
  Disclaimer            : You running this script means you won't blame me if this breaks your stuff. This script is
                          provided AS IS without warranty of any kind. I disclaim all implied warranties including,
                          without limitation, any implied warranties of merchantability or of fitness for a particular
                          purpose. The entire risk arising out of the use or performance of the sample scripts and
                          documentation remains with you. In no event shall I be liable for any damages whatsoever
                          (including, without limitation, damages for loss of business profits, business interruption,
                          loss of business information, or other pecuniary loss) arising out of the use of or inability
                          to use the script or documentation.
  Assumptions           : ExecutionPolicy of AllSigned (recommended), RemoteSigned or Unrestricted (not recommended)
  Limitations           :
  Known issues          : None yet, but I'm sure you'll find some!
  Acknowledgements      :

.LINK


.EXAMPLE
  PS C:\>

  Description
  -----------


.INPUTS
  None. You cannot pipe objects to this function.

.OUTPUTS
  Boolean output
#>
  [CmdletBinding(SupportsShouldProcess = $true)]
  param (
    [Parameter(ValueFromPipeline = $False, ValueFromPipelineByPropertyName = $true)]
    [ValidateNotNullOrEmpty()]
    [string] $FilePath = $MyInvocation.ScriptName
  )

  BEGIN{
    # Executes once before first item in pipeline is processed
    # the $_ variable represents the current input object
  } # end BEGIN

  PROCESS{
    # Executes once for each pipeline object
    # the $_ variable represents the current input object
    if ((Get-AuthenticodeSignature -FilePath $FilePath).Status -eq "Valid"){
      return $true
    }
  } # end PROCESS

  END{
    # Executes once after last pipeline object is processed
    # the $_ variable represents the current input object
  } # end END
} # end function Test-IsSigned

#endregion functions

if ((-not(Test-IsSigned)) -and (-not $SkupUpdateCheck)){
  Test-ScriptUpdates -ChangelogUrl 2642 -Age 90
}

Set-ModuleStatus Lync
if ($PoolFqdn){
  $services = @(Get-CsService | Where-Object {$_.version -ge 5 -and $_.PoolFqdn -eq $PoolFqdn})
}else{
  $services = @(Get-CsService | Where-Object {$_.version -ge 5})
}
$AllowedServices = # @("ApplicationServer","ArchivingServer","BackupServer","CentralManagement","ConferencingServer","Director","EdgeServer","MediationServer","MonitoringServer","PersistentChatServer","Registrar","UserServer","WacServer","WebServer")

@("ApplicationServer","ArchivingServer","BackupServer","CentralManagement","ConferencingServer","Director","EdgeServer","MediationServer","MonitoringServer","PersistentChatServer","Registrar","UserServer","WebServer")

[string] $ComputerFqdn = [System.Net.Dns]::GetHostByName(($env:ComputerName)).HostName

foreach ($service in $services)  {
  if ($AllowedServices -icontains $service.role) {
    $pools += @($service.PoolFqdn)
  }
}

$pools = $pools | Select-Object -Unique | Sort-Object
Write-Output "`n"
foreach ($Fqdn in $pools)  {
  $computers = @((Get-CsPool $Fqdn).computers | Sort-Object)

  if ($computers) {
    Write-Host "Pool: $Fqdn `($($computers.count) servers`)`n" -ForegroundColor white

    foreach ($computer in $computers) {
      if(!(Test-Connection -ComputerName $computer -BufferSize 16 -Count 1 -ErrorAction SilentlyContinue -Quiet)) {
        Write-Host "      $computer (" -ForegroundColor white -NoNewline
        Write-Host "offline" -ForegroundColor red -NoNewline
        Write-Host ")" -ForegroundColor white
      } else {
        if ($computer -ne $ComputerFqdn){ # computer is a remote machine
          $version = Invoke-Command -ScriptBlock {
            $filter = @("Debugging Tools","Resource Kit Tools","Best Practices Analyzer","Meeting Room Portal")
            [regex]$regex = '(' + (($filter | foreach {[regex]::escape($_)}) –join "|") + ')'
          @((Get-ChildItem "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall" | ForEach-Object {Get-ItemProperty $_.pspath} | Where-Object {($_.DisplayName -imatch "Microsoft Lync Server" -and $_.DisplayName -inotmatch $regex) -or ($_.DisplayName -imatch "Microsoft Office Web Apps Server 2013") -or ($_.DisplayName -imatch "Skype for Business Server" -and $_.DisplayName -inotmatch $regex)}) | Sort-Object DisplayVersion -Descending | Select-Object DisplayVersion,DisplayName -First 1)} -ComputerName $computer -ErrorAction SilentlyContinue -WarningAction SilentlyContinue
          $version = $version.DisplayVersion
        }else{ # computer is a local machine (which sometimes has issues with PSRemoting to/from itself)
          # New regex query to filter out non critical apps
          $filter = @("Debugging Tools","Resource Kit Tools","Best Practices Analyzer","Meeting Room Portal")
          [regex]$regex = '(' + (($filter |foreach {[regex]::escape($_)}) –join "|") + ')'
          $version = (Get-ChildItem "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall" | ForEach-Object {Get-ItemProperty $_.pspath} | Where-Object {($_.DisplayName -imatch "Microsoft Lync Server" -and $_.DisplayName -inotmatch $regex) -or ($_.DisplayName -imatch "Microsoft Office Web Apps Server 2013") -or ($_.DisplayName -imatch "Skype for Business Server" -and $_.DisplayName -inotmatch $regex)}) | Sort-Object DisplayVersion -Descending | Select-Object DisplayVersion,DisplayName -First 1
          $version = $version.DisplayVersion
        }
        Write-Host "      $($computer.split('.')[0]) (" -ForegroundColor white -NoNewline
        if (!$version) {
          Write-Host "PSRemoting failure" -ForegroundColor red -NoNewline
          Write-Host ")" -ForegroundColor white
        } else {
          # Set a default value in case it doesn't match anything below
          $FriendlyResult="Unknown Version"

          [bool] $NotUpToDate = $false

          switch ([string] $version) {
            # Lync Server 2010 updates - https://support.microsoft.com/en-us/kb/2493736
            "4.0.7577.0" {$FriendlyResult = "Lync Server 2010 RTM - 10/2010";$NotUpToDate = $true}
            "4.0.7577.108" {$FriendlyResult = "Lync Server 2010 CU1 - 01/2011";$NotUpToDate = $true}
            "4.0.7577.118" {$FriendlyResult = "Lync Server 2010 post-CU1 - 02/2011";$NotUpToDate = $true}
            "4.0.7577.137" {$FriendlyResult = "Lync Server 2010 CU2 - 04/2011";$NotUpToDate = $true}
            "4.0.7577.166" {$FriendlyResult = "Lync Server 2010 CU3 - 07/2011";$NotUpToDate = $true}
            "4.0.7577.183" {$FriendlyResult = "Lync Server 2010 CU4 - 11/2011";$NotUpToDate = $true}
            "4.0.7577.184" {$FriendlyResult = "Lync Server 2010 post-CU4 - 12/2011";$NotUpToDate = $true}
            "4.0.7577.190" {$FriendlyResult = "Lync Server 2010 CU5 - 02/2012";$NotUpToDate = $true}
            "4.0.7577.197" {$FriendlyResult = "Lync Server 2010 post-CU5 - 03/2012";$NotUpToDate = $true}
            "4.0.7577.199" {$FriendlyResult = "Lync Server 2010 CU6 - 06/2012";$NotUpToDate = $true}
            "4.0.7577.203" {$FriendlyResult = "Lync Server 2010 CU7 - 10/2012";$NotUpToDate = $true}
            "4.0.7577.205" {$FriendlyResult = "Lync Server 2010 post-CU7 - 10/2012";$NotUpToDate = $true}
            "4.0.7577.216" {$FriendlyResult = "Lync Server 2010 CU8 - 03/2013";$NotUpToDate = $true}
            "4.0.7577.217" {$FriendlyResult = "Lync Server 2010 CU9 - 07/2013";$NotUpToDate = $true}
            "4.0.7577.223" {$FriendlyResult = "Lync Server 2010 CU10 - 10/2013";$NotUpToDate = $true}
            "4.0.7577.225" {$FriendlyResult = "Lync Server 2010 CU11 - 01/2014";$NotUpToDate = $true}
            "4.0.7577.230" {$FriendlyResult = "Lync Server 2010 CU12 - 04/2014";$NotUpToDate = $true}
            "4.0.7577.707" {$FriendlyResult = "Lync Server 2010 CU13 - 09/2014";$NotUpToDate = $true}
            # "4.0.7577.708" {$FriendlyResult = "Lync Server 2010 CU14 - 12/2014";$NotUpToDate = $true}
            "4.0.7577.709" {$FriendlyResult = "Lync Server 2010 CU14 - 12/2014";$NotUpToDate = $true}
            "4.0.7577.710" {$FriendlyResult = "Lync Server 2010 CU15 - 12/2014";$NotUpToDate = $true}
            "4.0.7577.713" {$FriendlyResult = "Lync Server 2010 CU16 - 05/2015"}

            # Lync Server 2013 updates - https://www.microsoft.com/en-us/download/details.aspx?id=36820 (KB 2809243)
            "5.0.8308.0" {$FriendlyResult = "Lync Server 2013 RTM";$NotUpToDate = $true}
            "5.0.8308.291" {$FriendlyResult = "Lync Server 2013 CU1 - 02/2013";$NotUpToDate = $true}
            "5.0.8308.420" {$FriendlyResult = "Lync Server 2013 CU2 - 07/2013";$NotUpToDate = $true}
            "5.0.8308.556" {$FriendlyResult = "Lync Server 2013 CU3 - 10/2013";$NotUpToDate = $true}
            "5.0.8308.577" {$FriendlyResult = "Lync Server 2013 CU4 - 01/2014";$NotUpToDate = $true}
            "5.0.8308.603" {$FriendlyResult = "Lync Server 2013 Post CU4";$NotUpToDate = $true}
            "5.0.8308.738" {$FriendlyResult = "Lync Server 2013 CU5 - 08/2014";$NotUpToDate = $true}
            "5.0.8308.803" {$FriendlyResult = "Lync Server 2013 Post CU5";$NotUpToDate = $true}
            "5.0.8308.815" {$FriendlyResult = "Lync Server 2013 CU6 - 09/2014";$NotUpToDate = $true}
            "5.0.8308.831" {$FriendlyResult = "Lync Server 2013 CU7 - 10/2014";$NotUpToDate = $true}
            "5.0.8308.834" {$FriendlyResult = "Lync Server 2013 CU8 - 11/2014";$NotUpToDate = $true}
            "5.0.8308.857" {$FriendlyResult = "Lync Server 2013 CU9 - 12/2014";$NotUpToDate = $true}
            "5.0.8308.866" {$FriendlyResult = "Lync Server 2013 CU10 - 12/31/2014";$NotUpToDate = $true}
            "5.0.8308.871" {$FriendlyResult = "Lync Server 2013 CU11 - 02/19/2015";$NotUpToDate = $true}
            "5.0.8308.872" {$FriendlyResult = "Lync Server 2013 CU11 - 02/19/2015 - updated";$NotUpToDate = $true}
            "5.0.8308.887" {$FriendlyResult = "Lync Server 2013 CU12 - 05/1/2015";$NotUpToDate = $true}
            "5.0.8308.920" {$FriendlyResult = "Lync Server 2013 CU13 - 07/10/2015";$NotUpToDate = $true}
            "5.0.8308.927" {$FriendlyResult = "Lync Server 2013 CU14 - 09/07/2015";$NotUpToDate = $true}
            "5.0.8308.933" {$FriendlyResult = "Lync Server 2013 CU15 - 10/01/2015";$NotUpToDate = $true}
            "5.0.8308.941" {$FriendlyResult = "Lync Server 2013 CU16 - 12/14/2015";$NotUpToDate = $true}
            "5.0.8308.945" {$FriendlyResult = "Lync Server 2013 CU17 - 01/03/2016"}


            # Skype for Business Server 2015 updates - https://www.microsoft.com/en-us/download/details.aspx?id=47690 (KB 3061064)
            "6.0.9319.0" {$FriendlyResult = "Skype for Business Server 2015 RTM";$NotUpToDate = $true}
            "6.0.9319.42" {$FriendlyResult = "Skype for Business Server 2015 - 05/15/2015 [TAP]";$NotUpToDate = $true}
            "6.0.9319.45" {$FriendlyResult = "Skype for Business Server 2015 - 06/19/2015 [TAP]";$NotUpToDate = $true}
            "6.0.9319.48" {$FriendlyResult = "Skype for Business Server 2015 - 05/24/2015 [TAP]";$NotUpToDate = $true}
            "6.0.9319.55" {$FriendlyResult = "Skype for Business Server 2015 - 06/16/2015";$NotUpToDate = $true}
            "6.0.9319.72" {$FriendlyResult = "Skype for Business Server 2015 - 09/04/2015";$NotUpToDate = $true}
            "6.0.9319.88" {$FriendlyResult = "Skype for Business Server 2015 - 09/26/2015";$NotUpToDate = $true}
            "6.0.9319.102" {$FriendlyResult = "Skype for Business Server 2015 - 11/17/2015"}
          }
          Write-Host $version -ForegroundColor yellow -NoNewline
          if ($FriendlyResult){
            Write-Host " [" -ForegroundColor white -NoNewline
            if (-not $NotUpToDate -and ($FriendlyResult -ne "Unknown Version")){
              Write-Host "$FriendlyResult" -ForegroundColor green -NoNewline
            }else{
              Write-Host "$FriendlyResult" -ForegroundColor red -NoNewline
            }
            Write-Host "]" -ForegroundColor white -NoNewline
          }
          Write-Host ")" -ForegroundColor white
        }
      }
    }
  }else{
    Write-Output "No computers found"
  }
  Write-Host
}

# SIG # Begin signature block
# MIINGgYJKoZIhvcNAQcCoIINCzCCDQcCAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQULlnbNb46o+9ClR7fb1Slj8Rm
# aw2gggpcMIIFJDCCBAygAwIBAgIQCW4jPbUU7ejqppBXGijcKjANBgkqhkiG9w0B
# AQsFADByMQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYD
# VQQLExB3d3cuZGlnaWNlcnQuY29tMTEwLwYDVQQDEyhEaWdpQ2VydCBTSEEyIEFz
# c3VyZWQgSUQgQ29kZSBTaWduaW5nIENBMB4XDTE1MDkwOTAwMDAwMFoXDTE2MTEx
# NjEyMDAwMFowazELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAk1JMRkwFwYDVQQHExBT
# dGVybGluZyBIZWlnaHRzMRkwFwYDVQQKExBJbm5lcnZhdGlvbiwgTExDMRkwFwYD
# VQQDExBJbm5lcnZhdGlvbiwgTExDMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIB
# CgKCAQEAvqXtZp4VZgIWqr6ju9BVcQ6MP7D5WyBmKSypjLNJ5PhZBjL0YiusEGHg
# 8auFojul0DSJngc9SM368BQib+SVHtlCTOP6M6j9Q5N7t3GYsROj3sc16/yRRM1i
# g3vmhcHzNWVJ6X68D3DeQl4tv3MpantuRxiktTmTBSlAtF/8YGxKJv0cNNq4JC3c
# k6MqY/5Q1dYdH9CrD/P40gaOnZazFxNYjhb5rK5caMxL4djzbwfZGSvTYedGX60g
# 3bq99B6jqRgvYTU7C4lI3gWtmh15sSw1Tdf0RKKpqwPwOy5gCWTcqKlTTJVqoQR8
# /haMjfUItRTi+CIwEnE3/i6R36C95wIDAQABo4IBuzCCAbcwHwYDVR0jBBgwFoAU
# WsS5eyoKo6XqcQPAYPkt9mV1DlgwHQYDVR0OBBYEFBpQMLQFvMYa0UjlMqp9Etla
# c9zYMA4GA1UdDwEB/wQEAwIHgDATBgNVHSUEDDAKBggrBgEFBQcDAzB3BgNVHR8E
# cDBuMDWgM6Axhi9odHRwOi8vY3JsMy5kaWdpY2VydC5jb20vc2hhMi1hc3N1cmVk
# LWNzLWcxLmNybDA1oDOgMYYvaHR0cDovL2NybDQuZGlnaWNlcnQuY29tL3NoYTIt
# YXNzdXJlZC1jcy1nMS5jcmwwQgYDVR0gBDswOTA3BglghkgBhv1sAwEwKjAoBggr
# BgEFBQcCARYcaHR0cHM6Ly93d3cuZGlnaWNlcnQuY29tL0NQUzCBhAYIKwYBBQUH
# AQEEeDB2MCQGCCsGAQUFBzABhhhodHRwOi8vb2NzcC5kaWdpY2VydC5jb20wTgYI
# KwYBBQUHMAKGQmh0dHA6Ly9jYWNlcnRzLmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydFNI
# QTJBc3N1cmVkSURDb2RlU2lnbmluZ0NBLmNydDAMBgNVHRMBAf8EAjAAMA0GCSqG
# SIb3DQEBCwUAA4IBAQAaw825iLtpqFCRUCJqAdyKAYB568K8HhviRfAcmj8aUcFD
# 012Co2BQbLgjtK2M+S1bByF4q7jX+4NIOT7hufi96udDXRPfAaCzYoNOqW9ihnwr
# iAfYDC2Z2z9LWrrKQx6peZQYV4U4Mf5QpiVX+rfHWAZLPy5QYNrkoNJ5xxlA3K8N
# yL1VtfUUrWQnVkhfnt0uE9xNaCZOUEUHGmIc8yZeD6IohX8+OEOe7c+lEyz87M2D
# RK9dzIYzOKPBXOSch29ijLoG2zoGR5fkrEVvLvFYzykQp00TTjdCyx0QP7Jkm42v
# mOXck7Zzcr9rKPyBETvJ4Ix7YmGJhHTQdLLcxMI8MIIFMDCCBBigAwIBAgIQBAkY
# G1/Vu2Z1U0O1b5VQCDANBgkqhkiG9w0BAQsFADBlMQswCQYDVQQGEwJVUzEVMBMG
# A1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3d3cuZGlnaWNlcnQuY29tMSQw
# IgYDVQQDExtEaWdpQ2VydCBBc3N1cmVkIElEIFJvb3QgQ0EwHhcNMTMxMDIyMTIw
# MDAwWhcNMjgxMDIyMTIwMDAwWjByMQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGln
# aUNlcnQgSW5jMRkwFwYDVQQLExB3d3cuZGlnaWNlcnQuY29tMTEwLwYDVQQDEyhE
# aWdpQ2VydCBTSEEyIEFzc3VyZWQgSUQgQ29kZSBTaWduaW5nIENBMIIBIjANBgkq
# hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA+NOzHH8OEa9ndwfTCzFJGc/Q+0WZsTrb
# RPV/5aid2zLXcep2nQUut4/6kkPApfmJ1DcZ17aq8JyGpdglrA55KDp+6dFn08b7
# KSfH03sjlOSRI5aQd4L5oYQjZhJUM1B0sSgmuyRpwsJS8hRniolF1C2ho+mILCCV
# rhxKhwjfDPXiTWAYvqrEsq5wMWYzcT6scKKrzn/pfMuSoeU7MRzP6vIK5Fe7SrXp
# dOYr/mzLfnQ5Ng2Q7+S1TqSp6moKq4TzrGdOtcT3jNEgJSPrCGQ+UpbB8g8S9MWO
# D8Gi6CxR93O8vYWxYoNzQYIH5DiLanMg0A9kczyen6Yzqf0Z3yWT0QIDAQABo4IB
# zTCCAckwEgYDVR0TAQH/BAgwBgEB/wIBADAOBgNVHQ8BAf8EBAMCAYYwEwYDVR0l
# BAwwCgYIKwYBBQUHAwMweQYIKwYBBQUHAQEEbTBrMCQGCCsGAQUFBzABhhhodHRw
# Oi8vb2NzcC5kaWdpY2VydC5jb20wQwYIKwYBBQUHMAKGN2h0dHA6Ly9jYWNlcnRz
# LmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydEFzc3VyZWRJRFJvb3RDQS5jcnQwgYEGA1Ud
# HwR6MHgwOqA4oDaGNGh0dHA6Ly9jcmw0LmRpZ2ljZXJ0LmNvbS9EaWdpQ2VydEFz
# c3VyZWRJRFJvb3RDQS5jcmwwOqA4oDaGNGh0dHA6Ly9jcmwzLmRpZ2ljZXJ0LmNv
# bS9EaWdpQ2VydEFzc3VyZWRJRFJvb3RDQS5jcmwwTwYDVR0gBEgwRjA4BgpghkgB
# hv1sAAIEMCowKAYIKwYBBQUHAgEWHGh0dHBzOi8vd3d3LmRpZ2ljZXJ0LmNvbS9D
# UFMwCgYIYIZIAYb9bAMwHQYDVR0OBBYEFFrEuXsqCqOl6nEDwGD5LfZldQ5YMB8G
# A1UdIwQYMBaAFEXroq/0ksuCMS1Ri6enIZ3zbcgPMA0GCSqGSIb3DQEBCwUAA4IB
# AQA+7A1aJLPzItEVyCx8JSl2qB1dHC06GsTvMGHXfgtg/cM9D8Svi/3vKt8gVTew
# 4fbRknUPUbRupY5a4l4kgU4QpO4/cY5jDhNLrddfRHnzNhQGivecRk5c/5CxGwcO
# kRX7uq+1UcKNJK4kxscnKqEpKBo6cSgCPC6Ro8AlEeKcFEehemhor5unXCBc2XGx
# DI+7qPjFEmifz0DLQESlE/DmZAwlCEIysjaKJAL+L3J+HNdJRZboWR3p+nRka7Lr
# ZkPas7CM1ekN3fYBIM6ZMWM9CBoYs4GbT8aTEAb8B4H6i9r5gkn3Ym6hU/oSlBiF
# LpKR6mhsRDKyZqHnGKSaZFHvMYICKDCCAiQCAQEwgYYwcjELMAkGA1UEBhMCVVMx
# FTATBgNVBAoTDERpZ2lDZXJ0IEluYzEZMBcGA1UECxMQd3d3LmRpZ2ljZXJ0LmNv
# bTExMC8GA1UEAxMoRGlnaUNlcnQgU0hBMiBBc3N1cmVkIElEIENvZGUgU2lnbmlu
# ZyBDQQIQCW4jPbUU7ejqppBXGijcKjAJBgUrDgMCGgUAoHgwGAYKKwYBBAGCNwIB
# DDEKMAigAoAAoQKAADAZBgkqhkiG9w0BCQMxDAYKKwYBBAGCNwIBBDAcBgorBgEE
# AYI3AgELMQ4wDAYKKwYBBAGCNwIBFTAjBgkqhkiG9w0BCQQxFgQUysk+jJia1uRa
# LyRawlslRLzqbwUwDQYJKoZIhvcNAQEBBQAEggEAWCK2Gu4ROYm8kMf/B/C/3/zC
# koxRBIe2mzsfPTmKNZd76Jd2pU/g/hpIqCJMPK8G+XqtPdWlFtgJN5J9nHgoxpFb
# ybfQP/Uv+LR1+DMlErviv6jcYYbpW9zgwRsuD2jiLIhXA8KZo7glvVin89YAgE3h
# vq4UgvWKbu5gSmjugp4dPOLOJHayYNIlYJ7zzbqZP/kAoJclYV93U5TAinbHUr0s
# x11lSQy8tctljIUiYLHg2h1JA2/l7H6/9cbcLXl2LNnbMRi76vkJDHQ6yZqi43BU
# +xtZtnMISfLPVukxgSeNurUiOhn2kEATyAzAh5JHiT5KSZlLebekoHET5QglqQ==
# SIG # End signature block
