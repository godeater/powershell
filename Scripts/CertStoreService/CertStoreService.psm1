﻿<#
Stuff
#>
#requires -Version 6

$Script:EncryptedRoleId   = "640036003600350066006300630066002d0034006300390066002d0033003000360064002d0065006600380063002d00640032003800380064003300370038006200310030006600"
$Script:EncryptedSecretId = "310032003400350036003800650031002d0033006400610036002d0037003200630039002d0063003600390033002d00370032003600660065006600370038003900660037003000"

$Script:OnAtlasEncryptedRoleId  = "01000000d08c9ddf0115d1118c7a00c04fc297eb01000000df5c47b97ca0b14fad0349d34a2cc0c70000000002000000000010660000000100002000000090728ef0e598f333259504744802988f841d801b5d01498b5a5a7a5937771e7c000000000e80000000020000200000005561e14b05e15569ed89ed2ca152f52e10008eea4731c8680ff7eff188a4172e500000004c29eab0b733aa9324357cdaecf86f4c734f25828e122fe5330d65b399eea1e88d8e64055a26519742d1e6c54c343ab21681fbb83337f0cf29a0ef61a24315a270e61113f1d53710326c85929d342bce40000000f8f7294725ae75d993162740216ae0015cc67236a33d17a3b686c5fc6c80a3764074a3f28e459bc436896329d4c868a898a46a28388026ce8751e60d11312e1b"
$Script:OnAtlasEncryptedSecretId = "01000000d08c9ddf0115d1118c7a00c04fc297eb01000000df5c47b97ca0b14fad0349d34a2cc0c7000000000200000000001066000000010000200000001b82441e26bf8657386e29ae9bb59f892a38e7215aa4c6c2ca7afa2cf6eda221000000000e8000000002000020000000395c748b13e4aef2f96b39742f27dec1a6e2638bee60e0f757d087b4f4beb13a50000000081db1b500045d1c6a93ab7f1facc05b94e8a320513e28e1185e2d4d6e157fb57a38fe0c6d16302141b21863321b6ef900d69506fb62feeb83cbe1945b4d3938c5241720a161bc8c0115e81b8420707e40000000e1b31178f09bfcddc69eb0216f798adea899f59ada05ee027c82d2f524039253d07b31ada7a59231f9c37d09463cc877bce21fd8f5d50e912ddf4f786ca936f1"

$Script:VaultAddress      = "http://localhost:8200"
$Script:VaultToken        = ""

Function DecryptString {
    param(
        [Parameter(Mandatory,ValueFromPipeline)]
        [String]$EncryptedValue
    )

    $TemporarySecureString = ConvertTo-SecureString $EncryptedValue
    $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($TemporarySecureString)
    $DecryptedPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)

    $DecryptedPassword
}

Function AuthToVault {
    param()

    $Method = "POST"
    $Path   = "$Script:VaultAddress/v1/auth/approle/login"

    $Payload = New-Object -Type PSCustomObject -Property @{
        role_id = DecryptString -EncryptedValue $Script:EncryptedRoleId
        secret_id = DecryptString -EncryptedValue $Script:EncryptedSecretId
    } | ConvertTo-Json -Compress

    try {
        $AuthResponse = Invoke-RestMethod -Method $Method -Uri $Path -Body $Payload
        $Script:VaultToken = $AuthResponse.auth.client_token
        $AuthResponse
    } catch {
        $_
        $_.Exception.Message
        $Payload
    }
}

Function StoreToVault {
    param(
	[Parameter(Mandatory)]
	[String]$Key,
	[Parameter(Mandatory)]
	[String]$Value
    )

    $Method = "POST"
    $Path   = "$Script:VaultAddress/v1/kv/data/$Key"

    $Payload = New-Object -Type PSCustomObject -Property @{
    }
}
