path "kv/data/*" {
  capabilities = [ "create", "read", "update" ]
}

path "kv/delete/*" {
  capabilities = [ "update" ]
}

path "kv/undelete/*" {
  capabilities = [ "update" ]
}

path "kv/destroy/*" {
  capabilities = [ "update" ]
}

path "kv/metadata/*" {
  capabilities = [ "delete", "list", "read" ]
}
