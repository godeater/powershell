﻿<#
.SYNOPSIS
Get all Tooling users
.DESCRIPTION
Enumerate all Tooling user groups across EUROPA and FM
Gets all users who are a member of at least one of them
Produce report
#>
#requires -version 3.0
#requires -modules ActiveDirectory,AdsiPS,ImportExcel
 
Set-Variable EUROPA -Option Constant -Value "europa.rbsgrp.net"
Set-Variable FM -Option Constant -Value "fm.rbsgrp.net"
 
Set-Variable E_PFX -Option Constant -Value "rApp-EDT-"
Set-Variable F_PFX -Option Constant -Value "G USR - ROL EIEDT "
 
Set-Variable E_LDF -Option Constant -Value "$E_PFX*"
Set-Variable F_LDF -Option Constant -Value "$F_PFX*"
 
$Europa_Tooling_Groups = Get-ADGroup -Server $EUROPA -LdapFilter "(name=$E_LDF)" | Select-Object DistinguishedName
$FM_Tooling_Groups     = Get-ADSIGroup -DomainName $FM -Name "$F_LDF" | Select-Object DistinguishedName
 
# Now we have the full lists of groups, try to build into an enormous LDAP filter ☺
 
$E_LDAP_START="(&(objectClass=user)(objectCategory=user)(|"
$E_LDAP_MIDDLE=""
$E_LDAP_END="))"
 
$Europa_Tooling_Groups | ForEach-Object  {
    $CurrentGroup = $_.DistinguishedName
 
    $LDAP_FRAGMENT = "(memberOf=$CurrentGroup)"
 
    $E_LDAP_MIDDLE += $LDAP_FRAGMENT
}
$E_LDAP_FILTER = $E_LDAP_START+$E_LDAP_MIDDLE+$E_LDAP_END
 
 
$F_LDAP_START=$E_LDAP_START
$F_LDAP_MIDDLE=""
$F_LDAP_END=$E_LDAP_END
 
$FM_Tooling_Groups | ForEach-Object {
    $CurrentGroup = $_.DistinguishedName
 
    $LDAP_FRAGMENT = "(memberOf=$CurrentGroup)"
 
    $F_LDAP_MIDDLE += $LDAP_FRAGMENT
}
$F_LDAP_FILTER = $F_LDAP_START+$F_LDAP_MIDDLE+$F_LDAP_END
 
$Europa_Tooling_Users = Get-AdUser -Server $EUROPA -LDAPFilter $E_LDAP_FILTER
$FM_Tooling_Users     = Get-ADSIUser -LDAPFilter $F_LDAP_FILTER
 
#$Europa_Tooling_Users | Export-Csv -NoTypeInformation C:\Temp\EuropaToolingUsers.csv
#$FM_Tooling_Users | Export-Csv C:\Temp\FMToolingUsers.csv
 
$ToolingUsers = @{
    Europa = $Europa_Tooling_Users
    FM = $FM_Tooling_Users
}
 
$Europa_Tooling_Users  | Export-Excel -Path C:\Temp\ToolingUsers.xlsx -AutoFilter -AutoSize -ClearSheet -WorkSheetName EuropaUsers
$Europa_Tooling_Groups | Export-Excel -Path C:\Temp\ToolingUsers.xlsx -AutoFilter -AutoSize -ClearSheet -WorkSheetName EuropaGroups
$FM_Tooling_Users      | Export-Excel -Path C:\Temp\ToolingUsers.xlsx -AutoFilter -AutoSize -ClearSheet -WorkSheetName FMUsers
$FM_Tooling_Groups     | Export-Excel -Path C:\Temp\ToolingUsers.xlsx -AutoFilter -AutoSize -ClearSheet -WorkSheetName FMGroups
