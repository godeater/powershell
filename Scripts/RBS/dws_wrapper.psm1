<#
.SYNOPSIS
    Provides helper functions for working with the dws command.
.DESCRIPTION
    The dws command line client for interacting with the various DWS
    server offerings currently returns raw JSON objects as output for
    a great deal of the functionality it provides. This script
    attempts to turn that JSON into usable objects which can be
    further interacted with more easily when using the tool
    interactively.
.EXAMPLE

.NOTES

#>
#requires -version 5
$ErrorActionPreference="Stop"
New-Alias -Name "dws" -Value "dummy" -Scope Script

class Stack{
    # Properties
    hidden [int32] $StackPointer
    hidden [Collections.ArrayList] $Stack

    # Constructor
    stack() {
        $this.StackPointer = 0
        $this.Stack = @()
    }

    # Methods
    [int32] Count() {
        return $this.StackPointer
    }

    [String] Peek() {
        if ($this.StackPointer -gt 0) {
            return $this.Stack[$this.StackPointer - 1]
        } else {
            return $null
        }
    }

    [void] Push([String] $Command, [String] $Result) {
        $Item = @{Command=$Command;Result=$Result}
        $this.Stack.Add($Item)
        $this.StackPointer++
    }

    [HashTable] Pop() {
        if($this.StackPointer -gt 0){
            $popValue = $this.Stack[$this.StackPointer -1]
            $this.Stack.RemoveAt($this.StackPointer -1)
            $this.StackPointer--
            return $popValue
        } else {
            return $null
        }
    }

    [String] Item([int32] $Index){
        if($this.StackPointer -gt 0){
            $RealIndex = $Index - 1
            $ItemValue = $this.Stack[$this.StackPointer - $RealIndex]
            return $ItemValue
        } else {
            return $null
        }
    }

    [Collections.ArrayList] CommandStack() {
        return $this.Stack
    }
}

Function Find-DwsExe {
    $FolderWithDws = ""
    $Found = $false
    $PathFolders = $Env:PATH -split ';'
    ForEach($Folder in $PathFolders){
        $FolderWithDws = Join-Path -Path $Folder -ChildPath "dws.exe"
        $Found = Test-Path -Path $FolderWithDws -PathType Leaf
        if ($Found) {
            break
        }
    }
    $FolderWithDws
}

Function Invoke-DwsCommand {
    $DwsOutput = & $Script:DwsExe $args
    $JsonOutput = $DwsOutput[1..$DwsOutput.Length] | ConvertFrom-Json
    $Script:DwsCallStack.Push($args,$JsonOutput)
    $JsonOutput
}

Function Get-DwsHistory {
    $Script:DwsCallStack
}

Function Test-DwsIsInPath {
    Try {
        $Script:DwsCommand = Get-Command "dws"
        If($Script:DwsCommand.CommandType -eq "Alias") {
            # 'dws' is already aliased, so we have to check through $ENV:Path manually
            # to make sure it's there.
            $Script:DwsCommand = Find-DwsExe
            If($Script:DwsCommand -eq ""){
                Write-Error "Couldn't find dws binary in your path"
                Exit 1
            }
        }
        Set-Alias -Name "dws" -Value "Invoke-DwsCommand" -Scope Script
    } Catch {
        $Message = $_.Exception.Message
        Write-Host $Message
        #Write-Error "Trying to find dws caused an exception"
        Exit 1
    }
}

Function Resolve-DwsExe {
    If($Script:DwsCommand -is [ApplicationInfo]){
        $Script:DwsExe = $Script:DwsCommand.Source
    } else {
        $Script:DwsExe = $Script:DwsCommand
    }
}

Function Initialize-Module {
    Test-DwsIsInPath
    Resolve-DwsExe
    [stack]$Script:DwsCallStack = [stack]::new()
}

Initialize-Module
Export-ModuleMember -Alias * -Function *