﻿$since_last_year = @()
foreach($user in $sorted){
	if($user.LastLogon > 1372636800){
		$since_last_year += $user
	}
}

$gpns = @()
foreach($user in $since_last_year){
    if($since_last_year $user.UserName)
}

$sly = @{}
for($i=0;$i -lt $since_last_year.Count;$i++){
    $sly.Add($since_last_year[$i].Username,$since_last_year[$i].LastLogon)
}

Function ConvertTo-HashTable {
    param(
        [Parameter(Mandatory=$true)]
        [object[]]$object
    )
    $h = @{}
    foreach($line in $since_last_year){
        $h.Add($line.Username,$line.LastLogon)
    }
    return $h
}

$possible_external = @()
$gpns = @()
foreach($line in $hrinfo){
    if($unique_users -contains $line.'NT ID'){
        # Found this user as their NT ID
    } elseif ($unique_users -contains $line.UUName){
        # Found this user as their UUName
    } elseif ($unique_users -contains $line.'T-Number'){
        # Foudn this user as their T-Number
    } else {
        # Didn't find this user at all - add them to possible externals?
        $possible_external += $line
    }

}

## This is momumentally slow
$logins_we_care_about = @()
foreach($line in $lastlogins){
    if($uniqueusers -contains $line.Username){
        $login = New-Object PSObject
        $login | Add-Member NoteProperty Username $line.Username        
        $login | Add-Member NoteProperty LastLogon $line.LastLogon
        $logins_we_care_about += $login
    }
}
##

## This is much faster
$ht_lastlogins = @{}
foreach($line in $lastlogins){
    $ht_lastlogins[$line.Username] = $line.LastLogon
}

$logins_we_care_about = @{}
foreach($line in $uniqueusers){
    $logins_we_care_about[$line] = $ht_lastlogins[$line]
}
##

Function Reconcile-Users{
    $ht_on_ntid = @{}
    $ht_on_uuname = @{}
    $ht_on_tnumber = @{}
    $users_by_ntid = @()
    $users_by_uuname = @()
    $users_by_tnumber = @()
    $external_users = @()
    $bots = @()
    $i = 0

    Write-Host "Loading users, please wait..."
    $unique_users = Get-Content -Path ".\Unique_Users.csv"
    Import-Csv "HR_UIDs.csv" | foreach {
        $ht_on_ntid[$_.'NT ID'] = $_
        $ht_on_uuname[$_.'UUName'] = $_
        $ht_on_tnumber[$_.'T-Number'] = $_
    }
    
    foreach($u in $unique_users){
        $i++
        Write-Progress -Activity "Reconciling Users" -Status "$u" -PercentComplete ($i/$unique_users.Count*100)
        if(!($ht_on_ntid[$u] -eq $null)){
            $users_by_ntid += $u
        } 
        elseif(!($ht_on_uuname[$u] -eq $null)){
            $users_by_uuname += $u
        }
        elseif(!($ht_on_tnumber[$u] -eq $null)){
            $users_by_tnumber += $u
        } else {
            # We've not found a match
            if($u -match "^_"){
                $bots += $u
            } else {
                #must be an external user
                $external_users += $u
            }
        }
    }
    
    if(($users_by_ntid.Count + $users_by_uuname.Count + $users_by_tnumber.Count + $bots.Count + $external_users.Count) -ne $unique_users.Count){
        Write-Warning "Totals don't add up"
    }
    
    Write-Host "Users by NT ID   :" $users_by_ntid.Count
    Write-Host "Users by UUName  :" $users_by_uuname.Count
    Write-Host "Users by T-Number:" $users_by_tnumber.Count
    Write-Host "Bots             :" $bots.Count
    Write-Host "External Users   :" $external_users.Count
    Write-Host "Total from file  :" $unique_users.Count

    $users_by_ntid    | Set-Content -Path "Users_by_NTID.csv"
    $users_by_uuname  | Set-Content -Path "Users_by_UUName.csv"
    $users_by_tnumber | Set-Content -Path "Users_by_TNumber.csv"
    $bots             | Set-Content -Path "Bots.csv"
    $external_users   | Set-Content -Path "External_Users.csv"
}




