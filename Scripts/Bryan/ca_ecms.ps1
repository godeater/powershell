<# 
.SYNOPSIS
    Script to check lockout status of FM\ca_ecm* accounts
.DESCRIPTION
    There's an on going problem with the FM\ca_ecm* accounts becoming locked
    out, which isn't obvious to people needing to use one (typically in a hurry)
    to deal with a production incident. This script enumerates them all, and
    returns their "Locked Out" status.
.EXAMPLE
    .\ca_ecms.ps1
.NOTES
    Running this script requires use of your FM\adm-* account, as most
    vanilla FM accounts do not have rights to read all user objects.
#>

Function Test-CaEcmAccounts {
    $domain   = New-Object DirectoryServices.DirectoryEntry("LDAP://DC=FM,DC=RBSGRP,DC=NET")
    $searcher = New-Object DirectoryServices.DirectorySearcher

    $searcher.SearchRoot = $domain
    $searcher.PageSize = 1000
    $searcher.Filter = "(&(objectCategory=user)(objectClass=user)(samAccountName=ca_ecm*))"
    $searcher.SearchScope = "Subtree"

    $results = $searcher.FindAll()

    $AccList = @()

    $results | ForEach-Object {
        $this_sam = ""
        $this_lot = 0
        $this_locked = $false
        $ad_props = $_.Properties
        $ad_props.GetEnumerator() | ForEach-Object {
            if($_.Key -eq "samAccountName"){
                $this_sam = $_.Value
            }
            if($_.Key -eq "lockouttime"){
                $this_lot = $_.Value
            }
        }
        if($this_lot -ge 1){
            $this_locked = $true
        }
        
        $AccountDetail = New-Object PSCustomObject
        $AccountDetail | Add-Member -Type NoteProperty -Name AccountName -Value $this_sam[0]
        $AccountDetail | Add-Member -Type NoteProperty -Name LockedOut -Value $this_locked
        $AccList += $AccountDetail
    }

    $AccList
}

Test-CaEcmAccounts