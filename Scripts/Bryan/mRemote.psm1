﻿<#
    .SYNOPSIS
    This module provides some useful helper functions for manipulating the confCons.xml file that mRemoteNG uses
    to hold it connections information in. Since a great deal of the connections I create share a large number of
    similar settings, I got fed up with creating new ones with the mouse and continually having to set the 
    'inherit all' value to yes
    .DESCRIPTION
    The module provides functions which:
        Load / Save the confCons.xml file
        Encrypt / Decrypt the value in the 'protected' attribute of the root node 'Connections'
            (This could be further extended to Encrypt / Decrypt the entire file, but I've not implemented that)

        Add new containers to a given existing container
        Add new connections to a given existing container
            Both of these rely on another function which isn't particularly mRemoteNG specific, which 
            works out the explicit XPath of a Node which has been found with a more general XPath notation.
    .EXAMPLE
        $confCons = Load-Connections "P:\ath\To\Your\ConfCons.xml"
        Add-Container $confCons.Document "Name of Existing Container" "Name of New Container"
        Save-Connections $confCons.Document 
    .EXAMPLE
        $confCons = Load-Connections "P:\ath\To\Your\ConfCons.xml"
        Add-Connection $confCons.Document "Name of Existing Container" "Name of New Container" "host.name.of.computer"
        Save-Connections $confCons.Document 
#>
Function Load-Connections{
    param(
        # Let's not default to the real confCons.xml file until we're *super* confident this
        # all works flawlessly ;)
        [Parameter(Mandatory=$false)]
        [string]$pathToConfCons="P:\Documents\WindowsPowershell\confCons.xml"
    )
    $confCons = "" | Select-Object -Property Document,Element

    $confConsFile = New-Object XMl
    $confConsFile.Load($pathToConfCons)
    $confConsXml = $confConsFile.DocumentElement

    $confCons.Document = $confConsFile
    $confCons.Element = $confConsXml

    return $confCons
}

Function Save-Connections{
    param(
        [Parameter(Mandatory=$true)]
        [xml]$confCons,
        [Parameter(Mandatory=$false)]
        [string]$pathToConfCons="P:\Documents\WindowsPowershell\confCons.xml"
    )

    $xmlWriter = New-Object System.Xml.xmlTextWriter($pathToConfCons,[System.Text.Encoding]::UTF8)
    $xmlWriter.Formatting = "Indented"
    $xmlWriter.Indentation = 4
    $xmlWriter.IndentChar ="`t"

    $confCons.Save($xmlWriter)
    $xmlWriter.Flush()
    $xmlWriter.Close()
}

Function Add-Node{
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[Alias('ConfCons')]
        [xml]$doc,
        [Parameter(Mandatory=$true)]
		[Alias('ParentContainer','Parent')]
        [string]$parentContainer,
        [Parameter(Mandatory=$true)]
		[Alias('Name')]
        [string]$NodeName,
        [Parameter(Mandatory=$false)]
		[Alias('Host')]
        [string]$hostName,
        [Parameter(Mandatory=$false)]
		[Alias('XPath')]
        [string]$explicitXpath
    )
    if($hostname){
        $NewNodeStr = Create-NewConnection
    } else {
        $NewNodeStr = Create-NewContainer
    }
    $docFrag = $doc.CreateDocumentFragment()
    $docFrag.InnerXml = $NewNodeStr
    $docFrag = Set-NameAttribute $docFrag $NodeName
    if($hostname){
        $docFrag.Node.Hostname = $hostname
    }
    if(!$explicitXpath){
        $explicitXpath = '//Node[@Name="' + $parentContainer + '"]'
    }
    $parentNode = $doc.SelectSingleNode($explicitXpath)
    if($parentNode){
        $parentNode.AppendChild($docFrag) | Out-Null
    } else {
        throw New-Object System.Exception("Cannot find parent node") 
    }

}

Function Add-Connection{
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[Alias('ConfCons')]
        [xml]$doc,
        [Parameter(Mandatory=$true)]
		[Alias('ParentNode','Parent')]
        [string]$targetContainer,
        [Parameter(Mandatory=$true)]
		[Alias('Name','ConnectionName')]
        [string]$connName,
        [Parameter(Mandatory=$true)]
		[Alias('Host','FQDN')]
        [string]$hostName
    )
    $newConnectionStr = Create-NewConnection
    $docFrag = $doc.CreateDocumentFragment()
    $docFrag.InnerXml = $newConnectionStr
    $docFrag = Set-NameAttribute $docFrag $connName
    $docFrag.Node.Hostname = $hostName
    $SearchPath = '//Node[@Name="' + $targetContainer + '"]'
    $parentNode = $doc.SelectSingleNode($SearchPath)
    
    $parentNode.AppendChild($docFrag) | Out-Null
}

Function Add-Container{
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[Alias('ConfCons')]
        [xml]$doc,
        [Parameter(Mandatory=$true)]
		[Alias('Parent')]
        [string]$parentContainer,
        [Parameter(Mandatory=$true)]
		[Alias('Name')]
        [string]$contName
    )
    $newContainer = Create-NewContainer
    $docFrag = $doc.CreateDocumentFragment()
    $docFrag.InnerXml = $newContainer
    $docFrag = Set-NameAttribute $docFrag $contName
    $SearchPath = '//Node[@Name="' + $parentContainer + '"]'
    $parentNode = $doc.SelectSingleNode($SearchPath)
    
    $parentNode.AppendChild($docFrag) | Out-Null
}

Function Encrypt-mRemoteNG{
    param(
        [Parameter(Mandatory=$false)]
        [string]$StrToEncrypt = "ThisIsNotProtected",
        [Parameter(Mandatory=$false)]
        [string]$StrSecret = "mR3m"
    )
    Try {
        $rd   = New-Object -TypeName System.Security.Cryptography.RijndaelManaged
        $md5  = New-Object -TypeName System.Security.Cryptography.MD5CryptoServiceProvider
        $utf8 = New-Object -TypeName System.Text.UTF8Encoding
        $ms   = New-Object -TypeName System.IO.MemoryStream

        [Byte[]]$key  = $md5.ComputeHash($utf8.GetBytes($StrSecret))

        $md5.Clear()
        $rd.Key = $key
        $rd.GenerateIV()

        [Byte[]]$iv = $rd.IV

        $ms.Write($iv, 0, $iv.Length)

        $cs = New-Object -TypeName System.Security.Cryptography.CryptoStream($ms, $rd.CreateEncryptor(), [System.Security.Cryptography.CryptoStreamMode]::Write)
        [Byte[]]$data = $utf8.GetBytes($StrToEncrypt)
        $cs.Write($data, 0, $data.Length)
        $cs.FlushFinalBlock()

        [Byte[]]$encdata = $ms.ToArray()
        $cs.Close()
        $rd.Clear()
        return [System.Convert]::ToBase64String($encdata)
    } Catch {
        [System.Exception]
        "It done broke it did"
    }
    #return $StrToEncrypt
}

Function Decrypt-mRemoteNG{
    param(
        [Parameter(Mandatory=$true)]
        [string]$ciphertextBase64,
        [Parameter(Mandatory=$false)] 
        [string]$password = "mR3m"
    )
    Try {
        [string]$plaintext = ""

        $rijndaelManaged   = New-Object -TypeName System.Security.Cryptography.RijndaelManaged
        $md5               = New-Object -TypeName System.Security.Cryptography.MD5CryptoServiceProvider
        $utf8              = New-Object -TypeName System.Text.UTF8Encoding

        [Byte[]]$key = $md5.ComputeHash($utf8.GetBytes($password))
        $rijndaelManaged.Key = $key

        [Byte[]]$ciphertext = [System.Convert]::FromBase64String($ciphertextBase64)

        # silly (, $object) syntax required because otherwise powershell expands the array
        # before passing it to the constructor, which doesn't work because the constructor 
        # doesn't take 48 arguments, it only takes one!
        $memoryStream = New-Object -TypeName System.IO.MemoryStream(,$ciphertext)
        [int]$ivLength = 16
        $iv = New-Object Byte[] $ivLength
        $memoryStream.Read($iv, 0, $ivLength) | Out-Null  # Out-Null required as this always outputs an int otherwise
        $rijndaelManaged.IV = $iv

        $cryptoStream = New-Object -TypeName System.Security.Cryptography.CryptoStream($memoryStream, $rijndaelManaged.CreateDecryptor(), [System.Security.Cryptography.CryptoStreamMode]::Read)
        $streamReader = New-Object -TypeName System.IO.StreamReader($cryptoStream, [System.Text.Encoding]::UTF8, $true)
        $plaintext = $streamReader.ReadToEnd()
        $rijndaelManaged.Clear()

        return $plaintext
    } Catch [System.Security.Cryptography.CryptographicException] {
        "Wrong password"
    } Catch {
        "It done broke it did"
    }
}


Function Find-XPath{
    param(
        [System.Xml.XmlNode]$node
    )
    $builder = New-Object -TypeName System.Text.StringBuilder
    while ($node -ne $null)
    {
        switch ($node.NodeType)
        {
            "Attribute" {
                $builder.Insert(0, "/@" + $node.Name)
                $node = ([System.Xml.XmlAttribute]$node).OwnerElement
            }
            "Element" {
                [int]$index = Find-ElementIndex([System.Xml.XmlElement]$node)
                # Because the mRemote author decided to use 'name' as the name of an attribute, .NET's XML parser ends up
                # clobbering the actual name of the element with the value from the attrbute, which breaks the XPath
                # that gets built up, because elements in XML cannot contain spaces, but attributes can.
                $realName = Find-RealElementName([System.Xml.XmlElement]$node)
                $builder.Insert(0, "/" + $realName + "[" + $index + "]") | Out-Null # Out-Null required again as this always writes output for some reason
                $node = $node.ParentNode
            }
            "Document" {
                return $builder.ToString()
            }
            default {
                throw New-Object System.ArgumentException("Only elements and attributes are supported");
            }
        }
    }
    throw new-object System.ArgumentException("Node was not in a document");
}

Function Find-RealElementName {
    param(
        [System.Xml.XmlElement]$element
    )
    $matches = $element.Outerxml | Select-String -Pattern "^<(\w*)"
    return $matches.Matches[0].Groups[1].Value

}

Function Find-ElementIndex{
    param(
        [System.Xml.XmlElement]$element
    )
    [System.Xml.XmlNode]$parentNode = $element.ParentNode;
    if ($parentNode -is [System.Xml.XmlDocument])
    {
        return 1
    }
    [System.Xml.XmlElement]$parent = [System.Xml.XmlElement]$parentNode
    [int]$index = 1
    foreach ($candidate in $parent.ChildNodes)
    {
        [System.Xml.XmlNode]$thisCandidate = $candidate
        if($thisCandidate.NodeType -eq "Element" -and (Find-RealElementName([System.Xml.XmlElement]$thisCandidate) -eq Find-RealElementName([System.Xml.XmlElement]$element)))
        {
            if ($thisCandidate -eq $element)
            {
                return $index
            }
            $index++
        }
    }
    throw New-Object System.ArgumentException("Couldn't find element within parent");
}

Function Set-NameAttribute{
    param(
        [Parameter(Mandatory=$true)]
        [System.Xml.XmlDocumentFragment]$docFrag,
        [Parameter(Mandatory=$true)]
        [string]$value
    )
    $attribs = $docFrag.Node.Attributes
    $nameAttrib = $attribs.GetNamedItem("Name")
    $nameAttrib.Value = $value

    return $docFrag
}

Function Create-NewContainer {
    [string]$container = '<Node
    Name="(nodename)" 
    Type="Container" 
    Expanded="True" 
    Descr="" 
    Icon="mRemoteNG" 
    Panel="Lync" 
    Username="(username)" 
    Domain="(domain)" 
    Password="" 
    Hostname="" 
    Protocol="RDP" 
    PuttySession="Default Settings" 
    Port="3389" 
    ConnectToConsole="True" 
    UseCredSsp="True" 
    RenderingEngine="IE" 
    ICAEncryptionStrength="EncrBasic" 
    RDPAuthenticationLevel="NoAuth" 
    LoadBalanceInfo="" 
    Colors="Colors16Bit" 
    Resolution="FitToWindow" 
    AutomaticResize="True" 
    DisplayWallpaper="False" 
    DisplayThemes="False" 
    EnableFontSmoothing="False" 
    EnableDesktopComposition="False" 
    CacheBitmaps="True" 
    RedirectDiskDrives="False" 
    RedirectPorts="False" 
    RedirectPrinters="False" 
    RedirectSmartCards="False" 
    RedirectSound="DoNotPlay" 
    RedirectKeys="True" 
    Connected="False" 
    PreExtApp="" 
    PostExtApp="" 
    MacAddress="" 
    UserField="" 
    ExtApp="" 
    VNCCompression="CompNone" 
    VNCEncoding="EncHextile" 
    VNCAuthMode="AuthVNC" 
    VNCProxyType="ProxyNone" 
    VNCProxyIP="" 
    VNCProxyPort="0" 
    VNCProxyUsername="" 
    VNCProxyPassword="" 
    VNCColors="ColNormal" 
    VNCSmartSizeMode="SmartSAspect" 
    VNCViewOnly="False" 
    RDGatewayUsageMethod="Never" 
    RDGatewayHostname="" 
    RDGatewayUseConnectionCredentials="Yes" 
    RDGatewayUsername="" 
    RDGatewayPassword="" 
    RDGatewayDomain="" 
    InheritCacheBitmaps="True" 
    InheritColors="True" 
    InheritDescription="True" 
    InheritDisplayThemes="True" 
    InheritDisplayWallpaper="True" 
    InheritEnableFontSmoothing="True" 
    InheritEnableDesktopComposition="True" 
    InheritDomain="True" 
    InheritIcon="True" 
    InheritPanel="True" 
    InheritPassword="True" 
    InheritPort="True" 
    InheritProtocol="True" 
    InheritPuttySession="True" 
    InheritRedirectDiskDrives="True" 
    InheritRedirectKeys="True" 
    InheritRedirectPorts="True" 
    InheritRedirectPrinters="True" 
    InheritRedirectSmartCards="True" 
    InheritRedirectSound="True" 
    InheritResolution="True" 
    InheritAutomaticResize="True" 
    InheritUseConsoleSession="True" 
    InheritUseCredSsp="True" 
    InheritRenderingEngine="True" 
    InheritUsername="True" 
    InheritICAEncryptionStrength="True" 
    InheritRDPAuthenticationLevel="True" 
    InheritLoadBalanceInfo="True" 
    InheritPreExtApp="True" 
    InheritPostExtApp="True" 
    InheritMacAddress="True" 
    InheritUserField="True" 
    InheritExtApp="True" 
    InheritVNCCompression="True" 
    InheritVNCEncoding="True"
    InheritVNCAuthMode="True" 
    InheritVNCProxyType="True" 
    InheritVNCProxyIP="True" 
    InheritVNCProxyPort="True" 
    InheritVNCProxyUsername="True" 
    InheritVNCProxyPassword="True" 
    InheritVNCColors="True" 
    InheritVNCSmartSizeMode="True" 
    InheritVNCViewOnly="True" 
    InheritRDGatewayUsageMethod="True" 
    InheritRDGatewayHostname="True" 
    InheritRDGatewayUseConnectionCredentials="True" 
    InheritRDGatewayUsername="True" 
    InheritRDGatewayPassword="True" 
    InheritRDGatewayDomain="True"
    />'

    return $container
}

Function Create-NewConnection{
    [string]$connection = '<Node 
    Name="(node name)" 
    Type="Connection" 
    Descr="" 
    Icon="mRemoteNG" 
    Panel="General" 
    Username="(username)" 
    Domain="(domain)" 
    Password="" 
    Hostname="(hostname)" 
    Protocol="RDP" 
    PuttySession="Default Settings" 
    Port="3389" 
    ConnectToConsole="True" 
    UseCredSsp="True" 
    RenderingEngine="IE" 
    ICAEncryptionStrength="EncrBasic" 
    RDPAuthenticationLevel="NoAuth" 
    LoadBalanceInfo="" 
    Colors="Colors16Bit" 
    Resolution="FitToWindow" 
    AutomaticResize="True" 
    DisplayWallpaper="False" 
    DisplayThemes="False" 
    EnableFontSmoothing="False" 
    EnableDesktopComposition="False" 
    CacheBitmaps="True" 
    RedirectDiskDrives="False" 
    RedirectPorts="False" 
    RedirectPrinters="False" 
    RedirectSmartCards="False" 
    RedirectSound="DoNotPlay" 
    RedirectKeys="False" 
    Connected="False" 
    PreExtApp="" 
    PostExtApp="" 
    MacAddress="" 
    UserField="" 
    ExtApp="" 
    VNCCompression="CompNone" 
    VNCEncoding="EncHextile" 
    VNCAuthMode="AuthVNC" 
    VNCProxyType="ProxyNone" 
    VNCProxyIP="" 
    VNCProxyPort="0" 
    VNCProxyUsername="" 
    VNCProxyPassword="" 
    VNCColors="ColNormal" 
    VNCSmartSizeMode="SmartSAspect" 
    VNCViewOnly="False" 
    RDGatewayUsageMethod="Never" 
    RDGatewayHostname="" 
    RDGatewayUseConnectionCredentials="Yes" 
    RDGatewayUsername="" 
    RDGatewayPassword="" 
    RDGatewayDomain="" 
    InheritCacheBitmaps="True" 
    InheritColors="True" 
    InheritDescription="True" 
    InheritDisplayThemes="True" 
    InheritDisplayWallpaper="True" 
    InheritEnableFontSmoothing="True" 
    InheritEnableDesktopComposition="True" 
    InheritDomain="True" 
    InheritIcon="True" 
    InheritPanel="True" 
    InheritPassword="True" 
    InheritPort="True" 
    InheritProtocol="True" 
    InheritPuttySession="True" 
    InheritRedirectDiskDrives="True" 
    InheritRedirectKeys="True" 
    InheritRedirectPorts="True" 
    InheritRedirectPrinters="True" 
    InheritRedirectSmartCards="True" 
    InheritRedirectSound="True" 
    InheritResolution="True" 
    InheritAutomaticResize="True" 
    InheritUseConsoleSession="True" 
    InheritUseCredSsp="True" 
    InheritRenderingEngine="True" 
    InheritUsername="True" 
    InheritICAEncryptionStrength="True" 
    InheritRDPAuthenticationLevel="True" 
    InheritLoadBalanceInfo="True" 
    InheritPreExtApp="True" 
    InheritPostExtApp="True" 
    InheritMacAddress="True" 
    InheritUserField="True" 
    InheritExtApp="True" 
    InheritVNCCompression="True" 
    InheritVNCEncoding="True" 
    InheritVNCAuthMode="True" 
    InheritVNCProxyType="True" 
    InheritVNCProxyIP="True" 
    InheritVNCProxyPort="True" 
    InheritVNCProxyUsername="True" 
    InheritVNCProxyPassword="True" 
    InheritVNCColors="True" 
    InheritVNCSmartSizeMode="True" 
    InheritVNCViewOnly="True" 
    InheritRDGatewayUsageMethod="True" 
    InheritRDGatewayHostname="True" 
    InheritRDGatewayUseConnectionCredentials="True" 
    InheritRDGatewayUsername="True" 
    InheritRDGatewayPassword="True" 
    InheritRDGatewayDomain="True" 
    />'

    return $connection
}

Export-ModuleMember -Variable * -Function *
