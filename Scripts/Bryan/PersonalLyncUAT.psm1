﻿if(!(Get-Module "GenericLync")){
    Import-Module -DisableNameChecking "\\ubsprod.msad.ubs.net\UserData\CHILDSB\Home\Documents\WindowsPowershell\Scripts\Bryan\GenericLync.psm1"
}

$global:GCDatabase = New-Object -TypeName PSOBject -Property @{Server="nzurc034usq.test.msad.ubstest.net";
														Name="LyncGCF";
														User="svc_chatweb";
														Pass="tUzeBi36NU6YyDUj1/h3bA=="}

$LyncUserProps = @('msRTCSIP-AcpInfo',
    'msRTCSIP-ApplicationOptions',
    'msRTCSIP-ArchivingEnabled',
    'msRTCSIP-DeploymentLocator',
    'msRTCSIP-FederationEnabled',
    'msRTCSIP-GroupingID',
    'msRTCSIP-InternetAccessEnabled',
    'msRTCSIP-Line',
    'msRTCSIP-LineServer',
    'msRTCSIP-OptionFlags',
    'msRTCSIP-OriginatorSid',
    'msRTCSIP-OwnerUrn',
    'msRTCSIP-PrimaryHomeServer',
    'msRTCSIP-PrimaryUserAddress',
    'msRTCSIP-PrivateLine',
    'msRTCSIP-TargetHomeServer',
    'msRTCSIP-TargetUserPolicies',
    'msRTCSIP-TenantId',
    'msRTCSIP-UserEnabled',
    'msRTCSIP-UserExtension',
    'msRTCSIP-UserLocationProfile',
    'msRTCSIP-UserPolicies',
    'msRTCSIP-UserPolicy')

$LyncPoolProps = @(
    'msRTCSIP-ApplicationList',
    'msRTCSIP-BackEndServer',
    'msRTCSIP-PoolData',
    'msRTCSIP-PoolDisplayName',
    'msRTCSIP-PoolDomainFQDN',
    'msRTCSIP-PoolFunctionality',
    'msRTCSIP-PoolType',
    'msRTCSIP-PoolVersion',
    'msRTCSIP-TrustedServiceLinks')

Export-ModuleMember -Variable * -Function * -Alias *
