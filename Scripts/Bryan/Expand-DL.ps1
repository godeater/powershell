﻿param(
    [Parameter(Mandatory=$true,Position=0)]
    [string]$DistributionList,
	[Parameter(Mandatory=$false,Position=1)]
	[ValidateSet("Prd","Uat","Eng")]
	[string]$Environment="Prd"
)

Import-Module ActiveDirectory

# Domain FQDNs
Set-Variable -Name DomainHash -Value @{
	Eng='eng.wintel.ubseng.net';
	Uat='test.msad.ubstest.net';
	Prd='ubsprod.msad.ubs.net';} -Option ReadOnly -Scope Script


try {
       $LdapFilter = "(&(displayName=" + $DistributionList + "))"
       $DLMembers = Get-AdGroup -LdapFilter $LdapFilter -Properties Members | Select-Object -ExpandProperty Members
       $FileName = ".\" + $DistributionList + "-Members.txt"
       $DLMembers | Set-Content $FileName
} catch {
       Write-Error $_
}


