﻿$NamespaceHash = @{
	'DocItemSet'            = 'urn:schema:Microsoft.Rtc.Management.Xds.AppLayer.2008';
	'BaseTypes'             = 'urn:schema:Microsoft.Rtc.Management.BaseTypes.2008';
	'AnchoredXml'           = 'urn:schema:Microsoft.Rtc.Management.ScopeFramework.2008';
	'Roles'                 = 'urn:schema:Microsoft.Rtc.Management.Settings.Roles.2008';
	'Topology'              = 'urn:schema:Microsoft.Rtc.Management.Deploy.Topology.2008';
	'ServiceRoles'          = 'urn:schema:Microsoft.Rtc.Management.Deploy.ServiceRoles.2008';
	'PolicyClient'          = 'urn:schema:Microsoft.Rtc.Management.Policy.Client.2008';
	'PolicyClientVersion'   = 'urn:schema:Microsoft.Rtc.Management.Policy.ClientVersion.2008';
	'PolicyExternalAccess'  = 'urn:schema:Microsoft.Rtc.Management.Policy.ExternalAccess.2008';
	'PolicyIm'              = 'urn:schema:Microsoft.Rtc.Management.Policy.Im.2008';
	'PolicyLocation'        = 'urn:schema:Microsoft.Rtc.Management.Policy.Location.2008';
	'PolicyMeeting'         = 'urn:schema:Microsoft.Rtc.Management.Policy.Meeting.2008';
	'PolicyUserPin'         = 'urn:schema:Microsoft.Rtc.Management.Policy.UserPin.2008';
	'PolicyVoice'           = 'urn:schema:Microsoft.Rtc.Management.Policy.Voice.2008';
	'SettingsBwPolicy'      = 'urn:schema:Microsoft.Rtc.Management.Settings.BandwidthPolicyServiceConfiguration.2008';
	'SettingsConfAccNum'    = 'urn:schema:Microsoft.Rtc.Management.Settings.ConferenceAccessNumbers.2009';
	'SettingsDiagnostics'   = 'urn:schema:Microsoft.Rtc.Management.Settings.Diagnostics.2008';
	'SettingsHealthMon'     = 'urn:schema:Microsoft.Rtc.Management.Settings.HealthMonitoring.2010';
	'SettingsImFilter'      = 'urn:schema:Microsoft.Rtc.Management.Settings.ImFilter.2008';
	'SettingsMedia'         = 'urn:schema:Microsoft.Rtc.Management.Settings.Media.2008';
	'SettingsNetworkConf'   = 'urn:schema:Microsoft.Rtc.Management.Settings.NetworkConfiguration.2008';
	'SettingsPstnConf'      = 'urn:schema:Microsoft.Rtc.Management.Settings.PstnConf.2008';
	'SettingsRegistrar'     = 'urn:schema:Microsoft.Rtc.Management.Settings.Registrar.2008';
	'SettingsServerApp'     = 'urn:schema:Microsoft.Rtc.Management.Settings.ServerApplication.2008';
	'SettingsServiceAssign' = 'urn:schema:Microsoft.Rtc.Management.Settings.ServiceAssignment.2008';
	'SettingsSimpleUrl'     = 'urn:schema:Microsoft.Rtc.Management.Settings.SimpleUrl.2008';
	'SettingsSipProxy'      = 'urn:schema:Microsoft.Rtc.Management.Settings.SipProxy.2008';
	'SettingsTrunkConf'     = 'urn:schema:Microsoft.Rtc.Management.Settings.TrunkConfiguration.2009';
	'SettingsUserServices'  = 'urn:schema:Microsoft.Rtc.Management.Settings.UserServices.2008';
	'SettingsWeb'           = 'urn:schema:Microsoft.Rtc.Management.Settings.Web.2008';
	'SettingsWebConf'       = 'urn:schema:Microsoft.Rtc.Management.Settings.WebConf.2008';
	'SettingsCDR'           = 'urn:schema:Microsoft.Rtc.Management.Settings.CallDetailRecording.2008'
}


Function Get-AdminCredential {
    param(
        [Parameter(Mandatory=$false,ValueFromPipeline=$true)]
        [string]$environment = 'p'
    )
    switch($environment){
        "p"{
            $global:AdminCreds = Get-Credential -Credential "UBSPROD\43409161.1" # -Message "UBSPROD Admin Credentials"
        }
        "u"{
            $global:AdminCreds = Get-Credential -Credential "t.43409161.1@test.msad.ubstest.net" # -Message "UBSTEST Admin Credentials"
        }
    }
}


Function Get-LyncAdminSession {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$false,ValueFromPipeline=$true)]
        [string]$where = "rtcadmin.ubs.com"
    )
    $uri = "https://" + $where + "/OcsPowershell"

    if(!($global:AdminCreds)){
        $global:AdminCreds = Get-Credential
    }
    $LyncOptions = New-PSSessionOption -SkipRevocationCheck -SkipCACheck -SkipCNCheck
    $PSSession   = New-PSSession -ConnectionUri $uri -SessionOption $LyncOptions -Credential $global:AdminCreds
    $global:LyncSession = Import-PSSession -Session $PSSession 
    Import-Module -Global $global:LyncSession | Out-Null
    # return $global:LyncSession
}

Function Load-CsConfig{
    if($(Get-Command -ErrorAction SilentlyContinue Export-CsConfiguration) -ne $null){
        [System.Reflection.Assembly]::LoadWithPartialName('System.IO.Compression') | Out-Null

        $ZipBytes = Export-CsConfiguration -AsBytes
        $ZipStream = New-Object System.IO.Memorystream
        $ZipStream.Write($ZipBytes,0,$ZipBytes.Length)
        $ZipArchive = New-Object System.IO.Compression.ZipArchive($ZipStream)
        $ZipEntry = $ZipArchive.GetEntry('DocItemSet.xml')
        $EntryReader = New-Object System.IO.StreamReader($ZipEntry.Open())
        $DocItemSet = $EntryReader.ReadToEnd()
        return $DocItemSet
        } else {
            Write-Warning "Lync module doesn't appear to be loaded. Load it and then retry."
        }
}

Function Get-PoolRole{
	param(
		[Parameter(Mandatory=$true)]
		$Roles,
		[Parameter(Mandatory=$true)]
		$TopNs,
		[Parameter(Mandatory=$true)]
		$TrustedApps,
		[Parameter(Mandatory=$true)]
		[string]$PoolFqdn,
		[Parameter(Mandatory=$true)]
		[int]$ClusterID,
		[Parameter(Mandatory=$true)]
		[int]$SiteId
	)
	$RoleArray = @()
	$PoolRole = ""
	
	$ThisClustersRoles = $Roles | Where-Object {
		($_.Descendants($TopNs + "InstalledOn").Descendants($TopNs + "ClusterId").Attribute("Number").Value -eq $ClusterId)	-and
		($_.Descendants($TopNs + "InstalledOn").Descendants($TopNs + "ClusterId").Attribute("SiteId").Value -eq $SiteId) }

	$ThisClustersRoles | ForEach-Object {
		$RoleArray += $_.Elements($TopNs + "ServiceId").Attribute("RoleName").Value
	}

	if(($RoleArray -contains "Registrar") -and ($RoleArray -contains "WebServices") -and ($RoleArray -notcontains "UserServices")){
		$PoolRole = "Director"
	} elseif (($RoleArray -contains "Registrar") -and ($RoleArray -contains "WebServices") -and ($RoleArray -contains "UserServices")){
		$PoolRole = "Front End"
	} elseif ($RoleArray -contains "MediationServer"){
		$PoolRole = "Mediation"
	} elseif ($RoleArray -contains "UserStore"){
		$PoolRole = "SQL Backend"
	} elseif (($RoleArray -contains "ConfServices") -and ($RoleArray -notcontains "UserServices")){
		$PoolRole = "AV"
	} elseif ($RoleArray -contains "PstnGateway") {
		$PoolRole = "PSTN Gateway"
	} elseif ($RoleArray -contains "MonitoringServer") {
		$PoolRole = "Monitoring"
	} elseif ($RoleArray -contains "FileStore") {
		$PoolRole = "File Store"
	} elseif (($RoleArray -contains "ExternalServer") -and ($RoleArray -notcontains "MonitoringServer")){
		$TrustedApps | ForEach-Object {
			if(($_.TrustedApplicationPoolFqdn -eq $PoolFqdn) -and ($_.LegacyApplicationName -eq "microsoft.rtc.chat.channelserver")){
				$PoolRole = "Group Chat"
			} 
		}
		if($PoolRole -ne "Group Chat"){
			$PoolRole = "External Server"
		}
	} elseif (($RoleArray -contains "EdgeServer") -and ($RoleArray -contains "ManagementServices")){
		$PoolRole = "Edge"
	}
	if($PoolRole -eq ""){
		$PoolRole = "Unknown role for SiteID:$SiteId ClusterID:$ClusterID"
	}
	return $PoolRole
}

Function Get-SiteDetails {
	param(
		[Parameter(Mandatory=$true)]
		$Sites,
		[Parameter(Mandatory=$true)]
		$TopNs,
		[Parameter(Mandatory=$true)]
		[int]$SiteId
	)

	$ThisSiteXml = $Sites | Where-Object  { $_.Attribute("SiteId").Value -eq $SiteId }
	$ThisSite = New-Object -Type PSCustomObject -Property @{
			SiteId=$ThisSiteXml.Attribute($TopNs + "SiteId").Value;
			Name=$ThisSiteXml.Descendants($TopNs + "Name").Value;
			Description=$ThisSiteXml.Descendants($TopNs + "Description").Value;
			City=$ThisSiteXml.Descendants($TopNs + "Location").Attribute("City").Value;
			State=$ThisSiteXml.Descendants($TopNs + "Location").Attribute("State").Value;
			CountryCode=$ThisSiteXml.Descendants($TopNs + "Location").Attribute("CountryCode").Value
	}
	return $ThisSite
}	

Function Get-TopologyFromCsConfig{
<#
    .SYNOPSIS
    Attempt to bring some sanity to working with the Lync topology
    .DESCRIPTION
    The function loads in the output of the Export-CsConfiguration cmdlet, expands
    the zip file it returns in memory, and pulls out the DocItemSet.xml file, which 
    contains the XML representation of the Lync Topology. (Dependency on Load-CsConfig)
    It then parses that XML to pull back each individual server in the topology,
    and gets the properties associated with that server, such as the pool it is
    in, the role it performs, the site it belongs to, etc.
#>
	# Working with Linq takes *some* of the pain of dealing with the hideousness
	# that is the Lync XML schema generated by Export-CsConfig.
	[Reflection.Assembly]::LoadWithPartialName("System.Xml.Linq") | Out-Null
	$myTopology  = @()
	$machineInfo = @()

	# Set up namespace for Topology in the XML
	$TopNs = [System.Xml.Linq.XNamespace]::Get($NamespaceHash['Topology'])
	# Linq XDocument constructor requires a StringReader
	$StringReader = New-Object System.IO.StringReader($(Load-CsConfig))
	$LyncTopology = [System.Xml.Linq.XDocument]::Load($StringReader)

	# Also load in Trusted Application definitions, so we can work out 
	# which servers are group chat servers
	$TrustedApps = Get-CsTrustedApplication

	# Find all the machines in the topology
	$machines = $LyncTopology.Descendants($TopNs + "Machine")

	# Find all the roles in the topology
	$roles = $LyncTopology.Descendants($TopNs + "Service")

	# Find all the sites in the topology
    $sites = $LyncTopology.Descendants($TopNs + "CentralSite")

	# Get the properties of these machines and store in custom object
	$machines | ForEach-Object {
		$ClusterId   = $_.Parent.Descendants($TopNS + "ClusterId").Attribute("Number").Value
		$SiteId      = $_.Parent.Descendants($TopNs + "ClusterId").Attribute("SiteId").Value
		$Ordinal     = $_.Attribute("OrdinalInCluster").Value;
		$Fqdn        = $_.Attribute("Fqdn").Value;
		$PoolFqdn    = $_.Parent.Attribute("Fqdn").Value;

		# Get pool role based on either Site:Cluster combo, or based on TrustedServer type
		$PoolRole    = Get-PoolRole -Roles $roles -TopNS $TopNs -TrustedApps $TrustedApps -PoolFqdn $PoolFqdn -ClusterID $ClusterId -SiteId $SiteId

		# Get Site name
		$SiteDetails = Get-SiteDetails -Sites $sites -TopNS $TopNs -SiteId $SiteId

		# Store details, and add to array
		$ThisMachine = New-Object -Type PSCustomObject -Property @{
			Fqdn=$Fqdn;
			OrdinalInCluster=$Ordinal
			ClusterId=$ClusterId;
			SiteId=$SiteId;
			PoolFqdn=$PoolFqdn;
			PoolRole=$PoolRole;
			Site=$SiteDetails.Name
		}
		$machineInfo += $ThisMachine
	}

	return $MachineInfo

}

Function Get-Certificate  {
<#
  .SYNOPSIS
   Retrieves  certificates from a local or remote system.
  .DESCRIPTION
   Retrieves certificates from a local or remote system.
  .PARAMETER Computername
   A single or list of computernames to perform search against
  .PARAMETER StoreName
   The name of the certificate store name that you want to search
  .PARAMETER StoreLocation
   The location of the certificate store.
  .PARAMETER DaysUntilExpired
   Find Certficates which expire within the next n days.
#>
	[cmdletbinding(DefaultParameterSetName = 'All')]
	Param (
		[parameter(ValueFromPipeline=$True,ValueFromPipelineByPropertyName=$True)]
		[Alias('PSComputername','__Server','IPAddress')]
		[string[]]$Computername = $env:COMPUTERNAME,
		[System.Security.Cryptography.X509Certificates.StoreName]$StoreName = 'My',
		[System.Security.Cryptography.X509Certificates.StoreLocation]$StoreLocation = 'LocalMachine',
		[parameter(ParameterSetName='Expire')]
		[Int]$DaysUntilExpired,
		[parameter(ParameterSetName='Expire')]
		[Switch]$HideExpired
	)
	Process {
		ForEach ($Computer in $Computername) {
			Try {
				Write-Verbose ("Connecting to {0}\{1}" -f "\\$($Computername)\$($StoreName)",$StoreLocation)
				$CertStore = New-Object System.Security.Cryptography.X509Certificates.X509Store -ArgumentList "\\$($Computername)\$($StoreName)", $StoreLocation
				$CertStore.Open('ReadOnly')
				Write-Verbose "ParameterSetName: $($PSCmdlet.ParameterSetName)"
				Switch ($PSCmdlet.ParameterSetName) {
					'All' {
						$CertStore.Certificates
					}
					'Expire' {
						$CertStore.Certificates | Where {
							$_.NotAfter -lt (Get-Date).AddDays($DaysUntilExpired)
						} | ForEach {
							$Days = Switch ((New-TimeSpan -End $_.NotAfter).Days) {
								{$_ -gt 0} {$_}
								Default {'Expired'}
							}
							$Cert = $_ | Add-Member -MemberType NoteProperty -Name DaysUntilExpired -Value $Days -PassThru
							If ($HideExpired -AND $_.DaysUntilExpired -ne 'Expired') {
								$Cert
							} ElseIf (-Not $HideExpired) {
								$Cert
							}
						}
					}
				}
			} Catch {
				Write-Warning "$($Computer): $_"
			}
		}
	}
}

Function ConvertTo-NTAccount ($sid) {
    (New-Object System.Security.Principal.SecurityIdentifier($sid)).Translate([System.Security.Principal.NtAccount]).Value
}

Function ConvertTo-Sid ($NtAccount) {
    (New-Object System.Security.Principal.NtAccount($NtAccount)).Translate([System.Security.Principal.SecurityIdentifier])
}

Function Get-FileHash {
    param(
        $Path,
        [ValidateSet("MD5","SHA1","SHA256","SHA384","SHA512")]
        $HashAlgorithm = "MD5"
    )

    $hashType = [Type] "System.Security.Cryptography.$HashAlgorithm"
    $hasher = $hashType::Create()

    $files = @()

    if($Path){
         $files += $Path
    } else {
        $files += @($input | ForEach-Object { $_.FullName })
    }

    foreach($file in $files){
        if(-not (Test-Path $file -Type Leaf)) { continue }
        $fileName = $(Resolve-Path $file).Path
        $inputStream = New-Object IO.StreamReader $fileName
        $hashBytes = $hasher.ComputeHash($inputStream.BaseStream)
        $inputStream.Close()

        $builder = New-Object System.Text.StringBuilder
        $hashBytes | ForEach-Object { [void] $builder.Append($_.ToString("X2"))}
        $output = New-Object PsObject -Property @{
            Path = ([IO.Path]::GetFileName($file))
            HashAlgorithm = $HashAlgorithm
            HashValue = $builder.ToString()
        }

        $output | ft -Autosize
    }
}

Function Get-StringHash {
    param(
        [string]$content,
        [ValidateSet("MD5","SHA1","SHA256","SHA384","SHA512")]
        $HashAlgorithm = "MD5"
    )

    $hashType = [Type] "System.Security.Cryptography.$HashAlgorithm"
    $hasher = $hashType::Create()
    $utf8 = New-Object -TypeName System.Text.UTF8Encoding

    $hashBytes = $hasher.ComputeHash($utf8.GetBytes($content))
    $builder = New-Object System.Text.StringBuilder
    $hashBytes | ForEach-Object { [void] $builder.Append($_.ToString("X2"))}

    $output = New-Object PsObject -Property @{
        HashAlgorithm = $HashAlgorithm
        HashValue = $builder.ToString()
    }
    $output | ft -Autosize
}


if(!($PSVersionTable.PSVersion.Major -ge 3 -and $PSVersionTable.CLRVersion.Major -ge 4)) {
	Write-Warning "The Powershell version / .NET version on this machine are not of sufficiently"
	Write-Warning "high enough version to include the cmdlets Load-CsConfig, and Get-TopologyFromCsConfig,"
	Write-Warning "and so they have been exluded from the function table this module has exported."
	Export-ModuleMember -Function Get-Certificate, Get-LyncAdminSession
} else {
	Export-ModuleMember -Function Get-Certificate, Get-LyncAdminSession, Get-TopologyFromCsConfig, Load-CsConfig
}
