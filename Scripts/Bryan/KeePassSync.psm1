# Keepass github sync
# 1) Pull from github
# 2) Sync local dbx with pulled github
# 3) git add dbx, git commit -m "Updated keepass dbx at <date:time>", git push

Write-Output "Installing Sync functions"

Function Get-Latest {
    param(
        [Parameter(Mandatory=$false)]
        [string]$RepositoryLocation = "C:\Users\childba\Projects\GitHub_Repos\security",
        [Parameter(Mandatory=$false)]
        [string]$GitExe = "C:\Program Files\Git\bin\git.exe"
    )
    Start-Transcript -Path "C:\Temp\KeePass_Update-GetLatest.log"
    Set-Location $RepositoryLocation
    $ErrorActionPreference = "SilentlyContinue"
    & $GitExe pull
    $ErrorActionPreference = "Continue"
    Stop-Transcript
    return $?
}

Function Push-Git {
    param(
        [Parameter(Mandatory=$false)]
        [string]$RepositoryLocation = "C:\Users\childba\Projects\GitHub_Repos\security",
        [Parameter(Mandatory=$false)]
        [string]$GitExe = "C:\Program Files\Git\bin\git.exe"
    )
    Start-Transcript -Path "C:\Temp\KeePass_Update-PushGit.log"
    Set-Location $RepositoryLocation
    $CommitMessage = "Updated with Database.kdbx on $(Get-Date)"

    & $GitExe add Database.kdbx
    & $GitExe commit -m $CommitMessage
    & $GitExe push
    Stop-Transcript
    return $?
}
