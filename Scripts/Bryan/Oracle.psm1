﻿# .NET 2.0 version
Add-Type -Path "C:\Programs\Oracle\Ora11g\DEV.NET\odp.net\4\Oracle.DataAccess.dll"

$malignp = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(PORT=1523)(HOST=malignp1-vip.zur.swissbank.com)))(CONNECT_DATA=(SERVICE_NAME=MALIGNP1)));User Id=svc_chat;Password=svc_chat!"
$malignd = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(PORT=1522)(HOST=malignd1.stm.swissbank.com)))(CONNECT_DATA=(SERVICE_NAME=MALIGND1)));User Id=svc_chat;Password=svc_chat!"

Function Connect-ToMalignP {
#    [CmdletBinding()]
    try {
        $connection = New-Object Oracle.DataAccess.Client.OracleConnection($malignp)
        $connection.Open()
        Write-Host ("Connected to database: {0} running on host: {1} - Servicename: {2} - Serverversion: {3}" -f $connection.DatabaseName, $connection.Hostname, $connection.ServiceName, $connection.ServerVersion)
    } catch {
        Write-Error ("Can't open connection: {0}`n{1}" -f $connection.ConnectionString, $_.Exception.ToString())
    } finally {
        if($connection.State -eq "Open") {
            $connection.Close()
        }
    }
    return $connection
}


Function Query-MalignP {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [string]$Query,
        [Parameter(Mandatory=$true)]
        [Oracle.DataAccess.Client.OracleConnection]$connection
    )
    try{
        $command = $connection.CreateCommand()
        $command.CommandText = $Query
        $dataAdater = New-Object Oracle.DataAccess.Client.OracleDataAdapter($command)
        $resultSet = New-Object System.Data.DataTable
        [void]$dataAdater.Fill($resultSet)
    } catch {
        Write-Error ("You did something dumb: {0}" -f $_.Exception.ToString())
    } finally {
        if($connection.State -eq "Open"){
            $connection.Close()
        }
    }
    return $resultSet
}

Export-ModuleMember -Variable * -Function *