Function Add-RollToLog {
    param()
}

Function Roll-Dice {
    param(
        [Param(Mandatory=$true)]
        [int]$DiceType,
        [Param(Mandatory=$false)]
        [int]$NumberOfDice=1,
        [Param(Mandatory=$false)]
        [bool]$Total=$true
    )
    [int]$RollAmount=0
    [Collections.ArrayList]$RollValues=@()
    1..$NumberOfDice | ForEach-Object {
        $Roll = $(Get-Random -Minimum 1 -Maximum $DiceType)
        $RollValues.Add($Roll)
        $RollAmount += $Roll
    }
    if($Total){
        $Return = $RollAmount
    } else {
        $Return = $RollValues
    }
    return $Return
}


Function Roll-D20 {
    return Roll-Dice -DiceType 20
}
Function Roll-D12 {
    return Roll-Dice -DiceType 12
}
Function Roll-D10 {
    return Roll-Dice -DiceType 10
}
Function Roll-D8  {
    return Roll-Dice -DiceType 8
}
Function Roll-D6  {
    return Roll-Dice -DiceType 6
}
Function Roll-D4  {
    return Roll-Dice -DiceType 4
}
Function Roll-D3  {
    return Roll-Dice -DiceType 3
}

