<#
.SYNOPSIS 
Powershell module to make adding third party palettes to ConEmu easier
.DESCRIPTION
The only export is the Add-ConEmuPalette function, which aims to write a new palette into
the correct place in an existing ConEmu.XML file, and update the color count accordingly.
Assumptions made :
  The new palette is in the same format as those found at https://github.com/joonro/ConEmu-Color-Themes
  i.e. no more than one new palette per file.
#>

Function Find-ConEmuXml {
    [CmdletBinding()]
    param()
    [string]$FilePart = "\ConEmu.xml"
    # First, see if it's in the Roaming profile:
    $AppdataConEmuXml = [string]::join('',$env:APPDATA,$FilePart)
    If(Test-Path -LiteralPath $AppdataConEmuXml -PathType Leaf){
        # ConEmu.xml located!
        Write-Verbose "XML is in AppData location"
        Write-Host "ConEmu.xml located at $AppdataConEmuXml"
        return $AppdataConEmuXml
    }

    # Check to see if it's already running?
    $ConEmuProcess = Get-Process -Name "ConEmu"
    if($ConEmuProcess -ne $null){
        $PathDir = [IO.Path]::GetDirectoryName($ConEmuProcess.Path)
        $ProcessConEmuXml = [String]::Join('',$PathDir,$FilePart)
        If(Test-Path -LiteralPath $ProcessConEmuXml -PathType Leaf){
            # ConEmu.xml located!
            Write-Verbose "XML is in running process location"
            Write-Host "ConEmu.xml located at $ProcessConEmuXml"
            return $ProcessConEmuXml
        }
    }   

    # It wasn't in the user's AppData dir, and there's no running process,
    # so check to see if it's where ConEmu itself is installed

    # Check Win32_Product WMI :
    $ConEmuWin32Product = Get-WmiObject Win32_Product -Filter "Name like '%ConEmu%'"
    If($ConEmuWin32Product -ne $null){
        # We found it installed
        $InstallDir = $ConEmuWin32Product.InstallLocation
        $InstallDirConEmuXml = [String]::Join('',$InstallDir,$FilePart)
        If(Test-Path -LiteralPath $InstallDirConEmuXml -PathType Leaf){
            # ConEmu.xml located!
            Write-Verbose "XML is in WMI Location"
            Write-Host "ConEmu.xml located at $InstallDirConEmuXml"
            return $InstallDirConEmuXml
        }
    }

    Write-Host "Could not locate ConEmu.xml"
    return $null
}

Function Add-ConEmuPalette {
    [CmdLetBinding()]
    param(
        [Parameter(Mandatory=$false)]
        [string]$NewPalette
    )
    # Constant
    [string]$ConEmuColorsXpath = "//key[@name='Colors']"

    # Find current ConEmu.xml
    [string]$ConEmuXmlPath = Find-ConEmuXml
    if($ConEmuXmlPath -eq $null){
        Write-Host "Can't find your ConEmu.Xml file"
        return $null
    }

    # Backup existing config
    [string]$InvocationTime = Get-Date -Format FileDateTimeUniversal
    [string]$BackupName = [String]::Join('',@("C:\Users\childba\Applications\ConEmu\ConEmu-",$InvocationTime,".xml-backup"))
    [string]$Message = "Backing up original config to " + $BackupName 
    Write-Output  $Message
    Copy-Item -LiteralPath $ConEmuXmlPath -Destination $BackupName

    # Load XML containing new palette
    [xml]$NewPaletteXml = New-Object Xml.XmlDocument
    $NewPaletteXml.Load($NewPalette)

    # Load current ConEmu.xml
    [xml]$ConEmuXml = New-Object Xml.XmlDocument
    $ConEmuXml.Load($ConEmuXmlPath)

    # Find the XML node where new palettes should be stored,
    # get the current count of palettes installed there, and
    # get a reference to the parent XML node for use in the 
    # InsertAfter method later on.
    $ConEmuColorsNode = $ConEmuXml.SelectSingleNode($ConEmuColorsXpath)
    $PaletteCountNode = $ConEmuColorsNode.Value
    $ParentNode       = $PaletteCountNode.ParentNode

    # Import the new palette's XML data into the ConEmu config,
    # insert it into the right place in the XML, and then increment
    # the palette count by one.
    $Imported    = $ConEmuXml.ImportNode($NewPaletteXml.DocumentElement,$true)
    $ParentNode.InsertAfter($Imported,$PaletteCountNode) | Out-Null
    [long]$Count = $PaletteCountNode.data
    $Count++
    $PaletteCountNode.data = [string]$Count

    # Save the new config
    $ConEmuXml.Save($ConEmuXmlPath)
}