﻿#requires -Modules oh-my-posh
$Global:ThemeSettings.MyThemesLocation = Join-Path -Path $([Environment]::GetFolderPath("MyDocuments")) -ChildPath "WindowsPowershell\PoshThemes"
Set-Theme bryantheme
#$ipAddressRegEx = "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"

$GitPromptSettings.BeforeStatus = ''
$GitPromptSettings.DelimStatus = '|'
$GitPromptSettings.AfterStatus = ''
$GitPromptSettings.FileAddedText = '+'
$GitPromptSettings.FileModifiedText = '±'
$GitPromptSettings.FileRemovedText ='-'
$GitPromptSettings.FileConflictedText = '✼'
$GitPromptSettings.LocalStagedStatusSymbol = '✔'
$GitPromptSettings.BranchGoneStatusSymbol = '×'
$GitPromptSettings.BranchIdenticalStatusSymbol = '≡'
$GitPromptSettings.BranchAheadStatusSymbol = '↑'
$GitPromptSettings.BranchBehindStatusSymbol = '↓'
$GitPromptSettings.BranchBehindAndAheadStatusSymbol = '↕'
$GitPromptSettings.DefaultPromptAbbreviateHomeDirectory = $false

# Load in java compatible crypto routine
$Script:JavaCryptLoadPath = $env:HOMEDRIVE + $env:HOMEPATH + "Documents\Git\.Net\PBEWithMD5AndDES\PKCSKeyGenerator.cs"
if(Test-Path $Script:JavaCryptLoadPath){
	Add-Type -LiteralPath $Script:JavaCryptLoadPath
}

Function Write-Color {
    $allColors = ("-Black", "-DarkBlue", "-DarkGreen", "-DarkCyan", "-DarkRed", "-DarkMagenta", "-DarkYellow", "-DarkGray",
                  "-White",     "-Blue",     "-Green",     "-Cyan",     "-Red",     "-Magenta",     "-Yellow", "    -Gray")
    $foreground = (Get-Host).UI.RawUI.ForegroundColor
    $color = $foreground
    [bool]$nonewline = $false
    $sofar = ""
    $total = ""

    foreach($arg in $args){
        if($arg -eq "-nonewline") {
            $nonewline = $true
        }
        elseif ($arg -eq "-foreground") {
            if($sofar) {Write-Host $sofar -foreground $color -nonewline}
            $color = $foreground
            $sofar = ""
        }
        elseif($allColors -contains $arg){
            if($sofar) {Write-Host $sofar -foreground $color -nonewline}
            $color = $arg.substring(1)
            $sofar = ""
        } else {
            $sofar += "$arg"
            $total += "$arg"
        }
    }
    Write-Host $sofar -foreground $color -nonewline:$nonewline
}

Function Write-BGColor {
    $allColors = ("-Black", "-DarkBlue", "-DarkGreen", "-DarkCyan", "-DarkRed", "-DarkMagenta", "-DarkYellow", "-DarkGray",
                  "-White",     "-Blue",     "-Green",     "-Cyan",     "-Red",     "-Magenta",     "-Yellow", "    -Gray")
    $background = (Get-Host).UI.RawUI.BackgroundColor
    $color = $background
    [bool]$nonewline = $false
    $sofar = ""
    $total = ""

    foreach($arg in $args){
        if($arg -eq "-nonewline") {
            $nonewline = $true
        }
        elseif ($arg -eq "-background") {
            if($sofar) {Write-Host $sofar -background $color -nonewline}
            $color = $background
            $sofar = ""
        }
        elseif($allColors -contains $arg){
            if($sofar) {Write-Host $sofar -background $color -nonewline}
            $color = $arg.substring(1)
            $sofar = ""
        } else {
            $sofar += "$arg"
            $total += "$arg"
        }
    }
    Write-Host $sofar -background $color -nonewline:$nonewline
}

Function Format-PathString {
    param(
        [string]$directorypath,
        [int]$trimto
    )
    $pathLength = $directorypath.Length
    if($pathLength -eq $trimto){
        return $directorypath
    } elseif ($pathLength -lt $trimto) {
        return $directorypath.PadRight($trimto)
    } else {
        return $directorypath.SubString(0,2) + ".." + $directorypath.SubString($pathLength - $trimto + 4, $trimto - 4)
    }
}

Function Expand-PromptLine {
	param(
		[int]$originalLength,
		[string]$padChar,
		[string]$endChar,
		[int]$padLength
	)
	[string]$padString = ""
	if(($originalLength -eq $padLength) -or ($originalLength -gt $padLength)){
		return $inputLine + $endChar
	} elseif ($originalLength -lt $padLength){
		return $padString.PadRight($padLength-$originalLength,$padChar) + $endChar
	} else {
		return "Um..."
		#return $inputLine.SubString(0,2) + ".." + $inputLine.SubString($inputLen - $padLength + 4, $padLength - 4)
	}
}

Function Out-Powerline {
	param(
		[Parameter(Mandatory=$false)]
		[int]$CwdMaxDepth = 5
	)
	$elip = $PowerlineSymbols.general.ellipsis
	$tsep = $PowerlineSymbols.patched.seperator_thin
	$sep  = $PowerlineSymbols.patched.seperator

	$Powerline = ""
	$CurrentDir = $(Resolve-Path .).ProviderPath.Split("\")
	if($CurrentDir.Count -gt $CwdMaxDepth){
		$CurrentDir[2] = $elip
		$Powerline += " " + $CurrentDir[0] + " " + $tsep
		$Powerline += " " + $CurrentDir[1] + " " + $tsep
		$Powerline += " " + $CurrentDir[2] + " " + $tsep
		for($x = $CurrentDir.Count - 3; $x -lt $CurrentDir.Count; $x++ ) {
			if($x -ne $CurrentDir.Count - 1){
				$Powerline += " " + $CurrentDir[$x] + " " + $tsep
			} else {
				$Powerline += " " + $CurrentDir[$x] + " "
			}
		}
	} else {
		for($x = 0; $x -lt $CurrentDir.Count; $x++){
			if($x -ne $CurrentDir.Count - 1){
				$Powerline += " " + $CurrentDir[$x] + " " + $tsep
			} else {
				$Powerline += " " + $CurrentDir[$x] + " "
			}
		}
	}
	Write-BGColor -nonewline -DarkCyan $Powerline
	Write-Color -DarkCyan $sep
}

Function Out-BryPrompt {
	# Stuff we always want:
	$realLastExitCode = $LASTEXITCODE

	# Keep .NET's view of current working directory the same as powershell's
	# (But only if it makes sense from the point of view of the current
	# powershell provider
	if($PWD.Provider.Name -eq 'Filesystem'){
        try{
		    [System.Environment]::CurrentDirectory = $PWD.ProviderPath
        } catch {

        }
	}

	if($global:LyncSession){
		$finalbit = "╰☊■▶"
	} else {
		$finalbit = "╰■■▶"
	}
	#$global:promptMarker = $finalbit

	$DmUser = $([System.Security.Principal.WindowsIdentity]::GetCurrent().Name)

	# Update the windows title as well as the prompt
	$host.UI.RawUI.WindowTitle = $DmUser + " " + $host.Name + " " + $host.Version + " Line: " + $host.UI.RawUI.CursorPosition.Y

	$DateTime  = $(Get-Date).ToString("yyyy-MM-dd hh:mm:ss")

	if($DmUser -eq $Global:WindowsIdentity.Name){
		Write-BGColor -nonewline -DarkGray " $DateTime "
		Write-Host -NoNewLine -ForegroundColor DarkGray -BackgroundColor DarkGreen ""
		Write-BGColor -nonewline -DarkGreen " $DmUser "
		Write-Color   -DarkGreen ""
	} else {
		Write-Color -Cyan "" -Yellow $DateTime -Cyan "" -Cyan "" -Red $DmUser -Cyan ""
	}
	Out-Powerline
	Write-Host -NoNewLine -ForegroundColor Cyan $finalbit
    $global:LASTEXITCODE = $realLastExitCode
    return " "
}

Function ConvertTo-NTAccount ($sid) {
	try{
		(New-Object System.Security.Principal.SecurityIdentifier($sid)).Translate([System.Security.Principal.NtAccount]).Value
	}
	catch [System.Security.Principal.IdentityNotMappedException] {
		Write-Host "Couldn't find mapping for $sid"
	}
}

Function ConvertTo-Sid ($NtAccount) {
    (New-Object System.Security.Principal.NtAccount($NtAccount)).Translate([System.Security.Principal.SecurityIdentifier])
}

Function Request-MSMQMessages {
    param(
        [Parameter(Mandatory=$true)]
        [string]$ComputerName,
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        [string]$queueName
    )
    if($null -eq $global:AdminCreds){
        Get-AdminCredential
    }
    $queues = Get-WmiObject Win32_PerfFormattedData_msmq_MSMQQueue -ComputerName $ComputerName -Credential $AdminCreds
    $queues | ForEach-Object {
        if($_.Name -match $queueName){
            $_.MessagesInQueue
        }
    }
}

Function Get-FileHash {
    param(
        $Path,
        [ValidateSet("MD5","SHA1","SHA256","SHA384","SHA512")]
        $HashAlgorithm = "MD5"
    )

    $hashType = [Type] "System.Security.Cryptography.$HashAlgorithm"
    $hasher = $hashType::Create()

    $files = @()

    if($Path){
         $files += $Path
    } else {
        $files += @($input | ForEach-Object { $_.FullName })
    }

    foreach($file in $files){
        if(-not (Test-Path $file -Type Leaf)) { continue }
        $fileName = $(Resolve-Path $file).Path
        $inputStream = New-Object IO.StreamReader $fileName
        $hashBytes = $hasher.ComputeHash($inputStream.BaseStream)
        $inputStream.Close()

        $builder = New-Object System.Text.StringBuilder
        $hashBytes | ForEach-Object { [void] $builder.Append($_.ToString("X2"))}
        $output = New-Object PsObject -Property @{
            Path = ([IO.Path]::GetFileName($file))
            HashAlgorithm = $HashAlgorithm
            HashValue = $builder.ToString()
        }

        $output | Format-Table -Autosize
    }
}

Function Get-StringHash {
    param(
        [string]$content,
        [ValidateSet("MD5","SHA1","SHA256","SHA384","SHA512")]
        $HashAlgorithm = "MD5"
    )

    $hashType = [Type] "System.Security.Cryptography.$HashAlgorithm"
    $hasher = $hashType::Create()
    $utf8 = New-Object -TypeName System.Text.UTF8Encoding

    $hashBytes = $hasher.ComputeHash($utf8.GetBytes($content))
    $builder = New-Object System.Text.StringBuilder
    $hashBytes | ForEach-Object { [void] $builder.Append($_.ToString("X2"))}

    $output = New-Object PsObject -Property @{
        HashAlgorithm = $HashAlgorithm
        HashValue = $builder.ToString()
    }
    $output | Format-Table -Autosize
}

Function Add-CustomTypeFromDll{
    param(
        [string]$dllName = $(throw "dllName is required"),
        [object]$object
    )

    process {
        if ($_) {
            $object = $_
        }
        if (! $object) {
            throw New-Object -Typename System.Exception "must pass an -object parameter or pipe one in"
        }
        # load the required dll
        $assembly = [System.Reflection.Assembly]::LoadFile($dllName)

        # add each type as a member property
        $assembly.GetTypes() |
        Where-Object {$_.ispublic -and !$_.IsSubclassOf( [Exception] ) -and $_.name -notmatch "event"} |
        Foreach-Object {
            # avoid error messages in case it already exists
            if (! ($object | get-member $_.name)) {
                add-member noteproperty $_.name $_ -inputobject $object
            }
        }
    }
}

Function Add-CustomTypeFromAssembly{
    param(
        [string]$assemblyName = $(throw "assemblyName is required"),
        [object]$object
    )

    process {
        if ($_) {
            $object = $_
        }
        if (! $object) {
            throw New-Object -Typename System.Exception "must pass an -object parameter or pipe one in"
        }
        # load the required dll
        $assembly = [System.Reflection.Assembly]::LoadWithPartialName($assemblyName)

        # add each type as a member property
        $assembly.GetTypes() |
        Where-Object {$_.ispublic -and !$_.IsSubclassOf( [Exception] ) -and $_.name -notmatch "event"} |
        Foreach-Object {
            # avoid error messages in case it already exists
            if (! ($object | get-member $_.name)) {
                add-member noteproperty $_.name $_ -inputobject $object
            }
        }
    }
}

function Test-WebServerSSL {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $true, ValueFromPipeline = $true, Position = 0)]
        [string]$URL,
        [Parameter(Position = 1)]
        [ValidateRange(1,65535)]
        [int]$Port = 443,
        [Parameter(Position = 2)]
        [Net.WebProxy]$Proxy,
        [Parameter(Position = 3)]
        [int]$Timeout = 15000,
        [switch]$UseUserContext
    )

    $definition = `
@"
        using System;
        using System.Net;
        using System.Security.Cryptography.X509Certificates;
        namespace PKI {
            namespace Web {
                public class WebSSL {
                    public Uri OriginalURi;
                    public Uri ReturnedURi;
                    public X509Certificate2 Certificate;
                    //public X500DistinguishedName Issuer;
                    //public X500DistinguishedName Subject;
                    public string Issuer;
                    public string Subject;
                    public string[] SubjectAlternativeNames;
                    public bool CertificateIsValid;
                    //public X509ChainStatus[] ErrorInformation;
                    public string[] ErrorInformation;
                    public HttpWebResponse Response;
                }
            }
        }
"@

    Add-Type -TypeDefinition $definition

    $ConnectString = "https://$url`:$port"
    $WebRequest = [Net.WebRequest]::Create($ConnectString)
    $WebRequest.Proxy = $Proxy
    $WebRequest.Credentials = $null
    $WebRequest.Timeout = $Timeout
    $WebRequest.AllowAutoRedirect = $true
    [Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}
    try {$Response = $WebRequest.GetResponse()}
    catch {}
    if ($null -eq $WebRequest.ServicePoint.Certificate) {
        $Cert = [Security.Cryptography.X509Certificates.X509Certificate2]$WebRequest.ServicePoint.Certificate.Handle
        try {$SAN = ($Cert.Extensions | Where-Object {$_.Oid.Value -eq "2.5.29.17"}).Format(0) -split ", "}
        catch {$SAN = $null}
        $chain = New-Object Security.Cryptography.X509Certificates.X509Chain -ArgumentList (!$UseUserContext)
        [void]$chain.ChainPolicy.ApplicationPolicy.Add("1.3.6.1.5.5.7.3.1")
        $Status = $chain.Build($Cert)
        New-Object PKI.Web.WebSSL -Property @{
            OriginalUri = $ConnectString;
            ReturnedUri = $Response.ResponseUri;
            Certificate = $WebRequest.ServicePoint.Certificate;
            Issuer = $WebRequest.ServicePoint.Certificate.Issuer;
            Subject = $WebRequest.ServicePoint.Certificate.Subject;
            SubjectAlternativeNames = $SAN;
            CertificateIsValid = $Status;
            Response = $Response;
            ErrorInformation = $chain.ChainStatus | ForEach-Object {$_.Status}
        }
        $chain.Reset()
        [Net.ServicePointManager]::ServerCertificateValidationCallback = $null
    } else {
        Write-Error $Error[0]
    }
}

Function Disable-CertificateValidation {
    add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Ssl3, [Net.SecurityProtocolType]::Tls, [Net.SecurityProtocolType]::Tls11, [Net.SecurityProtocolType]::Tls12
}


Function New-ObjectWithAddPropertyScriptMethod {
    <#
    .synopsis
    Return a PSObject with AddProperty ScriptMethod

    .description
    Return a PSObject with AddProperty ScriptMethod that takes two positional arguments, Name and Value.

    .example
    $object = New-ObjectWithAddPropertyScriptMethod
    $object.AddProperty('username', 'johndoe')
    #>

    $record = New-Object -TypeName PSObject;

    Add-Member -Name AddProperty -InputObject $record -MemberType ScriptMethod -Value {
        if ($args.Count -eq 2) {
            ($name, $value) = $args
            if (Get-Member -InputObject $this -Name $name) {
                $this.$name = $value
            } else {
                Add-Member -InputObject $this -MemberType NoteProperty -Name $name -Value $value
            }
        }
    }
    $record
}

Function Get-NslookupSrvRecord {
    <#
    .synopsis
    Get SRV record for specified FQDN(s)

    .description
    PowerShell lacks a way to return SRV records because .NET lacks a way to return SRV records.
    Does not return any data, warning, or errror if no SRV record found.

    .oputputs
    PSObject with properties

    - FQDN: FQDN specified, which is the first line returned that does not begin with whitespace.

    - Text: Array of any lines returned that do not begin with whitespace after the first one.  Can be empty array.

    - Other properties vary with the SRV record.  Any line beginning with whitepsace is split into name/value pairs over the ' = ' delimiter.

    .parameter FQDN
    Specified FQDN(s) for which to attempt to return SRV records

    .example
    Get-NslookupSrvRecord mail.microsoft.com

    #>
    param(
        [Parameter(ValueFromPipeline=$true)]
        [string[]]$FQDN
    )

    Begin {
        $ErrorActionPreference = 'SilentlyContinue'
    }
    Process {
        Foreach ($_fqdn in $FQDN) {
            $fullcommand = "& nslookup.exe -q=srv " + $fqdn
            #$cmd = "set type=srv`n$fqdn"
            Write-Verbose "$($MyInvocation.MyCommand.Name) -FQDN $_fqdn"

            $record = $null
            $data = @()

#            $cmd | nslookup.exe 2>&1 | ? {
            Invoke-Expression $fullcommand | Where-Object {
                $_ -and
                $_ -notmatch '^Address:' -and
                $_ -notmatch '^Server:' -and
                $_ -notmatch '^Default Server:' -and
                $_ -notmatch '^>'
            } |
            ForEach-Object {
                Write-Debug $_
                Switch -Regex ($_)
                {
                    "^[^\s]" {
                        if ($record) {
                            $data += $_
                        } else {
                            $record = New-ObjectWithAddPropertyScriptMethod
                            $record.AddProperty('FQDN', ($_ -replace '\s.*'))
                        }
                    }
                    "^\s" {
                        if ($_ -match ' = ') {
                            $name = $_ -replace '^\s*' -replace '\s.* = .*'
                            $value = $_ -replace '.* = ' -replace '\s*$'
                            $record.AddProperty($name, $value)
                        } else {
                            $data += $_
                        }
                    }
                }
            }
            $record.AddProperty('Text', $data)
            $record
        }
    }
}

Function Test-Socket{
    param(
        [Parameter(Mandatory=$true)]
        [string]$hostname,
        [Parameter(Mandatory=$true)]
        [ValidateRange(1,65535)]
        [int]$port
    )

    try{
        $socket = New-Object Net.Sockets.TcpClient($hostname,$port)
        $socket
        Remove-Variable socket
    } catch {
        throw New-Object System.Exception "Failed"
    }
}

Function Get-DomainController{
    $nltest  = &nltest /dclist:fm
    $pattern = '(?x)                              (?# regex mode specifier allowing white space and comments)
                ^\s{4}                            (?# first four leading spaces)
                (\w{10})                          (?# match leading part of host name, and assign to capture group 1)
                \.fm\.rbsgrp\.net                 (?# match remaing part of the host name)
                (?:                               (?# Open passive non-capturing group)
                (?:\s{8})                         (?# Sub passive group to match 8 spaces)
                |                                 (?# or)
                (?:\s\[PDC\]\s{2})                (?# Sub passive group to match " [PDC]  ")
                )                                 (?# close parent passive group)
                \[DS\]\sSite\:\WLON              (?# match "[DS] Site: London")'
    $dclist  = $nltest | Select-String -Pattern $pattern
    $dcarray = @()
    foreach($dc in $dclist){
        $dcarray += $dc.Matches[0].Groups[1].Value
    }
    return $dcarray
}

Function ConvertFrom-UnixEpoch{
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        [int64]$UnixEpoch
    )
    [datetime]$origin = '1970-01-01 00:00:00'
	if($UnixEpoch -gt 1000000000000){
		return $origin.AddMilliseconds($UnixEpoch)
	}else{
		return $origin.AddSeconds($UnixEpoch)
	}
}

Function Get-Netstats{
    if (-not ([System.Management.Automation.PSTypeName]'IpHlpApidotnet.IPHelper').Type) {
        $iph = New-Object -TypeName PSObject
        $iph | Add-CustomTypeFromDll "C:\Users\childsb\appdata\Roaming\Powershell\dot_Net\IpHlpApi.net.dll"
    }
    [IpHlpApidotnet.IPHelper]$iphelper = New-Object $iph.IPHelper
    $iphelper.GetTcpConnexions()

    $iphelper.tcpConnexion | Select-Object -ExpandProperty table
}

Function Search-AD{
	param(
		[Parameter(Mandatory=$true)]
		[string]$LdapFilter,
		[Parameter(Mandatory=$false)]
		[string[]]$PropsToLoad=@(),
		[Parameter(Mandatory=$false)]
		[ValidateSet("P","UAT","ENG")]
		[string]$Environment="P",
		[Parameter(Mandatory=$false)]
		[ValidateSet("Default","Config","RootDSE","Schema")]
		[string]$Context="Default",
		[Parameter(Mandatory=$false)]
		[ValidateSet($true,$false)]
		[bool]$AsAdmin
	)

	if($Environment -ne "P" -and !($global:adCreds)){
		switch($Environment){
			"UAT" { $global:adCreds = Get-Credential -Credential "TEST\t.43409161.1" }
			"ENG" { $global:adCreds = Get-Credential }
		}
	}
	if($Environment -eq "UAT"){
		$global:directoryEntry = "LDAP://TEST.MSAD.UBSTEST.NET/OU=Accounts,DC=TEST,DC=MSAD,DC=UBSTEST,DC=NET"
	} elseif ($Environment -eq "ENG") {
		$global:directoryEntry = "LDAP://ENG.WINTEL.UBSENG.NET/OU=Accounts,DC=ENG,DC=WINTEL,DC=UBSENG,DC=NET"
	} else {
		if($AsAdmin -eq $true){
			$global:adCreds = Get-Credential -Credential "UBSPROD\43409161.1"
		}
		if($context -eq "Config"){
			$global:directoryEntry = "LDAP://UBSPROD.MSAD.UBS.NET/CN=Configuration,DC=UBSPROD,DC=MSAD,DC=UBS,DC=NET"
		}else{
			$global:directoryEntry="LDAP://UBSPROD.MSAD.UBS.NET/dc=UBSPROD,DC=MSAD,DC=UBS,DC=NET"
		}
	}

	if($global:adCreds){
		$domain = New-Object DirectoryServices.DirectoryEntry($global:directoryEntry,$global:adCreds.GetNetworkCredential().username,$global:adCreds.GetNetworkCredential().password)
	} else {
		$domain = New-Object DirectoryServices.DirectoryEntry($global:directoryEntry)
	}

	$searcher = New-Object DirectoryServices.DirectorySearcher

    $searcher.SearchRoot = $domain
    $searcher.PageSize = $pageSize
    $searcher.Filter = $ldapFilter
    $searcher.SearchScope = "Subtree"

    if(!($propsToLoad.Count -eq 0)){
        foreach($i in $propsToLoad){
            $searcher.PropertiesToLoad.Add($i)
        }
    }

    $results = $searcher.FindAll()
    $output = @()

    foreach($resultset in $results){
        $properties = New-Object 'System.Collections.Generic.SortedDictionary[string,string]'
        $one_ad_object = $resultset.Properties
        $one_ad_object.GetEnumerator() | ForEach-Object {
			$properties.Add($_.Key,$_.Value)
        }
        $output += $properties
    }

    return $output
}

Function Set-Alert{
    param(
        [Parameter(Mandatory=$true)]
        [datetime]$AlarmTime,
        [Parameter(Mandatory=$false)]
        [string]$Message="Message!",
        [Parameter(Mandatory=$false)]
        [string]$Title="Title!"
    )
    #[datetime]$alarmTime = "November 7, 2013 10:30:00 PM"
    $nowTime = get-date
    $tsSeconds = ($AlarmTime - $nowTime).TotalSeconds
    $timeSpan = New-TimeSpan -Seconds $tsSeconds

    $timer = New-Object System.Timers.Timer
    $timer.Autoreset = $false
    $timer.Interval = $timeSpan.TotalMilliseconds
    $timer.Enabled = $true

    $eventData = New-Object -Typename PSObject
    $eventData | Add-Member -MemberType NoteProperty -Name "Message" -Value $Message
    $eventData | Add-Member -MemberType NoteProperty -Name "Title" -Value $Title

    Register-ObjectEvent -InputObject $timer -EventName Elapsed -MessageData $eventData -Action {
        [System.Windows.Forms.MessageBox]::Show($event.MessageData.Message , $event.MessageData.Title)
    } | Out-Null
}

Function Format-MediaEngineLog {
    param(
        [Parameter(Mandatory=$true)]
        $Logfile,
        [Parameter(Mandatory=$true)]
        [string]$Component,
        [Parameter(Mandatory=$false)]
        [string]$SubComponent,
        [Parameter(Mandatory=$false)]
        [string]$Message
    )
    $pattern = '(?x)                                        (?# regex mode specifier allowing white space and comments)
                ^                                           (?# beginning of line anchor)
                (\w+)                                       (?# Match component name writing this entry - group 1)
                (\[\d+])                                    (?# Component process id - group 2)
                \:\s                                        (?# colon and space after component name)
                (\w{3}\s\d{2}\s\d{2}\:\d{2}\:\d{2}\.\d{3})  (?# Date and time - group 3)
                \s*                                         (?# possible space after date and time)
                (\[[\w\:\s/]+\])?                           (?# subcomponent name - group 4)
                \s*                                         (?# possible space after subcomponent name)
                (.+)                                        (?# contents of log message - group 5)
                $                                           (?# end of line anchor)'
    [int]$extra = 0
    $findings = $Logfile | Select-String -Pattern $pattern
    foreach($find in $findings){
        if($find.Matches.Groups[1].Value -eq $Component){
            if($SubComponent -ne $null -and $find.Matches.Groups[4].Value -eq $SubComponent){
                $extra = 1
                $find
            }
            if($Message -ne $null -and $find.Matches.Groups[5].Value -match $Message){
                $extra = 1
                $find
            }
            if($extra -eq 0){
                $find
            }
        }
    }
}

Function Update-HelpWithCreds{
    Write-Warning "Remember to run this as local admin!"
    $browser = New-Object System.Net.WebClient
    $credentials = Get-Credential
    $browser.Proxy.Credentials = $credentials
    Update-Help
}

Function Get-RemoteRegistry{
    param(
        [Parameter(Mandatory=$true,Position=0)]
        [string[]]$Computers,
        [Parameter(Mandatory=$true,Position=1)]
        [string]$Key
    )
    $UserName = Read-Host -Prompt "Enter Admin User name"
    $Password = Read-Host -Prompt "Enter Admin password" -AsSecureString
    $Results  = [ordered]@{}
    # HKLM constant
    $Hive     = 2147483650

    $Computers | ForEach-Object {
        $Computer = $_
        $path="\\$Computer\root\default:stdregprov"
        $opt = New-Object Management.ConnectionOptions
        $opt.Username = $UserName
        $opt.SecurePassword = $Password

        $scope = New-Object Management.ManagementScope $path, $opt
        $gopt = New-Object Management.ObjectGetOptions
        try{
            $scope.Connect()
            $class = New-Object Management.ManagementClass $scope,$path,$gopt
            $RegValueNames = $class.EnumValues($Hive,$Key)
            $MachineResults = [ordered]@{}
            foreach($valueName in $RegValueNames.sNames){
                $value = $class.GetStringValue($Hive,$Key,$ValueName).sValue
                $MachineResults[$ValueName]=$Value
            }
        } catch {
            $Exception = $_
            $Warning = "Connection to " + $Computer + " failed"
            Write-Warning $Warning
            Write-Warning $Exception
        }
        $Results[$Computer]=$MachineResults
    }
    return $Results
}

Function Send-Email{
    param(
        [Parameter(Mandatory=$true,Position=0)]
        [string]$Recipient,
        [Parameter(Mandatory=$true,Position=1)]
        [string]$Subject,
        [Parameter(Mandatory=$true,Position=2,ValueFromPipeline=$true)]
        [string]$Body,
        [Parameter(Mandatory=$false,Position=3)]
        [bool]$AsHtml=$true
    )
    $ol = New-Object -comObject Outlook.Application

    # call the save method to save the email in the drafts folder
    $mail = $ol.CreateItem(0)
    $null = $mail.Recipients.Add($Recipient)
    $mail.Subject = $Subject
    if($AsHtml){
        $mail.HTMLBody = $Body
    } else {
        $mail.Body = $Body
    }
    $mail.Save()

    # get it back from drafts and update the body
    $draft = $mail.GetInspector
    $draft.Display()
}

Function Send-Clipboard{
    param(
        [Parameter(Mandatory=$true,Position=0)]
        [string]$Recipient,
        [Parameter(Mandatory=$false,Position=1)]
        [string]$Subject="Powershell code",
        [Parameter(Mandatory=$false,Position=2)]
        [bool]$AsHtml=$true,
        [Parameter(Mandatory=$false,Position=3)]
        [bool]$IncludeSignature=$true
    )

    if( $null -eq $(Get-Module "Pscx")){
        Write-Error "Needs Pscx module loaded"
        Return $false
    }
    if( $null -eq  $(Get-Module "Highlight-Syntax")){
        Write-Error "Needs Highlight-Syntax module loaded"
        Return $false
    }
    $clip = Get-Clipboard
    $code = $clip | Highlight-Syntax -IncludeSignature:$true
    $body = '<p style="font-family:Calibri;">Powershell:<\p>' + $code
    Send-Email -Recipient $Recipient -Subject $Subject -Body $body -AsHtml:$true
}


Function Start-SSHAgent{
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseDeclaredVarsMoreThanAssignments","", Justification="It is used - just not here.")]
    param(
        [Parameter(Mandatory=$false,ValueFromPipeline=$true)]
        [string]$socketfile="/p/.ssh-socket",                       # Used as input to ssh-agent, which expects POSIX format paths
        [Parameter(Mandatory=$false)]
        [string]$pidFile="p:\.ssh-agent-pid"                        # Used as input to Out-File which expects Windows format paths
    )
    $env:SSH_AUTH_SOCK=$socketfile
    $agent_is_running = Get-Process | Where-Object { $_.ProcessName -like "ssh-agent*"}
    if($null -eq $agent_is_running){
        $sshAgentOutput = ssh-agent -a $env:SSH_AUTH_SOCK
        $parse = Select-String -InputObject $sshAgentOutput -Pattern "(?m)SSH_AGENT_PID=(\d+)"
        $sshAgentPid = $parse.Matches[0].Groups[1].Value
        $sshAgentPid | Out-File $pidFile
        $env:SSH_AGENT_PID = $sshAgentPid
    }
}

Function Compare-ComplexObject{
    [CmdletBinding()]
    param (
        [parameter(
            Position=0,
            Mandatory=$true
        )]
        [AllowEmptyCollection()]
        $ReferenceObject,
        [parameter(
            Position=1,
            Mandatory=$true,
            ValueFromPipeline=$true
        )]
        [AllowEmptyCollection()]
        $DifferenceObject,
        [int]$SyncWindow,
        [array]$Property,
        [switch]$ExcludeDifferent,
        [switch]$IncludeEqual,
        [string]$Culture,
        [switch]$CaseSensitive
    )

    $newCompareParameters = @{} + $psBoundParameters

    if ($Property) {
        $newCompareParameters.Remove("Property")
        $newCompareParameters.ReferenceObject = $ReferenceObject | Select-Object -Property $Property | Format-List | Out-String -Stream
        $newCompareParameters.DifferenceObject = $DifferenceObject | Select-Object -Property $Property | Format-List | Out-String -Stream
    }
    else {
        $newCompareParameters.ReferenceObject = $ReferenceObject | Format-List * | Out-String -Stream
        $newCompareParameters.DifferenceObject = $DifferenceObject | Format-List * | Out-String -Stream
    }

    Compare-Object @newCompareParameters
}

Function Use-GarbageCollection{
    [GC]::Collect()
}

Function Get-LDAPObjectwithAuth {
<#
.DESCRIPTION
Sets an LDAP User object attribute to specified Value
.EXAMPLES
SetLDAPObject -LDAPServer "LDAPSERVER Name" -LDAPPort 389 -SSL $false -baseDN "O=BASEDN" -Filter "(uid=smithj01)"

#Test example:

$NovellCred = Get-Credential -Message "Enter your Novell Username and Password. Example username: smithj" # Getting the Novell Credential Information

$EDIRLDAPServer = "LDAPSERVER"
$EDIRBaseDN = "O=TEST"
$userID = $NovellCred.UserName.ToUpper()
$TrgtUserID = "smitha"


$PRODLDAPObj = GetLDAPObject -LDAPServer $EDIRLDAPServer -LDAPPort 389 -SSL $false -baseDN $EDIRBaseDN -Filter "(uid=$($userID))"

$userDN = ""
$userDN = (($PRODLDAPObj | select "DistinguishedName" | Out-String).Split() | Select-String -Pattern '^(\w+[=]{1}\w+)([,{1}]\w+[=]{1}\w+)*$').ToString().ToUpper()

$LDAPObj2 = GetLDAPObjectwithAuth -LDAPServer $EDIRLDAPServer -LDAPPort 636 -SSL $true -baseDN $EDIRBaseDN -Filter "(uid=$($TrgtUserID))" -UserName $userDN.toString().ToUpper() -SecPassWord $NovellCred.Password

"Fullname is: $($LDAPObj2.attributes["fullname"].getValues([string]))"


$trgtUserDN = (($LDAPObj2 | select "DistinguishedName" | Out-String).Split() | Select-String -Pattern '^(\w+[=]{1}\w+)([,{1}]\w+[=]{1}\w+)*$').ToString().ToUpper()

"DN is: $($trgtUserDN)"

#>

	param(
		[string]$LDAPServer,
		[int]$LDAPPort,
		[boolean]$SSL,
		[string]$baseDN,
		[string]$Filter,
		[string]$UserName,
		[securestring]$SecPassWord
	)

	#Load the assemblies
	[System.Reflection.Assembly]::LoadWithPartialName("System.DirectoryServices.Protocols") | Out-Null
	[System.Reflection.Assembly]::LoadWithPartialName("System.Net") | Out-Null

	#Connects to LDAP Server using SSL on a non-standard port
	$c = New-Object System.DirectoryServices.Protocols.LdapConnection "$($LDAPServer):$($LDAPPort)"
	#Set session options
	$c.SessionOptions.SecureSocketLayer = $SSL;
	$c.SessionOptions.VerifyServerCertificate = { return $true;} #needed for self-signed certificates

	# Pick Authentication type:
	# Anonymous, Basic, Digest, DPA (Distributed Password Authentication),
	# External, Kerberos, Msn, Negotiate, Ntlm, Sicily
	$c.AuthType = [System.DirectoryServices.Protocols.AuthType]::Basic

	# Gets username and password. Required for Authentication Types requiring Credentials
	#$user = Read-Host -Prompt "Username"
	#$pass = Read-Host -AsSecureString "Password"
	#Creates a credential object to pass to bind to LDAP Connection Object
	$NovellCredentials = new-object "System.Net.NetworkCredential" -ArgumentList $UserName,$SecPassWord

	# Bind with the network credentials. Depending on the type of server,
	# the username will take different forms. Authentication type is controlled
	# above with the AuthType
	$c.Bind($NovellCredentials);
	$scope = [System.DirectoryServices.Protocols.SearchScope]::Subtree
	$attrlist = ,"*" #Returns all Attributes
	$r = New-Object System.DirectoryServices.Protocols.SearchRequest -ArgumentList $baseDN,$Filter,$scope,$attrlist

	#$re is a System.DirectoryServices.Protocols.SearchResponse
	$re = $c.SendRequest($r);

	#How many results do we have?

	#"A Total of $($re.Entries.Count) Entry(s) found in LDAP Search"

	If ($re.Entries.Count -eq 1) {
		try {
			$object = @{}
			foreach($pair in $re.Attributes[0].GetEnumerator()) {
				$object[$pair.Name] = $re.Attributes[$pair.Name][0]
			}
			return $object
			#Return $Re.Entries[0]
		} catch {
			Write-Host $Re.Entries[0] | Get-Member
		}
	} elseIf ($re.Entries.Count -eq 0) {
		Return $null
	} else {
		$readable = @()
		foreach ($result in $re.Entries.Attributes) {
			$user = @{}
			foreach($pair in $result.Attributes.GetEnumerator()) {
				$user[$pair.Name] = $result.Attributes[$pair.Name][0]
			}
			$readable += $user
		}
		#Return $re.Entries
		return $readable
	}

} #End Function

Function Get-OSEdition {
	foreach($pool in $LyncEFEServers.Keys){
		foreach($server in $LyncEFEServers.Item($pool)){
			$OS = Get-WMIObject -Class Win32_OperatingSystem -Namespace root/cimv2 -ComputerName $server -ErrorAction SilentlyContinue
			Write-Host $server is running $OS.Caption
		}
	}
}

function Invoke-WindowsAPI {
	param(
		[string] $dllName,
		[Type] $returnType,
		[string] $methodName,
		[Type[]] $parameterTypes,
		[Object[]] $parameters
    )

	## Begin to build the dynamic assembly
	$domain = [AppDomain]::CurrentDomain
	$name = New-Object Reflection.AssemblyName 'PInvokeAssembly'
	$assembly = $domain.DefineDynamicAssembly($name, 'Run')
	$module = $assembly.DefineDynamicModule('PInvokeModule')
	$type = $module.DefineType('PInvokeType', "Public,BeforeFieldInit")

	## Go through all of the parameters passed to us.  As we do this,
	## we clone the user's inputs into another array that we will use for
	## the P/Invoke call.
	$inputParameters = @()
	$refParameters = @()

	for($counter = 1; $counter -le $parameterTypes.Length; $counter++){
		## If an item is a PSReference, then the user
		## wants an [out] parameter.
		if($parameterTypes[$counter - 1] -eq [Ref]) {
			## Remember which parameters are used for [Out] parameters
			$refParameters += $counter

			## On the cloned array, we replace the PSReference type with the
			## .Net reference type that represents the value of the PSReference,
			## and the value with the value held by the PSReference.
			$parameterTypes[$counter - 1] =
			$parameters[$counter - 1].Value.GetType().MakeByRefType()
			$inputParameters += $parameters[$counter - 1].Value
		}
		else {
			## Otherwise, just add their actual parameter to the
			## input array.
			$inputParameters += $parameters[$counter - 1]
		}
	}

	## Define the actual P/Invoke method, adding the [Out]
	## attribute for any parameters that were originally [Ref]
	## parameters.
	$method = $type.DefineMethod($methodName, 'Public,HideBySig,Static,PinvokeImpl', $returnType, $parameterTypes)
	foreach($refParameter in $refParameters){
		[void] $method.DefineParameter($refParameter, "Out", $null)
	}

	## Apply the P/Invoke constructor
	$ctor = [Runtime.InteropServices.DllImportAttribute].GetConstructor([string])
	$attr = New-Object Reflection.Emit.CustomAttributeBuilder $ctor, $dllName
	$method.SetCustomAttribute($attr)

	## Create the temporary type, and invoke the method.
	$realType = $type.CreateType()

	$realType.InvokeMember($methodName, 'Public,Static,InvokeMethod', $null, $null, $inputParameters)

	## Finally, go through all of the reference parameters, and update the
	## values of the PSReference objects that the user passed in.
	foreach($refParameter in $refParameters) {
		$parameters[$refParameter - 1].Value = $inputParameters[$refParameter - 1]
	}
}

function Get-PrivateProfileString {
	param(
		$file,
		$category,
		$key
	)

	## Prepare the parameter types and parameter values for the Invoke-WindowsApi script
	$returnValue    = New-Object System.Text.StringBuilder 500
	$parameterTypes = [string], [string], [string], [System.Text.StringBuilder], [int], [string]
	$parameters    = [string] $category, [string] $key, [string] "",

	[System.Text.StringBuilder] $returnValue, [int] $returnValue.Capacity, [string] $file

	## Invoke the API
	[void] (Invoke-WindowsApi "kernel32.dll" ([UInt32]) "GetPrivateProfileString" $parameterTypes $parameters)

	## And return the results
	$returnValue.ToString()
}

Function ConvertFrom-SDDL {
<#
.SYNOPSIS
    Convert a raw security descriptor from SDDL form to a parsed security descriptor.
.DESCRIPTION
    ConvertFrom-SDDL generates a parsed security descriptor based upon any string in
    raw security descriptor definition language (SDDL) form. ConvertFrom-SDDL will
    parse the SDDL regardless of the type of object the security descriptor represents.
.PARAMETER RawSDDL
    Specifies the security descriptor in raw SDDL form.
.EXAMPLE
    ConvertFrom-SDDL -RawSDDL 'D:PAI(A;;0xd01f01ff;;;SY)(A;;0xd01f01ff;;;BA)(A;;0x80120089;;;NS)'
.EXAMPLE
    'O:BAG:SYD:(D;;0xf0007;;;AN)(D;;0xf0007;;;BG)(A;;0xf0005;;;SY)(A;;0x5;;;BA)', 'O:BAG:SYD:PAI(D;OICI;FA;;;BG)(A;OICI;FA;;;BA)(A;OICIIO;FA;;;CO)(A;OICI;FA;;;SY)' | ConvertFrom-SDDL
.INPUTS
    System.String
    ConvertFrom-SDDL accepts SDDL strings from the pipeline
.OUTPUTS
    System.Management.Automation.PSObject
#>

    Param (
        [Parameter( Position = 0, Mandatory = $True, ValueFromPipeline = $True )]
        [ValidateNotNullOrEmpty()]
        [String[]]
        $RawSDDL
    )

    Set-StrictMode -Version 2

    # Get reference to sealed RawSecurityDescriptor class
    $RawSecurityDescriptor = [Int].Assembly.GetTypes() | Where-Object { $_.FullName -eq 'System.Security.AccessControl.RawSecurityDescriptor' }

    # Create an instance of the RawSecurityDescriptor class based upon the provided raw SDDL
    try {
        $Sddl = [Activator]::CreateInstance($RawSecurityDescriptor, [Object[]] @($RawSDDL))
    } catch [Management.Automation.MethodInvocationException] {
        throw $Error[0]
    }

    if ($null -eq $Sddl.Group){
        $Group = $null
    } else {
        $SID = $Sddl.Group
        $Group = $SID.Translate([Security.Principal.NTAccount]).Value
    }

    if ($null -eq $Sddl.Owner){
        $Owner = $null
    } else {
        $SID = $Sddl.Owner
        $Owner = $SID.Translate([Security.Principal.NTAccount]).Value
    }

    $ObjectProperties = @{
        Group = $Group
        Owner = $Owner
    }

    if ($null -eq $Sddl.DiscretionaryAcl) {
        $Dacl = $null
    } else {
        $DaclArray = New-Object PSObject[](0)

        $ValueTable = @{}

        $EnumValueStrings = [Enum]::GetNames([System.Security.AccessControl.CryptoKeyRights])
        $CryptoEnumValues = $EnumValueStrings | ForEach-Object {
            $EnumValue = [Security.AccessControl.CryptoKeyRights] $_
            if (-not $ValueTable.ContainsKey($EnumValue.value__)) {
                $EnumValue
            }
            $ValueTable[$EnumValue.value__] = 1
        }

        $EnumValueStrings = [Enum]::GetNames([System.Security.AccessControl.FileSystemRights])
        $FileEnumValues = $EnumValueStrings | ForEach-Object {
            $EnumValue = [Security.AccessControl.FileSystemRights] $_
            if (-not $ValueTable.ContainsKey($EnumValue.value__)) {
                $EnumValue
            }

            $ValueTable[$EnumValue.value__] = 1
        }

        $EnumValues = $CryptoEnumValues + $FileEnumValues

        foreach ($DaclEntry in $Sddl.DiscretionaryAcl) {
            $SID = $DaclEntry.SecurityIdentifier
            $Account = $SID.Translate([Security.Principal.NTAccount]).Value

            $Values = New-Object String[](0)

            # Resolve access mask
            foreach ($Value in $EnumValues) {
                if (($DaclEntry.Accessmask -band $Value) -eq $Value) {
                    $Values += $Value.ToString()
                }
            }

            $Access = "$($Values -join ',')"

            $DaclTable = @{
                Rights = $Access
                IdentityReference = $Account
                IsInherited = $DaclEntry.IsInherited
                InheritanceFlags = $DaclEntry.InheritanceFlags
                PropagationFlags = $DaclEntry.PropagationFlags
            }

            if ($DaclEntry.AceType.ToString().Contains('Allowed')) {
                $DaclTable['AccessControlType'] = [Security.AccessControl.AccessControlType]::Allow
            } else {
                $DaclTable['AccessControlType'] = [Security.AccessControl.AccessControlType]::Deny
            }

            $DaclArray += New-Object PSObject -Property $DaclTable
        }

        $Dacl = $DaclArray
    }

    $ObjectProperties['Access'] = $Dacl

    $SecurityDescriptor = New-Object PSObject -Property $ObjectProperties

    Write-Output $SecurityDescriptor
}

function Copy-WithProgress {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseDeclaredVarsMoreThanAssignments","", Justification="It is used - you're just wrong")]
    [CmdletBinding()]
    param (
            [Parameter(Mandatory = $true)]
            [string] $Source
        , [Parameter(Mandatory = $true)]
            [string] $Destination
        , [int] $Gap = 200
        , [int] $ReportGap = 2000
    )
    # Define regular expression that will gather number of bytes copied
    $RegexBytes = '(?<=\s+)\d+(?=\s+)';

    #region Robocopy params
    # MIR = Mirror mode
    # NP  = Don't show progress percentage in log
    # NC  = Don't log file classes (existing, new file, etc.)
    # BYTES = Show file sizes in bytes
    # NJH = Do not display robocopy job header (JH)
    # NJS = Do not display robocopy job summary (JS)
    # TEE = Display log in stdout AND in target log file
    $CommonRobocopyParams = '/MIR /NP /NDL /NC /BYTES /NJH /NJS';
    #endregion Robocopy params

    #region Robocopy Staging
    Write-Verbose -Message 'Analyzing robocopy job ...';
    $StagingLogPath = '{0}\temp\{1} robocopy staging.log' -f $env:windir, (Get-Date -Format 'yyyy-MM-dd hh-mm-ss');

    $StagingArgumentList = '"{0}" "{1}" /LOG:"{2}" /L {3}' -f $Source, $Destination, $StagingLogPath, $CommonRobocopyParams;
    Write-Verbose -Message ('Staging arguments: {0}' -f $StagingArgumentList);
    Start-Process -Wait -FilePath robocopy.exe -ArgumentList $StagingArgumentList -NoNewWindow;
    # Get the total number of files that will be copied
    $StagingContent = Get-Content -Path $StagingLogPath;
    # $FileCount = $StagingContent.Count;

    # Get the total number of bytes to be copied
    [RegEx]::Matches(($StagingContent -join "`n"), $RegexBytes) | ForEach-Object { $BytesTotal = 0; } { $BytesTotal += $_.Value; };
    Write-Verbose -Message ('Total bytes to be copied: {0}' -f $BytesTotal);
    #endregion Robocopy Staging

    #region Start Robocopy
    # Begin the robocopy process
    $RobocopyLogPath = '{0}\temp\{1} robocopy.log' -f $env:windir, (Get-Date -Format 'yyyy-MM-dd hh-mm-ss');
    $ArgumentList = '"{0}" "{1}" /LOG:"{2}" /ipg:{3} {4}' -f $Source, $Destination, $RobocopyLogPath, $Gap, $CommonRobocopyParams;
    Write-Verbose -Message ('Beginning the robocopy process with arguments: {0}' -f $ArgumentList);
    $Robocopy = Start-Process -FilePath robocopy.exe -ArgumentList $ArgumentList -Verbose -PassThru -NoNewWindow;
    Start-Sleep -Milliseconds 100;
    #endregion Start Robocopy

    #region Progress bar loop
    while (!$Robocopy.HasExited) {
        Start-Sleep -Milliseconds $ReportGap;
        $BytesCopied = 0;
        $LogContent = Get-Content -Path $RobocopyLogPath;
        $BytesCopied = [Regex]::Matches($LogContent, $RegexBytes) | ForEach-Object -Process { $BytesCopied += $_.Value; } -End { $BytesCopied; };
        Write-Verbose -Message ('Bytes copied: {0}' -f $BytesCopied);
        Write-Verbose -Message ('Files copied: {0}' -f $LogContent.Count);
        Write-Progress -Activity Robocopy -Status ("Copied {0} files; Copied {1} of {2} bytes" -f $LogContent.Count, $BytesCopied, $BytesTotal) -PercentComplete (($BytesCopied/$BytesTotal)*100);
    }
    #endregion Progress loop

    #region Function output
    [PSCustomObject]@{
        BytesCopied = $BytesCopied;
        FilesCopied = $LogContent.Count;
    };
    #endregion Function output
}

Function ConvertTo-Base64String{
	param(
		[Parameter(Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[string]$InputString
	)
	[byte[]]$InputBytes = [Text.Encoding]::UTF8.GetBytes($InputString)
	return [Convert]::ToBase64String($InputBytes)
}

Function ConvertFrom-Base64String{
	param(
		[Parameter(Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[string]$InputString
	)
	[byte[]]$InputBytes = [Convert]::FromBase64String($InputString)
	return [Text.Encoding]::UTF8.GetString($InputBytes)
}


Function PBE_Encrypt{
	param(
		[Parameter(Mandatory=$true)]
		[string]$clearText
	)
	# Manju's salt, turned into unsigned byte values from
	# byte[] salt = { -87, -101, -56, 50, 86, 52, -29, 3 };
	[byte[]]$salt = @( 0xa9, 0x9b, 0xc8, 0x32, 0x56, 0x34, 0xe3, 0x03 )
	[string]$passphrase = "ILikeMySamplePhrase98765"
	$crypto = New-Object Demo.Cryptography.PKCSKeyGenerator($passphrase,$salt,19,1)
	$encrypt = $crypto.Encryptor
	$cipherBytes = $encrypt.TransformFinalBlock([System.Text.Encoding]::UTF8.GetBytes($clearText),0,$clearText.Length)
	return [System.Convert]::ToBase64String($cipherBytes)
}

Function PBE_Decrypt{
	param(
		[Parameter(Mandatory=$true)]
		[string]$cipherText
	)
	[byte[]]$salt = @( 0xa9, 0x9b, 0xc8, 0x32, 0x56, 0x34, 0xe3, 0x03 )
	[string]$passphrase = "ILikeMySamplePhrase98765"
	$crypto = New-Object Demo.Cryptography.PKCSKeyGenerator($passphrase,$salt,19,1)
	$decrypt = $crypto.Decryptor
	$cipherBytes = [System.Convert]::FromBase64String($cipherText)
	$clearBytes = $decrypt.TransformFinalBlock($cipherBytes,0,$cipherBytes.Length)
	return [System.Text.Encoding]::UTF8.GetString($clearBytes)
}

Function Get-HashtableAsObject{
	param(
		[Parameter(Mandatory=$true)]
		[Hashtable]$hashtable
	)
	#.Synopsis
	#   Turns a Hashtable into a Powershell object
	#.Description
	#   Creates a new object from a hashtable
	#.Example
	#   # Creates a new object with a property foo and the value bar
	#   Get-HashtableAsObject @{"Foo"="Bar"}
	#.Example
	#   # Create a new object with a property Random and a value
	#   # that is generated each time the property is retrieved
	#   Get-HashtableAsObject @{"Random"= { Get-Random }}
    #.Example
	#   # Create a new object from a hashtable with nested hashtables
    #   Get-HashtableAsObject @{"Foo"=@{"Random"={Get-Random}}}
	process{
		$outputObject = New-Object Object
		if($hashtable -and $hashtable.Count){
			$hashtable.GetEnumerator() | ForEach-Object {
				if ($_.Value -is [ScriptBlock]){
					$outputObject = $outputObject | Add-Member ScriptProperty $_.Key $_.Value -passThru
				}else{
					if($_.Value -is [Hashtable]){
						$outputObject = $outputObject | Add-Member NoteProperty $_.Key (Get-HashtableAsObject $_.Value) -passThru
					}else{
						$outputObject = $outputObject | Add-Member NoteProperty $_.Key $_.Value -passThru
					}
				}
			}
		}
	return $outputObject
	}
}

function Find-WinAPIFunction {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseDeclaredVarsMoreThanAssignments","", Justification="It is used - you're just wrong")]
<#
.SYNOPSIS
    Searches all loaded assemblies in your PowerShell session for a
    Windows API function.
.PARAMETER Module
    Specifies the name of the module that implements the function. This
    is typically a system dll (e.g. kernel32.dll).
.PARAMETER FunctionName
    Specifies the name of the function you're searching for.
.OUTPUTS
    [System.Reflection.MethodInfo]
.EXAMPLE
    Find-WinAPIFunction kernel32.dll CopyFile
#>
    [CmdletBinding()]
    [OutputType([System.Reflection.MethodInfo])]
    Param
    (
        [Parameter(Mandatory = $True, Position = 0)]
        [ValidateNotNullOrEmpty()]
        [String]
        $Module,
        [Parameter(Mandatory = $True, Position = 1)]
        [ValidateNotNullOrEmpty()]
        [String]
        $FunctionName
    )
    [System.AppDomain]::CurrentDomain.GetAssemblies() |
      ForEach-Object { $_.GetTypes() } |
      ForEach-Object { $_.GetMethods('NonPublic, Public, Static') } |
      ForEach-Object { $MethodInfo = $_; $_.GetCustomAttributes($false) } |
      Where-Object {
          $MethodInfo.Name.ToLower() -eq $FunctionName.ToLower() -and
          $_.Value -eq $Module
      } | ForEach-Object { $MethodInfo }
}

function Get-DTPUser {
	param(
		[Parameter(Mandatory=$true, ParameterSetName="GPN", Position=0)]
		[string]$GPN,
		[Parameter(Mandatory=$true, ParameterSetName="TNUM", Position=0)]
		[string]$TNumber
	)
    if($null -eq $(Get-Module "ActiveDirectory")){
        Write-Error "Needs ActiveDirectory module loaded"
        Exit
    }
	switch($PSCmdlet.ParameterSetName) {
		"GPN"   {
			$LdapFilter = '(&(employeeNumber=' + $GPN +'))'
			Get-AdUser -Properties * -LdapFilter $LdapFilter
		}
		"TNUM"  {
			$LdapFilter = '(&(extensionAttribute5=' + $TNumber + '))'
			Get-AdUser -Properties * -LdapFilter $LdapFilter
		}
	}
}

Function Get-LoadedAssemblies {
	[AppDomain]::CurrentDomain.GetAssemblies()
}

Function Get-ActiveTCPConnections {
	[cmdletbinding()]
	param(
	)

	try {
		$TCPProperties = [System.Net.NetworkInformation.IPGlobalProperties]::GetIPGlobalProperties()
		$Connections = $TCPProperties.GetActiveTcpConnections()
		foreach($Connection in $Connections) {
			if($Connection.LocalEndPoint.AddressFamily -eq "InterNetwork" ) { $IPType = "IPv4" } else { $IPType = "IPv6" }
			$OutputObj = New-Object -TypeName PSobject
			$OutputObj | Add-Member -MemberType NoteProperty -Name "LocalAddress" -Value $Connection.LocalEndPoint.Address
			$OutputObj | Add-Member -MemberType NoteProperty -Name "LocalPort" -Value $Connection.LocalEndPoint.Port

			$OutputObj | Add-Member -MemberType NoteProperty -Name "RemoteAddress" -Value $Connection.RemoteEndPoint.Address
			$OutputObj | Add-Member -MemberType NoteProperty -Name "RemotePort" -Value $Connection.RemoteEndPoint.Port

			$OutputObj | Add-Member -MemberType NoteProperty -Name "State" -Value $Connection.State
			$OutputObj | Add-Member -MemberType NoteProperty -Name "IPV4Or6" -Value $IPType
			$OutputObj
		}
	} catch {
		Write-Error "Failed to get active connections. $_"
	}
}

function Trace-Port(){
    param(
        [int]$port=2223,
        [string]$IPAddress="127.0.0.1",
        [switch]$Echo=$false
    )
	    $listener = new-object System.Net.Sockets.TcpListener([System.Net.IPAddress]::Parse($IPAddress), $port)
	    $listener.start()
	    [byte[]]$bytes = 0..255| ForEach-Object {0}
	    write-host "Waiting for a connection on port $port..."
	    $client = $listener.AcceptTcpClient()
	    write-host "Connected from $($client.Client.RemoteEndPoint)"
	    $stream = $client.GetStream()
	    while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0)
	    {
	        $bytes[0..($i-1)] | ForEach-Object { $_ }
	        if ($Echo){$stream.Write($bytes,0,$i)}
	    }
	    $client.Close()
	    $listener.Stop()
	    write-host "Connection closed."
}

function Out-CliXml {
    [CmdletBinding()] Param (

        [Int32]
        $Depth = 2,

        [Parameter(ValueFromPipeline = $True, Mandatory = $True)]
        [PSObject]
        $InputObject
    )

    PROCESS
    {
        [System.Management.Automation.PSSerializer]::Serialize($InputObject, $Depth)
    }
}

Function Disable-ExecutionPolicy {
    ($ctx = $executioncontext.gettype().getfield(
        "_context","nonpublic,instance").getvalue(
            $executioncontext)).gettype().getfield(
                "_authorizationManager","nonpublic,instance").setvalue(
        $ctx, (new-object System.Management.Automation.AuthorizationManager `
                  "Microsoft.PowerShell"))
}
#Disable-ExecutionPolicy

Function Test-FileLock {
	param(
		[Parameter(Mandatory=$true)]
		[string]$FilePath
	)

	$oFile = New-Object System.IO.FileInfo $FilePath

	if((Test-Path -Path $FilePath) -eq $False){
		return $False
	}

	try{
		$oStream = $oFile.Open([System.IO.FileMode]::Open, [System.IO.FileAccess]::ReadWrite, [System.IO.FileShare]::None)
		if($oStream){
			$oStream.Close()
		}
		return $False
	} catch {
		return $True
	}
}

Function Hide-ConsoleWriteLine {
	[string]$definition = @"
	using System;
	using System.IO;
	using System.Text;
	public class ConsoleOverride : TextWriter {
		private TextWriter originalOut = Console.Out;

		public override void WriteLine (string value){
			return;
		}

		public override Encoding Encoding {
			get { throw new Exception ("The method or operation is not implemented"); }
		}
	}
"@
	if (-not ('ConsoleOverride' -as [type])){
		Add-Type -TypeDefinition $definition
	}
	$ConsoleOverride = New-Object ConsoleOverride
	[Console]::SetOut($ConsoleOverride)
}

Function Get-HostByName {
	param(
		[Parameter(Mandatory=$true,ValueFromPipeline=$True)]
		[ValidateNotNullOrEmpty()]
		[string]$Hostname
	)
	BEGIN {}
	END{}
	PROCESS {
		return [Net.Dns]::GetHostByName($Hostname)
	}
}

Function Get-HostByAddress {
	param(
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[ValidateNotNullOrEmpty()]
		[string]$Address
	)
	BEGIN {}
	END {}
	PROCESS {
		return [Net.Dns]::GetHostByAddress($Address)
	}
}

Function Set-ConsolePosition {
	param(
		[int]$x,
		[int]$y
	)
	$position = $host.ui.RawUI.CursorPosition
	$position.x = $x
	$position.y = $y
	$host.ui.RawUI.CursorPosition = $position
}

Function Write-Line {
	param(
		[int]$x,
		[int]$y,
		[int]$length,
		[bool]$vertical
	)

	# Move to assigned X/Y position in Console
	Set-ConsolePosition $x $y
	# Draw the Beginning of the line
	Write-Host "█" -nonewline
	# Is this vertically drawn?  Set direction variables and appropriate character to draw
	If ($vertical){
		$linechar="│"; $vert=1;$horz=0
	}else{
		$linechar="─"; $vert=0;$horz=1
	}
	# Draw the length of the line, moving in the appropriate direction
    foreach ($count in 1..($length-1)) {
        set-ConsolePosition (($horz*$count)+$x) (($vert*$count)+$y)
        write-host $linechar -nonewline
    }
	# Bump up the counter and draw the end
	$count++
    set-ConsolePosition (($horz*$count)+$x) (($vert*$count)+$y)
	Write-Host "█" -nonewline
}

Function Write-Box{
	param(
		[int]$width,
		[int]$length,
		[int]$x,
		[int] $y
	)
	# Do the four sides
	foreach ($box in 0..3) {
		# Variable to flip whether we’re on the left / top of the box or not
		$side=$box%2
		# Variable to switch whether it’s a vertical or horizontal line
		$vert=[int](($box-.5)/2)
		# compute the Width and Length so we can “switch them”
		$totalside=$width+$length
		# Length of line will be dependant on the Direction
		# (vertical or Horizontal)
		$linelength=($vert*$length)+([int](!$vert)*$width)
		$result=$totalside-$linelength
		# flip in the correct X Y coordinates for the maximum
		$ypass=([int](!$vert)*$side*$result)+$y
		$xpass=($vert*$side*$result)+$x
		# Draw the line
		Write-Line $xpass $ypass $linelength $vert
	}
}

Function Write-At {
	param(
		[int]$x,
		[int]$y,
		[string]$Message,
		[ConsoleColor]$Foreground,
		[ConsoleColor]$Background
	)

	$OriginalPosition              = $Host.Ui.RawUI.CursorPosition
	$OriginalForeground            = $Host.Ui.RawUI.ForegroundColor
	$OriginalBackground            = $Host.Ui.RawUI.BackgroundColor

	[Console]::SetCursorPosition($x,$y)
	$Host.Ui.RawUI.ForegroundColor = $Foreground
	$Host.Ui.RawUI.BackgroundColor = $Background
	[Console]::Write($Message)
	[Console]::SetCursorPosition($OriginalPosition.X,$y+1)
	$Host.Ui.RawUI.ForegroundColor = $OriginalForeground
	$Host.Ui.RawUI.BackgroundColor = $OriginalBackground
}

Function Get-Uptime {
	param(
		[Parameter(Mandatory=$true)]
		[string]$ServerName
    )
    begin{
        if($null -eq $AdminCreds){
            $AdminCreds = Get-Credential -Message "Please enter your admin credential details"
        }
    }
	Process {
		$timeVal = (Get-WmiObject -Credential $AdminCreds -ComputerName $ServerName -Query "SELECT LastBootUpTime FROM Win32_OperatingSystem").LastBootUpTime
		#$timeVal
		#$DbPoint = [char]58
		$Years = $timeVal.substring(0,4)
		$Months = $timeVal.substring(4,2)
		$Days = $timeVal.substring(6,2)
		$Hours = $timeVal.substring(8,2)
		$Mins = $timeVal.substring(10,2)
		$Secondes = $timeVal.substring(12,2)
		$dayDiff = New-TimeSpan  $(Get-Date -month $Months -day $Days -year $Years -hour $Hours -minute $Mins -Second $Secondes) $(Get-Date)

		$Info = "" | Select-Object ServerName, Uptime
		$Info.servername = $servername
		$d =$dayDiff.days
		$h =$dayDiff.hours
		$m =$dayDiff.Minutes
		$s = $daydiff.Seconds
		$info.Uptime = "$d Days $h Hours $m Min $s Sec"

		$Info
	}
}

Function Send-StringToOccsProxy {
	[cmdletbinding()]
	param(
		[Parameter(Mandatory=$False)]
		[ValidateSet('UAT','PROD')]
		[String]$Environment="UAT",
		[Parameter(Mandatory=$true)]
		[String]$Channel,
		[Parameter(Mandatory=$True)]
		[String]$DataToSend
	)
	[String]$Hostname = "nsyd0025pap.syd.swissbank.com"
	[UInt16]$Port = 0

	if($Environment -eq "UAT"){
		$Port = 2000
	}elseif($Environment -eq "PROD"){
		$Port = 1000
	}

    Try
    {
        $ErrorActionPreference = "Stop"

        $tcpConnection    = New-Object Net.Sockets.TcpClient($([Net.Dns]::GetHostEntry($Hostname)).AddressList[0], $Port)
        $tcpStream        = $tcpConnection.GetStream()
		$Reader           = New-Object IO.StreamReader($tcpStream)
		$Writer           = New-Object IO.StreamWriter($tcpStream)
		$Writer.Autoflush = $true

		$ReadBuffer      = New-Object Byte[] 1024
		$Encoding        = New-Object Text.AsciiEncoding

		while($tcpConnection.Connected){
			do {
				$null
			} until ($tcpStream.DataAvailable)

			while($tcpStream.DataAvailable){
				Write-Verbose "First data receive loop"
				$rawResponse = $Reader.Read($ReadBuffer,0,1024)
				$Response = $Encoding.GetString($ReadBuffer,0,$rawResponse)
				Write-Verbose $Response
			}
			Write-Verbose "Sending post message"
			$channel = $channel.Replace('#','')
			$Writer.WriteLine("post #" + $channel + " <" + $DataToSend + ">")

			while($tcpStream.DataAvailable){
				Write-Verbose "Second data receive loop"
				$rawResponse = $Reader.Read($ReadBuffer,0,1024)
				$Response = [Text.Encoding]::GetString($ReadBuffer,0,$rawResponse)
				Write-Verbose $Response
			}
			$Writer.WriteLine("quit")
			break
		}
    }
    Finally
    {
		$Reader.Close()
		$Writer.Close()
		$tcpStream.Close()
        If ($tcpStream) { $tcpStream.Dispose() }
        If ($tcpConnection) { $tcpConnection.Dispose() }
    }
}

Function Connect-QADToTest {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseDeclaredVarsMoreThanAssignments","", Justification="It is used - just not here")]
    param()
	if(Get-Command Connect-QADService){
		$pw = ConvertTo-SecureString -Force -AsPlainText $(PBE_Decrypt $global:EncryptTestPwd)
		# Make sure to export the returned connection object to the outer environment via
		# the global variable QADConnection, or else this doesn't do much
		$Global:QADConnection = Connect-QADService -Service test.msad.ubstest.net -ConnectionAccount "test\t.43409161.1" -ConnectionPassword $pw
	} else {
		Write-Error "Quest tools not active"
	}
}

Function FizzBuzz {
	1..100 | ForEach-Object {
		$s = ""
		if($_ % 3 -eq 0){$s += "Fizz"}
		if($_ % 5 -eq 0){$s += "Buzz"}
		if($s.Length -eq 0){$s = $_}
		$s
	}
}

Function Register-GCLib {
	set-location "C:\ubs\dev\Visual Studio 2015\Projects\GroupChatLibrary\GroupChatLibrary\bin\Debug\"
	[reflection.assembly]::LoadFile($(Resolve-Path .\Microsoft.Rtc.Collaboration.dll).Path)
	[reflection.assembly]::LoadFile($(Resolve-Path .\Microsoft.Rtc.Collaboration.GroupChat.dll).Path)
	[reflection.assembly]::LoadFile($(Resolve-Path .\MgcCommon.dll).Path)
	[reflection.assembly]::LoadFile($(Resolve-Path .\Endpoint.dll).Path)
	[reflection.assembly]::LoadFile($(Resolve-Path .\Protocols.dll).Path)
	[reflection.assembly]::LoadFile($(Resolve-Path .\SIPEPS.dll).Path)
	[reflection.assembly]::LoadFile($(Resolve-Path .\C5.dll).Path)
	[reflection.assembly]::LoadFile($(Resolve-Path .\GroupChatLibrary.dll).Path)
	[Accelerators]::Add("GcLib","Ubs.Component.GroupChatLib.ChatClient")
}

Function Invoke-MultiThreaded {
#.Synopsis
#    This is a quick and open-ended script multi-threader searcher
#
#.Description
#    This script will allow any general, external script to be multithreaded by providing a single
#    argument to that script and opening it in a seperate thread.  It works as a filter in the
#    pipeline, or as a standalone script.  It will read the argument either from the pipeline
#    or from a filename provided.  It will send the results of the child script down the pipeline,
#    so it is best to use a script that returns some sort of object.
#
#.PARAMETER Command
#    This is where you provide the PowerShell Cmdlet / Script file that you want to multithread.
#    You can also choose a built in cmdlet.  Keep in mind that your script.  This script is read into
#    a scriptblock, so any unforeseen errors are likely caused by the conversion to a script block.
#
#.PARAMETER ObjectList
#    The objectlist represents the arguments that are provided to the child script.  This is an open ended
#    argument and can take a single object from the pipeline, an array, a collection, or a file name.  The
#    multithreading script does it's best to find out which you have provided and handle it as such.
#    If you would like to provide a file, then the file is read with one object on each line and will
#    be provided as is to the script you are running as a string.  If this is not desired, then use an array.
#
#.PARAMETER InputParam
#    This allows you to specify the parameter for which your input objects are to be evaluated.  As an example,
#    if you were to provide a computer name to the Get-Process cmdlet as just an argument, it would attempt to
#    find all processes where the name was the provided computername and fail.  You need to specify that the
#    parameter that you are providing is the "ComputerName".
#
#.PARAMETER AddParam
#    This allows you to specify additional parameters to the running command.  For instance, if you are trying
#    to find the status of the "BITS" service on all servers in your list, you will need to specify the "Name"
#    parameter.  This command takes a hash pair formatted as follows:
#
#    @{"ParameterName" = "Value"}
#    @{"ParameterName" = "Value" ; "ParameterTwo" = "Value2"}
#
#.PARAMETER AddSwitch
#    This allows you to add additional switches to the command you are running.  For instance, you may want
#    to include "RequiredServices" to the "Get-Service" cmdlet.  This parameter will take a single string, or
#    an aray of strings as follows:
#
#    "RequiredServices"
#    @("RequiredServices", "DependentServices")
#
#.PARAMETER MaxThreads
#    This is the maximum number of threads to run at any given time.  If resources are too congested try lowering
#    this number.  The default value is 20.
#
#.PARAMETER SleepTimer
#    This is the time between cycles of the child process detection cycle.  The default value is 200ms.  If CPU
#    utilization is high then you can consider increasing this delay.  If the child script takes a long time to
#    run, then you might increase this value to around 1000 (or 1 second in the detection cycle).
#
#
#.EXAMPLE
#    Both of these will execute the script named ServerInfo.ps1 and provide each of the server names in AllServers.txt
#    while providing the results to the screen.  The results will be the output of the child script.
#
#    gc AllServers.txt | .\Run-CommandMultiThreaded.ps1 -Command .\ServerInfo.ps1
#    .\Run-CommandMultiThreaded.ps1 -Command .\ServerInfo.ps1 -ObjectList (gc .\AllServers.txt)
#
#.EXAMPLE
#    The following demonstrates the use of the AddParam statement
#
#    $ObjectList | .\Run-CommandMultiThreaded.ps1 -Command "Get-Service" -InputParam ComputerName -AddParam @{"Name" = "BITS"}
#
#.EXAMPLE
#    The following demonstrates the use of the AddSwitch statement
#
#    $ObjectList | .\Run-CommandMultiThreaded.ps1 -Command "Get-Service" -AddSwitch @("RequiredServices", "DependentServices")
#
#.EXAMPLE
#    The following demonstrates the use of the script in the pipeline
#
#    $ObjectList | .\Run-CommandMultiThreaded.ps1 -Command "Get-Service" -InputParam ComputerName -AddParam @{"Name" = "BITS"} | Select Status, MachineName
#


    Param($Command = $(Read-Host "Enter the script file"),
        [Parameter(ValueFromPipeline=$true,ValueFromPipelineByPropertyName=$true)]$ObjectList,
        $InputParam = $Null,
        $MaxThreads = 20,
        $SleepTimer = 200,
        $MaxResultTime = 120,
        [HashTable]$AddParam = @{},
        [Array]$AddSwitch = @()
    )

    Begin{
        $ISS = [system.management.automation.runspaces.initialsessionstate]::CreateDefault()
        $RunspacePool = [runspacefactory]::CreateRunspacePool(1, $MaxThreads, $ISS, $Host)
        $RunspacePool.Open()

        If ($(Get-Command | Select-Object Name) -match $Command){
            $Code = $Null
        }Else{
            $OFS = "`r`n"
            $Code = [ScriptBlock]::Create($(Get-Content $Command))
            Remove-Variable OFS
        }
        $Jobs = @()
    }

    Process{
        Write-Progress -Activity "Preloading threads" -Status "Starting Job $($jobs.count)"
        ForEach ($Object in $ObjectList){
            If ($null -eq $Code){
                $PowershellThread = [powershell]::Create().AddCommand($Command)
            }Else{
                $PowershellThread = [powershell]::Create().AddScript($Code)
            }
            If ($null -eq $InputParam){
                $PowershellThread.AddParameter($InputParam, $Object.ToString()) | out-null
            }Else{
                $PowershellThread.AddArgument($Object.ToString()) | out-null
            }
            ForEach($Key in $AddParam.Keys){
                $PowershellThread.AddParameter($Key, $AddParam.$key) | out-null
            }
            ForEach($Switch in $AddSwitch){
                $Switch
                $PowershellThread.AddParameter($Switch) | out-null
            }
            $PowershellThread.RunspacePool = $RunspacePool
            $Handle = $PowershellThread.BeginInvoke()
            $Job = "" | Select-Object Handle, Thread, object
            $Job.Handle = $Handle
            $Job.Thread = $PowershellThread
            $Job.Object = $Object.ToString()
            $Jobs += $Job
        }

    }

    End{
        $ResultTimer = Get-Date
        While (@($Jobs | Where-Object {$null -eq $_.Handle}).count -gt 0)  {

            $Remaining = "$($($Jobs | Where-Object {$_.Handle.IsCompleted -eq $False}).object)"
            If ($Remaining.Length -gt 60){
                $Remaining = $Remaining.Substring(0,60) + "..."
            }
            Write-Progress `
                -Activity "Waiting for Jobs - $($MaxThreads - $($RunspacePool.GetAvailableRunspaces())) of $MaxThreads threads running" `
                -PercentComplete (($Jobs.count - $($($Jobs | Where-Object {$_.Handle.IsCompleted -eq $False}).count)) / $Jobs.Count * 100) `
                -Status "$(@($($Jobs | Where-Object {$_.Handle.IsCompleted -eq $False})).count) remaining - $remaining"

            ForEach ($Job in $($Jobs | Where-Object {$_.Handle.IsCompleted -eq $True})){
                $Job.Thread.EndInvoke($Job.Handle)
                $Job.Thread.Dispose()
                $Job.Thread = $Null
                $Job.Handle = $Null
                $ResultTimer = Get-Date
            }
            If (($(Get-Date) - $ResultTimer).totalseconds -gt $MaxResultTime){
                Write-Error "Child script appears to be frozen, try increasing MaxResultTime"
                Exit
            }
            Start-Sleep -Milliseconds $SleepTimer

        }
        $RunspacePool.Close() | Out-Null
        $RunspacePool.Dispose() | Out-Null
    }
}

Function Read-HostTimeout {
	#  Description:  Mimics the built-in "read-host" cmdlet but adds an expiration timer for
	#  receiving the input.  Does not support -assecurestring

	# Set parameters.  Keeping the prompt mandatory
	# just like the original
	param(
		[Parameter(Mandatory=$true,Position=1)]
		[string]$prompt,

		[Parameter(Mandatory=$false,Position=2)]
		[int]$delayInSeconds=5,

		[Parameter(Mandatory=$false,Position=3)]
		[string]$defaultValue = 'n'
	)

	# Do the math to convert the delay given into milliseconds
	# and divide by the sleep value so that the correct delay
	# timer value can be set
	$sleep = 250
	$delay = ($delayInSeconds*1000)/$sleep
	$count = 0
	$charArray = New-Object System.Collections.ArrayList
	Write-host -nonewline "$($prompt):  "

	# While loop waits for the first key to be pressed for input and
	# then exits.  If the timer expires it returns null
	While ( (!$host.ui.rawui.KeyAvailable) -and ($count -lt $delay) ){
		start-sleep -m $sleep
		$count++
		If ($count -eq $delay) { Write-Host "`n"; return $defaultValue}
	}

	# Retrieve the key pressed, add it to the char array that is storing
	# all keys pressed and then write it to the same line as the prompt
	$key = $host.ui.rawui.readkey("NoEcho,IncludeKeyUp").Character
	$charArray.Add($key) | out-null
	Write-host -nonewline $key

	# This block is where the script keeps reading for a key.  Every time
	# a key is pressed, it checks if it's a carriage return.  If so, it exits the
	# loop and returns the string.  If not it stores the key pressed and
	# then checks if it's a backspace and does the necessary cursor
	# moving and blanking out of the backspaced character, then resumes
	# writing.
	$key = $host.ui.rawui.readkey("NoEcho,IncludeKeyUp")
	While ($key.virtualKeyCode -ne 13) {
		If ($key.virtualKeycode -eq 8) {
			$charArray.Add($key.Character) | out-null
			Write-host -nonewline $key.Character
			$cursor = $host.ui.rawui.get_cursorPosition()
			write-host -nonewline " "
			$host.ui.rawui.set_cursorPosition($cursor)
			$key = $host.ui.rawui.readkey("NoEcho,IncludeKeyUp")
		}
		Else {
			$charArray.Add($key.Character) | out-null
			Write-host -nonewline $key.Character
			$key = $host.ui.rawui.readkey("NoEcho,IncludeKeyUp")
		}
	}
	Write-Host ""
	$finalString = -join $charArray
	return $finalString
}

Function Test-Input{
    param(
        [Parameter(Mandatory=$true)]
        [string]$prompt,
        [Parameter(Mandatory=$false,ValueFromPipeline=$true)]
        [string[]]$desired=@('y','n')
    )
	[string]$inputchars = Read-HostTimeout -prompt $prompt -delayInSeconds 5 -defaultValue 'p'
    while(!$desired -contains $inputchars){
        Write-Warning "Your choice was invalid, please try again"
        $inputchars = Read-Host
    }
    return $inputchars.ToUpper()
}

Function Add-ConEmuPalette {
    [CmdLetBinding()]
    param(
        [Parameter(Mandatory=$false)]
        [string]$NewPalette
    )
    # Constants
    [string]$ConEmuXmlPath = "C:\Users\childba\Applications\ConEmu\ConEmu.xml"
    [string]$ConEmuColorsXpath = "//key[@name='Colors']"

    # Backup existing config
    [string]$InvocationTime = Get-Date -Format FileDateTimeUniversal
    [string]$BackupName = [String]::Join('',@("C:\Users\childba\Applications\ConEmu\ConEmu-",$InvocationTime,".xml-backup"))
    [string]$Message = "Backing up original config to " + $BackupName
    Write-Output  $Message
    Copy-Item -LiteralPath $ConEmuXmlPath -Destination $BackupName

    # Load XML containing new palette
    [xml]$NewPaletteXml = New-Object Xml.XmlDocument
    $NewPaletteXml.Load($NewPalette)

    # Load current ConEmu.xml
    [xml]$ConEmuXml = New-Object Xml.XmlDocument
    $ConEmuXml.Load($ConEmuXmlPath)

    # Find the XML node where new palettes should be stored,
    # get the current count of palettes installed there, and
    # get a reference to the parent XML node for use in the
    # InsertAfter method later on.
    $ConEmuColorsNode = $ConEmuXml.SelectSingleNode($ConEmuColorsXpath)
    $PaletteCountNode = $ConEmuColorsNode.Value
    $ParentNode       = $PaletteCountNode.ParentNode

    # Import the new palette's XML data into the ConEmu config,
    # insert it into the right place in the XML, and then increment
    # the palette count by one.
    $Imported    = $ConEmuXml.ImportNode($NewPaletteXml.DocumentElement,$true)
    $ParentNode.InsertAfter($Imported,$PaletteCountNode) | Out-Null
    [long]$Count = $PaletteCountNode.data
    $Count++
    $PaletteCountNode.data = [string]$Count

    # Save the new config
    $ConEmuXml.Save($ConEmuXmlPath)
}

Function Start-Pageant {
    [string[]]$PrivateKeys = @()
    [string]$PageantPath = "C:\ProgramData\chocolatey\bin\PAGEANT.EXE"
    [string]$KeyDirectory  = "H:\MyGEOSProfile\FDR\MyDocuments\Keys\"

    if($global:AtHome){
        if($env:COMPUTERNAME -eq "ATLAS"){
            $PageantPath = "C:\Program Files\ExtraPuTTY\Bin\pageant.exe"
            $KeyDirectory = "C:\tools\Cygwin\Root\home\bryan\.ssh\"
        } else {
            $KeyDirectory = "C:\tools\Cygwin\home\bryan\.ssh\"
        }
    }

    $PrivateKeys = Get-ChildItem $([String]::Join('',$KeyDirectory,"*.ppk"))
    [string]$KeyList = [String]::Join(' ',$PrivateKeys)

    Start-Process -FilePath $PageantPath -WorkingDirectory $KeyDirectory -ArgumentList $KeyList -NoNewWindow:$true
}

Function Start-Emacs {
	param(
		[Parameter(Mandatory=$False)]
		[string]$EmacsPath = "C:\msys64\mingw64\bin\runemacs.exe",
		[Parameter(Mandatory=$False)]
		[string]$EmacsHome = "C:\msys64\home\childba"
	)
    $real_home = $env:HOME
    $env:HOME = $EmacsHome
    & $EmacsPath
    $env:HOME = $real_home
}

Function Add-LdapObject {
    [System.Reflection.Assembly]::LoadWithPartialName("System.DirectoryServices.Protocols")
    [System.Reflection.Assembly]::LoadWithPartialName("System.Net")
    $c = New-Object -TypeName System.DirectoryServices.Protocols.LdapConnection -ArgumentList "***.local.com:777,uid=testuser,ou=system,o=company", "password" , $authenticationType
    $c.Bind()
    $r = New-Object -TypeName System.DirectoryServices.Protocols.AddRequest
    $r.DistinguishedName = "uid= xxxx, ou=user, o=company"
    $r.Attributes.Add((New-Object -TypeName System.DirectoryServices.Protocols.DirectoryAttribute -ArgumentList "objectclass",@("top","organizationalPerson","person","inetorgperson","inetuser","mailrecipient","pwmuser","posixAccount"))) | Out-Null
    $r.Attributes.Add((New-Object -TypeName System.DirectoryServices.Protocols.DirectoryAttribute -ArgumentList "cn",($FirstName+" "+$LastName))) | Out-Null
}

Function Start-VM {
    param(
        [Parameter(Mandatory=$true)]
        [string]$VMName
    )
    $VboxPath = "C:\Program Files\Oracle\VirtualBox"
    $VboxCmd  = "./VboxManage startvm $VMName --type headless"
    If(Test-Path $VboxPath -PathType Container){
        $CurrentDir = $(Get-Location)
        Set-Location $VboxPath
        & $VboxCmd
        Set-Location $CurrentDir
    } else {
        "Couldn't find VirtualBox install"
    }
}

Function ConvertTo-MooltiPass {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        $KeepassExport
    )
    BEGIN{
        $MooltiPassImport = @()
    }
    PROCESS{
        If($_.Website.Length -ne 0){
            $MPEntry = New-Object PSObject -Property {website=$_."Web Site";login=$_."Login Name";password=$_.Password;description=$_.Account}
            $MooltiPassImport+=$MPEntry
        }
    }
    END{
        $MooltiPassImport
    }
}

Function New-ImpersonateUser {
    <#
    .SYNOPSIS
    Impersonates another user on a local machine or Active Directory.

    .DESCRIPTION
    New-ImpersonateUser uses the LogonUser method from the advapi32.dll to get a token that can then be used to call the WindowsIdentity.Impersonate method in order to impersonate another user without logging off from the current session.  You can pass it either a PSCredential or each field separately. Once impersonation is done, it is highly recommended that Remove-ImpersonateUser (a function added to the global scope at runtime) be called to revert back to the original user.

    .PARAMETER Credential
    The PS Credential to be used, eg. from Get-Credential

    .PARAMETER Username
    The username of the user to impersonate.

    .PARAMETER Domain
    The domain of the user to impersonate.  If the user is local, use the name of the local computer stored in $env:COMPUTERNAME

    .PARAMETER Password
    The password of the user to impersonate.  This is in cleartext which is why sending a PSCredential is recommended.

    .PARAMETER Quiet
    Using the Quiet parameter will force New-ImpersonateUser to have no outputs.

    .INPUTS
    None.  You cannot pipe objects to New-ImpersonateUser

    .OUTPUTS
    System.String
    By default New-ImpersonateUser will output strings confirming Impersonation and a reminder to revert back.

    None
    The Quiet parameter will force New-ImpersonateUser to have no outputs.

    .EXAMPLE
    PS C:\> New-ImpersonateUser -Credential (Get-Credential)

    This command will impersonate the user supplied to the Get-Credential cmdlet.
    .EXAMPLE
    PS C:\> New-ImpersonateUser -Username "user" -Domain "domain" -Password "password"

    This command will impersonate the user "domain\user" with the password "password."
    .EXAMPLE
    PS C:\> New-ImpersonateUser -Credential (Get-Credential) -Quiet

    This command will impersonate the user supplied to the Get-Credential cmdlet, but it will not produce any outputs.
    .NOTES
    It is recommended that you read some of the documentation on MSDN or Technet regarding impersonation and its potential complications, limitations, and implications.
    Author:  Chris Carter
    Version: 1.0

    .LINK
    http://msdn.microsoft.com/en-us/library/chf6fbt4(v=vs.110).aspx (Impersonate Method)
    http://msdn.microsoft.com/en-us/library/windows/desktop/aa378184(v=vs.85).aspx (LogonUser function)
    Add-Type

    #>
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSAvoidUsingPlainTextForPassword","", Justification="I know what I'm doing")]
    [CmdletBinding(DefaultParameterSetName="Credential")]
    Param(
        [Parameter(ParameterSetName="ClearText", Mandatory=$true)][string]$Username,
        [Parameter(ParameterSetName="ClearText", Mandatory=$true)][string]$Domain,
        [Parameter(ParameterSetName="ClearText", Mandatory=$true)][string]$Password,
        [Parameter(ParameterSetName="Credential", Mandatory=$true, Position=0)][PSCredential]$Credential,
        [Parameter()][Switch]$Quiet
    )

    #Import the LogonUser Function from advapi32.dll and the CloseHandle Function from kernel32.dll
    Add-Type -Namespace Import -Name Win32 -MemberDefinition @"
[DllImport("advapi32.dll", SetLastError = true)]
public static extern bool LogonUser(string user, string domain, string password, int logonType, int logonProvider, out IntPtr token);

[DllImport("kernel32.dll", SetLastError = true)]
public static extern bool CloseHandle(IntPtr handle);
"@

    #Set Global variable to hold the Impersonation after it is created so it may be ended after script run
    $Global:ImpersonatedUser = @{}
    #Initialize handle variable so that it exists to be referenced in the LogonUser method
    $tokenHandle = 0

    #Pass the PSCredentials to the variables to be sent to the LogonUser method
    if ($Credential) {
        Get-Variable Username, Domain, Password | ForEach-Object {
            Set-Variable $_.Name -Value $Credential.GetNetworkCredential().$($_.Name)}
    }

    #Call LogonUser and store its success.  [ref]$tokenHandle is used to store the token "out IntPtr token" from LogonUser.
    $returnValue = [Import.Win32]::LogonUser($Username, $Domain, $Password, 2, 0, [ref]$tokenHandle)

    #If it fails, throw the verbose with the error code
    if (!$returnValue) {
        $errCode = [System.Runtime.InteropServices.Marshal]::GetLastWin32Error();
        Write-Host "Impersonate-User failed a call to LogonUser with error code: $errCode"
        throw [System.ComponentModel.Win32Exception]$errCode
    }
    #Successful token stored in $tokenHandle
    else {
        #Call the Impersonate method with the returned token. An ImpersonationContext is returned and stored in the
        #Global variable so that it may be used after script run.
        $Global:ImpersonatedUser.ImpersonationContext = [System.Security.Principal.WindowsIdentity]::Impersonate($tokenHandle)

        #Close the handle to the token. Voided to mask the Boolean return value.
        [void][Import.Win32]::CloseHandle($tokenHandle)

        #Write the current user to ensure Impersonation worked and to remind user to revert back when finished.
        if (!$Quiet) {
            Write-Host "You are now impersonating user $([System.Security.Principal.WindowsIdentity]::GetCurrent().Name)"
            Write-Host "It is very important that you call Remove-ImpersonateUser when finished to revert back to your user." `
              -ForegroundColor DarkYellow -BackgroundColor Black
        }
    }

    #Clean up sensitive variables
    $Username = $Domain = $Password = $Credential = $null
}

#Function put in the Global scope to be used when Impersonation is finished.
Function Remove-ImpersonateUser {
    <#
    .SYNOPSIS
    Used to revert back to the orginal user after New-ImpersonateUser is called. You can only call this function once; it is deleted after it runs.

    .INPUTS
    None.  You cannot pipe objects to Remove-ImpersonateUser

    .OUTPUTS
    None.  Remove-ImpersonateUser does not generate any output.
    #>

    #Calling the Undo method reverts back to the original user.
    $ImpersonatedUser.ImpersonationContext.Undo()

    #Clean up the Global variable.
    Remove-Variable ImpersonatedUser -Scope Global
}

Function Split-PFX {
    param(
	[Parameter(Mandatory)]
	[string]$PfxFilename,
	[Parameter(Mandatory=$false)]
	[switch]$EncryptKey6
    )
<#
.SYNOPSIS
Takes a .pfx (pkcs12) format certificate / key, and uses openssl to split it into separate key / certificate files

.PARAMETER PfxFilename

.PARAMETER EncryptKey

.OUTPUTS
Two files, one containing the certificate, one containing the key.
#>
    Set-Variable -Name OpenSSL -Option Constant -Value "C:\OpenSSL-Win64\bin\openssl.exe"
    $SourceDir       = (Get-ChildItem $PfxFilename).DirectoryName
    $DestinationCert = $PfxFilename.Replace(".pfx","-cert.pem")
    $DestinationKey  = $PfxFilename.Replace(".pfx","-key.pem")

    # Get password from user so they only have to enter it once
    $EncryptedPfxPassword = Read-Host -Prompt "Enter password for PFX" -AsSecureString
    $BstrPfxPassword      = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($EncryptedPfxPassword)
    $PfxPassword          = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BstrPfxPassword)

    # Extract the key
    if($EncryptKey){
	& $OpenSSL 'pkcs12' -in $PfxFilename -nocerts -password pass:$PfxPassword -out $DestinationKey
    } else {
	& $OpenSSL 'pkcs12' -in $PfxFilename -nocerts -nodes -password pass:$PfxPassword -out $DestinationKey
    }

    # Extract the cert
    & $OpenSSL 'pkcs12' -in $PfxFilename -nokeys -password pass:$PfxPassword -out $DestinationCert
}

Function Start-Arch {
		param()
		& "C:\Program Files\Oracle\Virtualbox\VBoxManage" startvm "Arch"
}


New-Alias -Name lsmod -Value Get-Module
New-Alias -Name rmmod -Value Remove-Module
New-Alias -Name emacs -Value Start-Emacs
New-Alias -Name ghbn  -Value Get-HostByName
New-Alias -Name ghba  -Value Get-HostByAddress
New-Alias -Name host  -Value Get-HostByName
New-Alias -Name which -Value Get-Command

Export-ModuleMember -Variable * -Function * -Alias *
