﻿<#
.SYNOPSIS
    Cmdlet to generate a .PEM encoded Certificate Request using .NET's builtin CryptoAPI
.DESCRIPTION
    Lync's built in certificate request Powershell Cmdlets all make the assumption that the CSR they generate
    can immediately be submitted to a Microsoft Certificate Authority. UBS's certificate request system (ZFLOW)
    does not lend itself to this model, so this script generates a .CSR which can be used with ZFLOW correctly. 
.PARAMETER
    subjectDN
    Used to specify the subject of the request, usually the FQDN of the server the certificate is for.
.PARAMETER
    friendName
    Optional - can be used to specify the friendly name seen in the certificate MMC snap in.
.EXAMPLE
    New-LyncCertRequest -subjectDN "botfw.ubstest.net" -friendlyName "Bot Framework SSO Certificate"
#>

# Set up enums for various CryptoAPI method calls. 
Add-Type -Language CSharpVersion3 -TypeDefinition @"
    public enum X509KeySpec { 
		XCN_AT_NONE         = 0,
		XCN_AT_KEYEXCHANGE  = 1,
		XCN_AT_SIGNATURE    = 2
	}

    [System.Flags]
    public enum X509PrivateKeyUsageFlags {
		XCN_NCRYPT_ALLOW_USAGES_NONE         = 0,
		XCN_NCRYPT_ALLOW_DECRYPT_FLAG        = 1,
		XCN_NCRYPT_ALLOW_SIGNING_FLAG        = 2,
		XCN_NCRYPT_ALLOW_KEY_AGREEMENT_FLAG  = 4,
		XCN_NCRYPT_ALLOW_ALL_USAGES          = 16777215
	}

	[System.Flags]
	public enum X509CertificateEnrollmentContext {
		ContextUser                      = 0x1,
        ContextMachine                   = 0x2,
        ContextAdministratorForceMachine = 0x3
	}
	
    [System.Flags]
    public enum X509KeyUsageFlags {
		XCN_CERT_NO_KEY_USAGE                 = 0,
		XCN_CERT_DIGITAL_SIGNATURE_KEY_USAGE  = 128,
		XCN_CERT_NON_REPUDIATION_KEY_USAGE    = 64,
		XCN_CERT_KEY_ENCIPHERMENT_KEY_USAGE   = 32,
		XCN_CERT_DATA_ENCIPHERMENT_KEY_USAGE  = 16,
		XCN_CERT_KEY_AGREEMENT_KEY_USAGE      = 8,
		XCN_CERT_KEY_CERT_SIGN_KEY_USAGE      = 4,
		XCN_CERT_OFFLINE_CRL_SIGN_KEY_USAGE   = 2,
		XCN_CERT_CRL_SIGN_KEY_USAGE           = 2,
		XCN_CERT_ENCIPHER_ONLY_KEY_USAGE      = 1,
		XCN_CERT_DECIPHER_ONLY_KEY_USAGE      = 32768
	}

    [System.Flags]
    public enum X509EncodingType:long { 
		XCN_CRYPT_STRING_BASE64HEADER         = 0,
		XCN_CRYPT_STRING_BASE64               = 0x1,
		XCN_CRYPT_STRING_BINARY               = 0x2,
		XCN_CRYPT_STRING_BASE64REQUESTHEADER  = 0x3,
		XCN_CRYPT_STRING_HEX                  = 0x4,
		XCN_CRYPT_STRING_HEXASCII             = 0x5,
		XCN_CRYPT_STRING_BASE64_ANY           = 0x6,
		XCN_CRYPT_STRING_ANY                  = 0x7,
		XCN_CRYPT_STRING_HEX_ANY              = 0x8,
		XCN_CRYPT_STRING_BASE64X509CRLHEADER  = 0x9,
		XCN_CRYPT_STRING_HEXADDR              = 0xa,
		XCN_CRYPT_STRING_HEXASCIIADDR         = 0xb,
		XCN_CRYPT_STRING_HEXRAW               = 0xc,
		XCN_CRYPT_STRING_NOCRLF               = 0x40000000,
		XCN_CRYPT_STRING_NOCR                 = 0x80000000
	}

    [System.Flags]
    public enum X500NameFlags:long {
		XCN_CERT_NAME_STR_NONE                       = 0,
		XCN_CERT_SIMPLE_NAME_STR                     = 1,
		XCN_CERT_OID_NAME_STR                        = 2,
		XCN_CERT_X500_NAME_STR                       = 3,
		XCN_CERT_XML_NAME_STR                        = 4,
		XCN_CERT_NAME_STR_SEMICOLON_FLAG             = 0x40000000,
		XCN_CERT_NAME_STR_NO_PLUS_FLAG               = 0x20000000,
		XCN_CERT_NAME_STR_NO_QUOTING_FLAG            = 0x10000000,
		XCN_CERT_NAME_STR_CRLF_FLAG                  = 0x8000000,
		XCN_CERT_NAME_STR_COMMA_FLAG                 = 0x4000000,
		XCN_CERT_NAME_STR_REVERSE_FLAG               = 0x2000000,
		XCN_CERT_NAME_STR_FORWARD_FLAG               = 0x1000000,
		XCN_CERT_NAME_STR_DISABLE_IE4_UTF8_FLAG      = 0x10000,
		XCN_CERT_NAME_STR_ENABLE_T61_UNICODE_FLAG    = 0x20000,
		XCN_CERT_NAME_STR_ENABLE_UTF8_UNICODE_FLAG   = 0x40000,
		XCN_CERT_NAME_STR_FORCE_UTF8_DIR_STR_FLAG    = 0x80000,
		XCN_CERT_NAME_STR_DISABLE_UTF8_DIR_STR_FLAG  = 0x100000
	}

    [System.Flags]
    public enum X509PrivateKeyExportFlags {
		XCN_NCRYPT_ALLOW_EXPORT_NONE               = 0,
		XCN_NCRYPT_ALLOW_EXPORT_FLAG               = 0x1,
		XCN_NCRYPT_ALLOW_PLAINTEXT_EXPORT_FLAG     = 0x2,
		XCN_NCRYPT_ALLOW_ARCHIVING_FLAG            = 0x4,
		XCN_NCRYPT_ALLOW_PLAINTEXT_ARCHIVING_FLAG  = 0x8
	}
        
"@

Function New-LyncCertRequest {
	[CmdletBinding()]
	param(
		[Parameter(Mandatory=$True)]
		[string]$subjectDN,
		[Parameter(Mandatory=$False)]
		[string]$friendlyName=""
	)
	# Check the format of the subjectDN, and if it doesn't start with "CN=", prepend that
	# before using it later in the script
	if($subjectDN -notmatch "^CN="){
		$subjectDN = "CN=" + $subjectDN
	}

	# Create all the required .NET objects
	$PKCS10          = New-Object -com "X509Enrollment.CX509CertificateRequestPkcs10.1"
	$PrivateKey      = New-Object -com "X509Enrollment.CX509PrivateKey.1"
	$DN              = New-Object -com "X509Enrollment.CX500DistinguishedName.1"
	$ExtKeyUsage     = New-Object -com "X509Enrollment.CX509ExtensionKeyUsage.1"
	$Enroll          = New-Object -com "X509Enrollment.CX509Enrollment.1"
	$ServerAuthOID   = New-Object -com "X509Enrollment.CObjectId.1"
	$EKU_OIDs        = New-Object -com "X509Enrollment.CObjectIds.1"
	$EKU_Ext         = New-Object -com "X509Enrollment.CX509ExtensionEnhancedKeyUsage.1"
	$

	# String to hold the BASE 64 encoded request
	[string]$Request = ""

	# Create the private key using UBS's required defaults
	$PrivateKey.ExportPolicy    = [X509PrivateKeyExportFlags]::XCN_CRYPT_ALLOW_EXPORT_FLAG
	if($friendlyName.Length -gt 0){
		$PrivateKey.FriendlyName    = $friendlyName
	}
	$PrivateKey.KeySpec         = [X509KeySpec]::XCN_AT_KEYEXCHANGE
	$PrivateKey.KeyUsage        = [X509PrivateKeyUsageFlags]::XCN_NCRYPT_ALLOW_DECRYPT -bxor [X509PrivateKeyUsageFlags]::XCN_NCRYPT_ALL_SIGNING_FLAG
	$PrivateKey.Length          = 2048
	$PrivateKey.MachineContext  = $True
	$PrivateKey.ProviderName    = "Microsoft RSA SChannel Cryptographic Provider"
	$PrivateKey.Create()

	# Create the initial certificate request
	$PKCS10.InitializeFromPrivateKey([X509CertificateEnrollmentContext]::ContextMachine,$PrivateKey,"")

	# Add the right key usage flags to the request
	$ExtKeyUsage.InitializeEncode([X509KeyUsageFlags]::XCN_CERT_DIGITAL_SIGNATURE_KEY_USAGE -bxor [X509KeyUsageFlags]::XCN_CERT_KEY_ENCIPHERMENT_KEY_USAGE)
	$PKCS10.X509Extensions.Add($ExtKeyUsage)

	# Add the Enhanced key usage OID for Server Authentication to the request
	$ServerAuthOID.InitializeFromValue("1.3.6.1.5.5.7.3.1")
	$EKU_OIDs.Add($ServerAuthOID)
	$EKU_Ext.InitializeEncode($EKU_OIDs)
	$PKCS10.X509Extensions.Add($EKU_Ext)

	# Create the distinguished name with the right subject
	$DN.Encode($subjectDN,[X500NameFlags]::XCN_CERT_NAME_STR_NONE)

	# Add the DN to the certificate request
	$PKCS10.Subject = $DN

	# This adds the certificate request to the Computer certificate container's "Certificate Enrollment Requests"
	$Enroll.InitializeFromRequest($PKCS10)

	# Turn the request into a PEM formatted string
	$Request = $Enroll.CreateRequest([X509EncodingType]::XCN_CRYPT_STRING_BASE64REQUESTHEADER)

	# Write out string (not actually required, but shows the code has done something)
	Write-Host $Request

}

Function Import-LyncCert {
	param
	(
		[IO.FileInfo] $CertFile = $(throw "Parameter -CertFile [System.IO.FileInfo] is required."),
		[string[]] $StoreNames = $(throw "Parameter -StoreNames [System.String] is required."),
		[switch] $LocalMachine,
		[switch] $CurrentUser,
		[string] $CertPassword,
		[switch] $Verbose
	)
	
	begin
	{
		[void][System.Reflection.Assembly]::LoadWithPartialName("System.Security") | Out-Null
	}
	
	process 
	{
        if ($Verbose)
		{
            $VerbosePreference = 'Continue'
        }
    
		if (-not $LocalMachine -and -not $CurrentUser)
		{
			Write-Warning "One or both of the following parameters are required: '-LocalMachine' '-CurrentUser'. Skipping certificate '$CertFile'."
		}

		try
		{
			if ($_)
            {
                $certfile = $_
            }
            $cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2 $certfile,$CertPassword
		}
		catch
		{
			Write-Error ("Error importing '$certfile': $_ .") -ErrorAction:Continue
		}
			
		if ($cert -and $LocalMachine)
		{
			$StoreScope = "LocalMachine"
			$StoreNames | ForEach-Object {
				$StoreName = $_
				if (Test-Path "cert:\$StoreScope\$StoreName")
				{
					try
					{
						$store = New-Object System.Security.Cryptography.X509Certificates.X509Store $StoreName, $StoreScope
						$store.Open([System.Security.Cryptography.X509Certificates.OpenFlags]::ReadWrite)
						$store.Add($cert)
						$store.Close()
						Write-Verbose "Successfully added '$certfile' to 'cert:\$StoreScope\$StoreName'."
					}
					catch
					{
						Write-Error ("Error adding '$certfile' to 'cert:\$StoreScope\$StoreName': $_ .") -ErrorAction:Continue
					}
				}
				else
				{
					Write-Warning "Certificate store '$StoreName' does not exist. Skipping..."
				}
			}
		}
		
		if ($cert -and $CurrentUser)
		{
			$StoreScope = "CurrentUser"
			$StoreNames | ForEach-Object {
				$StoreName = $_
				if (Test-Path "cert:\$StoreScope\$StoreName")
				{
					try
					{
						$store = New-Object System.Security.Cryptography.X509Certificates.X509Store $StoreName, $StoreScope
						$store.Open([System.Security.Cryptography.X509Certificates.OpenFlags]::ReadWrite)
						$store.Add($cert)
						$store.Close()
						Write-Verbose "Successfully added '$certfile' to 'cert:\$StoreScope\$StoreName'."
					}
					catch
					{
						Write-Error ("Error adding '$certfile' to 'cert:\$StoreScope\$StoreName': $_ .") -ErrorAction:Continue
					}
				}
				else
				{
					Write-Warning "Certificate store '$StoreName' does not exist. Skipping..."
				}
			}
		}
	}
	
	end
	{ }
}