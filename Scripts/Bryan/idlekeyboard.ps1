Write-Host "Keep-alive with Scroll Lock toggle"

$WShell = New-Object -Com "WScript.Shell"

while ($true) {
    $WShell.sendkeys("{SCROLLLOCK}")
    Start-Sleep -Milliseconds 100
    $WShell.sendkeys("{SCROLLLOCK}")
    Start-Sleep -Seconds 60
}