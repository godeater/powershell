﻿Function Load-AppConfig {
	param(
		[Parameter(Mandatory=$true)]
		[string]$Path
	)
	$global:appSettings = @{}
	$config = [xml](Get-Content $Path)
	foreach($addNode in $config.configuration.appSettings.add) {
		if($addNode.Value.Contains(',')){
			# Array case
			$value = $addNode.Value.Split(',')
			for($i =0;$i -lt $value.Length; $i++){
				$value[$i] = $value[$i].Trim()
			}
		} else {
			# Scalar case
			$value = $addNode.Value
		}
		$global:appSettings[$addNode.Key] = $value
	}
}

Function Configure-Logging {
	[Reflection.Assembly]::LoadFile("P:\Documents\WindowsPowershell\Modules\Log4Net\log4net.dll") | Out-Null
	$LogManager = [log4net.LogManager]
	$global:logger = $LogManager::GetLogger("PowerShell")

	if((Test-Path $appsettings["logConfigFilePath"]) - eq $false){
		$message = "WARNING:logging config file not found:" + $appSettings["logConfigFilePath"]
		Write-Host
		Write-Host $message -foregroundcolor Yellow
		Write-Host
	} else {
		$configFile = New-Object System.IO.FileInfo($appSettings["logConfigFilePath"])
		$xmlConfigurator = [log4net.Config.XmlConfigurator]::ConfigureAndWatch($configFile)
	}
}

Function log-info {
	param(
		[Parameter(Mandatory=$true)]
		[string]$message
	)
	Write-Host $message
	$logger.Info($message)
}

Function log-warn {
	param(
		[Parameter(Mandatory=$true)]
		[string]$message
	)
	Write-Warn $message
	$logger.Warn($message)
}

Function Startup {
	Load-AppConfig "Test-Log4Net.config"
	Configure-Logging
}
	

