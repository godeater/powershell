﻿#requires -version 2.0

# Highlight-Syntax.ps1
# version 2.0
# by Jeff Hillman
#
# this script uses the System.Management.Automation.PsParser class
# to highlight PowerShell syntax with HTML.
$emailSignatureStyle =
@"
<!--
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Arial","sans-serif";
	mso-fareast-language:EN-US;}
h1
	{mso-style-priority:9;
	mso-style-link:"Heading 1 Char";
	margin-top:24.0pt;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:0cm;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:14.0pt;
	font-family:"Arial","sans-serif";
	color:#3E6080;
	mso-fareast-language:EN-US;}
h2
	{mso-style-priority:9;
	mso-style-link:"Heading 2 Char";
	margin-top:10.0pt;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:0cm;
	margin-bottom:.0001pt;
	page-break-after:avoid;
	font-size:13.0pt;
	font-family:"Arial","sans-serif";
	color:#5381AC;
	mso-fareast-language:EN-US;}
p.MsoTitle, li.MsoTitle, div.MsoTitle
	{mso-style-priority:10;
	mso-style-link:"Title Char";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:15.0pt;
	margin-left:0cm;
	mso-add-space:auto;
	border:none;
	padding:0cm;
	font-size:26.0pt;
	font-family:"Arial","sans-serif";
	color:#001A4E;
	letter-spacing:.25pt;
	mso-fareast-language:EN-US;}
p.MsoTitleCxSpFirst, li.MsoTitleCxSpFirst, div.MsoTitleCxSpFirst
	{mso-style-priority:10;
	mso-style-link:"Title Char";
	mso-style-type:export-only;
	margin:0cm;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	border:none;
	padding:0cm;
	font-size:26.0pt;
	font-family:"Arial","sans-serif";
	color:#001A4E;
	letter-spacing:.25pt;
	mso-fareast-language:EN-US;}
p.MsoTitleCxSpMiddle, li.MsoTitleCxSpMiddle, div.MsoTitleCxSpMiddle
	{mso-style-priority:10;
	mso-style-link:"Title Char";
	mso-style-type:export-only;
	margin:0cm;
	margin-bottom:.0001pt;
	mso-add-space:auto;
	border:none;
	padding:0cm;
	font-size:26.0pt;
	font-family:"Arial","sans-serif";
	color:#001A4E;
	letter-spacing:.25pt;
	mso-fareast-language:EN-US;}
p.MsoTitleCxSpLast, li.MsoTitleCxSpLast, div.MsoTitleCxSpLast
	{mso-style-priority:10;
	mso-style-link:"Title Char";
	mso-style-type:export-only;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:15.0pt;
	margin-left:0cm;
	mso-add-space:auto;
	border:none;
	padding:0cm;
	font-size:26.0pt;
	font-family:"Arial","sans-serif";
	color:#001A4E;
	letter-spacing:.25pt;
	mso-fareast-language:EN-US;}
p.MsoSubtitle, li.MsoSubtitle, div.MsoSubtitle
	{mso-style-priority:11;
	mso-style-link:"Subtitle Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Arial","sans-serif";
	color:#5381AC;
	letter-spacing:.75pt;
	mso-fareast-language:EN-US;
	font-style:italic;}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:#5381AC;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:#5381AC;
	text-decoration:underline;}
span.Heading1Char
	{mso-style-name:"Heading 1 Char";
	mso-style-priority:9;
	mso-style-link:"Heading 1";
	font-family:"Arial","sans-serif";
	color:#3E6080;
	font-weight:bold;}
span.Heading2Char
	{mso-style-name:"Heading 2 Char";
	mso-style-priority:9;
	mso-style-link:"Heading 2";
	font-family:"Arial","sans-serif";
	color:#5381AC;
	font-weight:bold;}
span.TitleChar
	{mso-style-name:"Title Char";
	mso-style-priority:10;
	mso-style-link:Title;
	font-family:"Arial","sans-serif";
	color:#001A4E;
	letter-spacing:.25pt;}
span.SubtitleChar
	{mso-style-name:"Subtitle Char";
	mso-style-priority:11;
	mso-style-link:Subtitle;
	font-family:"Arial","sans-serif";
	color:#5381AC;
	letter-spacing:.75pt;
	font-style:italic;}
span.EmailStyle23
	{mso-style-type:personal-compose;
	font-family:"Arial","sans-serif";
	color:windowtext;}
.MsoChpDefault
	{mso-style-type:export-only;
	font-family:"Arial","sans-serif";
	mso-fareast-language:EN-US;}
@page WordSection1
	{size:612.0pt 792.0pt;
	margin:72.0pt 72.0pt 72.0pt 72.0pt;}
div.WordSection1
	{page:WordSection1;}
-->
"@
$emailSignatureBody =
@"
<br>
<div class=WordSection1>
<p class=MsoNormal><o:p>&nbsp;</o:p></p>
<p class=MsoNormal><o:p>&nbsp;</o:p></p>
<p class=MsoNormal><b><span style='font-size:9.0pt;color:#1F497D;mso-fareast-language:EN-GB'>Bryan Childs | Digital Platform Engineer</span></b><b><span style='font-size:9.0pt;color:#333333;mso-fareast-language:EN-GB'> | </span></b><b><span style='font-size:9.0pt;color:#1F497D;mso-fareast-language:EN-GB'>Digital &amp; Engineering Services&nbsp;| RBS</span></b><b><span style='font-size:9.0pt;color:#333333;mso-fareast-language:EN-GB'> | </span></b><span style='font-size:8.5pt;color:black;mso-fareast-language:EN-GB'>250 Bishopsgate | London | EC2M 4AA | Tel: +44 (0) 207 085 0904&nbsp;</span><b><span style='font-size:9.0pt;color:#323E4F;mso-fareast-language:EN-GB'><o:p></o:p></span></b></p><p class=MsoNormal><b><span style='font-size:7.5pt;color:#1F497D;mso-fareast-language:EN-GB'><o:p>&nbsp;</o:p></span></b></p><p class=MsoNormal><b><span style='font-size:7.5pt;color:#1F497D;mso-fareast-language:EN-GB'>DATA CLASSIFICATION: Unless otherwise stated the content and any information contained within this e-mail is&nbsp;Confidential.</span></b><span style='font-size:11.0pt;mso-fareast-language:EN-GB'><o:p></o:p></span></p><p class=MsoNormal><o:p>&nbsp;</o:p></p></div>
"@


Filter Html-Encode {
    $_ = $_ -replace "&", "&amp;"
    $_ = $_ -replace " ", "&nbsp;"
    $_ = $_ -replace "<", "&lt;"
    $_ = $_ -replace ">", "&gt;"

    $_
}

Function Highlight-Syntax{
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        [string]$code,
        [switch]$LineNumbers,
        [switch]$IncludeSignature
    )

    #if ( Test-Path $code -ErrorAction SilentlyContinue ){
    if ( Test-Path $code){
         $code = Get-Content $code | Out-String
    }

    $backgroundColor = "#DDDDDD"
    $foregroundColor = "#000000"
    $lineNumberColor = "#404040"

    $PSTokenType = [System.Management.Automation.PSTokenType]

    $colorHash = @{
        #$PSTokenType::Unknown            = $foregroundColor;
        $PSTokenType::Command            = "#C86400";
        #$PSTokenType::CommandParameter   = $foregroundColor;
        #$PSTokenType::CommandArgument    = $foregroundColor;
        $PSTokenType::Number             = "#800000";
        $PSTokenType::String             = "#800000";
        $PSTokenType::Variable           = "#000080";
        #$PSTokenType::Member             = $foregroundColor;
        #$PSTokenType::LoopLabel          = $foregroundColor;
        #$PSTokenType::Attribute          = $foregroundColor;
        $PSTokenType::Type               = "#404040";
        $PSTokenType::Operator           = "#C86400";
        #$PSTokenType::GroupStart         = $foregroundColor;
        #$PSTokenType::GroupEnd           = $foregroundColor;
        $PSTokenType::Keyword            = "#C86400";
        $PSTokenType::Comment            = "#008000";
        $PSTokenType::StatementSeparator = "#C86400";
        #$PSTokenType::NewLine            = $foregroundColor;
        $PSTokenType::LineContinuation   = "#C86400";
        #$PSTokenType::Position           = $foregroundColor;

    }


    # replace the tabs with spaces
    $code = $code -replace "\t", ( " " * 4 )
    if($IncludeSignature){
        $sigStyle = $emailSignatureStyle
        $sigBody = $emailSignatureBody
    } else {
        $sigStyle = ""
        $sigBody = ""
    }
    if ( $LineNumbers ){
        $highlightedCode = "<li style='color: $lineNumberColor; padding-left: 5px'>"
    }
    else{
        $highlightedCode = ""
    }

    $parser = [System.Management.Automation.PsParser]
    $lastColumn = 1
    $lineCount = 1

    foreach ( $token in $parser::Tokenize( $code, [ref] $null ) | Sort-Object StartLine, StartColumn ){
        # get the color based on the type of the token
        $color = $colorHash[ $token.Type ]

        if ( $color -eq $null ){
            $color = $foregroundColor
        }

        # add whitespace
        if ( $lastColumn -lt $token.StartColumn ){
            $highlightedCode += ( "&nbsp;" * ( $token.StartColumn - $lastColumn ) )
        }
        switch ( $token.Type ){
            $PSTokenType::String {
                $string = "<span style='color: {0}'>{1}</span>" -f $color, ( $code.SubString( $token.Start, $token.Length ) | Html-Encode )

                # we have to highlight each piece of multi-line strings
                if ( $string -match "\r\n" ){
                    # highlight any line continuation characters as operators
                    $string = $string -replace "(``)(?=\r\n)",
                    ( "<span style='color: {0}'>``</span>" -f $colorHash[ $PSTokenType::Operator ] )

                    $stringHtml = "</span><br />`r`n"

                    if ( $LineNumbers ){
                        $stringHtml += "<li style='color: $lineNumberColor; padding-left: 5px'>"
                    }

                    $stringHtml += "<span style='color: $color'>"

                    $string = $string -replace "\r\n", $stringHtml
                }

                $highlightedCode += $string
                break
            }

            $PSTokenType::NewLine {
                $highlightedCode += "<br />`r`n"
                #$highlightedCode = "<p>" + $highlightedCode + "</p>`r`n"

                if ( $LineNumbers ){
                    $highlightedCode += "<li style='color: $lineNumberColor; padding-left: 5px'>"
                }

                $lastColumn = 1
                ++$lineCount
                break
            }
            default {
                if ( $token.Type -eq $PSTokenType::LineContinuation ){
                    $lastColumn = 1
                    ++$lineCount
                }

                $highlightedCode += "<span style='color: {0}'>{1}</span>" -f $color, ( $code.SubString( $token.Start, $token.Length ) | Html-Encode )
            }
        }

        $lastColumn = $token.EndColumn
    }

# Working style in div = "font-family: 'Consolas'; font-size: 10px; background-color: #3F3F3F; color: #7F9F7F; text-align: start; text-indent: 0px;"
# put the highlighted code in the pipeline
[string]$output =
"<!DOCTYPE html PUBLIC `"-//W3C//DTD XHTML 1.0 Transitional//EN`" `"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd`">
<HTML xmlns=`"http://www.w3.org/1999/xhtml`">
<HEAD>
<META http-equiv=`"content-type`" content=`"text/html; charset=utf-8`"/>
<META name=`"GENERATOR`" content=`"Powershell Script`"/>
<TITLE>Powershell Script</TITLE>
<STYLE type=`"text/css`">
<!--
P{margin:0;}" + $sigStyle + "
-->
</STYLE>
</HEAD>
<BODY>
<DIV class=`"powershell`" style=`"font-family: Consolas; " +
            "font-size: 10pt; " +
            "background-color: $backgroundColor; " +
            "color: $foregroundColor; " +
            "text-align: start; " +
            "text-indent: 0px;`">"

if ( $LineNumbers ) {
    $digitCount =  $lineCount.ToString().Length

    $output += "<ol start='1' style='border-left: " +
                      "solid 1px $lineNumberColor; " +
                      "margin-left: $( ( $digitCount * 10 ) + 15 )px; " +
                      "padding: 0px;'>"
}

$output += $highlightedCode

if ( $LineNumbers ){
    $output += "</ol>"
}

$output += "</div>" + $sigBody + "
</body>
</html>"

return $output

}