﻿Function Select-KItems{
	param(
		[int64[]]$inputStream,
		[int64]$n,
		[int64]$k
	)

	[int64]$i # index for elements in $inputStream
	[int64[]]$reservoir = $inputStream[0..$k]

	for($i=$k+1;$i -lt $n;$i++){
		[int64]$j = Get-Random -Maximum $i
		if($j -lt $k){
			$reservoir[$j] = $inputStream[$i]
		}
	}
	"Following are {0} randomly selected items from input" -f $k
	$reservoir
}

Function Find-VitterR{
	param(
		[int64[]]$a, # Not even sure we'll need this in Powershell
		[int64]$n,
		[int64]$CapN
	)
	[int64]$i = 0
	[int64]$j = -1
	[int64]$t
	[int64]$qu1 = - $n + 1 + $CapN
	[int64]$S
	[int64]$negalphainv = -13
	[int64]$threshold = - $nagaphainv * $n

	[double]$nreal = $n
	[double]$CapNreal = $CapN
	[double]$ninv = 1.0/$n
	[double]$nminlinv = 1.0/($n - 1)
	[double]$Vprime = [math]::exp([math]::log( $(Get-Random -Maximum ([double]::MaxValue))) * $ninv)
	[double]$qu1real = -$nreal + 1.0 + $CapNreal
	[double]$negSreal
	[double]$U
	[double]$X
	[double]$y1
	[double]$y2
	[double]$top
	[double]$bottom
	[double]$limit

	while($true){
		$nminlinv=1.0/(-1.0+$nreal)
		while($true){
			while($true){
				$X = $CapNreal * (- $Vprime + 1.0)
				$S = [math]::Floor($X)
				if($S -lt $qu1){
					break
				}
				$Vprime = [math]::exp([math]::log( $(Get-Random -Maximum ([double]::MaxValue))) * $ninv)
			}
			$U = Get-Random -Maximum ([double]::MaxValue)
			$negSreal = -$S
			$y1 = [math]::exp([math]::log($U * $CapNreal / $qu1real) * $nminlinv)
			$Vprime = $y1 * (- $X/$CapNreal+1.0)*($qu1real/($negSreal+$qu1real))
			if($Vprime -le 1.0){
				break
			}
			$y2 = 1.0
			$top = -1.0 + $CapNReal
			if(-1+$n -gt $S){
				$bottom = - $nreal + $CapNreal
				$limit = - $S + $CapN
			}else{
				$bottom = -1.0 + $negSreal + $CapNreal
				$limit = - $S + $CapN
			}
			for($t = $CapN - 1; $t -ge $limit; $t--){
				$y2 = ($y2 * $top) / $bottom
				$top--
				$bottom--
			}
			if($CapNreal / (- $X/+$CapNreal) -ge $y1 * [math]::exp([math]::log($y2)*$nminlinv)){
				$Vprime = [math]::exp([math]::log( $(Get-Random -Maximum ([double]::MaxValue))) * $ninv)
				break
			}
			$Vprime = [math]::exp([math]::log( $(Get-Random -Maximum ([double]::MaxValue))) * $ninv)
		}
		$j += $S + 1
		$a[$i++] = $j
		$CapN = - $S + (-1 + $CapN)
		$CapNreal = $negSReal + ( 1.0 + $CapNreal)
		$n--
		$nreal--
		$ninv = $nminlinv
		$qu1 = - $S + $qu1
		$qu1real = $negSreal + $qu1real
		$threshold += $negalphainv
	}

	if($n -gt 1){
		Find-Vitter_a $($a+$i) $n $capN $j
	} else {
		$S = [math]::floor($CapN * $Vprime)
		$j += $S + 1
		$a[$i++] = $j
	}
}

Function Find-Vitter_a{
	param(
		[int64[]]$a,
		[int64]$n,
		[int64]$CapN,
		[int64]$j
	)
	[int64]$S
	[int64]$i = 0
	[double]$top = $CapN - $n
	[double]$CapNreal = $CapN
	[double]$V
	[double]$quot
	while($n -ge 2){
		$V = Get-Random -Maximum ([double]::MaxValue)
		$S = 0
		$quot = $top / $CapNreal
		while($quot -gt $V){
			$S++
			$top--
			$CapNreal--
			$quot = ($quot * $top) / $CapNreal
		}
		$j += $S + 1
		$a[$i++] = $j
		$CapNreal--
		$n--
	}
	$S = [math]::floor([math]::round($CapNreal) * $(Get-Random -Maximum ([double]::MaxValue)))
	$j += $S + 1
	$a[$i++] = $j
}
			
	