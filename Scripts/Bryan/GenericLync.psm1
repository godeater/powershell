﻿$ErrorActionPreference = "Stop"
$NamespaceHash = @{
	"DocItemSet"               = "urn:schema:Microsoft.Rtc.Management.Xds.AppLayer.2008";
	"BaseTypes"                = "urn:schema:Microsoft.Rtc.Management.BaseTypes.2008";
	"AnchoredXml"              = "urn:schema:Microsoft.Rtc.Management.ScopeFramework.2008";
	"Roles"                    = "urn:schema:Microsoft.Rtc.Management.Settings.Roles.2008";
	"Topology"                 = "urn:schema:Microsoft.Rtc.Management.Deploy.Topology.2008";
	"ServiceRoles"             = "urn:schema:Microsoft.Rtc.Management.Deploy.ServiceRoles.2008";
	"PolicyClient"             = "urn:schema:Microsoft.Rtc.Management.Policy.Client.2008";
	"PolicyClientVersion"      = "urn:schema:Microsoft.Rtc.Management.Policy.ClientVersion.2008";
	"PolicyExternalAccess"     = "urn:schema:Microsoft.Rtc.Management.Policy.ExternalAccess.2008";
	"PolicyIm"                 = "urn:schema:Microsoft.Rtc.Management.Policy.Im.2008";
	"PolicyLocation"           = "urn:schema:Microsoft.Rtc.Management.Policy.Location.2008";
	"PolicyMeeting"            = "urn:schema:Microsoft.Rtc.Management.Policy.Meeting.2008";
	"PolicyUserPin"            = "urn:schema:Microsoft.Rtc.Management.Policy.UserPin.2008";
	"PolicyVoice"              = "urn:schema:Microsoft.Rtc.Management.Policy.Voice.2008";
	"SettingsBwPolicy"         = "urn:schema:Microsoft.Rtc.Management.Settings.BandwidthPolicyServiceConfiguration.2008";
	"SettingsConfAccNum"       = "urn:schema:Microsoft.Rtc.Management.Settings.ConferenceAccessNumbers.2009";
	"SettingsDiagnostics"      = "urn:schema:Microsoft.Rtc.Management.Settings.Diagnostics.2008";
	"SettingsHealthMon"        = "urn:schema:Microsoft.Rtc.Management.Settings.HealthMonitoring.2010";
	"SettingsImFilter"         = "urn:schema:Microsoft.Rtc.Management.Settings.ImFilter.2008";
	"SettingsMedia"            = "urn:schema:Microsoft.Rtc.Management.Settings.Media.2008";
	"SettingsNetworkConf"      = "urn:schema:Microsoft.Rtc.Management.Settings.NetworkConfiguration.2008";
	"SettingsPstnConf"         = "urn:schema:Microsoft.Rtc.Management.Settings.PstnConf.2008";
	"SettingsRegistrar"        = "urn:schema:Microsoft.Rtc.Management.Settings.Registrar.2008";
	"SettingsServerApp"        = "urn:schema:Microsoft.Rtc.Management.Settings.ServerApplication.2008";
	"SettingsServiceAssign"    = "urn:schema:Microsoft.Rtc.Management.Settings.ServiceAssignment.2008";
	"SettingsSimpleUrl"        = "urn:schema:Microsoft.Rtc.Management.Settings.SimpleUrl.2008";
	"SettingsSipProxy"         = "urn:schema:Microsoft.Rtc.Management.Settings.SipProxy.2008";
	"SettingsTrunkConf"        = "urn:schema:Microsoft.Rtc.Management.Settings.TrunkConfiguration.2009";
	"SettingsUserServices"     = "urn:schema:Microsoft.Rtc.Management.Settings.UserServices.2008";
	"SettingsWeb"              = "urn:schema:Microsoft.Rtc.Management.Settings.Web.2008";
	"SettingsWebConf"          = "urn:schema:Microsoft.Rtc.Management.Settings.WebConf.2008";
	"SettingsCDR"              = "urn:schema:Microsoft.Rtc.Management.Settings.CallDetailRecording.2008";
	"DeployWebServices"        = "urn:schema:Microsoft.Rtc.Management.Deploy.WebServices.2011";
    "SettingsAddressBook"      = "urn:schema:Microsoft.Rtc.Management.Settings.AddressBook.2008";
	"SettingsCallParkOR"       = "urn:schema:Microsoft.Rtc.Management.Settings.CallParkOrbitRouting.2009";
	"SettingsCallParkSS"       = "urn:schema:Microsoft.Rtc.Management.Settings.CallParkServiceSettings.2008";
	"SettingsCentralLog"       = "urn:schema:Microsoft.Rtc.Management.Settings.CentralizedLoggingConfig.2012";
	"SettingsCMSCerts"         = "urn:schema:Microsoft.Rtc.Management.Settings.CMSCertificates.2012";
	"SettingsEdge"             = "urn:schema:Microsoft.Rtc.Management.Settings.Edge.2008";
	"SettingsUserGroupRouting" = "urn:schema:Microsoft.Rtc.Management.Settings.UserRoutingGroup.2011"
}

Function Get-LyncAdminSession {
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$false,ValueFromPipeline=$true)]
		[ValidateSet("Prod","UAT","ENG")]
        [string]$where = "Prod"
	)
	# Lync 2010
	#$produri = "https://rtcadmin.ubs.com/OcsPowershell"
	# Skype for Business
	#$produri = "https://gb-sfb-fee-01.ubs.net/OcsPowershell"
    $produri = "https://nldn9221pap.ubsprod.msad.ubs.net/OcsPowershell"
	$testuri = "https://gb-sfb-web-01.ubstest.net/OcsPowershell"
	#$testuri = "https://nzur1110uap.test.msad.ubstest.net/OcsPowershell"
	#$testuri = "https://nzur1109uap.test.msad.ubstest.net/OcsPowershell"
	#$testuri = "https://rtcadmin.ubstest.com/OcsPowershell"
    #$testuri = "https://gb-sfb-web-01.ubstest.net/OcsPowershell"
	$enguri  = "https://rtcadmin.ubseng.net/OcsPowershell"

    if(!($global:AdminCreds)){
        switch($where){
            "Prod"{
                Get-AdminCredential -Environment "p"
				$uri = $produri
            }
            "UAT"{
                Get-AdminCredential -Environment "u"
				$uri = $testuri
            }
			"ENG"{
				Get-AdminCredential -Environment "e"
				$uri = $enguri
			}
        }
    }
	switch($where){
        "Prod"{
			$uri = $produri
        }
        "UAT"{
			$uri = $testuri
        }
		"ENG"{
			$uri = $enguri
		}
    }
    $LyncOptions = New-PSSessionOption -SkipRevocationCheck -SkipCACheck -SkipCNCheck
    $PSSession   = New-PSSession -ConnectionUri $uri -SessionOption $LyncOptions -Credential $global:AdminCreds
    $global:LyncSession = Import-PSSession -Session $PSSession
    Import-Module -Global $global:LyncSession | Out-Null
    # return $global:LyncSession
}

Function ConvertFrom-HomePool{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        [string]$PoolID
    )
    # TODO : Take pool and server identifiers in the form n:n-n
    #        and turn into a discrete fqdn.
	# Note, this currently assumes prod

	$propsToLoad = @("dNSHostName","msRTCSIP-PoolData","msRTCSIP-PoolDisplayName","msRTCSIP-PoolType","msRTCSIP-PoolVersion") | Out-Null

	$directoryEntry = "LDAP://UBSPROD.MSAD.UBS.NET/CN=Pools,CN=RTC Service,CN=Services,CN=Configuration,DC=UBSPROD,DC=MSAD,DC=UBS,DC=NET"
	$domain         = New-Object DirectoryServices.DirectoryEntry($directoryEntry)
	$searcher       = New-Object DirectoryServices.DirectorySearcher($directoryEntry)

    $searcher.SearchRoot  = $domain
    $searcher.PageSize    = $pageSize
    $searcher.Filter      = "(&(name=$PoolID))"
    $searcher.SearchScope = "Subtree"

    foreach($i in $propsToLoad){
        $searcher.PropertiesToLoad.Add($i)
    }

    $results = $searcher.FindAll()
    $output = @()

    foreach($resultset in $results){
        $properties = New-Object 'System.Collections.Generic.SortedDictionary[string,string]'
        $one_ad_object = $resultset.Properties
        $one_ad_object.GetEnumerator() | ForEach-Object {
            $properties.Add($_.Key,$_.Value)
        }
        $output += $properties
    }
	if($($output | Select-Object -Property "msrtcsip-pooldisplayname")){
		return $output | Select-Object -Property  "msrtcsip-pooldisplayname"
	} else {
		return $output
	}
}

Function Get-ClusterID{
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        [string]$Machines
    )
    $parsed = $Machines | Select-String -Pattern "^(\d+)\:(\d+)-(\d+)$"
    $result = New-Object PSObject -Property @{SiteID=$parsed.Matches.Groups[1].Value;
                                              ClusterID=$parsed.Matches.Groups[2].Value;
                                              MachineID=$parsed.Matches.Groups[3].Value;}
    return $result
}

Function Get-PoolID{
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        [string]$PoolID
    )
    $parsed = $PoolID | Select-String -Pattern "^(\d+)\:(\d+)$"
    $result = New-Object PSObject -Property @{SiteID=$parsed.Matches.Groups[1].Value;
                                              ClusterID=$parsed.Matches.Groups[2].Value;}
    return $result
}

Function Import-CsConfig{
    if($(Get-Command -ErrorAction SilentlyContinue Export-CsConfiguration) -ne $null){
        [System.Reflection.Assembly]::LoadWithPartialName('System.IO.Compression') | Out-Null

        $ZipBytes = Export-CsConfiguration -AsBytes
        $ZipStream = New-Object System.IO.Memorystream
        $ZipStream.Write($ZipBytes,0,$ZipBytes.Length)
        $ZipArchive = New-Object System.IO.Compression.ZipArchive($ZipStream)
        $ZipEntry = $ZipArchive.GetEntry('DocItemSet.xml')
        $EntryReader = New-Object System.IO.StreamReader($ZipEntry.Open())
        $DocItemSet = $EntryReader.ReadToEnd()
        return $DocItemSet
        } else {
            Write-Warning "Lync module doesn't appear to be loaded. Load it and then retry."
        }
}

Function Get-PoolRole{
	param(
		[Parameter(Mandatory=$true)]
		$Roles,
        [Parameter(Mandatory=$true)]
        $sqlRoles,
		[Parameter(Mandatory=$true)]
		$TopNs,
		[Parameter(Mandatory=$true)]
		$TrustedApps,
		[Parameter(Mandatory=$true)]
		[string]$PoolFqdn,
		[Parameter(Mandatory=$true)]
		[int]$ClusterID,
		[Parameter(Mandatory=$true)]
		[int]$SiteId
	)
	$RoleArray = @()
	$PoolRole = ""

	$ThisClustersRoles = $Roles | Where-Object {
		($_.Descendants($TopNs + "InstalledOn").Descendants($TopNs + "ClusterId").Attribute("Number").Value -eq $ClusterId)	-and
		($_.Descendants($TopNs + "InstalledOn").Descendants($TopNs + "ClusterId").Attribute("SiteId").Value -eq $SiteId)
    }

	$ThisClustersRoles | ForEach-Object {
		$RoleArray += $_.Elements($TopNs + "ServiceId").Attribute("RoleName").Value
		[int]$ThisClustersVersion = $_.Attribute("ServiceVersion").Value
	}

    $ThisClustersSQLRoles = $sqlRoles | Where-Object {
        ($_.Descendants($TopNs + "ClusterId").Attribute("Number").Value -eq $ClusterId) -and
        ($_.Descendants($TopNs + "ClusterId").Attribute("SiteId").Value -eq $SiteID)
    }

    $ThisClustersSQLRoles | ForEach-Object {
        $RoleArray += "SQL Backend"
        if($_.Attribute("AlwaysOnPrimaryNodeFqdn").Value -ne $null){
            [int]$ThisClustersVersion = 7
        }else{
            [int]$ThisClustersVersion = 5
        }
    }

	if(($RoleArray -contains "Registrar") -and ($RoleArray -contains "WebServices") -and ($RoleArray -notcontains "UserServices")){
		$PoolRole = "Director"
	} elseif (($RoleArray -contains "Registrar") -and ($RoleArray -contains "WebServices") -and ($RoleArray -contains "UserServices")){
		$PoolRole = "Front End"
	} elseif ($RoleArray -contains "MediationServer"){
		$PoolRole = "Mediation"
	} elseif ($RoleArray -contains "UserStore"){
		$PoolRole = "SQL Backend"
	} elseif (($RoleArray -contains "ConfServices") -and ($RoleArray -notcontains "UserServices")){
		$PoolRole = "AV"
	} elseif ($RoleArray -contains "PstnGateway") {
		$PoolRole = "PSTN Gateway"
	} elseif ($RoleArray -contains "MonitoringServer") {
		$PoolRole = "Monitoring"
	} elseif ($RoleArray -contains "MonitoringStore") {
		$PoolRole = "Monitoring Store"
	} elseif ($RoleArray -contains "FileStore") {
		$PoolRole = "File Store"
	} elseif ($RoleArray -contains "WacService") {
		$PoolRole = "WAC Server"
	} elseif (($RoleArray -contains "ExternalServer") -and ($RoleArray -notcontains "MonitoringServer")){
		$TrustedApps | ForEach-Object {
			if(($_.TrustedApplicationPoolFqdn -eq $PoolFqdn) -and ($_.LegacyApplicationName -eq "microsoft.rtc.chat.channelserver")){
				$PoolRole = "Group Chat"
			}
		}
		if($PoolRole -ne "Group Chat"){
			$PoolRole = "External Server"
		}
	} elseif (($RoleArray -contains "EdgeServer") -and ($RoleArray -contains "ManagementServices")){
		$PoolRole = "Edge"
	}
	if($PoolRole -eq ""){
        # Might be a SQL Instance
        if($RoleArray -contains "SQL Backend"){
            $PoolRole = "SQL Backend"
        } else {
            $PoolRole = "Unknown role for SiteID:$SiteId ClusterID:$ClusterID"
        }
	}
	if($ThisClustersVersion -eq 5){
		$Infrastructure = "Lync"
	}elseif($ThisClustersVersion -eq 7){
		$Infrastructure = "S4B"
	} else {
		[string]$Infrastructure = $ThisClustersVersion
	}

	return @{PoolRole=$PoolRole;PoolVersion=$Infrastructure}
}

Function Get-SiteDetails {
	param(
		[Parameter(Mandatory=$true)]
		$Sites,
		[Parameter(Mandatory=$true)]
		$TopNs,
		[Parameter(Mandatory=$true)]
		[int]$SiteId
	)

	$ThisSiteXml = $Sites | Where-Object  { $_.Attribute("SiteId").Value -eq $SiteId }
	$ThisSite = New-Object -Type PSCustomObject -Property @{
			SiteId=$ThisSiteXml.Attribute($TopNs + "SiteId").Value;
			Name=$ThisSiteXml.Descendants($TopNs + "Name").Value;
			Description=$ThisSiteXml.Descendants($TopNs + "Description").Value;
			City=$ThisSiteXml.Descendants($TopNs + "Location").Attribute("City").Value;
			State=$ThisSiteXml.Descendants($TopNs + "Location").Attribute("State").Value;
			CountryCode=$ThisSiteXml.Descendants($TopNs + "Location").Attribute("CountryCode").Value
	}
	return $ThisSite
}

Function Get-TopologyFromCsConfig{
<#
    .SYNOPSIS
    Attempt to bring some sanity to working with the Lync topology
    .DESCRIPTION
    The function loads in the output of the Export-CsConfiguration cmdlet, expands
    the zip file it returns in memory, and pulls out the DocItemSet.xml file, which
    contains the XML representation of the Lync Topology. (Dependency on Load-CsConfig)
    It then parses that XML to pull back each individual server in the topology,
    and gets the properties associated with that server, such as the pool it is
    in, the role it performs, the site it belongs to, etc.
#>
	# Working with Linq takes *some* of the pain of dealing with the hideousness
	# that is the Lync XML schema generated by Export-CsConfig.
	[Reflection.Assembly]::LoadWithPartialName("System.Xml.Linq") | Out-Null
	$machineInfo = @()

	# Set up namespace for Topology in the XML
	$TopNs = [System.Xml.Linq.XNamespace]::Get($NamespaceHash['Topology'])
	# Linq XDocument constructor requires a StringReader
	$StringReader = New-Object System.IO.StringReader($(Load-CsConfig))
	$LyncTopology = [System.Xml.Linq.XDocument]::Load($StringReader)

	# Also load in Trusted Application definitions, so we can work out
	# which servers are group chat servers
	$TrustedApps = Get-CsTrustedApplication

	# Find all the machines in the topology
	$machines = $LyncTopology.Descendants($TopNs + "Machine")

	# Find all the roles in the topology except SQL
	$roles = $LyncTopology.Descendants($TopNs + "Service")

    # Find all the SQL roles in the topolgy
    $sqlroles = $LyncTopology.Descendants($TopNS + "SqlInstance")

	# Find all the sites in the topology
    $sites = $LyncTopology.Descendants($TopNs + "CentralSite")

	# Get the properties of these machines and store in custom object
	$machines | ForEach-Object {
		$ClusterId         = $_.Parent.Descendants($TopNS + "ClusterId").Attribute("Number").Value
		$SiteId            = $_.Parent.Descendants($TopNs + "ClusterId").Attribute("SiteId").Value
		$Ordinal           = $_.Attribute("OrdinalInCluster").Value
		$Fqdn              = $_.Attribute("Fqdn").Value
		$PoolFqdn          = $_.Parent.Attribute("Fqdn").Value
		$PoolRoleVersion   = Get-PoolRole -Roles $roles -sqlRoles $sqlroles -TopNS $TopNs -TrustedApps $TrustedApps -PoolFqdn $PoolFqdn -ClusterID $ClusterId -SiteId $SiteId
		$SiteDetails       = Get-SiteDetails -Sites $sites -TopNS $TopNs -SiteId $SiteId
        $Shortname         = $($Fqdn.split(".")[0])
        try{
            $DistinguishedName = Get-AdComputer $Shortname | Select-Object -ExpandProperty DistinguishedName
        } catch {
            $DistinguishedName = ""
            Write-Warning "$Shortname is just a DNS name (probably)"
        }

		$ThisMachine = New-Object -Type PSCustomObject -Property @{
			Fqdn=$Fqdn;
			OrdinalInCluster=[int]$Ordinal
			ClusterId=[int]$ClusterId;
			SiteId=[int]$SiteId;
			PoolFqdn=$PoolFqdn;
			PoolRole=$PoolRoleVersion.PoolRole;
			Site=$SiteDetails.Name;
			Infrastructure=$PoolRoleVersion.PoolVersion;
            DistinguishedName=$DistinguishedName
		}
		$machineInfo += $ThisMachine
	}

	return $machineInfo

}

Function Get-QoSValue{
    [CmdletBinding()]
    param(
        [Parameter(Mandatory=$true)]
        $CsConfig
    )
    begin{
        $QoSArray = @()
        New-ImpersonateUser -Credential $AdminCreds
    }
    # HKLM\SOFTWARE\POLICIES\MICROSOFT\WINDOWS\QOS\ > Lync Audio QOS / Lync SIP QOS / Lync Video QOS
    process {
        $CsConfig | ForEach-Object {
            $Shortname = $($_.Fqdn.Split("."))[0]
            try{
                Write-Verbose "Attempting to open registry on $Shortname"
                $Reg = [Microsoft.Win32.RegistryKey]::OpenRemoteBaseKey('LocalMachine', $Shortname)
                $LyncAudioKey   = $Reg.OpenSubKey("SOFTWARE\\Policies\\Microsoft\\Windows\\QOS\\Lync Audio QoS")
                Write-Verbose "Got Lync Audio Key"
                $LyncSIPKey     = $Reg.OpenSubKey("SOFTWARE\\Policies\\Microsoft\\Windows\\QOS\\Lync SIP QoS")
                Write-Verbose "Got Lync SIP Key"
                $LyncVideoKey   = $Reg.OpenSubKey("SOFTWARE\\Policies\\Microsoft\\Windows\\QOS\\Lync Video QoS")
                Write-Verbose "Got Lync Video Key"
                $LyncAudioValue = $LyncAudioKey.GetValue("DSCP Value")
                Write-Verbose "Got Lync Audio Value"
                $LyncSIPValue   = $LyncSIPKey.GetValue("DSCP Value")
                Write-Verbose "Got Lync SIP Value"
                $LyncVideoValue = $LyncVideoKey.GetValue("DSCP Value")
                Write-Verbose "Got Lync Video Value"
                $QoSObject = New-Object -Type PSCustomObject -Property @{
                    Computer=$Shortname;
                    LyncAudioQoS=$LyncAudioValue;
                    LyncSIPQoS=$LyncSIPValue;
                    LyncVideoQoS=$LyncVideoValue
                }
                $QoSArray += $QoSObject
            } catch {
                Write-Warning $_
                $QoSObject = New-Object -Type PSCustomObject -Property @{
                    Computer=$Shortname;
                    LyncAudioQoS="";
                    LyncSIPQoS="";
                    LyncVideoQoS=""
                }
                $QoSArray += $QoSObject
            }
        }
    }
    end{
        Remove-ImpersonateUser
        return $QoSArray
    }
}


Function Get-SiteFromClusterID{
    param(
        [Parameter(Mandatory=$true)]
        [string]$DocItemSet,
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        [string]$PoolString
    )

	$parsed = $PoolString | Select-String -Pattern "^(\d+)\:(\d+)$"
    $PoolID = New-Object PSObject -Property @{SiteID=$parsed.Matches.Groups[1].Value;
                                              ClusterID=$parsed.Matches.Groups[2].Value;}
    [xml]$CsConfig = New-Object Xml.XmlDocument
    $CsConfig.LoadXml($DocItemSet)

    $NameSpace = New-Object Xml.XmlNamespaceManager($CsConfig.NameTable)
	foreach($NsEntry in $NamespaceHash.Keys){
		$NameSpace.AddNamespace($NsEntry, $NamespaceHash[$NsEntry])
	}

    $topology       = $CsConfig.SelectNodes("//DocItemSet:DocItem[@Name='urn:xds:Microsoft.Rtc.Management.Deploy.Topology.2008:Topology.Host.Global']",$NameSpace)
    $XPathToSite    = "//Topology:CentralSite[@SiteId='" + $PoolID.SiteID + "']"
    $Site           = $topology.SelectNodes($XPathToSite,$Namespace)
    $XPathToCluster = "//Topology:Cluster[Topology:ClusterId[@SiteId='" +
                      $PoolID.SiteID +
                      "' and @Number='" +
                      $PoolID.ClusterID +
                      "']]"
    $Cluster        = $topology.SelectNodes($XPathToCluster,$NameSpace)
    if($Site -ne $null -and $Cluster -ne $null){
        Write-Host ("Site Name: {0}`r`nPool FQDN: {1}" -f $Site.Name,$Cluster.Fqdn)
    } else {
        Write-Warning "Not found in topology"
    }

}

Function Get-ServerFromClusterID{
    param(
        [Parameter(Mandatory=$true)]
        [string]$DocItemSet,
        [Parameter(Mandatory=$true)]
        [String]$PoolMachineString
    )
    $parsed = $PoolMachineString | Select-String -Pattern "^(\d+)\:(\d+)-(\d+)$"
    #$MachineConstruct = New-Object PSObject -Property @{SiteID=$parsed.Matches.Groups[1].Value;
    #                                          ClusterID=$parsed.Matches.Groups[2].Value;
    #                                          MachineID=$parsed.Matches.Groups[3].Value;}
    [xml]$CsConfig = New-Object Xml.XmlDocument
    $CsConfig.LoadXml($DocItemSet)

    $NameSpace = New-Object Xml.XmlNamespaceManager($CsConfig.NameTable)
    $NameSpace.AddNamespace("DocItemSetNS", "urn:schema:Microsoft.Rtc.Management.Xds.AppLayer.2008")
    $NameSpace.AddNamespace("TopologyNS",   "urn:schema:Microsoft.Rtc.Management.Deploy.Topology.2008")

    $topology   = $CsConfig.SelectNodes("//DocItemSetNS:DocItem[@Name='urn:xds:Microsoft.Rtc.Management.Deploy.Topology.2008:Topology.Host.Global']",$NameSpace)
    $XPathToMachine = "//TopologyNS:Cluster/TopologyNS:Machine[@OrdinalInCluster='" +
                      $PoolMachineString.MachineID +
                      "' and ../TopologyNS:ClusterId[@Number='" +
                      $PoolMachineString.ClusterID +
                      "' and @SiteId='" +
                      $PoolMachineString.SiteId + "']]"

    $Machine    = $topology.SelectNodes($XPathToMachine,$NameSpace)
    if($Machine -ne $null){
        return $Machine.Fqdn
    } else {
        Write-Warning "Not found in topology"
    }
}

Function Expand-PoolNames{
    param(
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        [string[]]$PoolIDs
    )
    $CsConfig = Import-CsConfig
    $Count = 0
    $PoolIDs | Foreach-Object {
        $Count++
        $PoolID = Get-ClusterID -Machines $_
        $Fqdn = Get-ServerFromClusterID -DocItemSet $CsConfig -PoolMachineString $PoolID
        Write-Host ("{0}:`t{1}" –f $Count,$Fqdn)
    }
}

Function Mount-VantageServerDrives{
	foreach($key in $vantagePools){
		foreach($server in $vantageAppServers[$key]){
			$PsDriveName = "VNT_" + $server
			$PsDriveRoot =  "\\" + $server + "\c$"
			New-PSDrive -Name $PsDriveName -PSProvider FileSystem -Scope Global -Credential $AdminCreds -Root $PsDriveRoot
		}
	}
}

Function Get-Certificate  {
<#
  .SYNOPSIS
   Retrieves  certificates from a local or remote system.
  .DESCRIPTION
   Retrieves certificates from a local or remote system.
  .PARAMETER Computername
   A single or list of computernames to perform search against
  .PARAMETER StoreName
   The name of the certificate store name that you want to search
  .PARAMETER StoreLocation
   The location of the certificate store.
  .PARAMETER DaysUntilExpired
   Find Certficates which expire within the next n days.
#>
	[cmdletbinding(DefaultParameterSetName = 'All')]
	Param (
		[parameter(ValueFromPipeline=$True,ValueFromPipelineByPropertyName=$True)]
		[Alias('PSComputername','__Server','IPAddress')]
		[string[]]$Computername = $env:COMPUTERNAME,
		[System.Security.Cryptography.X509Certificates.StoreName]$StoreName = 'My',
		[System.Security.Cryptography.X509Certificates.StoreLocation]$StoreLocation = 'LocalMachine',
		[parameter(ParameterSetName='Expire')]
		[Int]$DaysUntilExpired,
		[parameter(ParameterSetName='Expire')]
		[Switch]$HideExpired,
		[parameter(ParameterSetName='Subject')]
		[string]$Subject
	)
	Process {
		ForEach ($Computer in $Computername) {
			Try {
				Write-Verbose ("Connecting to {0}\{1}" -f "\\$($Computername)\$($StoreName)",$StoreLocation)
				$CertStore = New-Object System.Security.Cryptography.X509Certificates.X509Store -ArgumentList "\\$($Computername)\$($StoreName)", $StoreLocation
				$CertStore.Open('ReadOnly')
				Write-Verbose "ParameterSetName: $($PSCmdlet.ParameterSetName)"
				Switch ($PSCmdlet.ParameterSetName) {
					'All' {
						$CertStore.Certificates
					}
					'Expire' {
						$CertStore.Certificates | Where {
							$_.NotAfter -lt (Get-Date).AddDays($DaysUntilExpired)
						} | ForEach {
							$Days = Switch ((New-TimeSpan -End $_.NotAfter).Days) {
								{$_ -gt 0} {$_}
								Default {'Expired'}
							}
							$Cert = $_ | Add-Member -MemberType NoteProperty -Name DaysUntilExpired -Value $Days -PassThru
							If ($HideExpired -AND $_.DaysUntilExpired -ne 'Expired') {
								$Cert
							} ElseIf (-Not $HideExpired) {
								$Cert
							}
						}
					}
					'Subject' {
						$CertStore.Certificates | Where {
							$_.Subject.Split(',')[0] -eq "CN=" + $Subject
						}
					}
				}
			} Catch {
				Write-Warning "$($Computer): $_"
			}
		}
	}
}

Function Add-LyncTopologyToMremote {
	param(
		[Parameter(Mandatory=$true)]
		[Alias('Parent')]
		[string]$ParentNode,
		[Parameter(Mandatory=$true)]
		[Alias('Env')]
		[string]$Environment
	)
	if(!(Get-Module "mRemote")){
		try{
			Import-Module "P:\Documents\WindowsPowershell\Scripts\Bryan\mRemote.psm1"
		} catch {
			Write-Error "Oh noes!"
			Exit
		}
	}
	$mRemoteConnections = Load-Connections "P:\Documents\WindowsPowershell\confCons.xml"
	$LyncTopology = Get-TopologyFromCsConfig

	Add-Container -ConfCons $mRemoteConnections.Document -Parent $ParentNode -Name ($ParentNode + $Environment)

	$LyncSites = $LyncTopology | Sort-Object -Unique Site | Select-Object Site -ExpandProperty Site
	ForEach($Site in $LyncSites){
		Add-Container -ConfCons $mRemoteConnections.Document -Parent ($ParentNode + $Environment) -Name $Site
		$PoolRoles = $LyncTopology | Where-Object { $_.Site -eq $Site -and $_.PoolRole -ne 'PSTN Gateway' } | Sort-Object -Unique PoolRole | Select-Object PoolRole -ExpandProperty PoolRole
		ForEach($Role in $PoolRoles){
			$SitePoolName = $Site + " - " + $Role
			Add-Container -ConfCons $mRemoteConnections.Document -Parent $Site -Name $SitePoolName
			$PoolServers = $LyncTopology | Where-Object { $_.Site -eq $Site -and $_.PoolRole -eq $Role } | Sort-Object -Unique Fqdn | Select-Object Fqdn -ExpandProperty Fqdn
			ForEach($LyncServer in $PoolServers){
				$LyncHost = $LyncServer
				Add-Connection -ConfCons $mRemoteConnections.Document -Parent $SitePoolName -Name $LyncHost -FQDN $LyncServer
				Write-Host "$SitePoolName \ $LyncHost"
			}
		}
	}

	Save-Connections $mRemoteConnections.Document -PathToConfCons "P:\Documents\WindowsPowershell\confCons_with_new_Lync.xml"
}

Function Get-GCDatabaseData {
	param(
		[Parameter(Mandatory=$true)]
		[string]$Query
	)
	$GCDatabase = $global:GCDatabase

	$sqlConnection = New-Object -TypeName Data.SqlClient.SqlConnection("Data Source="      + $GCDatabase.Server + `
	                                                                   ";Initial Catalog=" + $GCDatabase.Name + `
	                                                                   ";User Id="         + $GCDatabase.User + `
	                                                                   ";Password="        + $(PBE_Decrypt -cipherText $GCDatabase.Pass) + ";")
	$sqlAdapter = New-Object -TypeName Data.SqlClient.SqlDataAdapter($Query,$sqlConnection)
	$sqlDataTable = New-Object -TypeName Data.DataTable
	$sqlAdapter.Fill($sqlDataTable) | Out-Null
	$sqlConnection.Close()
	return $sqlDataTable
}

Function Get-ChatWebData {
	param(
		[Parameter(Mandatory=$true)]
		[string]$Query
	)
	$CWebDB = $global:ChatWebDatabase

	$sqlConnection = New-Object -TypeName Data.SqlClient.SqlConnection("Data Source=" + $CWebDB.Server + `
	                                                                   ";Initial Catalog=" + $CWebDB.Name + `
	                                                                   ";User Id=" + $CWebDB.User + `
	                                                                   ";Password=" + $(PBE_Decrypt -cipherText $CWebDB.Pass) + ";")
	$sqlAdapter = New-Object -TypeName Data.SqlClient.SqlDataAdapter($Query,$sqlConnection)
	$sqlDataTable = New-Object -TypeName Data.DataTable
	$sqlAdapter.Fill($sqlDataTable) | Out-Null
	$sqlConnection.Close()
	return $sqlDataTable
}


Function Get-CsGcUser {
	param(
		[Parameter(Mandatory=$true)]
		[string]$User
	)
	$UserInfo = $null

	Function Get-FromSip {
		param(
			[Parameter(Mandatory=$true)]
			[bool]$WithPrefix,
			[Parameter(Mandatory=$true)]
			[string]$sipstring
		)
		if(!($WithPrefix)){
			$sipstring = "sip:" + $sipstring
		}
		$sqlQuery = "select * from tblPrincipal where prinUri like '%" + $sipstring + "%'"
		return $(Get-GCDatabaseData -Query $sqlQuery)
	}

	Function Get-FromDomainName {
		param(
			[Parameter(Mandatory=$true)]
			[string]$DomainString
		)
		$Domain     = $DomainString.Split('\')[0]
		$User       = $DomainString.Split('\')[1]
		$sipstring  = $(Get-AdUser -Server $Domain -Identity $User -Properties msRTCSIP-PrimaryUserAddress).'msRTCSIP-PrimaryUserAddress'
		$sqlQuery   = $sqlQuery = "select * from tblPrincipal where prinUri like '%" + $sipstring + "%'"
		return $(Get-GCDatabaseData -Query $sqlQuery)
	}


	#What does $User look like?
	switch -regex ($User){
		<# sip address #>        "^sip:.*"                                    { $UserInfo = Get-FromSip -WithPrefix:$true  -sipstring $User }
		<# email address #>      "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$"   { $UserInfo = Get-FromSip -WithPrefix:$false -sipstring $User }
		<# domain\user #>        "^(\w*)\\(\w*)$"                             { $UserInfo = Get-FromDomainName -DomainString $User }
	    <# default #>            default                                      { $UserInfo = "Could not determine user format" }
	}

	return $UserInfo
}

Function Get-CsGcChannel {
	[CmdletBinding(DefaultParameterSetName="ChannelName")]
	param(
		[Parameter(Mandatory=$true,ParameterSetName="ChannelName",Position=0)]
		[string]$Channel,
		[Parameter(Mandatory=$true,ParameterSetName="BBSID",Position=0)]
		[string]$SingleBBSId,
		[Parameter(Mandatory=$true,ParameterSetName="MultipleBBSIDs",Position=0)]
		[string[]]$MultipleBBSIds,
		[Parameter(Mandatory=$true,ParameterSetName="GCID",Position=0)]
		[int]$SingleGCId,
		[Parameter(Mandatory=$true,ParameterSetName="MultipleGCIDs",Position=0)]
		[int[]]$MultipleGCIds
	)

	Function Get-FromChannelName {
		param(
			[Parameter(Mandatory=$true)]
			[string]$ChannelName
		)
		$sqlQuery = "select * from tblNode where nodeName like '%" + $ChannelName + "%'"
		return $(Get-GCDatabaseData -Query $sqlQuery)
	}

	Function Get-FromGCID {
		[CmdletBinding()]
		param(
			[Parameter(Mandatory=$true,ParameterSetName="Single")]
			[int]$SingleGCId,
			[Parameter(Mandatory=$true,ParameterSetName="Mulitple")]
			[int[]]$MultipleGCIds
		)
		switch($PSCmdLet.ParameterSetName){
			"Single" {
				$sqlQuery = "select * from tblNode where nodeId =" + $SingleGCId
			}
			"Multiple" {
				$IdList = $MultipleGCIds -join ','
				$sqlQuery = "select * from tblNode where nodeId in (" + $IdList + ")"
			}
		}
		return $(Get-GCDatabaseData -Query $sqlQuery)
	}

	Function Get-FromBBSID {
		[CmdletBinding()]
		param(
			[Parameter(Mandatory=$true,ParameterSetName="Single")]
			[string]$SingleBBSId,
			[Parameter(Mandatory=$true,ParameterSetName="Multiple")]
			[string[]]$MultipleBBSIds
		)
		switch($PSCmdLet.ParameterSetName) {
			"Single" {
				$sqlQuery = "select gcID from bbs_groupchat_mapping where bbsID='" + $SingleBBSId + "'"
				$gcName   = $(Get-ChatWebData -Query $sqlQuery).gcID
				$sqlQuery = "select * from tblNode where nodeName='" + $gcName + "'"
			}
			"Multiple"{
				$IdList = $MultipleBBSIds -join "','"
				$sqlQuery = "select gcID from bbs_groupchat_mapping where bbsID in ('" + $IdList + "')"
				$gcNames  = $(Get-ChatWebData -Query $sqlQuery).gcID -join "','"
				$sqlQuery = "select * from tblNode where nodeName in ('" + $gcNames + "')"
			}
		}
		return $(Get-GCDatabaseData -Query $sqlQuery)
	}

	switch($PSCmdlet.ParameterSetName){
		"ChannelName"    { Get-FromChannelName -ChannelName $Channel }
		"BBSID"          { Get-FromBBSID -SingleBBSId $SingleBBSId  }
		"MultipleBBSIDs" { Get-FromBBSID -MultipleBBSIds $MultipleBBSIds }
		"GCID"           { Get-FromGCID  -SingleGCId $SingleGCId   }
	    "MultipleGCIDs"  { Get-FromGCID  -MultipleGCIds $MultipleGCIds }
	}
}

Function Expand-GCPref {
	param(
		[ValidateNotNullOrEmpty()]
		[string]$base64String
	)

	# decode preference string from base 64 to a byte array
	[byte[]]$base64Bytes = [System.Convert]::FromBase64String($base64String)

	# Write byte array to a memoryStream
	$Stream = New-Object IO.Memorystream (,$base64Bytes)

	# Create a gzipStream to decompress it
	$gzipStream = New-Object IO.Compression.GzipStream $Stream, ([IO.Compression.CompressionMode]::Decompress)

	# Prepare a seperate memorystream for the output
	$preferenceStream = New-Object IO.MemoryStream

	# Perform the decompression
	if($PSVersionTable.CLRVersion.Major -lt 4){
		Copy-Stream $gzipStream $PreferenceStream
	} else {
		$gzipStream.CopyTo($preferenceStream)
	}
	$preferenceStream.Seek(0,[IO.SeekOrigin]::Begin) | Out-Null

	# Put preferenceStream into a string
	$streamReader = New-Object IO.StreamReader $preferenceStream
	$preference = $streamReader.ReadToEnd()

	$preferenceStream.Close()

	return $preference
}

Function Compress-GCPref {
	param(
		[ValidateNotNullOrEmpty()]
		[string]$xmlString
	)
	[byte[]]$xmlBytes = [System.Text.Encoding]::UTF8.GetBytes($xmlString)
	$Stream = New-Object IO.Memorystream
	$gzipStream = New-Object IO.Compression.GZipStream $Stream, ([IO.Compression.CompressionMode]::Compress)
	$gzipStream.Write($xmlBytes,0,$xmlBytes.Length)
	$gzipStream.Close()

	[byte[]]$zippedResult = $Stream.ToArray()

	$base64String = [System.Convert]::ToBase64String($zippedResult)

	return $base64String
}

Function Copy-Stream{
	param(
		$Source,
		$Destination
	)

	[Byte[]]$Buffer   = New-Object Byte[](2048)
	[int]$bytesRead   = 0
	[long]$totalBytes = 0

	while(($bytesRead = $Source.Read($buffer,0,$buffer.Length)) -gt 0){
		$destination.Write($buffer,0,$bytesRead)
		$totalBytes += $bytesRead
	}
	return $totalBytes
}

Function ZipBytes {
	param(
		[ValidateNotNullOrEmpty()]
		[byte[]]$bytes
	)
	$memoryStream = New-Object IO.MemoryStream
	$gzipStream   = New-Object IO.Compression.GZipStream $memoryStream, ([IO.Compression.CompressionMode]::Compress)
	$gzipStream.Write($bytes,0,$bytes.Length)
	$gzipStream.Close()
	[byte[]]$result = $memoryStream.ToArray()
	$memoryStream.Close()
	return $result
}

Function UnzipBytes {
	param(
		[ValidateNotNullOrEmpty()]
		[byte[]]$bytes
	)
	$stream       = New-Object IO.MemoryStream (,$bytes)
	$gzipStream   = New-Object IO.Compression.GZipStream $stream, ([IO.Compression.CompressionMode]::Decompress)
	$memoryStream = New-Object IO.MemoryStream
	[int]$num = 0
	while(($num = $gzipStream.ReadByte()) -ne -1){
		$memoryStream.WriteByte([byte]$num)
	}
	$gzipStream.Close()
	[byte[]]$result = $memoryStream.ToArray()
	$memoryStream.Close()
	return $result
}

Function UnzipBytesFromBase64 {
	param(
		[ValidateNotNullOrEmpty()]
		[string]$base64String
	)
	[byte[]]$fromBase64 = UnzipBytes([System.Convert]::FromBase64String($base64String))
	return [System.Text.Encoding]::UTF8.GetString($fromBase64)
}

Function ZipBytesToBase64 {
	param(
		[ValidateNotNullOrEmpty()]
		[byte[]]$bytes
	)
	[string]$toBase64 = [System.Convert]::ToBase64String($(ZipBytes($bytes)))
	return $toBase64
}

Function Get-GCPrefToXml {
	param(
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[ValidateNotNullOrEmpty()]
        [ValidatePattern('^(sip:){0,1}[\w-]')]
		[ValidatePattern('^(sip:){0,1}[\w-]+\.[\w-]+@ubs.com$')]
        [ValidatePattern('^(?=^.{1,254}$)(^(?:(?!\d+\.)[a-zA-Z0-9_\-]{1,63}\.?)+(?:[a-zA-Z]{2,})$)')]

		[string]$SipAddress,
		[Parameter(Mandatory=$false)]
		[ValidateSet('DisplayThemes','FileDownloadPrefs','Folders','GeneratedFilters','GroupChannels','IMChannels',
					 'Invitations','KnownDomains','ShowFeature','UserFilters','UserOptions')]
		[string]$PreferenceSet="GroupChannels"
	)
	$connectionString = "Server=nzurc036psq.ubsprod.msad.ubs.net;Initial Catalog=LyncGCF;Trusted_Connection=True;"

	if(!$SipAddress.StartsWith('sip:')){
		$SipAddress = "sip:" + $SipAddress
	}

	[string]$sqlQuery = "select prefContent from tblPreference where prefLabel like '" + $SipAddress + "%" + $PreferenceSet + "'"
	$sqlResult = Run-PrivilegedSql -ConnectionString $connectionString -QueryString $sqlQuery -DatabaseCredentials $global:AdminCreds
	if($sqlResult.Tables[0].Rows.Count -eq 1){
		[string]$base64String = $sqlResult.Tables[0].Rows[0].prefContent
	} else {
		throw "No data for $SipAddress found in the Group Chat Database"
		exit
	}

	return UnzipBytesFromBase64 $base64String
}

Function Set-GCPrefFromXml {
	param(
		[Parameter(Mandatory=$true,ValueFromPipeline=$true)]
		[ValidateNotNullOrEmpty()]
		[ValidatePattern('^(sip:){0,1}[\w-]+\.[\w-]+@ubs.com$')]
		[string]$SipAddress,
		[Parameter(Mandatory=$false)]
		[ValidateSet('DisplayThemes','FileDownloadPrefs','Folders','GeneratedFilters','GroupChannels','IMChannels',
					 'Invitations','KnownDomains','ShowFeature','UserFilters','UserOptions')]
		[string]$PreferenceSet="GroupChannels",
		[Parameter(Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[string]$PreferenceXML
	)
	[int]$prefSeqId = 0
	$connectionString = "Server=nzurc036psq.ubsprod.msad.ubs.net;Initial Catalog=LyncGCF;Trusted_Connection=True;"

	if(!$SipAddress.StartsWith('sip:')){
		$SipAddress = "sip:" + $SipAddress
	}
	[string]$base64String = ZipBytesToBase64 $([System.Text.Encoding]::UTF8.GetBytes($PreferenceXML))

	[string]$sqlQuery = "select prefSeqID from tblPreference where prefLabel like '" + $SipAddress + "%" + $PreferenceSet + "'"
	$sqlResult = Run-PrivilegedSql -ConnectionString $connectionString -QueryString $sqlQuery -DatabaseCredentials $global:AdminCreds
	if($sqlResult.Tables[0].Rows.Count -eq 1){
		$prefSeqID = $sqlResult.Tables[0].Rows[0].prefSeqID
		$prefSeqID++
	} else {
		throw "Can't find current prefSeqID for this preferenceSet"
		exit
	}

	[string]$sqlQuery = "update tblPreference set prefSeqID = " + $prefSeqID + ", prefContent = '" + $base64String + "' where prefLabel like '" + $SipAddress + "%" + $PreferenceSet + "'"
	#return $sqlQuery
	$sqlResult = Run-PrivilegedSql -ConnectionString $connectionString -QueryString $sqlQuery -DatabaseCredentials $global:AdminCreds
	return $sqlResult
}

Function New-ImpersonateUser {
	<#
    .SYNOPSIS
    Impersonates another user on a local machine or Active Directory.

    .DESCRIPTION
    New-ImpersonateUser uses the LogonUser method from the advapi32.dll to get a token that can then be used to call the WindowsIdentity.Impersonate method in order to impersonate another user without logging off from the current session.  You can pass it either a PSCredential or each field separately. Once impersonation is done, it is highly recommended that Remove-ImpersonateUser (a function added to the global scope at runtime) be called to revert back to the original user.

    .PARAMETER Credential
    The PS Credential to be used, eg. from Get-Credential

    .PARAMETER Username
    The username of the user to impersonate.

    .PARAMETER Domain
    The domain of the user to impersonate.  If the user is local, use the name of the local computer stored in $env:COMPUTERNAME

    .PARAMETER Password
    The password of the user to impersonate.  This is in cleartext which is why sending a PSCredential is recommended.

    .PARAMETER Quiet
    Using the Quiet parameter will force New-ImpersonateUser to have no outputs.

    .INPUTS
    None.  You cannot pipe objects to New-ImpersonateUser

    .OUTPUTS
    System.String
    By default New-ImpersonateUser will output strings confirming Impersonation and a reminder to revert back.

    None
    The Quiet parameter will force New-ImpersonateUser to have no outputs.

    .EXAMPLE
    PS C:\> New-ImpersonateUser -Credential (Get-Credential)

    This command will impersonate the user supplied to the Get-Credential cmdlet.
    .EXAMPLE
    PS C:\> New-ImpersonateUser -Username "user" -Domain "domain" -Password "password"

    This command will impersonate the user "domain\user" with the password "password."
    .EXAMPLE
    PS C:\> New-ImpersonateUser -Credential (Get-Credential) -Quiet

    This command will impersonate the user supplied to the Get-Credential cmdlet, but it will not produce any outputs.
    .NOTES
    It is recommended that you read some of the documentation on MSDN or Technet regarding impersonation and its potential complications, limitations, and implications.
    Author:  Chris Carter
    Version: 1.0

    .LINK
    http://msdn.microsoft.com/en-us/library/chf6fbt4(v=vs.110).aspx (Impersonate Method)
    http://msdn.microsoft.com/en-us/library/windows/desktop/aa378184(v=vs.85).aspx (LogonUser function)
    Add-Type

    #>

	[CmdletBinding(DefaultParameterSetName="Credential")]
	Param(
		[Parameter(ParameterSetName="ClearText", Mandatory=$true)][string]$Username,
		[Parameter(ParameterSetName="ClearText", Mandatory=$true)][string]$Domain,
		[Parameter(ParameterSetName="ClearText", Mandatory=$true)][string]$Password,
		[Parameter(ParameterSetName="Credential", Mandatory=$true, Position=0)][PSCredential]$Credential,
		[Parameter()][Switch]$Quiet
	)

	#Import the LogonUser Function from advapi32.dll and the CloseHandle Function from kernel32.dll
	Add-Type -Namespace Import -Name Win32 -MemberDefinition @"
[DllImport("advapi32.dll", SetLastError = true)]
public static extern bool LogonUser(string user, string domain, string password, int logonType, int logonProvider, out IntPtr token);

[DllImport("kernel32.dll", SetLastError = true)]
public static extern bool CloseHandle(IntPtr handle);
"@

	#Set Global variable to hold the Impersonation after it is created so it may be ended after script run
	$Global:ImpersonatedUser = @{}
	#Initialize handle variable so that it exists to be referenced in the LogonUser method
	$tokenHandle = 0

	#Pass the PSCredentials to the variables to be sent to the LogonUser method
	if ($Credential) {
		Get-Variable Username, Domain, Password | ForEach-Object {
			Set-Variable $_.Name -Value $Credential.GetNetworkCredential().$($_.Name)}
	}

	#Call LogonUser and store its success.  [ref]$tokenHandle is used to store the token "out IntPtr token" from LogonUser.
	$returnValue = [Import.Win32]::LogonUser($Username, $Domain, $Password, 2, 0, [ref]$tokenHandle)

	#If it fails, throw the verbose with the error code
	if (!$returnValue) {
		$errCode = [System.Runtime.InteropServices.Marshal]::GetLastWin32Error();
		Write-Host "Impersonate-User failed a call to LogonUser with error code: $errCode"
		throw [System.ComponentModel.Win32Exception]$errCode
	}
	#Successful token stored in $tokenHandle
	else {
		#Call the Impersonate method with the returned token. An ImpersonationContext is returned and stored in the
		#Global variable so that it may be used after script run.
		$Global:ImpersonatedUser.ImpersonationContext = [System.Security.Principal.WindowsIdentity]::Impersonate($tokenHandle)

		#Close the handle to the token. Voided to mask the Boolean return value.
		[void][Import.Win32]::CloseHandle($tokenHandle)

		#Write the current user to ensure Impersonation worked and to remind user to revert back when finished.
		if (!$Quiet) {
			Write-Host "You are now impersonating user $([System.Security.Principal.WindowsIdentity]::GetCurrent().Name)"
			Write-Host "It is very important that you call Remove-ImpersonateUser when finished to revert back to your user." `
              -ForegroundColor DarkYellow -BackgroundColor Black
		}
	}

	#Clean up sensitive variables
	$Username = $Domain = $Password = $Credential = $null
}

#Function put in the Global scope to be used when Impersonation is finished.
Function Remove-ImpersonateUser {
	<#
    .SYNOPSIS
    Used to revert back to the orginal user after New-ImpersonateUser is called. You can only call this function once; it is deleted after it runs.

    .INPUTS
    None.  You cannot pipe objects to Remove-ImpersonateUser

    .OUTPUTS
    None.  Remove-ImpersonateUser does not generate any output.
    #>

	#Calling the Undo method reverts back to the original user.
	$ImpersonatedUser.ImpersonationContext.Undo()

	#Clean up the Global variable.
	Remove-Variable ImpersonatedUser -Scope Global
}

Function Invoke-PrivilegedSql {
	param(
		[Parameter(Mandatory=$true,Position=0,HelpMessage="e.g. 'Server=myServerAddress;Database=myDataBase;Trusted_Connection=True;' ")]
		[string]$ConnectionString,
		[Parameter(Mandatory=$true,Position=1)]
		[string]$QueryString,
		[Parameter(Mandatory=$true,Position=2)]
		[PSCredential]$DatabaseCredentials
	)

	try{
		New-ImpersonateUser -Credential $DatabaseCredentials -Quiet
		# Connect to DB
		$Connection = New-Object Data.SqlClient.SQLConnection
		$Connection.ConnectionString = $ConnectionString
		$Connection.Open()

		$Command = New-Object Data.SqlClient.SqlCommand($QueryString,$Connection)
		$Adapter = New-Object Data.SqlClient.SqlDataAdapter
		$DataSet = New-Object Data.DataSet

		$Adapter.SelectCommand = $Command
		[void]$Adapter.Fill($DataSet)

		$Connection.Close()

		return $DataSet
	}catch{
		Write-Error $_
	}finally{
		Remove-ImpersonateUser
	}
}

Function Send-MindAlignMessage {
	param(
		[Parameter(Mandatory=$false)]
		[string]$UBSChatLib = "P:\Documents\WindowsPowershell\Modules\MindAlignChatLib\MindAlignChatLib.dll",
		[ValidateNotNullOrEmpty()]
		[Alias('Server')]
		[string]$MessageServer,
		[Parameter(Mandatory=$false)]
		[string]$AuthServer,
		[ValidateNotNullOrEmpty()]
		[string]$MindAlignUser,
		[ValidateNotNullOrEmpty()]
		[string]$MindAlignPassword,
		[ValidateNotNullOrEmpty()]
		[string]$Channel,
		[ValidateNotNullOrEmpty()]
		[string]$Message
	)
	if(-not $AuthServer){
		$AuthServer = $MessageServer
	}
	if(-not ('Ubs.Component.MindAlignChatLib.ChatClient' -as [type])){
		if(Test-Path $UBSChatLib -PathType Leaf){
			[string]$ResolvedPath = (Resolve-Path $UBSChatLib).Path
			[Reflection.Assembly]::LoadFile($ResolvedPath) | Out-Null
		} else {
			throw "$UBSChatLib not found"
		}
	}
	try{
		$ChatClient = New-Object -Type Ubs.Component.MindAlignChatLib.ChatClient($MindAlignUser,$MindAlignPassword,"NotRequired",$MessageServer,$AuthServer)
		$ChatClient.doJoin($Channel)
		$ChatClient.doSend($Channel,$Message)
		$ChatClient.doQuit()
		Remove-Variable ChatClient
	} catch {
		throw "Failed $_"
	}
}

Function Enable-PointsharpUser {
	param(
		[ValidatePattern('^(sip:){0,1}[\w-]+\.[\w-]+@ubs.com$')]
		$SipAddress,
		[ValidateSet("GB","US","CH")]
		$Region
	)
	switch($region) {
		"GB" { Move-CsUser -Identity $SipAddress -Target gb-sfb-fee-01.ubs.net -Confirm:$False }
		"CH" { Move-CsUser -Identity $SipAddress -Target ch-sfb-fee-01.ubs.net -Confirm:$False }
		"US" { Move-CsUser -Identity $SipAddress -Target us-sfb-fee-01.ubs.net -Confirm:$False }
	}

	Get-CsUser -Identity $SipAddress | Grant-CsClientVersionPolicy -PolicyName "Default Client Version policy plus Mobile"
	Get-CsUser -Identity $SipAddress | Grant-CsMobilityPolicy -PolicyName "EXT_AVP External Mobility Policy with PSTN"
	Get-CsUser -Identity $SipAddress
}

Export-ModuleMember -Variable * -Function * -Alias *

