﻿
Function Convert-FWProfileType {
	param ($ProfileCode)
	$FWprofileTypes.keys | foreach -begin {[String[]]$descriptions= @()} `
	                               -process {if ($profileCode -bAND $_) {$descriptions += $FWProfileTypes[$_]} } `
	                               -end {$descriptions}
}

Function Get-FirewallConfig {
	$fw = New-Object -ComObject HNetCfg.FwPolicy2
	"Active Profiles(s) :" + (Convert-fwprofileType $fw.CurrentProfileTypes)
	@(1,2,4) | Select-Object @{Name="Network Type"     ;expression={$fwProfileTypes[$_]}},
                             @{Name="Firewall Enabled" ;expression={$fw.FireWallEnabled($_)}},
                             @{Name="Block All Inbound";expression={$fw.BlockAllInboundTraffic($_)}},
                             @{name="Default In"       ;expression={$FwAction[$fw.DefaultInboundAction($_)]}},
                             @{Name="Default Out"      ;expression={$FwAction[$fw.DefaultOutboundAction($_)]}}
}

Function Get-FireWallRule {
	param ($Name, $Direction, $Enabled, $Protocol, $fwprofile, $action, $grouping)
	$Rules=(New-object -ComObject HNetCfg.FwPolicy2).rules
	If ($name)      {$rules= $rules | where-object {$_.name     -like $name}}
	If ($direction) {$rules= $rules | where-object {$_.direction  -eq $direction}}
	If ($Enabled)   {$rules= $rules | where-object {$_.Enabled    -eq $Enabled}}
	If ($protocol)  {$rules= $rules | where-object {$_.protocol   -eq $protocol}}
	If ($fwprofile) {$rules= $rules | where-object {$_.Profiles -bAND $fwprofile}}
	If ($Action)    {$rules= $rules | where-object {$_.Action     -eq $Action}}
	If ($Grouping)  {$rules= $rules | where-object {$_.Grouping -Like $Grouping}}
	return $rules
}

Get-FireWallRule