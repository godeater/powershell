﻿if(!(Get-Module "GenericLync")){
    Import-Module -DisableNameChecking "\\ubsprod.msad.ubs.net\UserData\CHILDSB\Home\Documents\WindowsPowershell\Scripts\Bryan\GenericLync.psm1"
}


$vantagePools                        = @('zurich')
$vantageAppServers = @{}
$vantageAppServers[$vantagePools[0]] = @('nzur4194uap','nzur4196uap','nzur4292uap','nzur5615uap','nzur6299uap')

$vantageSqlServers = @{}
$vantageSqlServers[$vantagePools[0]] = @('nzur4232usq','nzur5621usq','nzur4222usq','nzur4223usq','nzur6300usq')

$LyncUserProps = @('msRTCSIP-AcpInfo',
    'msRTCSIP-ApplicationOptions',
    'msRTCSIP-ArchivingEnabled',
    'msRTCSIP-DeploymentLocator',
    'msRTCSIP-FederationEnabled',
    'msRTCSIP-GroupingID',
    'msRTCSIP-InternetAccessEnabled',
    'msRTCSIP-Line',
    'msRTCSIP-LineServer',
    'msRTCSIP-OptionFlags',
    'msRTCSIP-OriginatorSid',
    'msRTCSIP-OwnerUrn',
    'msRTCSIP-PrimaryHomeServer',
    'msRTCSIP-PrimaryUserAddress',
    'msRTCSIP-PrivateLine',
    'msRTCSIP-TargetHomeServer',
    'msRTCSIP-TargetUserPolicies',
    'msRTCSIP-TenantId',
    'msRTCSIP-UserEnabled',
    'msRTCSIP-UserExtension',
    'msRTCSIP-UserLocationProfile',
    'msRTCSIP-UserPolicies',
    'msRTCSIP-UserPolicy')

$LyncPoolProps = @(
    'msRTCSIP-ApplicationList',
    'msRTCSIP-BackEndServer',
    'msRTCSIP-PoolData',
    'msRTCSIP-PoolDisplayName',
    'msRTCSIP-PoolDomainFQDN',
    'msRTCSIP-PoolFunctionality',
    'msRTCSIP-PoolType',
    'msRTCSIP-PoolVersion',
    'msRTCSIP-TrustedServiceLinks')

Export-ModuleMember -Variable * -Function * -Alias *
