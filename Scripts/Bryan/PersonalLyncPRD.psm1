﻿if(!(Get-Module "GenericLync")){
    Import-Module -DisableNameChecking "\\ubsprod.msad.ubs.net\UserData\CHILDSB\Home\Documents\WindowsPowershell\Scripts\Bryan\GenericLync.psm1"
}

$global:GCDatabase = New-Object -TypeName PSObject -Property @{Server="nzurc036psq";
															   Name="LyncGCF";
															   User="svc_gcchatweb";
															   Pass="tUzeBi36NU6YyDUj1/h3bA=="}

$global:ChatWebDatabase = New-Object -TypeName PSObject -Property @{Server="nzurc036psq";
																	Name="CWebDB1";
																	User="svc_gcchatweb";
																	Pass="tUzeBi36NU6YyDUj1/h3bA=="}


$LyncUserProps = @('msRTCSIP-AcpInfo',
    'msRTCSIP-ApplicationOptions',
    'msRTCSIP-ArchivingEnabled',
    'msRTCSIP-DeploymentLocator',
    'msRTCSIP-FederationEnabled',
    'msRTCSIP-GroupingID',
    'msRTCSIP-InternetAccessEnabled',
    'msRTCSIP-Line',
    'msRTCSIP-LineServer',
    'msRTCSIP-OptionFlags',
    'msRTCSIP-OriginatorSid',
    'msRTCSIP-OwnerUrn',
    'msRTCSIP-PrimaryHomeServer',
    'msRTCSIP-PrimaryUserAddress',
    'msRTCSIP-PrivateLine',
    'msRTCSIP-TargetHomeServer',
    'msRTCSIP-TargetUserPolicies',
    'msRTCSIP-TenantId',
    'msRTCSIP-UserEnabled',
    'msRTCSIP-UserExtension',
    'msRTCSIP-UserLocationProfile',
    'msRTCSIP-UserPolicies',
    'msRTCSIP-UserPolicy')

$LyncPoolProps = @(
    'msRTCSIP-ApplicationList',
    'msRTCSIP-BackEndServer',
    'msRTCSIP-PoolData',
    'msRTCSIP-PoolDisplayName',
    'msRTCSIP-PoolDomainFQDN',
    'msRTCSIP-PoolFunctionality',
    'msRTCSIP-PoolType',
    'msRTCSIP-PoolVersion',
    'msRTCSIP-TrustedServiceLinks')

Function Map-GCServerDrives {
	$Servers = ("nzur1130pap","nzur1132pap","nzur1134pap")
	$NetworkCredential = $AdminCreds.GetNetworkCredential()
	$Domain = $NetworkCredential.Domain
	$UserName = $NetworkCredential.Username
	$Password = $NetworkCredential.Password
	foreach($server in $Servers){
		net use * \\$server\c$ /user:$Domain\$UserName $Password
	}
}


Export-ModuleMember -Variable * -Function * -Alias *
