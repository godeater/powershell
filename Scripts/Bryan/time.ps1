﻿function Measure-Command2 {
    param(
	[Parameter(Mandatory)]
	[ScriptBlock]$Expression,
	[Parameter(Mandatory=$false)]
	[int]$Samples=1,
	[Parameter(Mandatory=$false)]
	[Switch]$Silent,
	[Parameter(Mandatory=$false)]
	[Switch]$Long
    )
<#
.SYNOPSIS
  Runs the given scriptblock and returns the execution duration.
  Discovered on stackoverflow. https://stackoverflow.com/questions/3513650/timing-a-commands-execution-in-powershell

.EXAMPLE
  Measure-Command2 { ping -n 1 google.com }
#>
    $timings = @()
    do {
	$sw = New-Object Diagnostics.Stopwatch
	if($Silent){
	    $sw.Start()
	    $null = & $Expression
	    $sw.Stop()
	    Write-Host "." -NoNewLine
	} else {
	    $sw.Start()
	    & $Expression
	    $sw.Stop()
	}
	$timings += $sw.Elapsed

	$Samples--
    } while ($Samples -gt 0)

    Write-Host

    $stats = $timings | Measure-Object -Average -Minimum -Maximum -Property Ticks

    if($Long){
	Write-Host "Avg: $((New-Object System.TimeSpan $stats.Average).ToString())"
	Write-Host "Min: $((New-Object System.TimeSpan $stats.Minimum).ToString())"
	Write-Host "Max: $((New-Object System.TimeSpan $stats.Maximum).ToString())"
    } else {
	Write-Host "Avg: $((New-Object System.TimeSpan $stats.Average).TotalMilliseconds)ms"
	Write-Host "Min: $((New-Object System.TimeSpan $stats.Minimum).TotalMilliseconds)ms"
	Write-Host "Max: $((New-Object System.TimeSpan $stats.Maximum).TotalMilliseconds)ms"
    }
}

New-Alias -Name Time -Value Measure-Command2
