﻿Import-Module PowerShellLogging

Function Initialize-Logging{
	param(
		[Parameter(Mandatory=$false)]
		[ValidateNotNullOrEmpty()]
		[string]$Log4NetLib = "P:\Documents\WindowsPowershell\Modules\Log4Net\log4net.dll",
		[Parameter(Mandatory=$false)]
		[ValidateNotNullOrEmpty()]
		[string]$Log4NetConfig = "P:\Documents\WindowsPowershell\Modules\Log4Net\logConfig.xml",
		[Parameter(Mandatory=$false)]
		[ValidateNotNullOrEmpty()]
		[string]$loggerName = "root"
	)
	# Load assembly
	if(-not ('log4net.LogManager' -as [type])){
		if(Test-Path $Log4NetLib -PathType Leaf){
			[string]$ResolvedPath = (Resolve-Path $Log4NetLib).Path
			Add-Type -LiteralPath $ResolvedPath | Out-Null
		}else{
			throw "Log4net library not found at $Log4NetLib"
		}
	}
	# Set up log4net
	[log4net.LogManager]::ResetConfiguration()
	$LogManager = [log4net.LogManager]
	if(Test-Path $Log4NetConfig -PathType Leaf){
		$logConfigFilePath = Resolve-Path $Log4NetConfig
		$configFile = New-Object IO.FileInfo($Log4NetConfig)
		$xmlConfigurator = [log4net.Config.XmlConfigurator]::ConfigureAndWatch($configFile)
		$Global:logger = $LogManager::GetLogger($loggerName) 
		<#
          $Global:logsubscriber = Enable-OutputSubscriber `
		  -OnWriteError    { Write-Log4Net -LogLevel "Error"   -Line $args[0] -Prefix "[E]" } `
		  -OnWriteWarning  { Write-Log4Net -LogLevel "Warning" -Line $args[0] -Prefix "[W]" } `
		  -OnWriteOutput   { Write-Log4Net -LogLevel "Info"    -Line $args[0] -Prefix "[I]" } `
		  -OnWriteDebug    { Write-Log4Net -LogLevel "Debug"   -Line $args[0] -Prefix "[D]" } `
		  -OnWriteVerbose  { Write-Log4Net -LogLevel "Debug"   -Line $args[0] -Prefix "[V]" }
        #>
		return $logger
	} else {
		Write-Error "Couldn't find $Log4NetConfig"
		return $null
	}
}

Function Write-Log4Net{
	param(
		[ValidateSet("Error","Warning","Info","Debug")]
		[string]$LogLevel="Info",
		[ValidateNotNull()]
		[string]$Line = "",
		[string]$Prefix
	)
	$Line = $Line.Trim()
	$Prefix = $Prefix.Trim()

	if($Prefix){$Prefix += " "}
	$Line = $Prefix + $Line

	switch($LogLevel){
		"Error"    {$Global:logger.Error($Line)}
		"Warning"  {$Global:logger.Warn($Line)}
		"Info"     {$Global:logger.Info($Line)}
		"Debug"    {$Global:logger.Debug($Line)}
		default    {throw "Unknown loglevel : $LogLevel for message $Line"}
	}
}
<#
Function Stop-Log4Net{
	$Global:logsubscriber | Disable-OutputSubscriber
}
#>



Export-ModuleMember -Variable * -Function * -Alias *

		
