﻿$CsharpDefinition = @"
using System.Runtime.Interopservices;
using System;

namespace bryan {
	#region DllImport
	enum HTTP_SERVICE_CONFIG_ID	{
		HttpServiceConfigIPListenList = 0,
		HttpServiceConfigSSLCertInfo,
		HttpServiceConfigUrlAclInfo,
		HttpServiceConfigMax
	}

	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct HTTP_SERVICE_CONFIG_URLACL_KEY {
		[MarshalAs(UnmanagedType.LPWStr)]
		public string pUrlPrefix;
		
		public HTTP_SERVICE_CONFIG_URLACL_KEY(string urlPrefix) {
			pUrlPrefix = urlPrefix;
		}
	}

	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	struct HTTP_SERVICE_CONFIG_URLACL_PARAM	{
		[MarshalAs(UnmanagedType.LPWStr)]
		public string pStringSecurityDescriptor;

		public HTTP_SERVICE_CONFIG_URLACL_PARAM(string securityDescriptor) {
			pStringSecurityDescriptor = securityDescriptor;
		}
	}

	[StructLayout(LayoutKind.Sequential)]
	struct HTTP_SERVICE_CONFIG_URLACL_SET {
		public HTTP_SERVICE_CONFIG_URLACL_KEY   KeyDesc;
		public HTTP_SERVICE_CONFIG_URLACL_PARAM ParamDesc;
	}

	[StructLayout(LayoutKind.Sequential, Pack = 2)]
    public struct HTTPAPI_VERSION
    {
        public ushort HttpApiMajorVersion;
        public ushort HttpApiMinorVersion;

        public HTTPAPI_VERSION(ushort majorVersion, ushort minorVersion)
        {
        HttpApiMajorVersion = majorVersion;
        HttpApiMinorVersion = minorVersion;
        }
    }

	[DllImport("httpapi.dll", SetLastError = true)]
    public static extern uint HttpInitialize(
        HTTPAPI_VERSION Version,
        uint Flags,
        IntPtr pReserved);
	
	[DllImport("httpapi.dll", SetLastError = true)]
	static extern uint HttpSetServiceConfiguration(
		IntPtr ServiceIntPtr,
		HTTP_SERVICE_CONFIG_ID ConfigId,
		IntPtr pConfigInformation,
		int ConfigInformationLength,
		IntPtr pOverlapped);

	[DllImport("httpapi.dll", SetLastError = true)]
    static extern uint HttpDeleteServiceConfiguration(
        IntPtr ServiceIntPtr,
        HTTP_SERVICE_CONFIG_ID ConfigId,
        IntPtr pConfigInformation,
        int ConfigInformationLength,
        IntPtr pOverlapped);

	[DllImport("httpapi.dll", SetLastError = true)]
    public static extern uint HttpTerminate(
        uint Flags,
        IntPtr pReserved);
	#endregion

	#region Constants

	public const uint HTTP_INITIALIZE_CONFIG = 0x00000002;
    public const uint HTTP_SERVICE_CONFIG_SSL_FLAG_USE_DS_MAPPER = 0x00000001;
    public const uint HTTP_SERVICE_CONFIG_SSL_FLAG_NEGOTIATE_CLIENT_CERT = 0x00000002;
    public const uint HTTP_SERVICE_CONFIG_SSL_FLAG_NO_RAW_FILTER = 0x00000004;
    private static int NOERROR = 0;
    private static int ERROR_ALREADY_EXISTS = 183;

	#endregion

	public static class StaticMethods {
	
		void ReserveURL(string networkURL, string securityDescriptor){

			uint retVal = (unit) ErrorCodes.NOERROR; // NOERROR = 0

			retval = HttpApi.HttpInitialize(HttpApi.HTTPAPI_VERSION, HttpApi.HTTP_INITIALIZE_CONFIG, IntPtr.Zero);

			if ((uint) ErrorCodes.NOERROR == retVal)
			{
				HTTP_SERVICE_CONFIG_URLACL_KEY keyDesc = new HTTP_SERVICE_CONFIG_URLACL_KEY(networkURL);
				HTTP_SERVICE_CONFIG_URLACL_PARAM paramDesc = new HTTP_SERVICE_CONFIG_URLACL_PARAM(securityDescriptor);

				HTTP_SERVICE_CONFIG_URLACL_SET inputConfigInfoSet = new HTTP_SERVICE_CONFIG_URLACL_SET();
				inputConfigInfoSet.KeyDesc = keyDesc;
				inputConfigInfoSet.ParamDesc = paramDesc;

				IntPtr pInputConfigInfo = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(HTTP_SERVICE_CONFIG_URLACL_SET)));
				Marshal.StructureToPtr(inputConfigInfoSet, pInputConfigInfo, false);

				retVal = HttpApi.HttpSetServiceConfiguration(IntPtr.Zero,
															 HTTP_SERVICE_CONFIG_ID.HttpServiceConfigUrlAclInfo,
															 pInputConfigInfo,
															 Marshal.SizeOf(inputConfigInfoSet),
															 IntPtr.Zero);

				if ((uint) ErrorCodes.ERROR_ALREADY_EXISTS == retVal)  // ERROR_ALREADY_EXISTS = 183
				{
					retVal = HttpApi.HttpDeleteServiceConfiguration(IntPtr.Zero,
																	HTTP_SERVICE_CONFIG_ID.HttpServiceConfigUrlAclInfo,
																	pInputConfigInfo,
																	Marshal.SizeOf(inputConfigInfoSet),
																	IntPtr.Zero);

					if ((uint) ErrorCodes.NOERROR == retVal)
					{
						retVal = HttpApi.HttpSetServiceConfiguration(IntPtr.Zero,
																	 HTTP_SERVICE_CONFIG_ID.HttpServiceConfigUrlAclInfo,
																	 pInputConfigInfo,
																	 Marshal.SizeOf(inputConfigInfoSet),
																	 IntPtr.Zero);
					}
				}

				Marshal.FreeCoTaskMem(pInputConfigInfo);
				HttpApi.HttpTerminate(HttpApi.HTTP_INITIALIZE_CONFIG, IntPtr.Zero);
			}

			if ((uint) ErrorCodes.NOERROR != retVal)
			{
				throw new Win32Exception(Convert.ToInt32(retVal));
			}
		}
	}
}
"@

Add-Type -MemberDefinition $CSharpDefinition
