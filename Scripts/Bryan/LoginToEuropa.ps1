$global:QuestPath = "C:\Program Files\Quest Software\Management Shell for AD\"
$global:EuropaPwd = "01000000d08c9ddf0115d1118c7a00c04fc297eb0100000002e310c65369624e8aff4bd6d9462e2b0000000002000000000003660000c0000000100000005fb3dfcaeb9f92893ec76ef8ec1153a30000000004800000a0000000100000003d624e3b617662f77d3185a71ebfb39a1800000070d1cbee4a40d9261f26cfe2c714f0b3abe596d8cc977c87140000003f45e10e3f80d244fa45e130c0db8be425acabf8"

$EuropaCredentials = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList "EUROPA\childba", ($global:EuropaPwd | ConvertTo-SecureString)

if(Test-Path $QuestPath -PathType Container){
    $QuestModule = $QuestPath + "Quest.ActiveRoles.ArsPowershellSnapIn.dll"
    $QuestFormat = $QuestPath + "Quest.ActiveRoles.ADManagement.Format.ps1xml"
    Import-Module -DisableNameChecking -Name $QuestModule
    Update-FormatData $QuestFormat
    Connect-QADService -Service Europa -Credential $EuropaCredentials
}