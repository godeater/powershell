﻿Function Disable-ConsoleWriteLine {
	[string]$definition = @"
	using System;
	using System.IO;
	using System.Text;
	public class ConsoleOverride : TextWriter {
		private TextWriter originalOut = Console.Out;

		public override void WriteLine (string value){
			return;
		}

		public override Encoding Encoding {
			get { throw new Exception ("The method or operation is not implemented"); }
		}
	}
"@
	if (-not ('ConsoleOverride' -as [type])){
		Add-Type -TypeDefinition $definition
	}
	$ConsoleOverride = New-Object ConsoleOverride
	[Console]::SetOut($ConsoleOverride)
}

Function Initialize-MindAlignChatLib{
	param(
		[Parameter(Mandatory=$false)]
		[string]$UBSChatLib = "P:\Documents\WindowsPowershell\Modules\MindAlignChatLib\MindAlignChatLib.dll"
	)
	if(-not ('Ubs.Component.MindAlignChatLib.ChatClient' -as [type])){
		if(Test-Path $UBSChatLib -PathType Leaf){
			[string]$ResolvedPath = (Resolve-Path $UBSChatLib).Path
			Add-Type -LiteralPath $ResolvedPath | Out-Null
		} else {
			throw "$UBSChatLib not found"
		}
	}
}


Function Connect-ToChatNetwork{
	param(
		[Parameter(Mandatory=$false,ValueFromPipeline=$true)]
		[Alias("Server","ChatServer","ChatNetwork")]
		[string]$MessageServer,
		[Parameter(Mandatory=$false)]
		[string]$AuthServer,
		[ValidateNotNullOrEmpty()]
		[string]$MindAlignUser,
		[ValidateNotNullOrEmpty()]
		[string]$MindAlignPassword
	)
	if(-not $AuthServer){
		$AuthServer = $MessageServer
	}
	try{
		$Script:ChatClient = New-Object -Type Ubs.Component.MindAlignChatLib.ChatClient($MindAlignUser,$MindAlignPassword,"NotRequired",$MessageServer,$AuthServer)
		return $Script:ChatClient
	} catch {
		throw "Failed $_"
	}
}

Function Confirm-IsOnChannel {
	param(
		[Parameter(Mandatory=$true)]
		[string]$Channel
	)
	try{
		$Script:ChatClient.IsOnChannel($Channel)
	}catch{
		throw $_
	}
}
Function Invoke-Join {
	param(
		[Parameter(Mandatory=$true)]
		[string]$Channel
	)
	try{
		$Script:ChatClient.doJoin($Channel)
	}catch{
		throw $_
	}
}
Function Invoke-Part {
	param(
		[Parameter(Mandatory=$true)]
		[string]$Channel
	)
	try{
		$Script:ChatClient.doPart($Channel)
	}catch{
		throw $_
	}
}
Function Invoke-Quit {
	try{
		$Script:ChatClient.doQuit()
	}catch{
		throw $_
	}
}
Function Invoke-PrivateAlert {
	param(
		[Parameter(Mandatory=$true)]
		[string]$NickName,
		[Parameter(Mandatory=$true)]
		[string]$Message
	)
	try{
		$Script:ChatClient.doPrivateAlert($NickName,$Message)
	}catch{
		throw $_
	}
}
Function Invoke-Private {
	param(
		[Parameter(Mandatory=$true)]
		[string]$NickName,
		[Parameter(Mandatory=$true)]
		[string]$Message
	)
	try{
		$Script:ChatClient.doPrivate($NickName,$Message)
	}catch{
		throw $_
	}
}
Function Invoke-SendAlert {
	param(
		[Parameter(Mandatory=$true)]
		[string]$Channel,
		[Parameter(Mandatory=$true)]
		[string]$Message
	)
	try{
		$Script:ChatClient.doSendAlert($Channel,$Message)
	}catch{
		throw $_
	}
}
Function Invoke-Send {
	param(
		[Parameter(Mandatory=$true)]
		[string]$Channel,
		[Parameter(Mandatory=$true)]
		[string]$Message
	)
	try{
		$Script:ChatClient.doSend($Channel,$Message)
	}catch{
		throw $_
	}
}

Function Get-Destination{
	param(
		[Parameter(Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[string]$Channel
	)
	try{
		return $Script:ChatClient.GetDestination($Channel)
	}catch{
		throw $_
	}
}

Function Set-Destination{
	param(
		[Parameter(Mandatory=$true)]
		[ValidateNotNullOrEmpty()]
		[string]$Channel
	)
	try{
		return $Script:ChatClient.SetDestination($Channel)
	}catch{
		throw $_
	}
}

Function Set-MessageListener {
	param(
		[Parameter(Mandatory=$true)]
		[Ubs.Component.MindAlignChatLib.ChtDestination]$Destination,
		[Parameter(Mandatory=$true)]
		[ScriptBlock]$OnMessage
	)
	try{
		$MsgReceivedFunction = New-Object -Typename Ubs.Component.MindAlignChatLib.ChatClient.OnMessage($OnMessage)
		$Script:ChatClient.SetMessageListener($Destination,$OnMessage)
	}catch{
		throw $_
	}
}

Function Send-MindAlignMessage {
	<#
       This is a terrible way to communicate with MindAlign or the bot framework
       as it's in flagrant violation of the bot standards. Seriously - don't use
       it.
    #>
	param(
		[Parameter(Mandatory=$false)]
		[string]$UBSChatLib = "P:\Documents\WindowsPowershell\Modules\MindAlignChatLib\MindAlignChatLib.dll",
		[ValidateNotNullOrEmpty()]
		[Alias('Server')]
		[string]$MessageServer,
		[Parameter(Mandatory=$false)]
		[string]$AuthServer,
		[ValidateNotNullOrEmpty()]
		[string]$MindAlignUser,
		[ValidateNotNullOrEmpty()]
		[string]$MindAlignPassword,
		[ValidateNotNullOrEmpty()]
		[string]$Channel,
		[ValidateNotNullOrEmpty()]
		[string]$Message
	)
	if(-not $AuthServer){
		$AuthServer = $MessageServer
	}
	if(-not ('Ubs.Component.MindAlignChatLib.ChatClient' -as [type])){
		if(Test-Path $UBSChatLib -PathType Leaf){
			[string]$ResolvedPath = (Resolve-Path $UBSChatLib).Path
			[Reflection.Assembly]::LoadFile($ResolvedPath) | Out-Null
		} else {
			throw "$UBSChatLib not found"
		}
	}
	try{
		$ChatClient = New-Object -Type Ubs.Component.MindAlignChatLib.ChatClient($MindAlignUser,$MindAlignPassword,"NotRequired",$MessageServer,$AuthServer)
		$ChatClient.doJoin($Channel)
		$ChatClient.doSend($Channel,$Message)
		$ChatClient.doQuit()
		Remove-Variable ChatClient
	} catch {
		throw "Failed $_"
	}
}