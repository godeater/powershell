#region assembly import
Add-Type -Path $PSScriptRoot\Library\PKI.Core.dll -ErrorAction Stop
Add-Type -AssemblyName System.Security -ErrorAction Stop
#endregion

#region helper functions
function __RestartCA ($ComputerName) {
	$wmi = Get-WmiObject Win32_Service -ComputerName $ComputerName -Filter "name='certsvc'"
	if ($wmi.State -eq "Running") {
		[void]$wmi.StopService()
		while ((Get-WmiObject Win32_Service -ComputerName $ComputerName -Filter "name='CertSvc'" -Property "State").State -ne "Stopped") {
			Write-Verbose "Waiting for 'CertSvc' service stop."
			Start-Sleep 1
		}
		[void]$wmi.StartService()
	}
}

function Test-XCEPCompat {
	if (
		[Environment]::OSVersion.Version.Major -lt 6 -or
		([Environment]::OSVersion.Version.Major -eq 6 -and
		[Environment]::OSVersion.Version.Minor -lt 1)
	) {return $false}
	return $true
}

function Ping-Wmi ($ComputerName) {
	$success = $true
	try {[wmiclass]"\\$ComputerName\root\DEFAULT:StdRegProv"}
	catch {$success = $false}
	$success
}

function Ping-ICertAdmin ($ConfigString) {
	$success = $true
	[void]($ConfigString -match "(.+)\\(.+)")
	$hostname = $matches[1]
	$caname = $matches[2]
	try {
		$CertAdmin = New-Object -ComObject CertificateAuthority.Admin
		$var = $CertAdmin.GetCAProperty($ConfigString,0x6,0,4,0)
	} catch {$success = $false}
	$success
}

function Write-ErrorMessage {
	param (
		[PKI.Utils.PSErrorSourceEnum]$Source,
		$ComputerName,
		$ExtendedInformation
	)
$DCUnavailable = @"
"Active Directory domain could not be contacted.
"@
$CAPIUnavailable = @"
Unable to locate required assemblies. This can be caused if attempted to run this module on a client machine where AdminPack/RSAT (Remote Server Administration Tools) are not installed.
"@
$WmiUnavailable = @"
Unable to connect to CA server '$ComputerName'. Make sure if Remote Registry service is running and you have appropriate permissions to access it.
Also this error may indicate that Windows Remote Management protocol exception is not enabled in firewall.
"@
$XchgUnavailable = @"
Unable to retrieve any 'CA Exchange' certificates from '$ComputerName'. This error may indicate that target CA server do not support key archival. All requests which require key archival will immediately fail.
"@
	switch ($source) {
		DCUnavailable {
			Write-Error -Category ObjectNotFound -ErrorId "ObjectNotFoundException" `
			-Message $DCUnavailable
		}
		CAPIUnavailable {
			Write-Error -Category NotImplemented -ErrorId "NotImplementedException" `
			-Message $NoCAPI; exit
		}
		CAUnavailable {
			Write-Error -Category ResourceUnavailable -ErrorId ResourceUnavailableException `
			-Message "Certificate Services are either stopped or unavailable on '$ComputerName'."
		}
		WmiUnavailable {
			Write-Error -Category ResourceUnavailable -ErrorId ResourceUnavailableException `
			-Message $WmiUnavailable
		}
		WmiWriteError {
			try {$text = Get-ErrorMessage $ExtendedInformation}
			catch {$text = "Unknown error '$code'"}
			Write-Error -Category NotSpecified -ErrorId NotSpecifiedException `
			-Message "An error occured during CA configuration update: $text"
		}
		ADKRAUnavailable {
			Write-Error -Category ObjectNotFound -ErrorId "ObjectNotFoundException" `
			-Message "No KRA certificates found in Active Directory."
		}
		ICertAdminUnavailable {
			Write-Error -Category ResourceUnavailable -ErrorId ResourceUnavailableException `
			-Message "Unable to connect to management interfaces on '$ComputerName'"
		}
		NoXchg {
			Write-Error -Category ObjectNotFound -ErrorId ObjectNotFoundException `
			-Message $XchgUnavailable
		}
		NonEnterprise {
			Write-Error -Category NotImplemented -ErrorAction NotImplementedException `
			-Message "Specified Certification Authority type is not supported. The CA type must be either 'Enterprise Root CA' or 'Enterprise Standalone CA'."
		}
	}
}
#endregion

#region module-scope variable definition
# define Configuration naming context DN path
#$ConfigContext = ([ADSI]"LDAP://RootDSE").ConfigurationNamingContext
try {
	$Domain = "CN=Configuration,DC=" + ([DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain().Forest.Name -replace "\.",",DC=")
	$ConfigContext = "CN=Public Key Services,CN=Services," + $Domain
	$NoDomain = $false
} catch {$NoDomain = $true}
$RegPath = "System\CurrentControlSet\Services\CertSvc\Configuration"
# check whether ICertAdmin CryptoAPI interfaces are available. The check is not performed when
# only client part is installed.
if (Test-Path $PSScriptRoot\Server) {
	try {$CertAdmin = New-Object -ComObject CertificateAuthority.Admin}
	catch {Write-ErrorMessage -Source "CAPIUnavailable"}
}
$Win2003	= if ([Environment]::OSVersion.Version.Major -lt 6) {$true} else {$false}
$Win2008	= if ([Environment]::OSVersion.Version.Major -eq 6 -and [Environment]::OSVersion.Version.Minor -eq 0) {$true} else {$false}
$Win2008R2	= if ([Environment]::OSVersion.Version.Major -eq 6 -and [Environment]::OSVersion.Version.Minor -eq 1) {$true} else {$false}
$Win2012	= if ([Environment]::OSVersion.Version.Major -eq 6 -and [Environment]::OSVersion.Version.Minor -eq 2) {$true} else {$false}
$Win2012R2	= if ([Environment]::OSVersion.Version.Major -eq 6 -and [Environment]::OSVersion.Version.Minor -eq 2) {$true} else {$false}

$RestartRequired = @"
New {0} are set, but will not be applied until Certification Authority service is restarted.
In future consider to use '-RestartCA' switch for this cmdlet to restart Certification Authority service immediatelly when new settings are set.

See more: Start-CertificationAuthority, Stop-CertificationAuthority and Restart-CertificationAuthority cmdlets.
"@
$NothingIsSet = @"
Input object was not modified since it was created. Nothing is written to the CA configuration.
"@
#endregion

#region module installation stuff
# dot-source all function files
Get-ChildItem -Path $PSScriptRoot -Include *.ps1 -Recurse | Foreach-Object { . $_.FullName }

if ($Win2008R2) {
	if (Test-Path $PSScriptRoot\Server) {
		New-Alias -Name Add-CEP					-Value Add-CertificateEnrollmentPolicyService -Force
		New-Alias -Name Add-CES					-Value Add-CertificateEnrollmentService -Force
		New-Alias -Name Remove-CEP				-Value Remove-CertificateEnrollmentPolicyService -Force
		New-Alias -Name Remove-CES				-Value Remove-CertificateEnrollmentService -Force
	}
}
if (!$Win2003) {
	if (Test-Path $PSScriptRoot\Client) {
		New-Alias -Name Get-CspCNG				-Value Get-CryptographicServiceProviderCNG -Force
		New-Alias -Name Get-Csp2				-Value Get-CryptographicServiceProviderEx -Force
		New-Alias -Name Oid2					-Value Get-ObjectIdentifierEx -Force
	}
}
if ($Win2008 -or $Win2008R2) {
	if (Test-Path $PSScriptRoot\Server) {
		New-Alias -Name Install-CA				-Value Install-CertificationAuthority -Force
		New-Alias -Name Uninstall-CA			-Value Uninstall-CertificationAuthority -Force
	}
}

if (!$NoDomain) {
	if (Test-Path $PSScriptRoot\Server) {
		New-Alias -Name Get-CA					-Value Get-CertificationAuthority -Force
		New-Alias -Name Get-KRAFlag				-Value Get-KeyRecoveryAgentFlag -Force
		New-Alias -Name Enable-KRAFlag			-Value Enable-KeyRecoveryAgentFlag -Force
		New-Alias -Name Disable-KRAFlag			-Value Disable-KeyRecoveryAgentFlag -Force
		New-Alias -Name Restore-KRAFlagDefault	-Value Restore-KeyRecoveryAgentFlagDefault -Force
	}
}
if (Test-Path $PSScriptRoot\Server) {
	New-Alias -Name Connect-CA					-Value Connect-CertificationAuthority -Force
	
	New-Alias -Name Add-AIA						-Value Add-AuthorityInformationAccess -Force
	New-Alias -Name Get-AIA						-Value Get-AuthorityInformationAccess -Force
	New-Alias -Name Remove-AIA					-Value Remove-AuthorityInformationAccess -Force
	New-Alias -Name Set-AIA						-Value Set-AuthorityInformationAccess -Force

	New-Alias -Name Add-CDP						-Value Add-CRLDistributionPoint -Force
	New-Alias -Name Get-CDP						-Value Get-CRLDistributionPoint -Force
	New-Alias -Name Remove-CDP					-Value Remove-CRLDistributionPoint -Force
	New-Alias -Name Set-CDP						-Value Set-CRLDistributionPoint -Force
	
	New-Alias -Name Get-CRLFlag					-Value Get-CertificateRevocationListFlag -Force
	New-Alias -Name Enable-CRLFlag				-Value Enable-CertificateRevocationListFlag -Force
	New-Alias -Name Disable-CRLFlag				-Value Disable-CertificateRevocationListFlag -Force
	New-Alias -Name Restore-CRLFlagDefault		-Value Restore-CertificateRevocationListFlagDefault -Force
	
	New-Alias -Name Remove-Request				-Value Remove-DatabaseRow -Force
	
	New-Alias -Name Get-CAACL					-Value Get-CASecurityDescriptor -Force
	New-Alias -Name Add-CAACL					-Value Add-CAAccessControlEntry -Force
	New-Alias -Name Remove-CAACL				-Value Remove-CAAccessControlEntry -Force
	New-Alias -Name Set-CAACL					-Value Set-CASecurityDescriptor -Force
}

if (Test-Path $PSScriptRoot\Client) {
	New-Alias -Name Oid							-Value Get-ObjectIdentifier -Force
	New-Alias -Name Oid2						-Value Get-ObjectIdentifierEx -Force

	New-Alias -Name Get-Csp						-Value Get-CryptographicServiceProvider -Force

	New-Alias -Name Get-CRL						-Value Get-CertificateRevocationList -Force
	New-Alias -Name Show-CRL					-Value Show-CertificateRevocationList -Force
	New-Alias -Name Get-CTL						-Value Get-CertificateTrustList -Force
	New-Alias -Name Show-CTL					-Value Show-CertificateTrustList -Force
}

# define restricted functions
$RestrictedFunctions =		"Get-RequestRow",
							"__RestartCA",
							"Test-XCEPCompat",
							"Ping-CA",
							"Ping-WMI",
							"Ping-ICertAdmin",
							"Write-ErrorMessage"
$NoDomainExcludeFunctions =	"Add-CAKRACertificate",
							"Add-CATemplate",
							"Add-CertificateEnrollmentPolicyService",
							"Add-CertificateEnrollmentService",
							"Add-CertificateTemplateAcl",
							"Disable-KeyRecoveryAgentFlag",
							"Enable-KeyRecoveryAgentFlag",
							"Get-ADKRACertificate",
							"Get-CAExchangeCertificate",
							"Get-CAKRACertificate",
							"Get-CATemplate",
							"Get-CertificateTemplate",
							"Get-CertificateTemplateAcl",
							"Get-EnrollmentServiceUri",
							"Get-KeyRecoveryAgentFlag",
							"Remove-CAKRACertificate",
							"Remove-CATemplate",
							"Remove-CertificateTemplate",
							"Remove-CertificateTemplateAcl",
							"Restore-KeyRecoveryAgentFlagDefault",
							"Set-CAKRACertificate",
							"Set-CATemplate",
							"Set-CertificateTemplateAcl",
							"Get-CertificationAuthority"
$Win2003ExcludeFunctions =	"Add-CertificateEnrollmentPolicyService",
							"Add-CertificateEnrollmentService",
							"Get-CryptographicServiceProviderCNG",
							"Get-CryptographicServiceProviderEx",
							"Install-CertificationAuthority",
							"Remove-CertificateEnrollmentPolicyService",
							"Remove-CertificateEnrollmentService",
							"Uninstall-CertificationAuthority"	
$Win2008ExcludeFunctions =	"Add-CertificateEnrollmentPolicyService",
							"Add-CertificateEnrollmentService",
							"Remove-CertificateEnrollmentPolicyService",
							"Remove-CertificateEnrollmentService"
$Win2012ExcludeFunctions =	"Install-CertificationAuthority",
							"Uninstall-CertificationAuthority",
							"Add-CertificateEnrollmentPolicyService",
							"Add-CertificateEnrollmentService",
							"Remove-CertificateEnrollmentPolicyService",
							"Remove-CertificateEnrollmentService"

if ($Win2003) {$RestrictedFunctions += $Win2003ExcludeFunctions}
if ($Win2008) {$RestrictedFunctions += $Win2008ExcludeFunctions}
if ($Win2012) {$RestrictedFunctions += $Win2012ExcludeFunctions}
if ($NoDomain) {$RestrictedFunctions += $NoDomainExcludeFunctions}

# export module members
Export-ModuleMember –Function @(Get-Command –Module $ExecutionContext.SessionState.Module | Where-Object {$RestrictedFunctions -notcontains $_.Name})
Export-ModuleMember -Alias @(Get-Command –Module $ExecutionContext.SessionState.Module)

# stub for types and formats (PS V3+)
if ($PSVersionTable["PSVersion"].Major -gt 2) {
	try {
		Update-TypeData $PSScriptRoot\Types\PSPKI.Types.ps1xml
		Update-FormatData $PSScriptRoot\Types\PSPKI.Format.ps1xml
	} catch { }
}
#endregion
# SIG # Begin signature block
# MIIakgYJKoZIhvcNAQcCoIIagzCCGn8CAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUmn0XPORCv5RfD1R6fLTrpmeH
# Ya2gghXIMIID7jCCA1egAwIBAgIQfpPr+3zGTlnqS5p31Ab8OzANBgkqhkiG9w0B
# AQUFADCBizELMAkGA1UEBhMCWkExFTATBgNVBAgTDFdlc3Rlcm4gQ2FwZTEUMBIG
# A1UEBxMLRHVyYmFudmlsbGUxDzANBgNVBAoTBlRoYXd0ZTEdMBsGA1UECxMUVGhh
# d3RlIENlcnRpZmljYXRpb24xHzAdBgNVBAMTFlRoYXd0ZSBUaW1lc3RhbXBpbmcg
# Q0EwHhcNMTIxMjIxMDAwMDAwWhcNMjAxMjMwMjM1OTU5WjBeMQswCQYDVQQGEwJV
# UzEdMBsGA1UEChMUU3ltYW50ZWMgQ29ycG9yYXRpb24xMDAuBgNVBAMTJ1N5bWFu
# dGVjIFRpbWUgU3RhbXBpbmcgU2VydmljZXMgQ0EgLSBHMjCCASIwDQYJKoZIhvcN
# AQEBBQADggEPADCCAQoCggEBALGss0lUS5ccEgrYJXmRIlcqb9y4JsRDc2vCvy5Q
# WvsUwnaOQwElQ7Sh4kX06Ld7w3TMIte0lAAC903tv7S3RCRrzV9FO9FEzkMScxeC
# i2m0K8uZHqxyGyZNcR+xMd37UWECU6aq9UksBXhFpS+JzueZ5/6M4lc/PcaS3Er4
# ezPkeQr78HWIQZz/xQNRmarXbJ+TaYdlKYOFwmAUxMjJOxTawIHwHw103pIiq8r3
# +3R8J+b3Sht/p8OeLa6K6qbmqicWfWH3mHERvOJQoUvlXfrlDqcsn6plINPYlujI
# fKVOSET/GeJEB5IL12iEgF1qeGRFzWBGflTBE3zFefHJwXECAwEAAaOB+jCB9zAd
# BgNVHQ4EFgQUX5r1blzMzHSa1N197z/b7EyALt0wMgYIKwYBBQUHAQEEJjAkMCIG
# CCsGAQUFBzABhhZodHRwOi8vb2NzcC50aGF3dGUuY29tMBIGA1UdEwEB/wQIMAYB
# Af8CAQAwPwYDVR0fBDgwNjA0oDKgMIYuaHR0cDovL2NybC50aGF3dGUuY29tL1Ro
# YXd0ZVRpbWVzdGFtcGluZ0NBLmNybDATBgNVHSUEDDAKBggrBgEFBQcDCDAOBgNV
# HQ8BAf8EBAMCAQYwKAYDVR0RBCEwH6QdMBsxGTAXBgNVBAMTEFRpbWVTdGFtcC0y
# MDQ4LTEwDQYJKoZIhvcNAQEFBQADgYEAAwmbj3nvf1kwqu9otfrjCR27T4IGXTdf
# plKfFo3qHJIJRG71betYfDDo+WmNI3MLEm9Hqa45EfgqsZuwGsOO61mWAK3ODE2y
# 0DGmCFwqevzieh1XTKhlGOl5QGIllm7HxzdqgyEIjkHq3dlXPx13SYcqFgZepjhq
# IhKjURmDfrYwggSjMIIDi6ADAgECAhAOz/Q4yP6/NW4E2GqYGxpQMA0GCSqGSIb3
# DQEBBQUAMF4xCzAJBgNVBAYTAlVTMR0wGwYDVQQKExRTeW1hbnRlYyBDb3Jwb3Jh
# dGlvbjEwMC4GA1UEAxMnU3ltYW50ZWMgVGltZSBTdGFtcGluZyBTZXJ2aWNlcyBD
# QSAtIEcyMB4XDTEyMTAxODAwMDAwMFoXDTIwMTIyOTIzNTk1OVowYjELMAkGA1UE
# BhMCVVMxHTAbBgNVBAoTFFN5bWFudGVjIENvcnBvcmF0aW9uMTQwMgYDVQQDEytT
# eW1hbnRlYyBUaW1lIFN0YW1waW5nIFNlcnZpY2VzIFNpZ25lciAtIEc0MIIBIjAN
# BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAomMLOUS4uyOnREm7Dv+h8GEKU5Ow
# mNutLA9KxW7/hjxTVQ8VzgQ/K/2plpbZvmF5C1vJTIZ25eBDSyKV7sIrQ8Gf2Gi0
# jkBP7oU4uRHFI/JkWPAVMm9OV6GuiKQC1yoezUvh3WPVF4kyW7BemVqonShQDhfu
# ltthO0VRHc8SVguSR/yrrvZmPUescHLnkudfzRC5xINklBm9JYDh6NIipdC6Anqh
# d5NbZcPuF3S8QYYq3AhMjJKMkS2ed0QfaNaodHfbDlsyi1aLM73ZY8hJnTrFxeoz
# C9Lxoxv0i77Zs1eLO94Ep3oisiSuLsdwxb5OgyYI+wu9qU+ZCOEQKHKqzQIDAQAB
# o4IBVzCCAVMwDAYDVR0TAQH/BAIwADAWBgNVHSUBAf8EDDAKBggrBgEFBQcDCDAO
# BgNVHQ8BAf8EBAMCB4AwcwYIKwYBBQUHAQEEZzBlMCoGCCsGAQUFBzABhh5odHRw
# Oi8vdHMtb2NzcC53cy5zeW1hbnRlYy5jb20wNwYIKwYBBQUHMAKGK2h0dHA6Ly90
# cy1haWEud3Muc3ltYW50ZWMuY29tL3Rzcy1jYS1nMi5jZXIwPAYDVR0fBDUwMzAx
# oC+gLYYraHR0cDovL3RzLWNybC53cy5zeW1hbnRlYy5jb20vdHNzLWNhLWcyLmNy
# bDAoBgNVHREEITAfpB0wGzEZMBcGA1UEAxMQVGltZVN0YW1wLTIwNDgtMjAdBgNV
# HQ4EFgQURsZpow5KFB7VTNpSYxc/Xja8DeYwHwYDVR0jBBgwFoAUX5r1blzMzHSa
# 1N197z/b7EyALt0wDQYJKoZIhvcNAQEFBQADggEBAHg7tJEqAEzwj2IwN3ijhCcH
# bxiy3iXcoNSUA6qGTiWfmkADHN3O43nLIWgG2rYytG2/9CwmYzPkSWRtDebDZw73
# BaQ1bHyJFsbpst+y6d0gxnEPzZV03LZc3r03H0N45ni1zSgEIKOq8UvEiCmRDoDR
# EfzdXHZuT14ORUZBbg2w6jiasTraCXEQ/Bx5tIB7rGn0/Zy2DBYr8X9bCT2bW+IW
# yhOBbQAuOA2oKY8s4bL0WqkBrxWcLC9JG9siu8P+eJRRw4axgohd8D20UaF5Mysu
# e7ncIAkTcetqGVvP6KUwVyyJST+5z3/Jvz4iaGNTmr1pdKzFHTx/kuDDvBzYBHUw
# ggaEMIIFbKADAgECAhAPQeJuTPcULjW7alEtlqslMA0GCSqGSIb3DQEBBQUAMG8x
# CzAJBgNVBAYTAlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3
# dy5kaWdpY2VydC5jb20xLjAsBgNVBAMTJURpZ2lDZXJ0IEFzc3VyZWQgSUQgQ29k
# ZSBTaWduaW5nIENBLTEwHhcNMTMxMTExMDAwMDAwWhcNMTUwMzEyMTIwMDAwWjBQ
# MQswCQYDVQQGEwJMVjENMAsGA1UEBxMEUmlnYTEYMBYGA1UEChMPU3lzYWRtaW5z
# IExWIElLMRgwFgYDVQQDEw9TeXNhZG1pbnMgTFYgSUswggEiMA0GCSqGSIb3DQEB
# AQUAA4IBDwAwggEKAoIBAQCtw9BW44GJuoGHLv7baBr8p+mxspoTTjNr4ECCsdZS
# 4u9jdzMlLkMCUilUZplfu0vzXmScshBZViCL0kKo+8wS0KnqIGDUD3gGZBbCFJhp
# cdH7a+SYxdTtq4fbA9DrvcE8iWmfs2+SvDlGstcOTM5PlJV9G4j56xP8+Mnr0i3R
# xXtWun3gTIQFJRi4VbIWQYkLwOcNn9t+CNyuqbGgwB6lhSEuE5G+Ckhy9JWaQ860
# YOTNFE5maeCb4h78TAFzpIZQhNLunVGifHKLyg3JvzrJ8k0uLe32MCgIwIJfGc46
# U85myyP1azolKAF0coMUByOCCvu2D5Qd4sU/nsF6tq8HAgMBAAGjggM5MIIDNTAf
# BgNVHSMEGDAWgBR7aM4pqsAXvkl64eU/1qf3RY81MjAdBgNVHQ4EFgQU5+q8k9QB
# lSStwXcPPHv9OE4FWqMwDgYDVR0PAQH/BAQDAgeAMBMGA1UdJQQMMAoGCCsGAQUF
# BwMDMHMGA1UdHwRsMGowM6AxoC+GLWh0dHA6Ly9jcmwzLmRpZ2ljZXJ0LmNvbS9h
# c3N1cmVkLWNzLTIwMTFhLmNybDAzoDGgL4YtaHR0cDovL2NybDQuZGlnaWNlcnQu
# Y29tL2Fzc3VyZWQtY3MtMjAxMWEuY3JsMIIBxAYDVR0gBIIBuzCCAbcwggGzBglg
# hkgBhv1sAwEwggGkMDoGCCsGAQUFBwIBFi5odHRwOi8vd3d3LmRpZ2ljZXJ0LmNv
# bS9zc2wtY3BzLXJlcG9zaXRvcnkuaHRtMIIBZAYIKwYBBQUHAgIwggFWHoIBUgBB
# AG4AeQAgAHUAcwBlACAAbwBmACAAdABoAGkAcwAgAEMAZQByAHQAaQBmAGkAYwBh
# AHQAZQAgAGMAbwBuAHMAdABpAHQAdQB0AGUAcwAgAGEAYwBjAGUAcAB0AGEAbgBj
# AGUAIABvAGYAIAB0AGgAZQAgAEQAaQBnAGkAQwBlAHIAdAAgAEMAUAAvAEMAUABT
# ACAAYQBuAGQAIAB0AGgAZQAgAFIAZQBsAHkAaQBuAGcAIABQAGEAcgB0AHkAIABB
# AGcAcgBlAGUAbQBlAG4AdAAgAHcAaABpAGMAaAAgAGwAaQBtAGkAdAAgAGwAaQBh
# AGIAaQBsAGkAdAB5ACAAYQBuAGQAIABhAHIAZQAgAGkAbgBjAG8AcgBwAG8AcgBh
# AHQAZQBkACAAaABlAHIAZQBpAG4AIABiAHkAIAByAGUAZgBlAHIAZQBuAGMAZQAu
# MIGCBggrBgEFBQcBAQR2MHQwJAYIKwYBBQUHMAGGGGh0dHA6Ly9vY3NwLmRpZ2lj
# ZXJ0LmNvbTBMBggrBgEFBQcwAoZAaHR0cDovL2NhY2VydHMuZGlnaWNlcnQuY29t
# L0RpZ2lDZXJ0QXNzdXJlZElEQ29kZVNpZ25pbmdDQS0xLmNydDAMBgNVHRMBAf8E
# AjAAMA0GCSqGSIb3DQEBBQUAA4IBAQA3vEtKvZ6tq2N9zv6WrbMMsgUDqicenbOi
# mISSeJoYuBJlaYaetBrnJu6LgWMECO3lwaJckSDvhe8aOtUqamWaTwKLCXU8MXdX
# ERJlkehw8V0rJjPWlGjs86zwfNz1JUj74p9GCdly+1p0lpx7V26yf1biKZUNOZlS
# UnSypgvgv1ve+RfVyC8TXlf9PTnPC2pBeaeCrvZxPgQcxcqQixiQBFL/lzUtkakI
# rfBIYPp3U3++p2qdkk5DpZpIjuKIxXigqonoY7qnYEU/TWseQ12XsuLVnZ39swJv
# RaYjZSdDbw9bGYJiTgzKzmJUdpVEGYzSLr3VU7y/iwAFGVKfEbMBMIIGozCCBYug
# AwIBAgIQD6hJBhXXAKC+IXb9xextvTANBgkqhkiG9w0BAQUFADBlMQswCQYDVQQG
# EwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3d3cuZGlnaWNl
# cnQuY29tMSQwIgYDVQQDExtEaWdpQ2VydCBBc3N1cmVkIElEIFJvb3QgQ0EwHhcN
# MTEwMjExMTIwMDAwWhcNMjYwMjEwMTIwMDAwWjBvMQswCQYDVQQGEwJVUzEVMBMG
# A1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3d3cuZGlnaWNlcnQuY29tMS4w
# LAYDVQQDEyVEaWdpQ2VydCBBc3N1cmVkIElEIENvZGUgU2lnbmluZyBDQS0xMIIB
# IjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnHz5oI8KyolLU5o87BkifwzL
# 90hE0D8ibppP+s7fxtMkkf+oUpPncvjxRoaUxasX9Hh/y3q+kCYcfFMv5YPnu2oF
# KMygFxFLGCDzt73y3Mu4hkBFH0/5OZjTO+tvaaRcAS6xZummuNwG3q6NYv5EJ4Kp
# A8P+5iYLk0lx5ThtTv6AXGd3tdVvZmSUa7uISWjY0fR+IcHmxR7J4Ja4CZX5S56u
# zDG9alpCp8QFR31gK9mhXb37VpPvG/xy+d8+Mv3dKiwyRtpeY7zQuMtMEDX8UF+s
# Q0R8/oREULSMKj10DPR6i3JL4Fa1E7Zj6T9OSSPnBhbwJasB+ChB5sfUZDtdqwID
# AQABo4IDQzCCAz8wDgYDVR0PAQH/BAQDAgGGMBMGA1UdJQQMMAoGCCsGAQUFBwMD
# MIIBwwYDVR0gBIIBujCCAbYwggGyBghghkgBhv1sAzCCAaQwOgYIKwYBBQUHAgEW
# Lmh0dHA6Ly93d3cuZGlnaWNlcnQuY29tL3NzbC1jcHMtcmVwb3NpdG9yeS5odG0w
# ggFkBggrBgEFBQcCAjCCAVYeggFSAEEAbgB5ACAAdQBzAGUAIABvAGYAIAB0AGgA
# aQBzACAAQwBlAHIAdABpAGYAaQBjAGEAdABlACAAYwBvAG4AcwB0AGkAdAB1AHQA
# ZQBzACAAYQBjAGMAZQBwAHQAYQBuAGMAZQAgAG8AZgAgAHQAaABlACAARABpAGcA
# aQBDAGUAcgB0ACAAQwBQAC8AQwBQAFMAIABhAG4AZAAgAHQAaABlACAAUgBlAGwA
# eQBpAG4AZwAgAFAAYQByAHQAeQAgAEEAZwByAGUAZQBtAGUAbgB0ACAAdwBoAGkA
# YwBoACAAbABpAG0AaQB0ACAAbABpAGEAYgBpAGwAaQB0AHkAIABhAG4AZAAgAGEA
# cgBlACAAaQBuAGMAbwByAHAAbwByAGEAdABlAGQAIABoAGUAcgBlAGkAbgAgAGIA
# eQAgAHIAZQBmAGUAcgBlAG4AYwBlAC4wEgYDVR0TAQH/BAgwBgEB/wIBADB5Bggr
# BgEFBQcBAQRtMGswJAYIKwYBBQUHMAGGGGh0dHA6Ly9vY3NwLmRpZ2ljZXJ0LmNv
# bTBDBggrBgEFBQcwAoY3aHR0cDovL2NhY2VydHMuZGlnaWNlcnQuY29tL0RpZ2lD
# ZXJ0QXNzdXJlZElEUm9vdENBLmNydDCBgQYDVR0fBHoweDA6oDigNoY0aHR0cDov
# L2NybDMuZGlnaWNlcnQuY29tL0RpZ2lDZXJ0QXNzdXJlZElEUm9vdENBLmNybDA6
# oDigNoY0aHR0cDovL2NybDQuZGlnaWNlcnQuY29tL0RpZ2lDZXJ0QXNzdXJlZElE
# Um9vdENBLmNybDAdBgNVHQ4EFgQUe2jOKarAF75JeuHlP9an90WPNTIwHwYDVR0j
# BBgwFoAUReuir/SSy4IxLVGLp6chnfNtyA8wDQYJKoZIhvcNAQEFBQADggEBAHty
# HWT/iMg6wbfp56nEh7vblJLXkFkz+iuH3qhbgCU/E4+bgxt8Q8TmjN85PsMV7LDa
# OyEleyTBcl24R5GBE0b6nD9qUTjetCXL8KvfxSgBVHkQRiTROA8moWGQTbq9KOY/
# 8cSqm/baNVNPyfI902zcI+2qoE1nCfM6gD08+zZMkOd2pN3yOr9WNS+iTGXo4NTa
# 0cfIkWotI083OxmUGNTVnBA81bEcGf+PyGubnviunJmWeNHNnFEVW0ImclqNCkoj
# kkDoht4iwpM61Jtopt8pfwa5PA69n8SGnIJHQnEyhgmZcgl5S51xafVB/385d2Tx
# hI2+ix6yfWijpZCxDP8xggQ0MIIEMAIBATCBgzBvMQswCQYDVQQGEwJVUzEVMBMG
# A1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3d3cuZGlnaWNlcnQuY29tMS4w
# LAYDVQQDEyVEaWdpQ2VydCBBc3N1cmVkIElEIENvZGUgU2lnbmluZyBDQS0xAhAP
# QeJuTPcULjW7alEtlqslMAkGBSsOAwIaBQCgeDAYBgorBgEEAYI3AgEMMQowCKAC
# gAChAoAAMBkGCSqGSIb3DQEJAzEMBgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsx
# DjAMBgorBgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBRYf/5PiRYa0ZIyp9JO3ZOS
# eui7JjANBgkqhkiG9w0BAQEFAASCAQBa6HDrgPHPTx0ZKoiW7IKlxLNUHRR2JdPD
# EB9E8Wd41tymbrOGfLpFfPA7mrDPei/QFcJEpwEz7WL3HWDTXb/xKYkpC3qdIn4v
# BAXhjNn3VTTAJmKIDHeTGt/nK/xX6hhIqxmjZSlkNgUKWj+bwoOi5fN7E9951d56
# QYo12iAuJQQy5DdgoyGwIjmOPrsVfhOWkIJc7/UD5kGKFfaAIvpU8BZJnDls3tI5
# 9LHYGB+HolTU9lyzlvKTxy8fC58gIgL8KTTlX9Q9Y8vq49IrCbDaxXxj0DPb6H5O
# oHKmnlaxcmIaTEM1rjk++Mc4bi4Oy0Lp1KwYy2tCUn4SbVedTEfQoYICCzCCAgcG
# CSqGSIb3DQEJBjGCAfgwggH0AgEBMHIwXjELMAkGA1UEBhMCVVMxHTAbBgNVBAoT
# FFN5bWFudGVjIENvcnBvcmF0aW9uMTAwLgYDVQQDEydTeW1hbnRlYyBUaW1lIFN0
# YW1waW5nIFNlcnZpY2VzIENBIC0gRzICEA7P9DjI/r81bgTYapgbGlAwCQYFKw4D
# AhoFAKBdMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8X
# DTE0MDgxMDE0NDUzMFowIwYJKoZIhvcNAQkEMRYEFKwHg9dYRtvjDmS48kR8rrCI
# ujCXMA0GCSqGSIb3DQEBAQUABIIBAFAYeIlRF7+SIpvSG4XBxn6uKkfCf82FPIjV
# maPC4W3JC/U/XSXwJGRcWmdGTWYTlL4iisFQw3hTDNFyrSOtjVZdlzQg2qlbG+s4
# 8qvZQYcwyTxCsf1SwUy1hLtz+K2QgbJIyLsHcpp4ouYyI05rkvZsKwEyZA6zaWrh
# 2cpfbd2otlrmlnIc3XVYfBSNxIpKylOHGUgOsKArhmgsXORXSBlHrnd2Y197UETU
# ov7zxIbA9RWFMU8TtHZ0LK8dDNRNvXbsRz+wJ2CjDfUIfdLNh26/4czED+6G7JuS
# yiPPNtdAQxekpkcslmOdONDob5n/KEdPTiH+Dje5UzNQ1CWa5IE=
# SIG # End signature block
