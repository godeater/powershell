function Get-RequestRow {
[CmdletBinding()]
	param(
		$CA,
		[String[]]$Property,
		[string]$Table,
		[String[]]$Filter,
		[PKI.CertificateServices.DB.Schema[]]$Schema
	)
	if (!$CA.Ping()) {
		Write-ErrorMessage -Source ICertAdminUnavailable -ComputerName $CA.ComputerName
		return
	}
	$CaView = New-Object -ComObject CertificateAuthority.View
	$CaView.OpenConnection($CA.ConfigString)
	$RColumn = $CaView.GetColumnIndex(0, "Disposition")
# set proper table
	switch ($Table) {
		"Revoked"	{$CaView.SetRestriction($RColumn,1,0,21)}
		"Issued"	{$CaView.SetRestriction($RColumn,1,0,20)}
		"Pending"	{$CaView.SetRestriction($RColumn,1,0,9)}
		"Failed"	{$CaView.SetRestriction(-3,0,0,0)}
		"Extension"	{$CaView.SetTable(0x3000)}
		"Attribute"	{$CaView.SetTable(0x4000)}
		"CRL"		{$CaView.SetTable(0x5000)}
	}
# parse restriction filters
	if ($Filter -ne $null) {
		foreach ($line in $Filter) {
			if ($line -notmatch "^(.+)\s(-eq|-lt|-le|-ge|-gt)\s(.+)$") {
				Write-Warning "Malformed pattern: '$line'!"
				[void][Runtime.InteropServices.Marshal]::ReleaseComObject($CaView)
				return
			}
			try {
				$Rcolumn = $CaView.GetColumnIndex($false, $matches[1])
			} catch {
				Write-Warning "Specified column '$($matches[1])' does not exist."
				[void][Runtime.InteropServices.Marshal]::ReleaseComObject($CaView)
				return
			}
			$Seek = switch ($matches[2]) {
				"-eq" {1}
				"-lt" {2}
				"-le" {4}
				"-ge" {8}
				"-gt" {16}
			}
			$Value = $matches[3]
			$SchemaRow = $Schema | Where-Object {$_.Name -eq $matches[1]}
			$Value = switch ([int]$SchemaRow.DataType) {
				1		{$matches[3] -as [int]}
				2		{[DateTime]::ParseExact($matches[3],"MM/dd/yyyy HH:mm:ss",[Globalization.CultureInfo]::InvariantCulture)}
				default	{$matches[3]}
			}
			if ($matches[1] -eq "CertificateTemplate") {
				if ($Value -ne "Machine") {
					$Value = ([Security.Cryptography.Oid]$Value).Value
				}
			}
			try {
				$CaView.SetRestriction($RColumn,$Seek,0,$Value)
			} catch {
				Write-Warning "Specified pattern '$line' is not valid."
				[void][Runtime.InteropServices.Marshal]::ReleaseComObject($CaView)
				return
			}
		}
	}
# set output columns
	if ($Property -contains "*") {
		$ColumnCount = $CaView.GetColumnCount(0)
		$CaView.SetResultColumnCount($ColumnCount)
		0..($ColumnCount - 1) | ForEach-Object {$CaView.SetResultColumn($_)}
	} else {
		$properties = switch ($Table) {
			"Revoked"	{"RequestID","Request.RevokedWhen","Request.RevokedReason","CommonName","SerialNumber","CertificateTemplate"}
			"Issued"	{"RequestID","Request.RequesterName","CommonName","NotBefore","NotAfter","SerialNumber","CertificateTemplate"}
			"Pending"	{"RequestID","Request.RequesterName","Request.SubmittedWhen","Request.CommonName","CertificateTemplate"}
			"Failed"	{"RequestID","Request.StatusCode","Request.DispositionMessage","Request.SubmittedWhen","Request.CommonName","CertificateTemplate"}
			"Request"	{"RequestID","Request.StatusCode","Request.DispositionMessage","Request.RequesterName","Request.SubmittedWhen","Request.CommonName","CertificateTemplate"}
			"Extension" {"ExtensionRequestId","ExtensionName","ExtensionFlags","ExtensionRawValue"}
			"Attribute" {"AttributeRequestId","AttributeName","AttributeValue"}
			"CRL"		{"CRLRowId","CRLNumber","CRLThisUpdate","CRLNextUpdate","CRLPublishStatusCode","CRLPublishError"}
		}
		$properties = $properties + $Property | Select-Object -Unique | Where-Object {$_}
		$CaView.SetResultColumnCount($properties.Count)
		$properties | ForEach-Object {$CaView.SetResultColumn($CaView.GetColumnIndex(0, $_))}
	}
# process search routine
	$Row = $CaView.OpenView()
	while ($Row.Next() -ne -1) {
		$cert = New-Object PKI.CertificateServices.DB.RequestRow
		$cert.ConfigString = $CA.ConfigString
		$cert.Table = switch ($Table) {
			"Extension"	{[PKI.CertificateServices.DB.TableList]::Extension}			
			"Attribute"	{[PKI.CertificateServices.DB.TableList]::Attribute}
			"CRL"		{[PKI.CertificateServices.DB.TableList]::CRL}
			default		{[PKI.CertificateServices.DB.TableList]::Request}
		}
		$Column = $Row.EnumCertViewColumn()
		while ($Column.Next() -ne -1) {
			$colName = $Column.GetName()
			$colVal = $Column.GetValue(1)
			if (
				$colName -eq "RequestID"			-or
				$colName -eq "ExtensionRequestId"	-or
				$colName -eq "AttributeRequestId"	-or
				$colName -eq "CRLRowId"
			) {
				$cert.RowId = $colVal
			}
			$Cert | Add-Member -MemberType NoteProperty $colName -Value $colVal -Force
			if ($Cert.CertificateTemplate -match "^(\d\.){3}") {
				if ([string]::IsNullOrEmpty(([Security.Cryptography.Oid]$Column.GetValue(1)).FriendlyName)) {
					$cert.CertificateTemplate = $Column.GetValue(1)
				} else {
					$cert.CertificateTemplate = ([Security.Cryptography.Oid]$Column.GetValue(1)).FriendlyName
				}
			}
		}
		$Cert
		[void][Runtime.InteropServices.Marshal]::ReleaseComObject($Column)
	}
	$CaView, $Row | ForEach-Object {[void][Runtime.InteropServices.Marshal]::ReleaseComObject($_)}
	Remove-Variable CaView, Row
	[GC]::Collect()
}
# SIG # Begin signature block
# MIIakgYJKoZIhvcNAQcCoIIagzCCGn8CAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQU41z3oCqOps90b+he3l9TFMHv
# gbegghXIMIID7jCCA1egAwIBAgIQfpPr+3zGTlnqS5p31Ab8OzANBgkqhkiG9w0B
# AQUFADCBizELMAkGA1UEBhMCWkExFTATBgNVBAgTDFdlc3Rlcm4gQ2FwZTEUMBIG
# A1UEBxMLRHVyYmFudmlsbGUxDzANBgNVBAoTBlRoYXd0ZTEdMBsGA1UECxMUVGhh
# d3RlIENlcnRpZmljYXRpb24xHzAdBgNVBAMTFlRoYXd0ZSBUaW1lc3RhbXBpbmcg
# Q0EwHhcNMTIxMjIxMDAwMDAwWhcNMjAxMjMwMjM1OTU5WjBeMQswCQYDVQQGEwJV
# UzEdMBsGA1UEChMUU3ltYW50ZWMgQ29ycG9yYXRpb24xMDAuBgNVBAMTJ1N5bWFu
# dGVjIFRpbWUgU3RhbXBpbmcgU2VydmljZXMgQ0EgLSBHMjCCASIwDQYJKoZIhvcN
# AQEBBQADggEPADCCAQoCggEBALGss0lUS5ccEgrYJXmRIlcqb9y4JsRDc2vCvy5Q
# WvsUwnaOQwElQ7Sh4kX06Ld7w3TMIte0lAAC903tv7S3RCRrzV9FO9FEzkMScxeC
# i2m0K8uZHqxyGyZNcR+xMd37UWECU6aq9UksBXhFpS+JzueZ5/6M4lc/PcaS3Er4
# ezPkeQr78HWIQZz/xQNRmarXbJ+TaYdlKYOFwmAUxMjJOxTawIHwHw103pIiq8r3
# +3R8J+b3Sht/p8OeLa6K6qbmqicWfWH3mHERvOJQoUvlXfrlDqcsn6plINPYlujI
# fKVOSET/GeJEB5IL12iEgF1qeGRFzWBGflTBE3zFefHJwXECAwEAAaOB+jCB9zAd
# BgNVHQ4EFgQUX5r1blzMzHSa1N197z/b7EyALt0wMgYIKwYBBQUHAQEEJjAkMCIG
# CCsGAQUFBzABhhZodHRwOi8vb2NzcC50aGF3dGUuY29tMBIGA1UdEwEB/wQIMAYB
# Af8CAQAwPwYDVR0fBDgwNjA0oDKgMIYuaHR0cDovL2NybC50aGF3dGUuY29tL1Ro
# YXd0ZVRpbWVzdGFtcGluZ0NBLmNybDATBgNVHSUEDDAKBggrBgEFBQcDCDAOBgNV
# HQ8BAf8EBAMCAQYwKAYDVR0RBCEwH6QdMBsxGTAXBgNVBAMTEFRpbWVTdGFtcC0y
# MDQ4LTEwDQYJKoZIhvcNAQEFBQADgYEAAwmbj3nvf1kwqu9otfrjCR27T4IGXTdf
# plKfFo3qHJIJRG71betYfDDo+WmNI3MLEm9Hqa45EfgqsZuwGsOO61mWAK3ODE2y
# 0DGmCFwqevzieh1XTKhlGOl5QGIllm7HxzdqgyEIjkHq3dlXPx13SYcqFgZepjhq
# IhKjURmDfrYwggSjMIIDi6ADAgECAhAOz/Q4yP6/NW4E2GqYGxpQMA0GCSqGSIb3
# DQEBBQUAMF4xCzAJBgNVBAYTAlVTMR0wGwYDVQQKExRTeW1hbnRlYyBDb3Jwb3Jh
# dGlvbjEwMC4GA1UEAxMnU3ltYW50ZWMgVGltZSBTdGFtcGluZyBTZXJ2aWNlcyBD
# QSAtIEcyMB4XDTEyMTAxODAwMDAwMFoXDTIwMTIyOTIzNTk1OVowYjELMAkGA1UE
# BhMCVVMxHTAbBgNVBAoTFFN5bWFudGVjIENvcnBvcmF0aW9uMTQwMgYDVQQDEytT
# eW1hbnRlYyBUaW1lIFN0YW1waW5nIFNlcnZpY2VzIFNpZ25lciAtIEc0MIIBIjAN
# BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAomMLOUS4uyOnREm7Dv+h8GEKU5Ow
# mNutLA9KxW7/hjxTVQ8VzgQ/K/2plpbZvmF5C1vJTIZ25eBDSyKV7sIrQ8Gf2Gi0
# jkBP7oU4uRHFI/JkWPAVMm9OV6GuiKQC1yoezUvh3WPVF4kyW7BemVqonShQDhfu
# ltthO0VRHc8SVguSR/yrrvZmPUescHLnkudfzRC5xINklBm9JYDh6NIipdC6Anqh
# d5NbZcPuF3S8QYYq3AhMjJKMkS2ed0QfaNaodHfbDlsyi1aLM73ZY8hJnTrFxeoz
# C9Lxoxv0i77Zs1eLO94Ep3oisiSuLsdwxb5OgyYI+wu9qU+ZCOEQKHKqzQIDAQAB
# o4IBVzCCAVMwDAYDVR0TAQH/BAIwADAWBgNVHSUBAf8EDDAKBggrBgEFBQcDCDAO
# BgNVHQ8BAf8EBAMCB4AwcwYIKwYBBQUHAQEEZzBlMCoGCCsGAQUFBzABhh5odHRw
# Oi8vdHMtb2NzcC53cy5zeW1hbnRlYy5jb20wNwYIKwYBBQUHMAKGK2h0dHA6Ly90
# cy1haWEud3Muc3ltYW50ZWMuY29tL3Rzcy1jYS1nMi5jZXIwPAYDVR0fBDUwMzAx
# oC+gLYYraHR0cDovL3RzLWNybC53cy5zeW1hbnRlYy5jb20vdHNzLWNhLWcyLmNy
# bDAoBgNVHREEITAfpB0wGzEZMBcGA1UEAxMQVGltZVN0YW1wLTIwNDgtMjAdBgNV
# HQ4EFgQURsZpow5KFB7VTNpSYxc/Xja8DeYwHwYDVR0jBBgwFoAUX5r1blzMzHSa
# 1N197z/b7EyALt0wDQYJKoZIhvcNAQEFBQADggEBAHg7tJEqAEzwj2IwN3ijhCcH
# bxiy3iXcoNSUA6qGTiWfmkADHN3O43nLIWgG2rYytG2/9CwmYzPkSWRtDebDZw73
# BaQ1bHyJFsbpst+y6d0gxnEPzZV03LZc3r03H0N45ni1zSgEIKOq8UvEiCmRDoDR
# EfzdXHZuT14ORUZBbg2w6jiasTraCXEQ/Bx5tIB7rGn0/Zy2DBYr8X9bCT2bW+IW
# yhOBbQAuOA2oKY8s4bL0WqkBrxWcLC9JG9siu8P+eJRRw4axgohd8D20UaF5Mysu
# e7ncIAkTcetqGVvP6KUwVyyJST+5z3/Jvz4iaGNTmr1pdKzFHTx/kuDDvBzYBHUw
# ggaEMIIFbKADAgECAhAPQeJuTPcULjW7alEtlqslMA0GCSqGSIb3DQEBBQUAMG8x
# CzAJBgNVBAYTAlVTMRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3
# dy5kaWdpY2VydC5jb20xLjAsBgNVBAMTJURpZ2lDZXJ0IEFzc3VyZWQgSUQgQ29k
# ZSBTaWduaW5nIENBLTEwHhcNMTMxMTExMDAwMDAwWhcNMTUwMzEyMTIwMDAwWjBQ
# MQswCQYDVQQGEwJMVjENMAsGA1UEBxMEUmlnYTEYMBYGA1UEChMPU3lzYWRtaW5z
# IExWIElLMRgwFgYDVQQDEw9TeXNhZG1pbnMgTFYgSUswggEiMA0GCSqGSIb3DQEB
# AQUAA4IBDwAwggEKAoIBAQCtw9BW44GJuoGHLv7baBr8p+mxspoTTjNr4ECCsdZS
# 4u9jdzMlLkMCUilUZplfu0vzXmScshBZViCL0kKo+8wS0KnqIGDUD3gGZBbCFJhp
# cdH7a+SYxdTtq4fbA9DrvcE8iWmfs2+SvDlGstcOTM5PlJV9G4j56xP8+Mnr0i3R
# xXtWun3gTIQFJRi4VbIWQYkLwOcNn9t+CNyuqbGgwB6lhSEuE5G+Ckhy9JWaQ860
# YOTNFE5maeCb4h78TAFzpIZQhNLunVGifHKLyg3JvzrJ8k0uLe32MCgIwIJfGc46
# U85myyP1azolKAF0coMUByOCCvu2D5Qd4sU/nsF6tq8HAgMBAAGjggM5MIIDNTAf
# BgNVHSMEGDAWgBR7aM4pqsAXvkl64eU/1qf3RY81MjAdBgNVHQ4EFgQU5+q8k9QB
# lSStwXcPPHv9OE4FWqMwDgYDVR0PAQH/BAQDAgeAMBMGA1UdJQQMMAoGCCsGAQUF
# BwMDMHMGA1UdHwRsMGowM6AxoC+GLWh0dHA6Ly9jcmwzLmRpZ2ljZXJ0LmNvbS9h
# c3N1cmVkLWNzLTIwMTFhLmNybDAzoDGgL4YtaHR0cDovL2NybDQuZGlnaWNlcnQu
# Y29tL2Fzc3VyZWQtY3MtMjAxMWEuY3JsMIIBxAYDVR0gBIIBuzCCAbcwggGzBglg
# hkgBhv1sAwEwggGkMDoGCCsGAQUFBwIBFi5odHRwOi8vd3d3LmRpZ2ljZXJ0LmNv
# bS9zc2wtY3BzLXJlcG9zaXRvcnkuaHRtMIIBZAYIKwYBBQUHAgIwggFWHoIBUgBB
# AG4AeQAgAHUAcwBlACAAbwBmACAAdABoAGkAcwAgAEMAZQByAHQAaQBmAGkAYwBh
# AHQAZQAgAGMAbwBuAHMAdABpAHQAdQB0AGUAcwAgAGEAYwBjAGUAcAB0AGEAbgBj
# AGUAIABvAGYAIAB0AGgAZQAgAEQAaQBnAGkAQwBlAHIAdAAgAEMAUAAvAEMAUABT
# ACAAYQBuAGQAIAB0AGgAZQAgAFIAZQBsAHkAaQBuAGcAIABQAGEAcgB0AHkAIABB
# AGcAcgBlAGUAbQBlAG4AdAAgAHcAaABpAGMAaAAgAGwAaQBtAGkAdAAgAGwAaQBh
# AGIAaQBsAGkAdAB5ACAAYQBuAGQAIABhAHIAZQAgAGkAbgBjAG8AcgBwAG8AcgBh
# AHQAZQBkACAAaABlAHIAZQBpAG4AIABiAHkAIAByAGUAZgBlAHIAZQBuAGMAZQAu
# MIGCBggrBgEFBQcBAQR2MHQwJAYIKwYBBQUHMAGGGGh0dHA6Ly9vY3NwLmRpZ2lj
# ZXJ0LmNvbTBMBggrBgEFBQcwAoZAaHR0cDovL2NhY2VydHMuZGlnaWNlcnQuY29t
# L0RpZ2lDZXJ0QXNzdXJlZElEQ29kZVNpZ25pbmdDQS0xLmNydDAMBgNVHRMBAf8E
# AjAAMA0GCSqGSIb3DQEBBQUAA4IBAQA3vEtKvZ6tq2N9zv6WrbMMsgUDqicenbOi
# mISSeJoYuBJlaYaetBrnJu6LgWMECO3lwaJckSDvhe8aOtUqamWaTwKLCXU8MXdX
# ERJlkehw8V0rJjPWlGjs86zwfNz1JUj74p9GCdly+1p0lpx7V26yf1biKZUNOZlS
# UnSypgvgv1ve+RfVyC8TXlf9PTnPC2pBeaeCrvZxPgQcxcqQixiQBFL/lzUtkakI
# rfBIYPp3U3++p2qdkk5DpZpIjuKIxXigqonoY7qnYEU/TWseQ12XsuLVnZ39swJv
# RaYjZSdDbw9bGYJiTgzKzmJUdpVEGYzSLr3VU7y/iwAFGVKfEbMBMIIGozCCBYug
# AwIBAgIQD6hJBhXXAKC+IXb9xextvTANBgkqhkiG9w0BAQUFADBlMQswCQYDVQQG
# EwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3d3cuZGlnaWNl
# cnQuY29tMSQwIgYDVQQDExtEaWdpQ2VydCBBc3N1cmVkIElEIFJvb3QgQ0EwHhcN
# MTEwMjExMTIwMDAwWhcNMjYwMjEwMTIwMDAwWjBvMQswCQYDVQQGEwJVUzEVMBMG
# A1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3d3cuZGlnaWNlcnQuY29tMS4w
# LAYDVQQDEyVEaWdpQ2VydCBBc3N1cmVkIElEIENvZGUgU2lnbmluZyBDQS0xMIIB
# IjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnHz5oI8KyolLU5o87BkifwzL
# 90hE0D8ibppP+s7fxtMkkf+oUpPncvjxRoaUxasX9Hh/y3q+kCYcfFMv5YPnu2oF
# KMygFxFLGCDzt73y3Mu4hkBFH0/5OZjTO+tvaaRcAS6xZummuNwG3q6NYv5EJ4Kp
# A8P+5iYLk0lx5ThtTv6AXGd3tdVvZmSUa7uISWjY0fR+IcHmxR7J4Ja4CZX5S56u
# zDG9alpCp8QFR31gK9mhXb37VpPvG/xy+d8+Mv3dKiwyRtpeY7zQuMtMEDX8UF+s
# Q0R8/oREULSMKj10DPR6i3JL4Fa1E7Zj6T9OSSPnBhbwJasB+ChB5sfUZDtdqwID
# AQABo4IDQzCCAz8wDgYDVR0PAQH/BAQDAgGGMBMGA1UdJQQMMAoGCCsGAQUFBwMD
# MIIBwwYDVR0gBIIBujCCAbYwggGyBghghkgBhv1sAzCCAaQwOgYIKwYBBQUHAgEW
# Lmh0dHA6Ly93d3cuZGlnaWNlcnQuY29tL3NzbC1jcHMtcmVwb3NpdG9yeS5odG0w
# ggFkBggrBgEFBQcCAjCCAVYeggFSAEEAbgB5ACAAdQBzAGUAIABvAGYAIAB0AGgA
# aQBzACAAQwBlAHIAdABpAGYAaQBjAGEAdABlACAAYwBvAG4AcwB0AGkAdAB1AHQA
# ZQBzACAAYQBjAGMAZQBwAHQAYQBuAGMAZQAgAG8AZgAgAHQAaABlACAARABpAGcA
# aQBDAGUAcgB0ACAAQwBQAC8AQwBQAFMAIABhAG4AZAAgAHQAaABlACAAUgBlAGwA
# eQBpAG4AZwAgAFAAYQByAHQAeQAgAEEAZwByAGUAZQBtAGUAbgB0ACAAdwBoAGkA
# YwBoACAAbABpAG0AaQB0ACAAbABpAGEAYgBpAGwAaQB0AHkAIABhAG4AZAAgAGEA
# cgBlACAAaQBuAGMAbwByAHAAbwByAGEAdABlAGQAIABoAGUAcgBlAGkAbgAgAGIA
# eQAgAHIAZQBmAGUAcgBlAG4AYwBlAC4wEgYDVR0TAQH/BAgwBgEB/wIBADB5Bggr
# BgEFBQcBAQRtMGswJAYIKwYBBQUHMAGGGGh0dHA6Ly9vY3NwLmRpZ2ljZXJ0LmNv
# bTBDBggrBgEFBQcwAoY3aHR0cDovL2NhY2VydHMuZGlnaWNlcnQuY29tL0RpZ2lD
# ZXJ0QXNzdXJlZElEUm9vdENBLmNydDCBgQYDVR0fBHoweDA6oDigNoY0aHR0cDov
# L2NybDMuZGlnaWNlcnQuY29tL0RpZ2lDZXJ0QXNzdXJlZElEUm9vdENBLmNybDA6
# oDigNoY0aHR0cDovL2NybDQuZGlnaWNlcnQuY29tL0RpZ2lDZXJ0QXNzdXJlZElE
# Um9vdENBLmNybDAdBgNVHQ4EFgQUe2jOKarAF75JeuHlP9an90WPNTIwHwYDVR0j
# BBgwFoAUReuir/SSy4IxLVGLp6chnfNtyA8wDQYJKoZIhvcNAQEFBQADggEBAHty
# HWT/iMg6wbfp56nEh7vblJLXkFkz+iuH3qhbgCU/E4+bgxt8Q8TmjN85PsMV7LDa
# OyEleyTBcl24R5GBE0b6nD9qUTjetCXL8KvfxSgBVHkQRiTROA8moWGQTbq9KOY/
# 8cSqm/baNVNPyfI902zcI+2qoE1nCfM6gD08+zZMkOd2pN3yOr9WNS+iTGXo4NTa
# 0cfIkWotI083OxmUGNTVnBA81bEcGf+PyGubnviunJmWeNHNnFEVW0ImclqNCkoj
# kkDoht4iwpM61Jtopt8pfwa5PA69n8SGnIJHQnEyhgmZcgl5S51xafVB/385d2Tx
# hI2+ix6yfWijpZCxDP8xggQ0MIIEMAIBATCBgzBvMQswCQYDVQQGEwJVUzEVMBMG
# A1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3d3cuZGlnaWNlcnQuY29tMS4w
# LAYDVQQDEyVEaWdpQ2VydCBBc3N1cmVkIElEIENvZGUgU2lnbmluZyBDQS0xAhAP
# QeJuTPcULjW7alEtlqslMAkGBSsOAwIaBQCgeDAYBgorBgEEAYI3AgEMMQowCKAC
# gAChAoAAMBkGCSqGSIb3DQEJAzEMBgorBgEEAYI3AgEEMBwGCisGAQQBgjcCAQsx
# DjAMBgorBgEEAYI3AgEVMCMGCSqGSIb3DQEJBDEWBBTqbdMR+ElAmuqgqdiWHgjw
# qyZiuzANBgkqhkiG9w0BAQEFAASCAQAvumx8QMzuWeTCyiJqpKsOTvLF9Ib4tw4j
# IgNkhLrKvXEx/+Njr3U9DhEwfT5bG3Hwxm8Us9DVC6UOYOqlB2muVWBK8tGe1EGd
# /Y7Sr6ejWUQofIKy/r9foIHnfW70q7hv+yNCYsxLFUoPayo7kMaw9b8qRbhAXiBI
# 8JDmX5sc4lhFdsdUXUdhGfGlrs335O8MBXVWijuBqtH0Kei59EOkOravXnhrBUZA
# xgcCgu+DSf5iWfpqZIMj5WwsoUu8Vz0H4nC1omjUjeAwxrBaCKR9RUYYSsEuNfHc
# u2OWw1jIXZP+g0qUaisKW+pjqYV2BqK918Y42xl/I1kddaioO1cLoYICCzCCAgcG
# CSqGSIb3DQEJBjGCAfgwggH0AgEBMHIwXjELMAkGA1UEBhMCVVMxHTAbBgNVBAoT
# FFN5bWFudGVjIENvcnBvcmF0aW9uMTAwLgYDVQQDEydTeW1hbnRlYyBUaW1lIFN0
# YW1waW5nIFNlcnZpY2VzIENBIC0gRzICEA7P9DjI/r81bgTYapgbGlAwCQYFKw4D
# AhoFAKBdMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8X
# DTE0MDgxMDE4MDc0MlowIwYJKoZIhvcNAQkEMRYEFIxNuFORvYCnaxzki8NeqOKI
# m35uMA0GCSqGSIb3DQEBAQUABIIBAF/7XTrQbFBqveQKNP3+KgJLsmgnFGNrPM20
# vy7a4CNtcwQo4iOjl8mbnKU6iMTa0g8ixKy2P7z70h2qcb+Vwee051QfP1/ULsfK
# v5PenZmiiXmQHZOrz6GMwXeY4cwD1F82cKlV+W2GQK6lA7MMjSiU1M6Ce/TIQq+1
# p+KnO/qVJoyQwAnMEwJXD6Co2Uz6Mk7Ti7w4uCG0xXVsUPubU7R/2Z3Nfk8kfI4K
# aAsqmOwNjpASy7vVOTLU56PiReqvJQcuBgv2QDVh5LhDpuWK5pJgNda2Hhy/QRXu
# Si6rmhkzsNMnkoVdFUyG7OtOYFZ3XKu03Synt0O3jNNQiG6ag7A=
# SIG # End signature block
