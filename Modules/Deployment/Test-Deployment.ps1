﻿<#
.SYNOPSIS
    Checks the syntax of a Deploy.xml file for correctness

.DESCRIPTION
    Loads the deploy.xml file and checks to see it contains 
    a sane node layout
.PARAMETER
    None.
.INPUTS
    None.
.OUTPUTS
    The script writes to the console to indicate progress.
.EXAMPLE
    .\Check-Deployment.ps1
#>

Function _Main{
	$pwd = pwd
	[xml]$deploy = New-Object -Type System.Xml.XmlDocument
	$deploy.Load("$pwd\deploy.xml")

	# A deploy.xml file should start with a <deployment> node, currently
	# with a version of "1.0"
}

_Main