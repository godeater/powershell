﻿using namespace System.Management.Automation
using namespace System.Management.Automation.Language

Register-ArgumentCompleter -Native -CommandName 'dws' -ScriptBlock {
    param($wordToComplete, $commandAst, $cursorPosition)

    Write-Host " "

    $commandElements = $commandAst.CommandElements

    $command = @(
        'dws'
        for($i = 1; $i -lt $commandElements.Count; $i++) {
            $element = $commandElements[$i]
            if ($element -isnot [StringConstantExpressionAst] -or
                $element.StringConstantType -ne [StringConstantType]::BareWord -or
                $element.Value.StartsWith('-')) {
                    break
		}
            $element.Value
        }
    ) -join ';'

    Write-Host $command

    $completions = @(switch ($command) {
			 'dws' {
			     [CompletionResult]::new('help', 'help', [CompletionResultType]::ParameterValue, 'help')
			     [CompletionResult]::new('ansible', 'ansible', [CompletionResultType]::ParameterValue, 'ansible')
			     [CompletionResult]::new('channel', 'channel', [CompletionResultType]::ParameterValue, 'channel')
			     [CompletionResult]::new('artifact', 'artifact', [CompletionResultType]::ParameterValue, 'artifact')
			     [CompletionResult]::new('audit', 'audit', [CompletionResultType]::ParameterValue, 'audit')
			     [CompletionResult]::new('config', 'config', [CompletionResultType]::ParameterValue, 'config')
			     [CompletionResult]::new('onboarding', 'onboarding', [CompletionResultType]::ParameterValue, 'onboarding')
			     [CompletionResult]::new('orchestrator', 'orchestrator', [CompletionResultType]::ParameterValue, 'orchestrator')
			     [CompletionResult]::new('pcf', 'pcf', [CompletionResultType]::ParameterValue, 'pcf')
			     [CompletionResult]::new('product', 'product', [CompletionResultType]::ParameterValue, 'product')
			     [CompletionResult]::new('server', 'server', [CompletionResultType]::ParameterValue, 'server')
			     [CompletionResult]::new('setup', 'setup', [CompletionResultType]::ParameterValue, 'setup')
			     [CompletionResult]::new('transport', 'transport', [CompletionResultType]::ParameterValue, 'transport')
			     [CompletionResult]::new('userRoles', 'userRoles', [CompletionResultType]::ParameterValue, 'userRoles')
			     [CompletionResult]::new('version', 'version', [CompletionResultType]::ParameterValue, 'version')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;help' {
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;ansible' {
			     [CompletionResult]::new('get', 'get', [CompletionResultType]::ParameterValue, 'get')
			     [CompletionResult]::new('operation', 'operation', [CompletionResultType]::ParameterValue, 'operation')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;ansible;get' {
			     [CompletionResult]::new('operationstatus', 'operationstatus', [CompletionResultType]::ParameterValue, 'operationstatus')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;ansible;get;operationstatus' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--id', 'id', [CompletionResultType]::ParameterName, 'id')
			     [CompletionResult]::new('--clusterid', 'clusterid', [CompletionResultType]::ParameterName, 'clusterid')
			     [CompletionResult]::new('--stdout', 'stdout', [CompletionResultType]::ParameterName, 'stdout')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;ansible;operation' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--environment', 'environment', [CompletionResultType]::ParameterName, 'environment')
			     [CompletionResult]::new('--role', 'role', [CompletionResultType]::ParameterName, 'role')
			     [CompletionResult]::new('--extravarsfile', 'extravarsfile', [CompletionResultType]::ParameterName, 'extravarsfile')
			     [CompletionResult]::new('--playbookpath', 'playbookpath', [CompletionResultType]::ParameterName, 'playbookpath')
			     [CompletionResult]::new('--credentialtype', 'credentialtype', [CompletionResultType]::ParameterName, 'credentialtype')
			     [CompletionResult]::new('--surveyfile', 'surveyfile', [CompletionResultType]::ParameterName, 'surveyfile')
			     [CompletionResult]::new('--scmprojectkey', 'scmprojectkey', [CompletionResultType]::ParameterName, 'scmprojectkey')
			     [CompletionResult]::new('--scmrepo', 'scmrepo', [CompletionResultType]::ParameterName, 'scmrepo')
			     [CompletionResult]::new('--scmbranch', 'scmbranch', [CompletionResultType]::ParameterName, 'scmbranch')
			     [CompletionResult]::new('--inventorypath', 'inventorypath', [CompletionResultType]::ParameterName, 'inventorypath')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel' {
			     [CompletionResult]::new('get', 'get', [CompletionResultType]::ParameterValue, 'get')
			     [CompletionResult]::new('create', 'create', [CompletionResultType]::ParameterValue, 'create')
			     [CompletionResult]::new('update', 'update', [CompletionResultType]::ParameterValue, 'update')
			     [CompletionResult]::new('delete', 'delete', [CompletionResultType]::ParameterValue, 'delete')
			     [CompletionResult]::new('add', 'add', [CompletionResultType]::ParameterValue, 'add')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel;get' {
			     [CompletionResult]::new('all', 'all', [CompletionResultType]::ParameterValue, 'all')
			     [CompletionResult]::new('products', 'products', [CompletionResultType]::ParameterValue, 'products')
			     [CompletionResult]::new('instances', 'instances', [CompletionResultType]::ParameterValue, 'instances')
			     [CompletionResult]::new('instancedetails', 'instancedetails', [CompletionResultType]::ParameterValue, 'instancedetails')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel;get;all' {
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel;get;products' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel;get;instances' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--product', 'product', [CompletionResultType]::ParameterName, 'product')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel;get;instancedetails' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--product', 'product', [CompletionResultType]::ParameterName, 'product')
			     [CompletionResult]::new('--instance', 'instance', [CompletionResultType]::ParameterName, 'instance')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel;create' {
			     [CompletionResult]::new('channel', 'channel', [CompletionResultType]::ParameterValue, 'channel')
			     [CompletionResult]::new('instance', 'instance', [CompletionResultType]::ParameterValue, 'instance')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel;create;channel' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--domain', 'domain', [CompletionResultType]::ParameterName, 'domain')
			     [CompletionResult]::new('--cmdbappid', 'cmdbappid', [CompletionResultType]::ParameterName, 'cmdbappid')
			     [CompletionResult]::new('--config-file', 'config-file', [CompletionResultType]::ParameterName, 'config-file')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel;create;instance' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--product', 'product', [CompletionResultType]::ParameterName, 'product')
			     [CompletionResult]::new('--instancename', 'instancename', [CompletionResultType]::ParameterName, 'instancename')
			     [CompletionResult]::new('--status', 'status', [CompletionResultType]::ParameterName, 'status')
			     [CompletionResult]::new('--http-port', 'http-port', [CompletionResultType]::ParameterName, 'http-port')
			     [CompletionResult]::new('--ajp-port', 'ajp-port', [CompletionResultType]::ParameterName, 'ajp-port')
			     [CompletionResult]::new('--ssl-port', 'ssl-port', [CompletionResultType]::ParameterName, 'ssl-port')
			     [CompletionResult]::new('--config-file', 'config-file', [CompletionResultType]::ParameterName, 'config-file')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel;update' {
			     [CompletionResult]::new('channel', 'channel', [CompletionResultType]::ParameterValue, 'channel')
			     [CompletionResult]::new('instance', 'instance', [CompletionResultType]::ParameterValue, 'instance')
			     [CompletionResult]::new('host', 'host', [CompletionResultType]::ParameterValue, 'host')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel;update;channel' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--domain', 'domain', [CompletionResultType]::ParameterName, 'domain')
			     [CompletionResult]::new('--cmdbappid', 'cmdbappid', [CompletionResultType]::ParameterName, 'cmdbappid')
			     [CompletionResult]::new('--config-file', 'config-file', [CompletionResultType]::ParameterName, 'config-file')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel;update;instance' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--product', 'product', [CompletionResultType]::ParameterName, 'product')
			     [CompletionResult]::new('--instancename', 'instancename', [CompletionResultType]::ParameterName, 'instancename')
			     [CompletionResult]::new('--status', 'status', [CompletionResultType]::ParameterName, 'status')
			     [CompletionResult]::new('--http-port', 'http-port', [CompletionResultType]::ParameterName, 'http-port')
			     [CompletionResult]::new('--ajp-port', 'ajp-port', [CompletionResultType]::ParameterName, 'ajp-port')
			     [CompletionResult]::new('--ssl-port', 'ssl-port', [CompletionResultType]::ParameterName, 'ssl-port')
			     [CompletionResult]::new('--config-file', 'config-file', [CompletionResultType]::ParameterName, 'config-file')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel;update;host' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--product', 'product', [CompletionResultType]::ParameterName, 'product')
			     [CompletionResult]::new('--instance-name', 'instance-name', [CompletionResultType]::ParameterName, 'instance-name')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--version', 'version', [CompletionResultType]::ParameterName, 'version')
			     [CompletionResult]::new('--username', 'username', [CompletionResultType]::ParameterName, 'username')
			     [CompletionResult]::new('--groupname', 'groupname', [CompletionResultType]::ParameterName, 'groupname')
			     [CompletionResult]::new('--loggroupname', 'loggroupname', [CompletionResultType]::ParameterName, 'loggroupname')
			     [CompletionResult]::new('--hoststatus', 'hoststatus', [CompletionResultType]::ParameterName, 'hoststatus')
			     [CompletionResult]::new('--config-file', 'config-file', [CompletionResultType]::ParameterName, 'config-file')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel;delete' {
			     [CompletionResult]::new('application', 'application', [CompletionResultType]::ParameterValue, 'application')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel;delete;application' {
			     [CompletionResult]::new('--name', 'name', [CompletionResultType]::ParameterName, 'name')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel;add' {
			     [CompletionResult]::new('host', 'host', [CompletionResultType]::ParameterValue, 'host')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;channel;add;host' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--product', 'product', [CompletionResultType]::ParameterName, 'product')
			     [CompletionResult]::new('--instance-name', 'instance-name', [CompletionResultType]::ParameterName, 'instance-name')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--version', 'version', [CompletionResultType]::ParameterName, 'version')
			     [CompletionResult]::new('--username', 'username', [CompletionResultType]::ParameterName, 'username')
			     [CompletionResult]::new('--groupname', 'groupname', [CompletionResultType]::ParameterName, 'groupname')
			     [CompletionResult]::new('--loggroupname', 'loggroupname', [CompletionResultType]::ParameterName, 'loggroupname')
			     [CompletionResult]::new('--hoststatus', 'hoststatus', [CompletionResultType]::ParameterName, 'hoststatus')
			     [CompletionResult]::new('--config-file', 'config-file', [CompletionResultType]::ParameterName, 'config-file')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;artifact' {
			     [CompletionResult]::new('get', 'get', [CompletionResultType]::ParameterValue, 'get')
			     [CompletionResult]::new('create', 'create', [CompletionResultType]::ParameterValue, 'create')
			     [CompletionResult]::new('versioncreate', 'versioncreate', [CompletionResultType]::ParameterValue, 'versioncreate')
			     [CompletionResult]::new('registerserver', 'registerserver', [CompletionResultType]::ParameterValue, 'registerserver')
			     [CompletionResult]::new('deployserver', 'deployserver', [CompletionResultType]::ParameterValue, 'deployserver')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;artifact;get' {
			     [CompletionResult]::new('all', 'all', [CompletionResultType]::ParameterValue, 'all')
			     [CompletionResult]::new('info', 'info', [CompletionResultType]::ParameterValue, 'info')
			     [CompletionResult]::new('servers', 'servers', [CompletionResultType]::ParameterValue, 'servers')
			     [CompletionResult]::new('artifacts', 'artifacts', [CompletionResultType]::ParameterValue, 'artifacts')
			     [CompletionResult]::new('deploystatus', 'deploystatus', [CompletionResultType]::ParameterValue, 'deploystatus')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;artifact;get;all' {
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;artifact;get;info' {
			     [CompletionResult]::new('--artifact', 'artifact', [CompletionResultType]::ParameterName, 'artifact')
			     [CompletionResult]::new('--version', 'version', [CompletionResultType]::ParameterName, 'version')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;artifact;get;servers' {
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;artifact;get;artifacts' {
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--artifact', 'artifact', [CompletionResultType]::ParameterName, 'artifact')
			     [CompletionResult]::new('--version', 'version', [CompletionResultType]::ParameterName, 'version')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;artifact;get;deploystatus' {
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--artifact', 'artifact', [CompletionResultType]::ParameterName, 'artifact')
			     [CompletionResult]::new('--version', 'version', [CompletionResultType]::ParameterName, 'version')
			     [CompletionResult]::new('--id', 'id', [CompletionResultType]::ParameterName, 'id')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;artifact;create' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--artifact', 'artifact', [CompletionResultType]::ParameterName, 'artifact')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;artifact;versioncreate' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--artifact', 'artifact', [CompletionResultType]::ParameterName, 'artifact')
			     [CompletionResult]::new('--version', 'version', [CompletionResultType]::ParameterName, 'version')
			     [CompletionResult]::new('--sourceurl', 'sourceurl', [CompletionResultType]::ParameterName, 'sourceurl')
			     [CompletionResult]::new('--md5sum', 'md5sum', [CompletionResultType]::ParameterName, 'md5sum')
			     [CompletionResult]::new('--destination', 'destination', [CompletionResultType]::ParameterName, 'destination')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;artifact;registerserver' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--artifact', 'artifact', [CompletionResultType]::ParameterName, 'artifact')
			     [CompletionResult]::new('--version', 'version', [CompletionResultType]::ParameterName, 'version')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;artifact;deployserver' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--artifact', 'artifact', [CompletionResultType]::ParameterName, 'artifact')
			     [CompletionResult]::new('--version', 'version', [CompletionResultType]::ParameterName, 'version')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;audit' {
			     [CompletionResult]::new('get', 'get', [CompletionResultType]::ParameterValue, 'get')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;audit;get' {
			     [CompletionResult]::new('sources', 'sources', [CompletionResultType]::ParameterValue, 'sources')
			     [CompletionResult]::new('events', 'events', [CompletionResultType]::ParameterValue, 'events')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;audit;get;sources' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;audit;get;events' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--source', 'source', [CompletionResultType]::ParameterName, 'source')
			     [CompletionResult]::new('--id', 'id', [CompletionResultType]::ParameterName, 'id')
			     [CompletionResult]::new('--startDate', 'startDate', [CompletionResultType]::ParameterName, 'startDate')
			     [CompletionResult]::new('--endDate', 'endDate', [CompletionResultType]::ParameterName, 'endDate')
			     [CompletionResult]::new('--userDomain', 'userDomain', [CompletionResultType]::ParameterName, 'userDomain')
			     [CompletionResult]::new('--userId', 'userId', [CompletionResultType]::ParameterName, 'userId')
			     [CompletionResult]::new('--correlationId', 'correlationId', [CompletionResultType]::ParameterName, 'correlationId')
			     [CompletionResult]::new('--httpMethod', 'httpMethod', [CompletionResultType]::ParameterName, 'httpMethod')
			     [CompletionResult]::new('--results', 'results', [CompletionResultType]::ParameterName, 'results')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;config' {
			     [CompletionResult]::new('get', 'get', [CompletionResultType]::ParameterValue, 'get')
			     [CompletionResult]::new('create', 'create', [CompletionResultType]::ParameterValue, 'create')
			     [CompletionResult]::new('update', 'update', [CompletionResultType]::ParameterValue, 'update')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;config;get' {
			     [CompletionResult]::new('servers', 'servers', [CompletionResultType]::ParameterValue, 'servers')
			     [CompletionResult]::new('registration', 'registration', [CompletionResultType]::ParameterValue, 'registration')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;config;get;servers' {
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;config;get;registration' {
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;config;create' {
			     [CompletionResult]::new('registration', 'registration', [CompletionResultType]::ParameterValue, 'registration')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;config;create;registration' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--description', 'description', [CompletionResultType]::ParameterName, 'description')
			     [CompletionResult]::new('--env', 'env', [CompletionResultType]::ParameterName, 'env')
			     [CompletionResult]::new('--domain', 'domain', [CompletionResultType]::ParameterName, 'domain')
			     [CompletionResult]::new('--hardware', 'hardware', [CompletionResultType]::ParameterName, 'hardware')
			     [CompletionResult]::new('--datacentre', 'datacentre', [CompletionResultType]::ParameterName, 'datacentre')
			     [CompletionResult]::new('--os', 'os', [CompletionResultType]::ParameterName, 'os')
			     [CompletionResult]::new('--assetnumber', 'assetnumber', [CompletionResultType]::ParameterName, 'assetnumber')
			     [CompletionResult]::new('--serialnumber', 'serialnumber', [CompletionResultType]::ParameterName, 'serialnumber')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;config;update' {
			     [CompletionResult]::new('registration', 'registration', [CompletionResultType]::ParameterValue, 'registration')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;config;update;registration' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--description', 'description', [CompletionResultType]::ParameterName, 'description')
			     [CompletionResult]::new('--env', 'env', [CompletionResultType]::ParameterName, 'env')
			     [CompletionResult]::new('--domain', 'domain', [CompletionResultType]::ParameterName, 'domain')
			     [CompletionResult]::new('--hardware', 'hardware', [CompletionResultType]::ParameterName, 'hardware')
			     [CompletionResult]::new('--datacentre', 'datacentre', [CompletionResultType]::ParameterName, 'datacentre')
			     [CompletionResult]::new('--os', 'os', [CompletionResultType]::ParameterName, 'os')
			     [CompletionResult]::new('--assetnumber', 'assetnumber', [CompletionResultType]::ParameterName, 'assetnumber')
			     [CompletionResult]::new('--serialnumber', 'serialnumber', [CompletionResultType]::ParameterName, 'serialnumber')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;onboarding' {
			     [CompletionResult]::new('register', 'register', [CompletionResultType]::ParameterValue, 'register')
			     [CompletionResult]::new('provision', 'provision', [CompletionResultType]::ParameterValue, 'provision')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;onboarding;register' {
			     [CompletionResult]::new('--cmdbappid', 'cmdbappid', [CompletionResultType]::ParameterName, 'cmdbappid')
			     [CompletionResult]::new('--appname', 'appname', [CompletionResultType]::ParameterName, 'appname')
			     [CompletionResult]::new('--capabilities', 'capabilities', [CompletionResultType]::ParameterName, 'capabilities')
			     [CompletionResult]::new('--adgrpdomain', 'adgrpdomain', [CompletionResultType]::ParameterName, 'adgrpdomain')
			     [CompletionResult]::new('--adgrpauthorisers', 'adgrpauthorisers', [CompletionResultType]::ParameterName, 'adgrpauthorisers')
			     [CompletionResult]::new('--adgrpowner', 'adgrpowner', [CompletionResultType]::ParameterName, 'adgrpowner')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;onboarding;provision' {
			     [CompletionResult]::new('--cmdbappid', 'cmdbappid', [CompletionResultType]::ParameterName, 'cmdbappid')
			     [CompletionResult]::new('--appname', 'appname', [CompletionResultType]::ParameterName, 'appname')
			     [CompletionResult]::new('--capabilities', 'capabilities', [CompletionResultType]::ParameterName, 'capabilities')
			     [CompletionResult]::new('--adgrpdomain', 'adgrpdomain', [CompletionResultType]::ParameterName, 'adgrpdomain')
			     [CompletionResult]::new('--adgrpauthorisers', 'adgrpauthorisers', [CompletionResultType]::ParameterName, 'adgrpauthorisers')
			     [CompletionResult]::new('--adgrpowner', 'adgrpowner', [CompletionResultType]::ParameterName, 'adgrpowner')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;orchestrator' {
			     [CompletionResult]::new('get', 'get', [CompletionResultType]::ParameterValue, 'get')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;orchestrator;get' {
			     [CompletionResult]::new('catalogue', 'catalogue', [CompletionResultType]::ParameterValue, 'catalogue')
			     [CompletionResult]::new('infra', 'infra', [CompletionResultType]::ParameterValue, 'infra')
			     [CompletionResult]::new('change', 'change', [CompletionResultType]::ParameterValue, 'change')
			     [CompletionResult]::new('incident', 'incident', [CompletionResultType]::ParameterValue, 'incident')
			     [CompletionResult]::new('server', 'server', [CompletionResultType]::ParameterValue, 'server')
			     [CompletionResult]::new('application', 'application', [CompletionResultType]::ParameterValue, 'application')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;orchestrator;get;catalogue' {
			     [CompletionResult]::new('--id', 'id', [CompletionResultType]::ParameterName, 'id')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;orchestrator;get;infra' {
			     [CompletionResult]::new('--id', 'id', [CompletionResultType]::ParameterName, 'id')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;orchestrator;get;change' {
			     [CompletionResult]::new('--id', 'id', [CompletionResultType]::ParameterName, 'id')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;orchestrator;get;incident' {
			     [CompletionResult]::new('--id', 'id', [CompletionResultType]::ParameterName, 'id')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;orchestrator;get;server' {
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;orchestrator;get;application' {
			     [CompletionResult]::new('--id', 'id', [CompletionResultType]::ParameterName, 'id')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf' {
			     [CompletionResult]::new('get', 'get', [CompletionResultType]::ParameterValue, 'get')
			     [CompletionResult]::new('operation', 'operation', [CompletionResultType]::ParameterValue, 'operation')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;get' {
			     [CompletionResult]::new('buildpacks', 'buildpacks', [CompletionResultType]::ParameterValue, 'buildpacks')
			     [CompletionResult]::new('channels', 'channels', [CompletionResultType]::ParameterValue, 'channels')
			     [CompletionResult]::new('channel', 'channel', [CompletionResultType]::ParameterValue, 'channel')
			     [CompletionResult]::new('spaces', 'spaces', [CompletionResultType]::ParameterValue, 'spaces')
			     [CompletionResult]::new('apps', 'apps', [CompletionResultType]::ParameterValue, 'apps')
			     [CompletionResult]::new('app', 'app', [CompletionResultType]::ParameterValue, 'app')
			     [CompletionResult]::new('audit', 'audit', [CompletionResultType]::ParameterValue, 'audit')
			     [CompletionResult]::new('routes', 'routes', [CompletionResultType]::ParameterValue, 'routes')
			     [CompletionResult]::new('services', 'services', [CompletionResultType]::ParameterValue, 'services')
			     [CompletionResult]::new('service', 'service', [CompletionResultType]::ParameterValue, 'service')
			     [CompletionResult]::new('quotas', 'quotas', [CompletionResultType]::ParameterValue, 'quotas')
			     [CompletionResult]::new('quota', 'quota', [CompletionResultType]::ParameterValue, 'quota')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;get;buildpacks' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;get;channels' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;get;channel' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;get;spaces' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;get;apps' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;get;app' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--appname', 'appname', [CompletionResultType]::ParameterName, 'appname')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;get;audit' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--appname', 'appname', [CompletionResultType]::ParameterName, 'appname')
			     [CompletionResult]::new('--results', 'results', [CompletionResultType]::ParameterName, 'results')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;get;routes' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--domain', 'domain', [CompletionResultType]::ParameterName, 'domain')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;get;services' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;get;service' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--service', 'service', [CompletionResultType]::ParameterName, 'service')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;get;quotas' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;get;quota' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--quota', 'quota', [CompletionResultType]::ParameterName, 'quota')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation' {
			     [CompletionResult]::new('create', 'create', [CompletionResultType]::ParameterValue, 'create')
			     [CompletionResult]::new('update', 'update', [CompletionResultType]::ParameterValue, 'update')
			     [CompletionResult]::new('delete', 'delete', [CompletionResultType]::ParameterValue, 'delete')
			     [CompletionResult]::new('deploy', 'deploy', [CompletionResultType]::ParameterValue, 'deploy')
			     [CompletionResult]::new('action', 'action', [CompletionResultType]::ParameterValue, 'action')
			     [CompletionResult]::new('map', 'map', [CompletionResultType]::ParameterValue, 'map')
			     [CompletionResult]::new('unmap', 'unmap', [CompletionResultType]::ParameterValue, 'unmap')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;create' {
			     [CompletionResult]::new('app', 'app', [CompletionResultType]::ParameterValue, 'app')
			     [CompletionResult]::new('route', 'route', [CompletionResultType]::ParameterValue, 'route')
			     [CompletionResult]::new('service', 'service', [CompletionResultType]::ParameterValue, 'service')
			     [CompletionResult]::new('channel', 'channel', [CompletionResultType]::ParameterValue, 'channel')
			     [CompletionResult]::new('space', 'space', [CompletionResultType]::ParameterValue, 'space')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;create;app' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--appname', 'appname', [CompletionResultType]::ParameterName, 'appname')
			     [CompletionResult]::new('--buildpack', 'buildpack', [CompletionResultType]::ParameterName, 'buildpack')
			     [CompletionResult]::new('--instances', 'instances', [CompletionResultType]::ParameterName, 'instances')
			     [CompletionResult]::new('--diskquota', 'diskquota', [CompletionResultType]::ParameterName, 'diskquota')
			     [CompletionResult]::new('--memory', 'memory', [CompletionResultType]::ParameterName, 'memory')
			     [CompletionResult]::new('--envfile', 'envfile', [CompletionResultType]::ParameterName, 'envfile')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--bindservices', 'bindservices', [CompletionResultType]::ParameterName, 'bindservices')
			     [CompletionResult]::new('--healthcheckendpoint', 'healthcheckendpoint', [CompletionResultType]::ParameterName, 'healthcheckendpoint')
			     [CompletionResult]::new('--healthchecktype', 'healthchecktype', [CompletionResultType]::ParameterName, 'healthchecktype')
			     [CompletionResult]::new('--healthchecktimeout', 'healthchecktimeout', [CompletionResultType]::ParameterName, 'healthchecktimeout')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;create;route' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--domain', 'domain', [CompletionResultType]::ParameterName, 'domain')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--path', 'path', [CompletionResultType]::ParameterName, 'path')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;create;service' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--service', 'service', [CompletionResultType]::ParameterName, 'service')
			     [CompletionResult]::new('--syslogurl', 'syslogurl', [CompletionResultType]::ParameterName, 'syslogurl')
			     [CompletionResult]::new('--credentialsfile', 'credentialsfile', [CompletionResultType]::ParameterName, 'credentialsfile')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;create;channel' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--quota', 'quota', [CompletionResultType]::ParameterName, 'quota')
			     [CompletionResult]::new('--projectcode', 'projectcode', [CompletionResultType]::ParameterName, 'projectcode')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;create;space' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;update' {
			     [CompletionResult]::new('app', 'app', [CompletionResultType]::ParameterValue, 'app')
			     [CompletionResult]::new('route', 'route', [CompletionResultType]::ParameterValue, 'route')
			     [CompletionResult]::new('service', 'service', [CompletionResultType]::ParameterValue, 'service')
			     [CompletionResult]::new('channel', 'channel', [CompletionResultType]::ParameterValue, 'channel')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;update;app' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--appname', 'appname', [CompletionResultType]::ParameterName, 'appname')
			     [CompletionResult]::new('--buildpack', 'buildpack', [CompletionResultType]::ParameterName, 'buildpack')
			     [CompletionResult]::new('--instances', 'instances', [CompletionResultType]::ParameterName, 'instances')
			     [CompletionResult]::new('--diskquota', 'diskquota', [CompletionResultType]::ParameterName, 'diskquota')
			     [CompletionResult]::new('--memory', 'memory', [CompletionResultType]::ParameterName, 'memory')
			     [CompletionResult]::new('--envfile', 'envfile', [CompletionResultType]::ParameterName, 'envfile')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--bindservices', 'bindservices', [CompletionResultType]::ParameterName, 'bindservices')
			     [CompletionResult]::new('--renameapp', 'renameapp', [CompletionResultType]::ParameterName, 'renameapp')
			     [CompletionResult]::new('--healthcheckendpoint', 'healthcheckendpoint', [CompletionResultType]::ParameterName, 'healthcheckendpoint')
			     [CompletionResult]::new('--healthchecktype', 'healthchecktype', [CompletionResultType]::ParameterName, 'healthchecktype')
			     [CompletionResult]::new('--healthchecktimeout', 'healthchecktimeout', [CompletionResultType]::ParameterName, 'healthchecktimeout')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;update;route' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--domain', 'domain', [CompletionResultType]::ParameterName, 'domain')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--oldpath', 'oldpath', [CompletionResultType]::ParameterName, 'oldpath')
			     [CompletionResult]::new('--newpath', 'newpath', [CompletionResultType]::ParameterName, 'newpath')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;update;service' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--service', 'service', [CompletionResultType]::ParameterName, 'service')
			     [CompletionResult]::new('--syslogurl', 'syslogurl', [CompletionResultType]::ParameterName, 'syslogurl')
			     [CompletionResult]::new('--credentialsfile', 'credentialsfile', [CompletionResultType]::ParameterName, 'credentialsfile')
			     [CompletionResult]::new('--newName', 'newName', [CompletionResultType]::ParameterName, 'newName')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;update;channel' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--quota', 'quota', [CompletionResultType]::ParameterName, 'quota')
			     [CompletionResult]::new('--projectcode', 'projectcode', [CompletionResultType]::ParameterName, 'projectcode')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;delete' {
			     [CompletionResult]::new('app', 'app', [CompletionResultType]::ParameterValue, 'app')
			     [CompletionResult]::new('route', 'route', [CompletionResultType]::ParameterValue, 'route')
			     [CompletionResult]::new('service', 'service', [CompletionResultType]::ParameterValue, 'service')
			     [CompletionResult]::new('channel', 'channel', [CompletionResultType]::ParameterValue, 'channel')
			     [CompletionResult]::new('space', 'space', [CompletionResultType]::ParameterValue, 'space')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;delete;app' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--appname', 'appname', [CompletionResultType]::ParameterName, 'appname')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;delete;route' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--domain', 'domain', [CompletionResultType]::ParameterName, 'domain')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--path', 'path', [CompletionResultType]::ParameterName, 'path')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;delete;service' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--service', 'service', [CompletionResultType]::ParameterName, 'service')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;delete;channel' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;delete;space' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;deploy' {
			     [CompletionResult]::new('app', 'app', [CompletionResultType]::ParameterValue, 'app')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;deploy;app' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--appname', 'appname', [CompletionResultType]::ParameterName, 'appname')
			     [CompletionResult]::new('--url', 'url', [CompletionResultType]::ParameterName, 'url')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;action' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--appname', 'appname', [CompletionResultType]::ParameterName, 'appname')
			     [CompletionResult]::new('--action', 'action', [CompletionResultType]::ParameterName, 'action')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--foundation', 'foundation', [CompletionResultType]::ParameterName, 'foundation')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;map' {
			     [CompletionResult]::new('route', 'route', [CompletionResultType]::ParameterValue, 'route')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;map;route' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--appname', 'appname', [CompletionResultType]::ParameterName, 'appname')
			     [CompletionResult]::new('--domain', 'domain', [CompletionResultType]::ParameterName, 'domain')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--path', 'path', [CompletionResultType]::ParameterName, 'path')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;unmap' {
			     [CompletionResult]::new('route', 'route', [CompletionResultType]::ParameterValue, 'route')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;pcf;operation;unmap;route' {
			     [CompletionResult]::new('--pcfenv', 'pcfenv', [CompletionResultType]::ParameterName, 'pcfenv')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--space', 'space', [CompletionResultType]::ParameterName, 'space')
			     [CompletionResult]::new('--appname', 'appname', [CompletionResultType]::ParameterName, 'appname')
			     [CompletionResult]::new('--domain', 'domain', [CompletionResultType]::ParameterName, 'domain')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--approvedrequest', 'approvedrequest', [CompletionResultType]::ParameterName, 'approvedrequest')
			     [CompletionResult]::new('--path', 'path', [CompletionResultType]::ParameterName, 'path')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;product' {
			     [CompletionResult]::new('get', 'get', [CompletionResultType]::ParameterValue, 'get')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;product;get' {
			     [CompletionResult]::new('all', 'all', [CompletionResultType]::ParameterValue, 'all')
			     [CompletionResult]::new('info', 'info', [CompletionResultType]::ParameterValue, 'info')
			     [CompletionResult]::new('action', 'action', [CompletionResultType]::ParameterValue, 'action')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;product;get;all' {
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;product;get;info' {
			     [CompletionResult]::new('--product', 'product', [CompletionResultType]::ParameterName, 'product')
			     [CompletionResult]::new('--version', 'version', [CompletionResultType]::ParameterName, 'version')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;product;get;action' {
			     [CompletionResult]::new('--product', 'product', [CompletionResultType]::ParameterName, 'product')
			     [CompletionResult]::new('--version', 'version', [CompletionResultType]::ParameterName, 'version')
			     [CompletionResult]::new('--transport', 'transport', [CompletionResultType]::ParameterName, 'transport')
			     [CompletionResult]::new('--action', 'action', [CompletionResultType]::ParameterName, 'action')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server' {
			     [CompletionResult]::new('get', 'get', [CompletionResultType]::ParameterValue, 'get')
			     [CompletionResult]::new('operation', 'operation', [CompletionResultType]::ParameterValue, 'operation')
			     [CompletionResult]::new('deployment', 'deployment', [CompletionResultType]::ParameterValue, 'deployment')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server;get' {
			     [CompletionResult]::new('all', 'all', [CompletionResultType]::ParameterValue, 'all')
			     [CompletionResult]::new('products', 'products', [CompletionResultType]::ParameterValue, 'products')
			     [CompletionResult]::new('instances', 'instances', [CompletionResultType]::ParameterValue, 'instances')
			     [CompletionResult]::new('instancedetails', 'instancedetails', [CompletionResultType]::ParameterValue, 'instancedetails')
			     [CompletionResult]::new('deploymentstatus', 'deploymentstatus', [CompletionResultType]::ParameterValue, 'deploymentstatus')
			     [CompletionResult]::new('operationstatus', 'operationstatus', [CompletionResultType]::ParameterValue, 'operationstatus')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server;get;all' {
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server;get;products' {
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server;get;instances' {
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--product', 'product', [CompletionResultType]::ParameterName, 'product')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server;get;instancedetails' {
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--product', 'product', [CompletionResultType]::ParameterName, 'product')
			     [CompletionResult]::new('--instance', 'instance', [CompletionResultType]::ParameterName, 'instance')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server;get;deploymentstatus' {
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--product', 'product', [CompletionResultType]::ParameterName, 'product')
			     [CompletionResult]::new('--instance', 'instance', [CompletionResultType]::ParameterName, 'instance')
			     [CompletionResult]::new('--id', 'id', [CompletionResultType]::ParameterName, 'id')
			     [CompletionResult]::new('--results', 'results', [CompletionResultType]::ParameterName, 'results')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server;get;operationstatus' {
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--product', 'product', [CompletionResultType]::ParameterName, 'product')
			     [CompletionResult]::new('--instance', 'instance', [CompletionResultType]::ParameterName, 'instance')
			     [CompletionResult]::new('--id', 'id', [CompletionResultType]::ParameterName, 'id')
			     [CompletionResult]::new('--results', 'results', [CompletionResultType]::ParameterName, 'results')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server;operation' {
			     [CompletionResult]::new('update', 'update', [CompletionResultType]::ParameterValue, 'update')
			     [CompletionResult]::new('action', 'action', [CompletionResultType]::ParameterValue, 'action')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server;operation;update' {
			     [CompletionResult]::new('requeststatus', 'requeststatus', [CompletionResultType]::ParameterValue, 'requeststatus')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server;operation;update;requeststatus' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--product', 'product', [CompletionResultType]::ParameterName, 'product')
			     [CompletionResult]::new('--instance', 'instance', [CompletionResultType]::ParameterName, 'instance')
			     [CompletionResult]::new('--id', 'id', [CompletionResultType]::ParameterName, 'id')
			     [CompletionResult]::new('--status', 'status', [CompletionResultType]::ParameterName, 'status')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server;operation;action' {
			     [CompletionResult]::new('apache', 'apache', [CompletionResultType]::ParameterValue, 'apache')
			     [CompletionResult]::new('jboss', 'jboss', [CompletionResultType]::ParameterValue, 'jboss')
			     [CompletionResult]::new('standalone', 'standalone', [CompletionResultType]::ParameterValue, 'standalone')
			     [CompletionResult]::new('tomcat', 'tomcat', [CompletionResultType]::ParameterValue, 'tomcat')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server;operation;action;apache' {
			     [CompletionResult]::new('--action', 'action', [CompletionResultType]::ParameterName, 'action')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--instance', 'instance', [CompletionResultType]::ParameterName, 'instance')
			     [CompletionResult]::new('--approvedRequest', 'approvedRequest', [CompletionResultType]::ParameterName, 'approvedRequest')
			     [CompletionResult]::new('--key', 'key', [CompletionResultType]::ParameterName, 'key')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server;operation;action;jboss' {
			     [CompletionResult]::new('--action', 'action', [CompletionResultType]::ParameterName, 'action')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--instance', 'instance', [CompletionResultType]::ParameterName, 'instance')
			     [CompletionResult]::new('--approvedRequest', 'approvedRequest', [CompletionResultType]::ParameterName, 'approvedRequest')
			     [CompletionResult]::new('--deployid', 'deployid', [CompletionResultType]::ParameterName, 'deployid')
			     [CompletionResult]::new('--md5sum', 'md5sum', [CompletionResultType]::ParameterName, 'md5sum')
			     [CompletionResult]::new('--path', 'path', [CompletionResultType]::ParameterName, 'path')
			     [CompletionResult]::new('--url', 'url', [CompletionResultType]::ParameterName, 'url')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server;operation;action;standalone' {
			     [CompletionResult]::new('--action', 'action', [CompletionResultType]::ParameterName, 'action')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--instance', 'instance', [CompletionResultType]::ParameterName, 'instance')
			     [CompletionResult]::new('--appDAccKey', 'appDAccKey', [CompletionResultType]::ParameterName, 'appDAccKey')
			     [CompletionResult]::new('--appDAccName', 'appDAccName', [CompletionResultType]::ParameterName, 'appDAccName')
			     [CompletionResult]::new('--appDController', 'appDController', [CompletionResultType]::ParameterName, 'appDController')
			     [CompletionResult]::new('--approvedRequest', 'approvedRequest', [CompletionResultType]::ParameterName, 'approvedRequest')
			     [CompletionResult]::new('--certArtifactGroup', 'certArtifactGroup', [CompletionResultType]::ParameterName, 'certArtifactGroup')
			     [CompletionResult]::new('--certArtifactName', 'certArtifactName', [CompletionResultType]::ParameterName, 'certArtifactName')
			     [CompletionResult]::new('--certArtifactVersion', 'certArtifactVersion', [CompletionResultType]::ParameterName, 'certArtifactVersion')
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--encryptionPassword', 'encryptionPassword', [CompletionResultType]::ParameterName, 'encryptionPassword')
			     [CompletionResult]::new('--environment', 'environment', [CompletionResultType]::ParameterName, 'environment')
			     [CompletionResult]::new('--group', 'group', [CompletionResultType]::ParameterName, 'group')
			     [CompletionResult]::new('--keep', 'keep', [CompletionResultType]::ParameterName, 'keep')
			     [CompletionResult]::new('--propertyArtifactGroup', 'propertyArtifactGroup', [CompletionResultType]::ParameterName, 'propertyArtifactGroup')
			     [CompletionResult]::new('--propertyArtifactName', 'propertyArtifactName', [CompletionResultType]::ParameterName, 'propertyArtifactName')
			     [CompletionResult]::new('--propertyArtifactVersion', 'propertyArtifactVersion', [CompletionResultType]::ParameterName, 'propertyArtifactVersion')
			     [CompletionResult]::new('--serviceConfigUrl', 'serviceConfigUrl', [CompletionResultType]::ParameterName, 'serviceConfigUrl')
			     [CompletionResult]::new('--timeout', 'timeout', [CompletionResultType]::ParameterName, 'timeout')
			     [CompletionResult]::new('--version', 'version', [CompletionResultType]::ParameterName, 'version')
			     [CompletionResult]::new('--zone', 'zone', [CompletionResultType]::ParameterName, 'zone')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server;operation;action;tomcat' {
			     [CompletionResult]::new('--action', 'action', [CompletionResultType]::ParameterName, 'action')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--instance', 'instance', [CompletionResultType]::ParameterName, 'instance')
			     [CompletionResult]::new('--approvedRequest', 'approvedRequest', [CompletionResultType]::ParameterName, 'approvedRequest')
			     [CompletionResult]::new('--deployid', 'deployid', [CompletionResultType]::ParameterName, 'deployid')
			     [CompletionResult]::new('--md5sum', 'md5sum', [CompletionResultType]::ParameterName, 'md5sum')
			     [CompletionResult]::new('--path', 'path', [CompletionResultType]::ParameterName, 'path')
			     [CompletionResult]::new('--url', 'url', [CompletionResultType]::ParameterName, 'url')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;server;deployment' {
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--product', 'product', [CompletionResultType]::ParameterName, 'product')
			     [CompletionResult]::new('--instance', 'instance', [CompletionResultType]::ParameterName, 'instance')
			     [CompletionResult]::new('--url', 'url', [CompletionResultType]::ParameterName, 'url')
			     [CompletionResult]::new('--type', 'type', [CompletionResultType]::ParameterName, 'type')
			     [CompletionResult]::new('--approvedRequest', 'approvedRequest', [CompletionResultType]::ParameterName, 'approvedRequest')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;transport' {
			     [CompletionResult]::new('update', 'update', [CompletionResultType]::ParameterValue, 'update')
			     [CompletionResult]::new('get', 'get', [CompletionResultType]::ParameterValue, 'get')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;transport;update' {
			     [CompletionResult]::new('--channel', 'channel', [CompletionResultType]::ParameterName, 'channel')
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--product', 'product', [CompletionResultType]::ParameterName, 'product')
			     [CompletionResult]::new('--instance', 'instance', [CompletionResultType]::ParameterName, 'instance')
			     [CompletionResult]::new('--transport', 'transport', [CompletionResultType]::ParameterName, 'transport')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;transport;get' {
			     [CompletionResult]::new('all', 'all', [CompletionResultType]::ParameterValue, 'all')
			     [CompletionResult]::new('instance', 'instance', [CompletionResultType]::ParameterValue, 'instance')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;transport;get;all' {
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;transport;get;instance' {
			     [CompletionResult]::new('--hostname', 'hostname', [CompletionResultType]::ParameterName, 'hostname')
			     [CompletionResult]::new('--product', 'product', [CompletionResultType]::ParameterName, 'product')
			     [CompletionResult]::new('--instance', 'instance', [CompletionResultType]::ParameterName, 'instance')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;userRoles' {
			     [CompletionResult]::new('--user', 'user', [CompletionResultType]::ParameterName, 'user')
			     [CompletionResult]::new('--domain', 'domain', [CompletionResultType]::ParameterName, 'domain')
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
			 'dws;version' {
			     [CompletionResult]::new('--help', 'help', [CompletionResultType]::ParameterName, 'help')
			     break
			 }
		     })

    $completions.Where{ $_.CompletionText -like "$wordToComplete*" } |
      Sort-Object -Property ListItemText
}
