﻿using namespace System.Management.Automation
using namespace System.Management.Automation.Language

Register-ArgumentCompleter -Native -CommandName 'bry' -ScriptBlock {
    param($wordToComplete, $commandAst, $cursorPosition)

    $commandElements = $commandAst.CommandElements

    $command = @(
	'bry'
	for($i = 1;$i -lt $commandElements.Count; $i++) {
	    $element = $commandElements[$i]
	    if ($element -isnot [StringConstantExpressionAst] -or
		$element.StringConstantType -ne [StringConstantType]::BareWord -or
		$element.Value.StartsWith('-')) {
		    break
		}
	    $element.Value
	}
    ) -join ';'

    $completions = @(switch ($command) {
			 {$_ -match 'bry;action'} {
			     [CompletionResult]::new('start', 'start', [CompletionResultType]::Keyword, 'start')
			     [CompletionResult]::new('stop', 'stop', [CompletionResultType]::Keyword, 'stop')
			     [CompletionResult]::new('log', 'log', [CompletionResultType]::Keyword, 'log')
			     [CompletionResult]::new('--help', '--help', [CompletionResultType]::ParameterValue, '--help')
			     break
			 }
			 {$_ -match 'bry;version'} {
			     [CompletionResult]::new('--help', '--help', [CompletionResultType]::ParameterValue, '--help')
			 }
			 {$_ -match 'bry'} {
			     [CompletionResult]::new('action', 'action', [CompletionResultType]::Keyword, 'action')
			     [CompletionResult]::new('version', 'version', [CompletionResultType]::Keyword, 'version')
			     [CompletionResult]::new('--help', '--help', [CompletionResultType]::ParameterValue, '--help')
			     break
			 }
		     })
    $completions.Where{ $_.CompletionText -like "$wordToComplete*" } |
      Sort-Object -Property ListItemText
}
